//
//  CreateStoryViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 17/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import YPImagePicker
import SDWebImage
import AWSS3
import AWSCore
import AVKit
import AVFoundation

class CreateStoryViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    var selectedPhoto:UIImage?
    var selectedVideoUrl:URL?
    
    @IBOutlet weak var imgStoryImage: UIImageView!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtProductSerial: UITextField!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var txtDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.imgStoryImage.image = selectedPhoto
        
        if selectedVideoUrl != nil {
            self.btnPlayVideo.isHidden = false
        }else {
            self.btnPlayVideo.isHidden = true
        }
        
         self.txtPrice.isHidden = false
        
//        if MyProfile.isBuyNow! {
//            self.txtPrice.isHidden = false
//        }else {
//            self.txtPrice.isHidden = true
//        }
        
        self.txtProductSerial.delegate = self
        self.txtDescription.delegate = self
    }
    
    var randomName:String!
    
    @IBAction func onSendClick(_ sender: Any) {
        
        if self.validate() {
            
            self.view.endEditing(true)
            
            var videosData = [Data]()
            
            if let url = self.selectedVideoUrl {
                do  {
                    let data = try Data(contentsOf: url)
                    videosData.append(data)
                }catch {
                    print("error while uploading video \(error)")
                }
            }
            
            let wait = appDel.topViewController?.addWaitSpinner()
            self.randomName = randomString(length: 20)
            
            uploadImagesToAws([imgStoryImage.image!], folderName: appDel.storyImagesFolder!, randomName: self.randomName) { (urls) in
                
                if let url = urls?.first {
                    
                    if videosData.count > 0 {
                        
                        uploadVideosToAws(videosData, folderName: appDel.storyImagesFolder!, randomName: self.randomName, callback: {  (videoUrls) in
                            
                            if let _ = videoUrls {
                                
                                DispatchQueue.main.async {
                                    self.callCreateStoryApi(isVideo: true, url: url)
                                    if let wait = wait {
                                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                                    }
                                }
                            }else{
                                let str = NSLocalizedString("Failed to upload story", comment: "Failed to upload story")
                                self.displayMessage(str)
                                if let wait = wait {
                                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                                }
                            }
                        })
                    }else {
                        DispatchQueue.main.async {
                            self.callCreateStoryApi(isVideo: false, url: url)
                            if let wait = wait {
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                        }
                    }
                }else{
                    let str = NSLocalizedString("Failed to upload story", comment: "Failed to upload story")
                    self.displayMessage(str)
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                }
            }
        }
    }
    
    fileprivate func callCreateStoryApi(isVideo:Bool,url:String) {
        
        var newAmount:String!
        
        if let amount = self.txtPrice.text, let doubleAmount = Double(amount) {
            newAmount = String(format: "%.2f", doubleAmount)
        }else{
            newAmount = self.txtPrice.text
        }
        
        var params = [String:String]()
        
        params["user_id"] = MyProfile.userId!
        params["url"] = url
        params["is_buyNow"] = "\(MyProfile.isBuyNow!)"
        params["is_video"] = "\(isVideo)"
        params["status"] = "running"
        params["product_serial"] = self.txtProductSerial.text!
        params["whatsapp_only"] = "\(MyProfile.isWhatsappOnly!)"
        params["whatsapp_and_call"] = "\(MyProfile.isWhatsappAndCall!)"
        params["buyNowPrice"] = newAmount!
        params["product_description"] = self.txtDescription.text!
        
        JSONRequest.makeRequest(kCreateStory, parameters: params, callback: {  (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else if let msg = data["message"] as? String{
                    if let storyFolder = appDel.storyImagesFolder{
                    deleteAwsFile(folderName: storyFolder, fullPath: url)
                    }
                    let str = NSLocalizedString(msg, comment: msg)
                    self.displayMessage(str)
                }
            }
            else{
                if let storyFolder = appDel.storyImagesFolder{
                    deleteAwsFile(folderName: storyFolder, fullPath: url)
                }
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                self.displayMessage(str)
            }
        })
    }
    
    @IBAction func onPlayClick(_ sender: Any) {
        
        guard let url = self.selectedVideoUrl else {
            return
        }
        
        let player = AVPlayer(url: url)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            player.play()
        }
    }
    
    @IBAction func onCloseClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onStorySettingClick(_ sender: Any) {
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "StorySettingsViewController") as? StorySettingsViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func validate() -> Bool {
        
        guard let productSerial = self.txtProductSerial.text, productSerial.trim() != "" else {
            let str = NSLocalizedString("Enter Product Serial", comment: "Enter Product Serial")
            self.displayMessage(str)
            return false
        }
        if self.txtDescription.text == "Enter Description" || self.txtDescription.text == "" {
            let str = NSLocalizedString("Please enter description", comment: "Please enter description")
            self.displayMessage(str)
            return false
        }
        if let amount = self.txtPrice.text, !amount.isEmpty, let doubleAmount = amount.doubleValue, doubleAmount > 0.0{
            if doubleAmount >= AMOUNT {
                let str = NSLocalizedString("Maximum Amount can not be more than 1000", comment: "Maximum Amount can not be more than 1000")
                self.displayMessage(str)
                return false
            }
        }else{
            let str = NSLocalizedString("Please enter valid Amount", comment: "Please enter valid Amount")
            self.displayMessage(str)
            return false
        }
        
        return true
    }
    
    //MARK:- TextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 30
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let textField = textField as? CustomTextField{
            return textField.textFieldShouldBeginEditing(_:textField)
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let textField = textField as? CustomTextField{
            return textField.textFieldShouldEndEditing(_:textField)
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Enter Description" {
            textView.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        
        guard let stringRange = Range(range, in: currentText) else {
            return false
        }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return updatedText.count <= 100
    }
}
