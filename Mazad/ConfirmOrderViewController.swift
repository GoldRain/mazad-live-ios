//
//  ConfirmOrderViewController.swift
//  Mazad Live
//
//  Created by alienbrainz on 05/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class ConfirmOrderViewController: UIViewController {
    
    @IBOutlet weak var btnConfirmOrder:UIButton!
    @IBOutlet weak var btnRejectOrder: UIButton!
    @IBOutlet weak var btnCancel:UIButton!
    
    var callback:((Bool?)->())?
    
    var btnTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.btnConfirmOrder.addShadow()
        self.btnRejectOrder.addShadow()
        self.btnCancel.addShadow()
        
        self.btnConfirmOrder.setTitle(self.btnTitle, for: .normal)
    }
    
    @IBAction func onConfirmOrderClick(_ sender:UIButton) {
        
        self.callback!(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onCancelClick(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onRejectClick(_ sender: Any) {
        
        self.callback!(false)
        self.dismiss(animated: true, completion: nil)
    }
    
}
