//
//  SelectedImageViewerViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 04/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import YPImagePicker
import ImageViewer
import AVKit
import AVFoundation

enum VCType {
    case NewPostVC
    case CategoryVC
}

class SelectedImageViewerViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, GalleryItemsDataSource, GalleryItemsDelegate {
   
    var dataImage: [GalleryItem] = [GalleryItem]()
    var galleryController:GalleryViewController?
    
    var vcType = VCType.NewPostVC
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //var selectedPhoto = [YPMediaPhoto]()
    
    var selectedPhoto = [UIImage]()
    var selectedVideoUrl = [URL]()
    
    var postImages = UIImage()
    
    var callback:(([YPMediaPhoto])->())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("All Post", comment: "All Post")
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedPhoto.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SelectedImageViewerCell
        
        cell.imgCell.image = selectedPhoto[indexPath.row]//.image
        
        if self.selectedVideoUrl.count > 0 {
            cell.imgPlay.isHidden = false
        }else {
            cell.imgPlay.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var type:String?
        
        let alertVC = getAlertController(title: nil, message: nil)
        
        var viewImageAlert:UIAlertAction!
        
        if self.selectedVideoUrl.count > 0 {
            
            type = "Video"
            
            viewImageAlert = UIAlertAction(title: "Play Video", style: .default) { (alert) in
                
                guard let url = self.selectedVideoUrl.first else {
                    return
                }
                
                let player = AVPlayer(url: url)
                let playerController = AVPlayerViewController()
                playerController.player = player
                self.present(playerController, animated: true) {
                    player.play()
                }
            }
        }else {
            type = "Image"
            viewImageAlert = UIAlertAction(title: "View Image", style: .default) { (alert) in
                
                self.showImagesPreview(index: indexPath.row)
            }
        }
        let deleteImageAlert = UIAlertAction(title: "Delete \(type ?? "")", style: .destructive) { (alert) in
            
            self.selectedPhoto.remove(at: indexPath.row)
            
            if self.selectedVideoUrl.count > 0 {
                self.selectedVideoUrl.remove(at: indexPath.row)
            }
            
            self.collectionView.reloadData()
            
            NotificationCenter.default.post(name: .imageDeleted, object: nil, userInfo: ["indexPath":indexPath])
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        
        alertVC.addAction(viewImageAlert)
        alertVC.addAction(deleteImageAlert)
        alertVC.addAction(cancel)
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    //MARK: GalleryItemsDataSource
    func itemCount() -> Int {
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    // MARK: Image preview
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        for image in self.selectedPhoto{
            let galleryItem = GalleryItem.image { imageCompletion in
                imageCompletion(image)//.image)
            }
            self.dataImage.append(galleryItem)
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
        //self.dismiss(animated: true, completion: nil)
    }
}

class SelectedImageViewerCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCell: UIImageView!
    
    @IBOutlet weak var imgPlay: UIImageView!
    
}

extension Notification.Name {
    
    static let imageDeleted = Notification.Name("imageDeleted")
}
