//
//  VerificationModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 21/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import Foundation

class VerificationModel: NSObject, NSSecureCoding {
    
    static var supportsSecureCoding: Bool = true
    
    var verification : VerificationModel!
    
    var user_id : String?
    var id_type : String?
    var id_number : String?
    var image_url: String?
    var status : String?
    var payment_status : String?
    var payment_data : String?
    var created_at:Int?
    
    init(data:[String:Any]) {
        
        if let value = data["user_id"] as? String{
            self.user_id = value
        }
        if let value = data["id_type"] as? String{
            self.id_type = value
        }
        if let value = data["id_number"] as? String{
            self.id_number = value
        }
        if let value = data["image_url"] as? String{
            self.image_url = value
        }
        if let value = data["status"] as? String{
            self.status = value
        }
        if let value = data["payment_status"] as? String{
            self.payment_status = value
        }
        if let value = data["payment_data"] as? String{
            self.payment_data = value
        }
        if let value = data["created_at"] as? Int {
            self.created_at = value
        }
    }
    
    init (user_id:String, id_type:String, id_number:String, image_url:String, status:String, payment_status:String) {
        self.user_id = user_id
        self.id_type = id_type
        self.id_number = id_number
        self.image_url = image_url
        self.status = status
        self.payment_status = payment_status
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(user_id, forKey: "user_id")
        aCoder.encode(id_type, forKey: "id_type")
        aCoder.encode(id_number, forKey: "id_number")
        aCoder.encode(image_url, forKey: "image_url")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(payment_status, forKey: "payment_status")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.user_id = aDecoder.decodeObject(forKey: "user_id") as? String
        self.id_type = aDecoder.decodeObject(forKey: "id_type") as? String
        self.id_number = aDecoder.decodeObject(forKey: "id_number") as? String
        self.image_url = aDecoder.decodeObject(forKey: "image_url") as? String
        self.status = aDecoder.decodeObject(forKey: "status") as? String
        self.payment_status = aDecoder.decodeObject(forKey: "payment_status") as? String
    }
}
