//
//  StoryModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 17/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

// status = "running" || "stopped"

class StoryModel {
    
    var _id : String?
    var bids = [BidModel]()
    var buyNowPrice : String?
    //var comments : [AnyObject]!
    var created_at : String?
    var is_buyNow : Bool?
    var url : String?
    var userId : String?
    var userDetails = [UserModel]()
    var status : String?
    var is_video: Bool?
    var feedback = [FeedbackModel]()
    var product_serial:String?
    var whatsapp_only:Bool?
    var whatsapp_and_call:Bool?
    var is_sold:Bool?
    var payment_type:String?
    var storyDescription:String?
    
    required init() {
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let bids = data["bids"] as? [[String:Any]]{
            for bid in bids{
                self.bids.append(BidModel(data: bid))
            }
        }
        
        if let bids = data["bidsAndComments"] as? [[String:Any]]{
            for bid in bids{
                self.bids.append(BidModel(data: bid))
            }
        }
        
        if let value = data["buyNowPrice"] as? String{
            self.buyNowPrice = value
        }
        
        if let value = data["created_at"] as? String{
            self.created_at = value
        }
        
        if let value = data["url"] as? String{
            self.url = value
        }
        
        if let value = data["user_id"] as? String{
            self.userId = value
        }
        
        if let value = data["status"] as? String{
            self.status = value
        }
        
        if let value = data["is_buyNow"] as? Bool{
            self.is_buyNow = value
        }
        if let users = data["userDetails"] as? [[String:Any]]{
            for user in users{
                self.userDetails.append(UserModel(data: user))
            }
        }
        if let value = data["is_video"] as? Bool{
            self.is_video = value
        }
        else{
            self.is_video = false
        }
        
        if let feedbacks = data["feedback"] as? [[String:Any]]{
            for feedback in feedbacks{
                self.feedback.append(FeedbackModel(data: feedback))
            }
        }
        
        if let value = data["product_serial"] as? String {
            self.product_serial = value
        }
        if let value = data["whatsapp_only"] as? Bool{
            self.whatsapp_only = value
        }
        if let value = data["whatsapp_and_call"] as? Bool{
            self.whatsapp_and_call = value
        }
        
        if let value = data["is_sold"] as? Bool{
            self.is_sold = value
        }
        
        if let value = data["payment_type"] as? String {
            self.payment_type = value
        }
        
        if let value = data["product_description"] as? String {
            self.storyDescription = value
        }
    }
}


