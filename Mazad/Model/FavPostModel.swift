//
//  FavPostModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 12/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class FavPostModel {
    
    var _id:String?
    var post_id:String?
    
    required init(){
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["post_id"] as? String{
            self.post_id = value
        }
    }
}
