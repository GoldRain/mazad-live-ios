//
//  AddressModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 26/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class AddressModel: NSObject, NSSecureCoding {
    
    static var supportsSecureCoding: Bool = true
    
    var addressLineOne: String?
    var addressLineTwo: String?
    var city: String?
    var state: String?
    var pincode: String?
    var addressId: String?
    var countryId: String?
    var is_active: Bool?
    var countryDetails = [CountryModel]()
    var name:String?
    var phone:String?
    
    required override init() {
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["addressLineOne"] as? String{
            self.addressLineOne = value
        }
        if let value = data["addressLineTwo"] as? String{
            self.addressLineTwo = value
        }
        if let value = data["city"] as? String{
            self.city = value
        }
        if let value = data["state"] as? String{
            self.state = value
        }
        if let value = data["pincode"] as? String{
            self.pincode = value
        }
        if let value = data["_id"] as? String{
            self.addressId = value
        }
        if let value = data["countryId"] as? String{
            self.countryId = value
        }
        if let value = data["is_active"] as? Bool{
            self.is_active = value
        }
        
        if let countries = data["CountryDetails"] as? [String:Any]{
            self.countryDetails.append(CountryModel(data: countries))
        }
        
        if let value = data["name"] as? String{
            self.name = value
        }
        if let value = data["phone"] as? String{
            self.phone = value
        }
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(addressLineOne, forKey: "AddressLineOne")
        aCoder.encode(addressLineTwo, forKey: "AddressLineTwo")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(state, forKey: "state")
        aCoder.encode(pincode, forKey: "pincode")
        aCoder.encode(addressId, forKey: "_id")
        aCoder.encode(countryId, forKey: "countryId")
        aCoder.encode(is_active, forKey: "is_active")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.addressLineOne = aDecoder.decodeObject(forKey: "AddressLineOne") as? String
        self.addressLineTwo = aDecoder.decodeObject(forKey: "AddressLineTwo") as? String
        self.city = aDecoder.decodeObject(forKey: "city") as? String
        self.state = aDecoder.decodeObject(forKey: "state") as? String
        self.pincode = aDecoder.decodeObject(forKey: "pincode") as? String
        self.addressId = aDecoder.decodeObject(forKey: "_id") as? String
        self.countryId = aDecoder.decodeObject(forKey: "countryId") as? String
        self.is_active = aDecoder.decodeObject(forKey: "is_active") as? Bool
    }
}
