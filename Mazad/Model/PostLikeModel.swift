//
//  PostLikeModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class PostLikeModel {
    
    var _id:String?
    var username:String?
    var country_code:String?
    var phone:String?
    var email:String?
    var password:String?
    var type:String?
    var profilePhoto:String?
    var __v:Int?
    var is_online:Bool?
    var socket_id:String?
    var is_live:String?
    
//    "followers": [
//    {
//    "user_id": "5c0619c93bf6c11c4f1b60e6",
//    "created_at": "Tue Dec 04 2018 18:11:52 GMT+0530 (IST)"
//    }
//    ],
//    "following": [
//    {
//    "user_id": "5c0619c93bf6c11c4f1b60e6",
//    "created_at": "Wed Dec 05 2018 10:26:57 GMT+0530 (IST)"
//    }
//    ]

    required init(){
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["user_id"] as? String{
            self._id = value
        }
        if let value = data["username"] as? String{
            self.username = value
        }
        if let value = data["country_code"] as? String{
            self.country_code = value
        }
        if let value = data["phone"] as? String{
            self.phone = value
        }
        if let value = data["email"] as? String{
            self.email = value
        }
        if let value = data["password"] as? String{
            self.password = value
        }
        if let value = data["type"] as? String{
            self.type = value
        }
        if let value = data["profilePhoto"] as? String{
            self.profilePhoto = value
        }
        if let value = data["__v"] as? Int{
            self.__v = value
        }
        if let value = data["is_online"] as? Bool{
            self.is_online = value
        }
        if let value = data["socket_id"] as? String{
            self.socket_id = value
        }
        if let value = data["is_live"] as? String{
            self.is_live = value
        }
    }

}
