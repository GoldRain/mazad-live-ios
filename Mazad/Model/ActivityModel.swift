//
//  ActivityModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 31/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class ActivityModel {
    
    var activityId : String?
    var message:String?
    var type : String?
    var user_id:String?
    var type_id : String?
    var created_at : String?
    var typeData = [BuyerSellerModel]()
    var user_data = [UserModel]()
   
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self.activityId = value
        }
        if let value = data["message"] as? String{
            self.message = value
        }
        if let value = data["type"] as? String{
            self.type = value
        }
        if let value = data["user_id"] as? String{
            self.user_id = value
        }
        if let value = data["type_id"] as? String{
            self.type_id = value
        }
        if let value = data["created_at"] as? String{
            self.created_at = value
        }
       
        if let users = data["user_data"] as? [[String:Any]]{
            for user in users{
                self.user_data.append(UserModel(data: user))
            }
        }
        
        if let typeData = data["type_data"] as? [[String:Any]]{
            for tData in typeData{
                self.typeData.append(BuyerSellerModel(data: tData))
            }
        }
    }
}
