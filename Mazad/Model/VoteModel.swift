//
//  VoteModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 13/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class VoteModel {
    
    var _id:String?
    var user_id:String?
    var response:String?
    
    required init(){
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["user_id"] as? String{
            self.user_id = value
        }
        if let value = data["response"] as? String{
            self.response = value
        }
    }
}
