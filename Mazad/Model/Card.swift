//
//  Card.swift
//  Mazad Live
//
//  Created by MAC_2 on 08/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import Foundation
/*
 "card_number" = 4544386484458448;
 "created_at" = 1532665064725;
 cvv = 245;
 "expire_month" = 05;
 "expire_year" = 2022;
 id = 12;
 name = Rahul;
 "primary_card" = 1;
 type = VISA;
 "updated_at" = 1532665674194;
 "user_id" = 13;
 */
public final class CardModel: NSObject {
    
    var cardNumber: String?
    var createdAt: String?
    var cvv: String?
    var expireMonth: String?
    var expireYear: String?
    var cardId: String?
    var id: String?
    var name: String?
    var primaryCard: Bool?
    var type: String?
    var updatedAt: String?
    var userId: String?
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    init(rawData: [String: Any]) {
        
        
        //self.cardNumber     = rawData["card_number"]        as? String
        self.createdAt      = rawData["created_at"]         as? String
        //self.cvv            = rawData["cvv"]                as? String
        
        //old_change-
        
        //self.expireMonth    = rawData["expiry_month"]       as? String
        //self.expireYear     = rawData["expiry_year"]        as? String
        
        //new
        //        self.expireMonth    = rawData["expire_month"]       as? String
        //        self.expireYear     = rawData["expire_year"]        as? String
        
        if let value = rawData["id"] as? Int{
            self.id = "\(value)"
        }
        //self.cardId         = rawData["card_id"]            as? String
        if let value = rawData["card"] as? String{
            
//            let key = MyProfile.email
//            let cryptLib = StringEncryption()
//            let secretKey = cryptLib.sha256(key, length: 32)!   // 32 bytes = 256 bits
            
            let iv = "fadebf5698234503"
            
            let encryptedData2 = Data(base64Encoded: value)
//            let originalData2 = cryptLib.decrypt(encryptedData2!, key: secretKey, iv: iv)
//            let plainText2 = String(data: originalData2!, encoding: .utf8)
//            if let text = plainText2{
//                let data = text.data(using: .utf8)
//                
//                if let dictData = try! JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]{
//                    print(dictData)
//                    if let card_no_2 = dictData["card_no_2"] as? String{
//                        self.cardNumber = card_no_2
//                    }
//                    if let card_id = dictData["card_id"] as? String{
//                        self.cardId = card_id
//                    }
//                    if let type = dictData["type"] as? String{
//                        self.type = type
//                    }
//                    if let name = dictData["name"] as? String{
//                        self.name = name
//                    }
//                }
//                
//                
//            }
        }
        
        //self.name           = rawData["name"]               as? String
        self.primaryCard    = rawData["primary_card"]       as? Bool
        //self.type           = rawData["type"]               as? String
        self.updatedAt      = rawData["updated_at"]         as? String
        self.userId         = rawData["user_id"]            as? String
    }
}
