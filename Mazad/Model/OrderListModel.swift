//
//  OrderListModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 29/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class OrderListModel {

    var orderId : String?
    var type : String?
    var status : String?
    var user_payment_status : String?
    var transaction_id : String?
    var amount : String?
    var feedback : String?
    var payment_method : String?
    var created_at:String?
    var user_id : String?
    var type_id : String?
    var address_id : String?
    var display_id:String?
    var seller_id:String?
    
    var postData = [BuyerSellerModel]()
    var storyData = [StoryModel]()
    
    var user_data = [UserModel]()
    var address = [AddressModel]()
    
    var user_name:String?
    var user_country:CountryModel?
    var buyerDetails = [UserModel]()
    
    var seller_data = [UserModel]()
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self.orderId = value
        }
        if let value = data["type"] as? String{
            self.type = value
        }
        if let value = data["status"] as? String{
            self.status = value
        }
        if let value = data["user_payment_status"] as? String{
            self.user_payment_status = value
        }
        if let value = data["transaction_id"] as? String{
            self.transaction_id = value
        }
        if let value = data["amount"] as? String{
            self.amount = value
        }
        if let value = data["feedback"] as? String{
            self.feedback = value
        }
        if let value = data["payment_method"] as? String{
            self.payment_method = value
        }
        if let value = data["user_id"] as? String{
            self.user_id = value
        }
        if let value = data["type_id"] as? String{
            self.type_id = value
        }
        if let value = data["address_id"] as? String{
            self.address_id = value
        }
        
        if let value = data["created_at"] as? String{
            self.created_at = value
        }
        if let value = data["display_id"] as? String{
            self.display_id = value
        }
        if let users = data["user_data"] as? [[String:Any]]{
            for user in users{
                self.user_data.append(UserModel(data: user))
            }
        }
        
        if let buyerDetails = data["buyerDetails"] as? [[String:Any]]{
            for buyer in buyerDetails{
                self.buyerDetails.append(UserModel(data: buyer))
            }
        }
        
        if let sellerDetails = data["seller_data"] as? [[String:Any]]{
            for seller in sellerDetails{
                self.seller_data.append(UserModel(data: seller))
            }
        }
        
        if let address = data["address"] as? [[String:Any]]{
            if let addressDict = address.first{
                if let addresses = addressDict["addresses"] as? [[String:Any]] {
                    for add in addresses{
                        self.address.append(AddressModel(data: add))
                    }
                }
            }
        }
        
        if let typeData = data["type_data"] as? [[String:Any]]{
            if self.type?.lowercased() == "post" {
                for tData in typeData{
                    self.postData.append(BuyerSellerModel(data: tData))
                }
            }
            if self.type?.lowercased() == "story" {
                for sData in typeData {
                    self.storyData.append(StoryModel(data: sData))
                }
            }
        }
        
        if let typeData = data["typeData"] as? [[String:Any]]{
            if self.type?.lowercased() == "post" {
                for tData in typeData {
                    self.postData.append(BuyerSellerModel(data: tData))
                }
            }
            if self.type?.lowercased() == "story" {
                for sData in typeData {
                    self.storyData.append(StoryModel(data: sData))
                }
            }
        }
        
        if let value = data["user_name"] as? String{
            self.user_name = value
        }
        
        if let value = data["user_country"] as? [String:Any]{
            self.user_country = CountryModel(data: value)
        }
    }
}
