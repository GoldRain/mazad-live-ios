//
//  PostImageModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class PostImageModel {
    
    var _id:String?
    var image_url:String?
    var is_video:Bool?
    
    required init() {
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["image_url"] as? String{
            self.image_url = value
        }
        if let value = data["is_video"] as? Bool{
            self.is_video = value
        }
    }
}
