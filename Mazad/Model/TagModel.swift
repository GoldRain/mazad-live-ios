//
//  Tag.swift
//  Mazad Live
//
//  Created by MAC_2 on 04/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class TagModel {
    
    var _id:String?
    var name:String?
    var createdAt:String?
    var updatedAt:String?
    var __v:Int?
    var number:Int = 0
    var isDeleted:Bool = false
    
    required init(){
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["name"] as? String{
            self.name = value
        }
        if let value = data["createdAt"] as? String{
            self.createdAt = value
        }
        if let value = data["updatedAt"] as? String{
            self.updatedAt = value
        }
        if let value = data["__v"] as? Int{
            self.__v = value
        }
        if let value = data["number"] as? Int {
            self.number = value
        }
        if let value = data["isDeleted"] as? Bool{
            self.isDeleted = value
        }
    }
    
}
