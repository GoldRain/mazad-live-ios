//
//  WalletPayementModel
//  Mazad Live
//
//  Created by MAC_2 on 03/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import Foundation

class WalletPayementModel {
    
    var message : String?
    var status : String?
    var payment_method : String?
    var created_at: String?
    var _id : String?
    var seller_id : String?
    var buyer_id : String?
    var order_id : String?
    var orderDetails = [OrderListModel]()
    var typeData = [BuyerSellerModel]()
    var type:String?
    var type_id:String?
    var seller_country:CountryModel?
    var seller_name:String?
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["message"] as? String{
            self.message = value
        }
        if let value = data["status"] as? String{
            self.status = value
        }
        if let value = data["payment_method"] as? String{
            self.payment_method = value
        }
        if let value = data["created_at"] as? String{
            self.created_at = value
        }
        if let value = data["seller_id"] as? String{
            self.seller_id = value
        }
        if let value = data["buyer_id"] as? String{
            self.buyer_id = value
        }
        if let value = data["order_id"] as? String{
            self.order_id = value
        }
        
        if let orderDetails = data["orderDetails"] as? [[String:Any]]{
            for order in orderDetails{
                self.orderDetails.append(OrderListModel(data: order))
            }
        }
        
        if let typeData = data["type_data"] as? [[String:Any]]{
            for tData in typeData{
                self.typeData.append(BuyerSellerModel(data: tData))
            }
        }
        
        if let value = data["type"] as? String{
            self.type = value
        }
        
        if let value = data["type_id"] as? String{
            self.type_id = value
        }
        
        if let value = data["seller_country"] as? [String:Any]{
            self.seller_country = CountryModel(data: value)
        }
        
        if let value = data["seller_name"] as? String{
            self.seller_name = value
        }
    }
}
