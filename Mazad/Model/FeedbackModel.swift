//
//  FeedbackModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 03/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import Foundation

class FeedbackModel {
    
    var _id : String?
    var user_id : String?
    var user_name : String?
    var user_country: CountryModel?
    var user_profile : String?
    var comment : String?
    var order_id : String?
    var created_at : String?
    var rating:String?
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["user_id"] as? String{
            self.user_id = value
        }
        if let value = data["user_name"] as? String{
            self.user_name = value
        }
        if let country = data["user_country"] as? [String:Any]{
            self.user_country = CountryModel(data: country)
        }
        if let value = data["user_profile"] as? String{
            self.user_profile = value
        }
        if let value = data["comment"] as? String{
            self.comment = value
        }
        if let value = data["order_id"] as? String{
            self.order_id = value
        }
        if let value = data["created_at"] as? String{
            self.created_at = value
        }
        if let value = data["rating"] as? String{
            self.rating = value
        }
    }
}


