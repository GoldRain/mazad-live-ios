//
//  Countries.swift
//  Mazad Live
//
//  Created by MAC_2 on 03/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class CountryModel {
    
    var _id:String?
    var countryName:String?
    var countryCode:String?
    var flagImageUrl:String?
    var createdAt:String?
    var __v:Int?
    
    required init(){
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["countryName"] as? String{
            self.countryName = value
        }
        if let value = data["countryCode"] as? String{
            self.countryCode = value
        }
        if let value = data["flagImageUrl"] as? String{
            self.flagImageUrl = value
        }
        if let value = data["createdAt"] as? String{
            self.createdAt = value
        }
        if let value = data["__v"] as? Int{
            self.__v = value
        }
    }
    
}
