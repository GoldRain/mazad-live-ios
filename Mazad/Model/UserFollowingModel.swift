//
//  UserFollowingModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 12/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class UserFollowingModel {
    
    var user_id:String?
    var created_at:String?
    
    required init(){
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["user_id"] as? String{
            self.user_id = value
        }
        if let value = data["created_at"] as? String{
            self.created_at = value
        }
    }
}
