//
//  BuyerModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class BuyerSellerModel {
    
    var _id:String?
    var tags = [TagModel]()
    var available_country_id = [CountryModel]()
    var post_images = [PostImageModel]()
    var bids = [BidModel]()
    var post_like = [PostLikeModel]()
    var latitude:String?
    var longitude:String?
    var place_name:String?
    var post_user_id:String?
    var is_instagram:Bool?
    var is_twitter:Bool?
    var amount:String?
    var is_auction:Bool?
    var buy_now:Bool?
    var whatsapp_only:Bool?
    var whatsapp_and_call:Bool?
    var payment_type:String?
    var language:String?
    var only_user_from_selected_country:Bool?
    var post_created_at:String?
    var status:String?
    var is_sold:Bool?
    var is_story:Bool?
    var __v:Int?
    var userDetails = [UserModel]()
    var vote = [VoteModel]()
    var details:String?
    var percent:String?
    var feedback = [FeedbackModel]()
    var is_deleted:Bool = false
    var product_serial:String?
    var is_promoted:Bool = false
    var is_stream:Bool = false
    
    required init() {
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let tags = data["tags"] as? [[String:Any]]{
            for tag in tags{
                self.tags.append(TagModel(data: tag))
            }
        }
        if let countries = data["available_country_id"] as? [[String:Any]]{
            for country in countries{
                self.available_country_id.append(CountryModel(data: country))
            }
        }
        if let postImages = data["post_images"] as? [[String:Any]]{
            for image in postImages{
                self.post_images.append(PostImageModel(data: image))
            }
        }
        if let bids = data["bids"] as? [[String:Any]]{
            for bid in bids{
                self.bids.append(BidModel(data: bid))
            }
        }
        if let postLikes = data["post_like"] as? [[String:Any]]{
            for like in postLikes{
                self.post_like.append(PostLikeModel(data: like))
            }
        }
        if let value = data["latitude"] as? String{
            self.latitude = value
        }
        if let value = data["longitude"] as? String{
            self.longitude = value
        }
        if let value = data["place_name"] as? String{
            self.place_name = value
        }
        if let value = data["post_user_id"] as? String{
            self.post_user_id = value
        }
        if let value = data["is_instagram"] as? Bool{
            self.is_instagram = value
        }
        if let value = data["is_twitter"] as? Bool{
            self.is_twitter = value
        }
        if let value = data["amount"] as? String{
            self.amount = value
        }
        if let value = data["is_auction"] as? Bool{
            self.is_auction = value
        }
        if let value = data["buy_now"] as? Bool{
            self.buy_now = value
        }
        if let value = data["whatsapp_only"] as? Bool{
            self.whatsapp_only = value
        }
        if let value = data["whatsapp_and_call"] as? Bool{
            self.whatsapp_and_call = value
        }
        if let value = data["payment_type"] as? String{
            self.payment_type = value
        }
        if let value = data["language"] as? String{
            self.language = value
        }
        if let value = data["only_user_from_selected_country"] as? Bool{
            self.only_user_from_selected_country = value
        }
        if let value = data["post_created_at"] as? String{
            self.post_created_at = value
        }
        if let value = data["status"] as? String{
            self.status = value
        }
        
        if let value = data["is_sold"] as? Bool{
            self.is_sold = value
        }
        if let value = data["is_story"] as? Bool{
            self.is_story = value
        }
        if let value = data["__v"] as? Int{
            self.__v = value
        }
        if let users = data["userDetails"] as? [[String:Any]]{
            for user in users{
                self.userDetails.append(UserModel(data: user))
            }
        }
        if let votes = data["vote"] as? [[String:Any]]{
            for vote in votes{
                self.vote.append(VoteModel(data: vote))
            }
        }
        
        if let value = data["description"] as? String{
            self.details = value
        }
        
        if let value = data["details"] as? String{
            self.details = value
        }
        
        if let value = data["percent"] as? String{
            if let intValue = Int(value.replacingOccurrences(of: "%", with: "")){
                self.percent = "\(intValue)%"
            }
            else{
                self.percent = "0%"
            }
        }
        else{
            self.percent = "0%"
        }
        
        if let feedbacks = data["feedback"] as? [[String:Any]] {
            for feedback in feedbacks{
                self.feedback.append(FeedbackModel(data: feedback))
            }
        }
        
        if let value = data["is_deleted"] as? Bool{
            self.is_deleted = value
        }
        
        if let value = data["product_serial"] as? String {
            self.product_serial = value
        }
        
        if let value = data["is_promoted"] as? Bool{
            self.is_promoted = value
        }
        
        if let value = data["is_stream"] as? Bool {
            self.is_stream = value
        }
    }
}
