//
//  TransactionResponse.swift
//  Mazad Live
//
//  Created by MAC_2 on 28/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation
class TransactionResponse {
    
    var amount: String?
    var txnId:String?
    var payment_method:String?
    var type_id:String?
    
    init(data: [String:Any]) {
        if let value = data["amount"] as? String {
            self.amount = value
        }
        if let value = data["transaction_id"] as? String {
            self.txnId = value
        }
        if let value = data["payment_method"] as? String {
            self.payment_method = value
        }
        if let value = data["type_id"] as? String {
            self.type_id = value
        }
    }
}
