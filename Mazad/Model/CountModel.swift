//
//  CountModel.swift
//  Mazad Live
//
//  Created by alienbrainz on 12/04/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import Foundation

class CountModel: NSObject, NSSecureCoding  {
    
    static var supportsSecureCoding: Bool = true
    
    var activation:String?
    var expiry:String?
    var count:Int?
    var status:Bool?
    
    init(data:[String:Any]) {
        
        if let value = data["activation"] as? String{
            self.activation = value
        }
        if let value = data["expiry"] as? String{
            self.expiry = value
        }
        if let value = data["count"] as? Int{
            self.count = value
        }
        if let value = data["status"] as? Bool{
            self.status = value
        }
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(activation, forKey: "activation")
        aCoder.encode(expiry, forKey: "expiry")
        aCoder.encode(count, forKey: "count")
        aCoder.encode(status, forKey: "status")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.activation = aDecoder.decodeObject(forKey: "activation") as? String
        self.expiry = aDecoder.decodeObject(forKey: "expiry") as? String
        self.count = aDecoder.decodeObject(forKey: "count") as? Int
        self.status = aDecoder.decodeObject(forKey: "status") as? Bool
    }
}
