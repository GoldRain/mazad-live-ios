//
//  BidModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class BidModel {
    
    var _id:String?
    var post_id:String?
    var story_id:String?
    var bid_user_id:String?
    var bid_comment:String?
    var bid_amount:String?
    var bid_created_at:String?
    var is_pinned:Bool?
    var status:String?
    var url:String?
    var user_name:String?
    var country:CountryModel?
    var is_paid:Bool?
    
    required init(){
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["post_id"] as? String{
            self.post_id = value
        }
        if let value = data["story_id"] as? String{
            self.story_id = value
        }
        //
        if let value = data["bid_user_id"] as? String{
            self.bid_user_id = value
        }
        if let value = data["user_id"] as? String{
            self.bid_user_id = value
        }
        //
        
        if let value = data["bid_comment"] as? String{
            self.bid_comment = value
        }
        if let value = data["bid_amount"] as? String{
            self.bid_amount = value
        }
        if let value = data["amount"] as? String{
            self.bid_amount = value
        }
        //
        
        if let value = data["bid_created_at"] as? String{
            self.bid_created_at = value
        }
        if let value = data["created_time"] as? String{
            self.bid_created_at = value
        }
        if let value = data["created_at"] as? String{
            self.bid_created_at = value
        }
        //
        if let value = data["is_pinned"] as? Bool{
            self.is_pinned = value
        }
        if let value = data["status"] as? String{
            self.status = value
        }
        if let value = data["url"] as? String{
            self.url = value
        }
        if let value = data["user_name"] as? String{
            self.user_name = value
        }
        if let value = data["country"] as? [String:Any]{
            self.country = CountryModel(data: value)
        }
        if let value = data["is_paid"] as? Bool{
            self.is_paid = value
        }
    }
}
