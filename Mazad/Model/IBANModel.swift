//
//  IBANModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 13/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class IBANModel {
    
    var countryCode:String?
    var banNumber:String?
    var bankName:String?
    var phoneNumber:String?
   
    init(data:[String:Any]) {
        
        if let value = data["country_code"] as? String{
            self.countryCode = value
        }
        if let value = data["banNumber"] as? String{
            self.banNumber = value
        }
        if let value = data["bankName"] as? String{
            self.bankName = value
        }
        if let value = data["phoneNumber"] as? String{
            self.phoneNumber = value
        }
    }
}
