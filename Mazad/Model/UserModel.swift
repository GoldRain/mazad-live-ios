//
//  UserModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import Foundation

class UserModel {
    
    var _id:String?
    var username:String?
    var country_code:String?
    var phone:String?
    var email:String?
    var password:String?
    var type:String?
    var profilePhoto:String?
    var __v:Int?
    var followers = [UserModel]()
    var following = [UserModel]()
    var is_online:Bool?
    var socket_id:String?
    var description:String?
    var countryDetails = [CountryModel]()
    var favPost = [FavPostModel]()
    var IBAN:IBANModel?
    var isVerified:Bool = false
    var isLive = false
    var available_posts:String?
    var available_stream:String?
    var verification:VerificationModel?
    var is_verified:Bool?
    var show_ads:Bool?
    
//    var available_post_count:Int?
//    var available_live_count:Int?
//    var available_story_count:Int?
    var push_notification:Bool = false
    var email_notification:Bool = false
    
    var available_post_count:CountModel?
    var available_story_count:CountModel?
    var available_live_count:CountModel?
    
    required init() {
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self._id = value
        }
        if let value = data["user_id"] as? String {
            self._id = value
        }
        if let value = data["username"] as? String{
            self.username = value.capitalized
        }
        if let value = data["country_code"] as? String{
            self.country_code = value
        }
        if let value = data["phone"] as? String{
            self.phone = value
        }
        if let value = data["email"] as? String{
            self.email = value
        }
        if let value = data["password"] as? String{
            self.password = value
        }
        if let value = data["type"] as? String{
            self.type = value
        }
        if let value = data["profilePhoto"] as? String{
            self.profilePhoto = value
        }
        if let value = data["__v"] as? Int{
            self.__v = value
        }
        if let followers = data["followers"] as? [[String:Any]]{
            for follower in followers {
                self.followers.append(UserModel(data: follower))
            }
        }
        if let followings = data["following"] as? [[String:Any]]{
            for following in followings {
                self.following.append(UserModel(data: following))
            }
        }
        if let value = data["is_online"] as? Bool{
            self.is_online = value
        }
        if let value = data["socket_id"] as? String{
            self.socket_id = value
        }
        if let value = data["description"] as? String{
            self.description = value
        }
        if let countries = data["countryDetails"] as? [[String:Any]]{
            for country in countries {
                self.countryDetails.append(CountryModel(data: country))
            }
        }
        
        // for seller order list Data
        if let country = data["country"] as? [String:Any]{
            self.countryDetails.append(CountryModel(data: country))
        }
        
        if let favPosts = data["favPost"] as? [[String:Any]]{
            for favPost in favPosts {
                self.favPost.append(FavPostModel(data: favPost))
            }
        }
        if let value = data["IBAN"] as? [String:Any]{
            let val = IBANModel(data: value)
            self.IBAN = val
        }
       
        if let value = data["is_live"] as? String{
            self.isLive = (value == "running")
        }
        if let value = data["available_posts"] as? String{
            self.available_posts = value
        }
        if let value = data["available_stream"] as? String{
            self.available_stream = value
        }
        
        if let value = data["verification"] as? [String:Any]{
            self.verification = VerificationModel(data: value)
        }

        if let value = data["is_verified"] as? Bool {
            self.isVerified = value
        }
        
        if let value = data["show_ads"] as? Bool {
            self.show_ads = value
        }
        
//        if let value = data["available_post_count"] as? Int {
//            self.available_post_count = value
//        }
//        if let value = data["available_live_count"] as? Int {
//            self.available_live_count = value
//        }
//        if let value = data["available_story_count"] as? Int {
//            self.available_story_count = value
//        }
        
        if let value = data["unlimited_post_count"] as? [String:Any] {
            self.available_post_count = CountModel(data: value)
        }
        if let value = data["unlimited_story_count"] as? [String:Any] {
            self.available_story_count = CountModel(data: value)
        }
        if let value = data["unlimited_live_count"] as? [String:Any] {
            self.available_live_count = CountModel(data: value)
        }
        
        if let value = data["push_notification"] as? Bool {
            self.push_notification = value
        }
        if let value = data["email_notification"] as? Bool {
            self.push_notification = value
        }
    }
}

