//
//  PendingFeedbackModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 03/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import Foundation

class PendingFeedbackModel {
    
    var order_id : String?
    var type:String?
    var status : String?
    var user_payment_status:String?
    var transaction_id : String?
    var amount : String?
    var feedback:String?
    var payment_method:String?
    var created_at : String?
    var user_id:String?
    var type_id : String?
    var address_id:String?
    var post_data = [BuyerSellerModel]()
    var story_data = [StoryModel]()
    var sellerData = [UserModel]()
    var user_name:String?
    
    init(data:[String:Any]) {
        
        if let value = data["_id"] as? String{
            self.order_id = value
        }
        if let value = data["type"] as? String{
            self.type = value
        }
        if let value = data["status"] as? String{
            self.status = value
        }
        if let value = data["user_payment_status"] as? String{
            self.user_payment_status = value
        }
        if let value = data["transaction_id"] as? String{
            self.transaction_id = value
        }
        if let value = data["amount"] as? String{
            self.amount = value
        }
        if let value = data["feedback"] as? String{
            self.feedback = value
        }
        if let value = data["payment_method"] as? String{
            self.payment_method = value
        }
        if let value = data["created_at"] as? String{
            self.created_at = value
        }
        if let value = data["user_id"] as? String{
            self.user_id = value
        }
        if let value = data["type_id"] as? String{
            self.type_id = value
        }
        if let value = data["address_id"] as? String{
            self.address_id = value
        }
     
        if let typeData = data["type_data"] as? [[String:Any]]{
            
            for tData in typeData{
                if self.type?.lowercased() == "post" {
                    self.post_data.append(BuyerSellerModel(data: tData))
                }
                if self.type?.lowercased() == "story" {
                    self.story_data.append(StoryModel(data: tData))
                }
            }
        }
        
        if let userData = data["sellerDetails"] as? [[String:Any]]{
            
            for uData in userData{
                self.sellerData.append(UserModel(data: uData))
            }
        }
        
        if let value = data["user_name"] as? String {
            self.user_name = value
        }
    }
}
