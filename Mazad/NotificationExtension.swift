//
//  NotificationExtension.swift
//  Mazad Live
//
//  Created by alienbrainz on 29/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

//this is notificatioin extensionfdsfdasfdsa
extension Notification.Name {
    
    static let UpdateStreamProductNotification = Notification.Name("UpdateStreamProductNotification")
    static let UpdateStreamNotification = Notification.Name("UpdateStreamNotification")
    static let UpdateVotePercent = Notification.Name("UpdateVotePercent")
    static let RoomUpdateNotification = Notification.Name("RoomUpdateNotification")
    static let OnMessageRecieved = Notification.Name("onMessageRecieved")
    static let OnSoldProduct = Notification.Name("onSoldProduct")
    static let ReloadProfileViewControllerAfterPostUpload = Notification.Name("ReloadProfileViewControllerAfterPostUpload")
    static let DismissFollowAlert = Notification.Name("DismissFollowAlert")

}
