

import UIKit

class AboutViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let data = [NSLocalizedString("Terms and Conditions", comment: "Terms and Conditions"),NSLocalizedString("Data Privacy", comment: "Data Privacy")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("About", comment: "About")
        let v = UIView()
        v.backgroundColor = .clear
        self.tableView.tableFooterView = v
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        if let label = cell.contentView.viewWithTag(3) as? UILabel{
            label.text = NSLocalizedString(self.data[indexPath.row], comment: self.data[indexPath.row])
        }
        if let img = cell.contentView.viewWithTag(4) as? UIImageView{
            UIView.animate(withDuration: 0.3) {
                img.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vc = UIStoryboard(name:"Main2", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionViewController") as? TermsAndConditionViewController {
            
//            vc.titleStr = self.data[indexPath.Ørow]
            
            vc.title = self.data[indexPath.row]
            
            if indexPath.row == 0 {
                
                if let currentValue = UserDefaults.standard.value(forKey: CurrentLanguageKey) as? String{
                    print("current value = \(currentValue)")
                    
                    if currentValue == "en"{ // for english
                        vc.url = URL(string: kTermsAndConditionEn)
                    }else{ // for arabic
                        vc.url = URL(string: kTermsAndConditionAr)
                    }
                }else{
                    vc.url = URL(string: kTermsAndConditionEn)
                }
            }else{
                if let currentValue = UserDefaults.standard.value(forKey: CurrentLanguageKey) as? String{
                    print("current value = \(currentValue)")
                    
                    if currentValue == "en"{ // for english
                        vc.url = URL(string: kDataPrivacyEn)
                    }else{ // for arabic
                        vc.url = URL(string: kDataPrivacyAr)
                    }
                }else{
                    vc.url = URL(string: kDataPrivacyEn)
                }
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
