

import UIKit

class PrivacySettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

   
    @IBOutlet weak var tableView: UITableView!
    let data = [
        "Blocked accounts",
        "Activity status"
//        "Saved login info"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Privacy and Security", comment: "Privacy and Security")
        let v = UIView()
        v.backgroundColor = .clear
        self.tableView.tableFooterView = v
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            if let label = cell.contentView.viewWithTag(3) as? UILabel{
                label.text = NSLocalizedString(self.data[indexPath.row], comment: self.data[indexPath.row])
            }
            if let img = cell.contentView.viewWithTag(4) as? UIImageView{
                UIView.animate(withDuration: 0.3) {
                    img.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
                }
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "switchCell")!
        if let label = cell.contentView.viewWithTag(3) as? UILabel{
            label.text = NSLocalizedString(self.data[indexPath.row], comment: self.data[indexPath.row])
        }
        if let switchButton = cell.contentView.viewWithTag(4) as? UIButton{
            switchButton.removeTarget(nil, action: nil, for: .allEvents)
            switchButton.addTarget(self, action: #selector(self.onSwitchBtnClick(_:)), for: .touchUpInside)
            
            if MyProfile.activityStatus{
                switchButton.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            }
            else{
                switchButton.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "BlockedAccountsViewController") as? BlockedAccountsViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    @IBAction func onSwitchBtnClick(_ sender: UIButton) {
        
        var keepSaving = false
        if sender.image(for: .normal) == #imageLiteral(resourceName: "icon_switch_on"){
            
            sender.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            
        }else{
            keepSaving = true
            sender.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
        }
        
        self.updateActivityStatus(keepSaving: keepSaving) { (isKeepSaving) in
            
            if isKeepSaving{
                sender.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            }
            else{
                sender.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            }
        }
    }
    
    func updateActivityStatus(keepSaving:Bool, callback: @escaping ((Bool)->())){
        
        guard let userId = MyProfile.userId else{
            callback(!keepSaving)
            return
        }
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kUpdateActivityStatus, parameters: ["user_id": userId, "status": keepSaving ? "1" : "0"]) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err{
                    if let userData = data["description"] as? [String:Any]{
                        if let value = userData["is_activity"] as? Bool{
                            MyProfile.activityStatus = value
                            callback(value)
                        }
                        else{
                            callback(!keepSaving)
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    self.displayMessage(title: nil, msg: msg)
                    callback(!keepSaving)
                }
                else{
                    callback(!keepSaving)
                }
            }
        }
    }
}
