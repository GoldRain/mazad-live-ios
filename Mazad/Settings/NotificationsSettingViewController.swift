
import UIKit

class NotificationsSettingViewController: UIViewController {

    @IBOutlet weak var btnPushNotification: UIButton!
    @IBOutlet weak var btnEmailNotification: UIButton!
    @IBOutlet weak var lblPushNotification: UILabel!
    @IBOutlet weak var lblEmailNotification: UILabel!
    
    var pushNotification = false
    var EmailNotification = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblPushNotification.text = NSLocalizedString("Push notification", comment: "Push notification")
        self.lblEmailNotification.text = NSLocalizedString("Email & SMS notification", comment: "Email & SMS notification")

        
        self.title = NSLocalizedString("Notification", comment: "Notification")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
        if let value = MyProfile.push_notification, value {
            self.pushNotification = true
            self.btnPushNotification.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
        }else{
            self.pushNotification = false
            self.btnPushNotification.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
        }
        
        if let value = MyProfile.email_and_notification, value {
            self.EmailNotification = true
            self.btnEmailNotification.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
        }else{
            self.EmailNotification = false
            self.btnEmailNotification.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onPushNotificationClick(_ sender: UIButton) {
        
        if btnPushNotification.currentImage == #imageLiteral(resourceName: "icon_switch_on"){
            self.pushNotification = false
        }else{
            self.pushNotification = true
        }
        self.callNotificationApi(pushNotification: self.pushNotification, emailNotification: self.EmailNotification)
    }
    
    @IBAction func onEmailAndSmsNotificationClick(_ sender: UIButton) {
        
        if btnEmailNotification.currentImage == #imageLiteral(resourceName: "icon_switch_on"){
            self.EmailNotification = false
        }else{
            self.EmailNotification = true
        }
        self.callNotificationApi(pushNotification: self.pushNotification, emailNotification: self.EmailNotification)
    }
    
    func callNotificationApi(pushNotification:Bool,emailNotification:Bool){
        
        guard let userId = MyProfile.userId else{
            return;
        }
        
        let params = ["user_id":userId,
                      "push_notification":"\(pushNotification)",
                      "email_and_notification":"\(emailNotification)"]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kNotificationAndEmail, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    if let des = data["description"] as? [String:Any] {
                        
                        if let push_notification = des["push_notification"] as? Bool {
                            
                            if push_notification {
                                self.btnPushNotification.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal) // for true
                            }else{
                                self.btnPushNotification.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal) // for false
                            }
                            MyProfile.push_notification = push_notification
                        }
                        if let email_and_notification = des["email_and_notification"] as? Bool {
                            
                            if email_and_notification {
                                self.btnEmailNotification.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal) // for true
                            }else{
                                self.btnEmailNotification.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal) // for false
                            }
                            MyProfile.email_and_notification = email_and_notification
                        }
                    }
                }
            }
        }
    }

}
