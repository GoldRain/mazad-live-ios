//
//  UnfollowAlertViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 13/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import SDWebImage

class UnfollowAlertViewController: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var imgProfileImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var userName:String!
    var userId:String!
    var profileImage:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.imgProfileImage.sd_setImage(with: URL(string: profileImage), placeholderImage: #imageLiteral(resourceName: "user"), options: [], completed: nil)
        let text = NSLocalizedString("Unfollow", comment: "Unfollow")
        self.lblName.text = "\(text) \(self.userName ?? "")"
        
        NotificationCenter.default.addObserver(forName: .DismissFollowAlert, object: nil, queue: nil) { (not) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .DismissFollowAlert, object: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let firstView = touches.first?.view{
            if firstView != alertView{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onCancelClick(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onUnfollowClick(_ sender: Any) {
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        tabBarVC?.callFollowApi(his_id: self.userId, isFollow: 0, callback: { (data) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            followVC?.getBlockedUserList()
        })
    }
}
