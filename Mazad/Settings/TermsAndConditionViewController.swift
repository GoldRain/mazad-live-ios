

import UIKit

class TermsAndConditionViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    var wait:WaitView!
    
    var url:URL?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        webView.delegate = self
        
        let urlRequest = URLRequest(url: url!)
        
        webView.loadRequest(urlRequest)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        wait = self.addWaitSpinner()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let wait = wait {
            self.removeWaitSpinner(waitView: wait)
        }
    }
}
