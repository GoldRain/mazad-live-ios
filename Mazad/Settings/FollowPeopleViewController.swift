

import UIKit

enum ActionType {
    case Follower
    case Following
    case FollowPeople
}

var followVC:FollowPeopleViewController?

class FollowPeopleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FollowPeopleDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var actionType = ActionType.Following
    
    var followers = [UserModel]()
    var following = [UserModel]()
    var allUsersList = [UserModel]()
    var blockedUsers = [UserModel]()
    
    var userId:String?
    
    //
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    var refreshControl:UIRefreshControl!
    
    //MARK:- View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if actionType == .Follower {
            self.title = NSLocalizedString("Followers", comment: "Followers")
        }else if actionType == .Following {
            self.title = NSLocalizedString("Following", comment: "Following")
        }else {
            self.title = NSLocalizedString("Follow People", comment: "Follow People")
        }
        
        let v = UIView()
        v.backgroundColor = .clear
        self.tableView.tableFooterView = v
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //
        self.getBlockedUserList()
    }
    
    @objc func pullToRefresh(){
        
        self.refreshControl.endRefreshing()
        
        self.getBlockedUserList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Getting data
    func getBlockedUserList() {
        
        guard let _ = MyProfile.userId else{
            self.getFollowData()
            return;
        }
        
        tabBarVC?.callBlockListApi(showWait: true, callback: { (blUsers) in
            
            if let bUsers = blUsers {
                self.blockedUsers.removeAll()
                self.blockedUsers = bUsers
            }
            self.getFollowData()
        })
    }
    
    func getFollowData() {
        
        if let userId = userId {
            
            if self.actionType == .Follower {
                
                tabBarVC?.getFollowerList(user_id: userId, addWait: true, callback: { (users, msg) in
                    
                    self.emptyText = "No Data Found!!"
                    
                    self.followers.removeAll()
                    
                    if var followers = users, followers.count > 0 {
                        
                        for u in self.blockedUsers{
                            autoreleasepool {
                                if let index = followers.firstIndex(where: { (us) -> Bool in
                                    return us._id == u._id
                                }){
                                    followers.remove(at: index)
                                }
                            }
                        }
                        self.followers = followers
                    }
                    self.tableView.reloadData()
                })
                
            }else if actionType == .Following{ // Following
                
                tabBarVC?.getFollowingList(user_id: userId, addWait: true, callback: { (users, msg) in
                    
                    self.emptyText = NSLocalizedString("No Data Found!!", comment: "No Data Found!!")
                    
                    self.following.removeAll()
                    
                    if var following = users, following.count > 0 {
                        
                        for u in self.blockedUsers{
                            autoreleasepool {
                                if let index = following.firstIndex(where: { (us) -> Bool in
                                    return us._id == u._id
                                }){
                                    following.remove(at: index)
                                }
                            }
                        }
                        self.following = following
                    }
                    self.tableView.reloadData()
                })
            }else{ // Follow People
                self.getUsersList(showWait: true)
            }
        }
    }
    
    func getUsersList(showWait:Bool) {
        
        if showWait{
            self.tableView.addWaitView()
        }
        
        self.getUsersListFromServer(showWait: false, callback: { (users, msg) in
            
            self.tableView.removeWaitView()
            
            self.allUsersList.removeAll()
            
            if var users = users, users.count > 0 {
                
                for u in self.blockedUsers {
                    autoreleasepool {
                        if let index = users.firstIndex(where: { (us) -> Bool in
                            return us._id == u._id
                        }){
                            users.remove(at: index)
                        }
                    }
                }
                self.allUsersList = users
            }
            self.tableView.reloadData()
        })
    }
    
    func getUsersListFromServer(showWait:Bool, callback:@escaping (([UserModel]?, String?)->())){
        
        guard let top = appDel.topViewController else {
            let str = NSLocalizedString("There is some error", comment: "There is some error")
            callback(nil,str)
            return
        }
        
        var wait:WaitView!
        
        if showWait{
            wait = top.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kSearchUserAll, parameters: ["text": "", "user_id": MyProfile.userId ?? ""]) { (data, error) in
            
            if showWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err{
                    if let array = data["description"] as? [[String:Any]]{
                        autoreleasepool {
                            var users = [UserModel]()
                            
                            for item in array{
                                users.append(UserModel(data: item))
                            }
                            
                            if let _ = MyProfile.userId {
                                users = users.filter({ (user) -> Bool in
                                    return user._id != MyProfile.userId
                                })
                            }
                            callback(users, nil)
                            return;
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
                else{
                    let msg = NSLocalizedString("No users found!!", comment: "No users found!!")
                    callback(nil, msg)
                }
            }
            else{
                let msg = NSLocalizedString("No users found!![1]", comment: "No users found!![1]")
                callback(nil, msg)
            }
        }
    }
    
    //MARK:- TableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if actionType == .Following {
            if following.count > 0 {
                return self.following.count
            }else {
                return 1
            }
        }else if actionType == .Following {
            if followers.count > 0 {
                return self.followers.count
            }else {
                return 1
            }
        }else{
            if allUsersList.count > 0 {
                return allUsersList.count
            }else{
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FollowPeopleTableViewCell
        
        cell.delegate = self
        cell.indexPath = indexPath
        
        if actionType == .Following {
            // Refactor code
            if self.following.count == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
                cell.textLabel?.text = self.emptyText
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .gray
                cell.selectionStyle = .none
                return cell
            }
            //
            cell.following = self.following[indexPath.row]
            cell.setUpForFollowing()
            cell.actionType = .Following
        }
        else if actionType == .Follower {
            // Refactor Code
            if self.followers.count == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
                cell.textLabel?.text = self.emptyText
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .gray
                cell.selectionStyle = .none
                return cell
            }
            cell.followers = self.followers[indexPath.row]
            cell.setUpForFollowers()
            cell.actionType = .Follower
        }
        else{ // follow people
            if self.allUsersList.count == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
                cell.textLabel?.text = self.emptyText
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .gray
                cell.selectionStyle = .none
                return cell
            }
            cell.userList = self.allUsersList[indexPath.row]
            cell.setUpForFollowPeople()
            cell.actionType = .FollowPeople
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var otherUserId:String?
        if actionType == .Follower {
            if self.followers.isEmpty{
                return;
            }
            otherUserId = self.followers[indexPath.row]._id
        }else if actionType == .Following {
            if self.following.isEmpty {
                return;
            }
            otherUserId = self.following[indexPath.row]._id
        }else{
            if self.allUsersList.isEmpty {
                return;
            }
            otherUserId = self.allUsersList[indexPath.row]._id
        }
        if let otherId = otherUserId {
            if let myUserId = MyProfile.userId {
                
                if otherId == myUserId {
                    return;
                }
                
                if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController{
                    vc.userId = otherUserId
                    let nav = UINavigationController(rootViewController: vc)
                    nav.modalTransitionStyle = .coverVertical
                    self.present(nav, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: Delegate
    func onButtonClick(sender: UIButton,userDetail:UserModel) {
        
        guard let _ = MyProfile.userId else {
            getLoginDialouge()
            return;
        }
        
        if sender.currentTitle == "Following" {
            
            if let userName = userDetail.username {
                if let userId = userDetail._id {
                    if let profileImage = userDetail.profilePhoto {
                        
                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "UnfollowAlertViewController") as? UnfollowAlertViewController {
                            
                            vc.userName = userName
                            vc.userId = userId
                            vc.profileImage = profileImage
                            
                            vc.modalPresentationStyle = .overCurrentContext
                            vc.modalTransitionStyle = .crossDissolve
                          
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
            }
        }else { // follow click
            if let userId = userDetail._id {
                
                let wait = appDel.topViewController?.addWaitSpinner()
                
                tabBarVC?.callFollowApi(his_id: userId, isFollow: 1, callback: { (data) in
                    
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    self.getBlockedUserList()
                })
            }
        }
    }
}

protocol FollowPeopleDelegate {
    func onButtonClick(sender: UIButton,userDetail:UserModel)
}

class FollowPeopleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserImage: UIImageView!
    @IBOutlet weak var btnFollow: UIButton!
    
    var delegate: FollowPeopleDelegate!
    var user: String!
    
    var indexPath:IndexPath!
    var actionType:ActionType!
    
    var followers:UserModel?
    var following:UserModel?
    var userList:UserModel?
    
    func setUpDataForSkipLoginUser() {
        if MyProfile.userId == nil || MyProfile.userId == "" {
            self.btnFollow.isHidden = false
            self.btnFollow.setTitle(NSLocalizedString("Follow", comment: "Follow"), for: .normal)
        }
    }
    
    func setUpForFollowing(){
        
        if let following = following {
            
            self.lblUserName.text = following.username
            if let url = following.profilePhoto {
                self.imgUserImage.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "user"), options: [], completed: nil)
            }
            
            if following._id == MyProfile.userId {
                self.btnFollow.isHidden = true
            }else{
                self.btnFollow.isHidden = false
            }
            
            if following.followers.contains(where: { (users) -> Bool in
                return users._id == MyProfile.userId
            }){
                self.btnFollow.setTitle(NSLocalizedString("Following", comment: "Following"), for: .normal)
            }else{
                self.btnFollow.setTitle(NSLocalizedString("Follow", comment: "Follow"), for: .normal)
            }
        }
        self.setUpDataForSkipLoginUser()
    }
    
    func setUpForFollowers() {
        
        if let followers = followers {
            self.lblUserName.text = followers.username
            if let url = followers.profilePhoto {
                self.imgUserImage.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "user"), options: [], completed: nil)
            }
            if followers._id == following?._id {
                
            }
            if followers._id == MyProfile.userId {
                self.btnFollow.isHidden = true
            }else{
                self.btnFollow.isHidden = false
            }
            
            if followers.followers.contains(where: { (users) -> Bool in
                return users._id == MyProfile.userId
            }){
                self.btnFollow.setTitle("Following", for: .normal)
            }else{
                self.btnFollow.setTitle("Follow", for: .normal)
            }
        }
        self.setUpDataForSkipLoginUser()
    }
    
    func setUpForFollowPeople() {
        if let user = userList {
            self.lblUserName.text = user.username
            if let url = user.profilePhoto {
                self.imgUserImage.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "user"), options: [], completed: nil)
            }
            if user._id == following?._id {
                
            }
            if user._id == MyProfile.userId {
                self.btnFollow.isHidden = true
            }else{
                self.btnFollow.isHidden = false
            }
            
            if user.followers.contains(where: { (users) -> Bool in
                return users._id == MyProfile.userId
            }){
                self.btnFollow.setTitle("Following", for: .normal)
                self.btnFollow.backgroundColor = .clear
                self.btnFollow.layer.borderColor = BLUECOLOR.cgColor
                self.btnFollow.layer.borderWidth = 1.0
                self.btnFollow.setTitleColor(BLUECOLOR, for: .normal)
            }else{
                self.btnFollow.setTitle("Follow", for: .normal)
                self.btnFollow.layer.borderWidth = 0.0
                self.btnFollow.setTitleColor(.white, for: .normal)
                self.btnFollow.backgroundColor = BLUECOLOR
            }
        }
    }
    
    @IBAction func onFollowUnfollowClick(_ sender: UIButton) {
        
        if actionType == .Following {
            delegate.onButtonClick(sender: sender,userDetail:following!)
        }else if actionType == .Follower{
            delegate.onButtonClick(sender: sender,userDetail:followers!)
        }else{
            delegate.onButtonClick(sender: sender,userDetail:userList!)
        }
    }
}
