

import UIKit

class ECommerceSettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    
    @IBOutlet weak var tableView: UITableView!
    let data = [
        "Story settings",
        "IBAN informations"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("ECommerce Settings", comment: "ECommerce Settings")
        let v = UIView()
        v.backgroundColor = .clear
        self.tableView.tableFooterView = v
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        if let label = cell.contentView.viewWithTag(3) as? UILabel{
            label.text = NSLocalizedString(self.data[indexPath.row], comment: self.data[indexPath.row])
        }
        if let img = cell.contentView.viewWithTag(4) as? UIImageView{
            UIView.animate(withDuration: 0.3) {
                img.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "StorySettingsViewController") as? StorySettingsViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "IBANInformationViewController") as? IBANInformationViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
}
