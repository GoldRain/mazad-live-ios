//
//  ContactViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 11/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import Contacts
import MessageUI

class ContactViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    var contacts:[CNContact]!
    var contactDict = [String:[CNContact]]()
    let indexList = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    var filteredData = [CNContact]()
    var inSearchMode = false
    
    var userList = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Invite", comment: "Invite")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.contacts = self.contacts.sorted(by: { first, second in
            if first.givenName.compare(second.givenName) == .orderedAscending{
                return true
            }
            return false
        })
        
        for i in 0 ..< indexList.count {
            contactDict[indexList[i]] = contacts.filter { (contact) -> Bool in
                return contact.givenName.capitalized.starts(with: indexList[i])
            }
        }
        
        searchBar.returnKeyType = UIReturnKeyType.done
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.black]
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = true
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]
    }
    
    //MARK:- table funciton
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if inSearchMode {
            return 1
        }
        return indexList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if inSearchMode {
            return filteredData.count
        }
        return (contactDict["\(indexList[section])"]?.count)!
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if let araayOfFilteredContact = contactDict["\(indexList[section])"], araayOfFilteredContact.count > 0{
            
            if inSearchMode {
                return "TOP NAME MATCHES"
            }
            return indexList[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? contactCell
        
        cell?.indexPath = indexPath
        
        if inSearchMode {
            cell?.contacts = filteredData[indexPath.row]
            cell?.contactName.text = filteredData[indexPath.row].givenName
            if filteredData[indexPath.row].imageDataAvailable {
                if let imageData = filteredData[indexPath.row].imageData {
                    cell?.contactImage.image = UIImage(data: imageData)
                }
            } else {
                cell?.contactImage.image = UIImage(named: "user")
            }
            let phoneNumbers = filteredData[indexPath.row].phoneNumbers
            if !phoneNumbers.isEmpty {
                if let digit = phoneNumbers[0].value.value(forKey: "digits") as? String {
                    if self.userList.contains(where: { (u) -> Bool in
                        return u.phone! == digit
                    }) {
                        cell?.btnInvite.isHidden = true
                        cell?.contactImage.image = #imageLiteral(resourceName: "mazad_ic_home.png")
                    }else{
                        cell?.btnInvite.isHidden = false
                    }
                }
            }
        } else {
            if let araayOfFilteredContact = contactDict["\(indexList[indexPath.section])"] {
                cell?.contacts = araayOfFilteredContact[indexPath.row]
                if araayOfFilteredContact.count != 0 {
                    cell?.contactName.text = araayOfFilteredContact[indexPath.row].givenName
                    
                    if araayOfFilteredContact[indexPath.row].imageDataAvailable {
                        if let imageData = araayOfFilteredContact[indexPath.row].imageData{
                            cell?.contactImage.image = UIImage(data: imageData)
                        }
                    }
                    else {
                        cell?.contactImage.image = UIImage(named: "user")
                    }
                }
                let phoneNumbers = araayOfFilteredContact[indexPath.row].phoneNumbers
                if !phoneNumbers.isEmpty {
                    if let digit = phoneNumbers[0].value.value(forKey: "digits") as? String {
                        if self.userList.contains(where: { (u) -> Bool in
                            return u.phone! == digit
                        }) {
                            cell?.btnInvite.isHidden = true
                            cell?.contactImage.image = #imageLiteral(resourceName: "mazad_ic_home.png")
                        }else{
                            cell?.btnInvite.isHidden = false
                        }
                    }
                }
            }
        }
        cell?.contactImage.layer.cornerRadius = (cell?.contactImage.bounds.height)! / 2
        cell?.contactImage.layer.masksToBounds = true
        cell?.btnInvite.layer.cornerRadius = 10
        cell?.selectionStyle = .none
        return cell!
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if inSearchMode{
            return nil
        }
        return indexList
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            
            inSearchMode = false
            view.endEditing(true)
            tableView.reloadData()
            
        } else {
            
            inSearchMode = true
            
            self.filteredData.removeAll()
            
            self.filteredData = self.contacts.filter({$0.givenName.lowercased().contains(searchBar.text!.lowercased())})
            
            tableView.reloadData()
        }
        
        self.searchBar.becomeFirstResponder()
    }
    
    // Dimissing Keyboard
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

class contactCell: UITableViewCell {
    
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var btnInvite: UIButton!
    
    var contacts:CNContact!
    var indexPath:IndexPath!
    
    let messageComposer = MessageComposer()
    
    @IBAction func onInviteClick(_ sender: Any) {
        
        let phoneNumbers = contacts!.phoneNumbers
        
        if phoneNumbers.isEmpty {
            let str = NSLocalizedString("Phone number is not available", comment: "Phone number is not available")
            
            if let top = appDel.topViewController {
                top.displayMessage(str)
            }
            return;
        }
        let digit = (phoneNumbers[0].value).value(forKey: "digits") as! String
        
        if (messageComposer.canSendText()) {
            let wait = appDel.topViewController?.addWaitSpinner()
            let messageComposeVC = messageComposer.configuredMessageComposeViewController(number:digit)
            
            if let top = appDel.topViewController {
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                top.present(messageComposeVC, animated: true, completion: nil)
            }
        } else {
            let str = NSLocalizedString("Your device is not able to send text messages", comment: "Your device is not able to send text messages")
            
            if let top = appDel.topViewController {
                top.displayMessage(str)
            }
        }
    }
}

class MessageComposer: NSObject, MFMessageComposeViewControllerDelegate {
    
    // A wrapper function to indicate whether or not a text message can be sent from the user's device
    func canSendText() -> Bool {
        return MFMessageComposeViewController.canSendText()
    }
    
    // Configures and returns a MFMessageComposeViewController instance
    func configuredMessageComposeViewController(number:String) -> MFMessageComposeViewController {
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self
        messageComposeVC.recipients = [number]
        messageComposeVC.body = "Hey friend - Free Download Mazad Live from AppStore :- \nhttps://apps.apple.com/in/app/mazad-live/id1450514377"
        return messageComposeVC
    }
    
    // MFMessageComposeViewControllerDelegate callback - dismisses the view controller when the user is finished with it
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}
