import UIKit


class AccountViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageViewMain: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var txtCountryCode: CustomTextField!
    @IBOutlet weak var txtPhone: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    @IBOutlet weak var txtDescription: CustomTextField!
    
    var userDetails:UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Account", comment: "Account")
        
        if let imgUrl = MyProfile.userProfilePhoto {
            self.profileImageView.sd_setImage(with: URL(string: imgUrl), completed: nil)
        }
        self.txtName.text = MyProfile.userName
        self.txtCountryCode.text = MyProfile.countryCode
        self.txtPhone.text = MyProfile.phone
        self.txtEmail.text = MyProfile.userEmail
        self.txtPassword.text = MyProfile.password
        self.txtConfirmPassword.text = MyProfile.password
        
        self.txtCountryCode.isUserInteractionEnabled = false
        self.txtPhone.isUserInteractionEnabled = false
        self.txtEmail.isUserInteractionEnabled = false
        
        if let des = MyProfile.description, des != ""{
            self.txtDescription.showTitleLable(isHide: false)
            self.txtDescription.text = des
        }
        
        self.scrollView.bringSubview(toFront: self.mainView)
        self.mainView.bringSubview(toFront: self.imageViewMain)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let view = touches.first?.view{
            if view == self.imageViewMain{
                self.showImagePicker()
            }
        }
    }
    
    @IBAction func onProfilePhotoClick(_ sender: UIButton) {
         self.showImagePicker()
    }
    
    @IBAction func onSaveClick(_ sender: UIButton) {
        
        if validate(){
            
            guard let userId = MyProfile.userId else {
                return
            }
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            var oldUrl:String?
            if let value = MyProfile.userProfilePhoto{
                oldUrl = value
            }
            let randomName = randomString(length: 20)
            uploadImagesToAws([profileImageView.image!], folderName: appDel.userImageFolder, randomName: randomName) { (url) in
                
                if let url = url?.first {
                    
                    DispatchQueue.main.async {
                        
                        let params = ["user_id":userId,
                                      "user_name":self.txtName.text!.trim(),
                                      "image_url":url,
                                      "description":self.txtDescription.text!.trim(),
                                      "password":self.txtPassword.text!.trim()
                                     ]
                        
                        JSONRequest.makeRequest(kUpdateProfile, parameters: params) { (data, error) in
                            
                            if let wait = wait {
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            
                            if let data = data as? [String:Any]{
                                
                                if let err = data["error"] as? Bool, !err{
                                    
                                    if let data = data["description"] as? [String:Any] {
                                        
                                        guard let username = data["username"] as? String else {
                                             deleteAwsFile(folderName: appDel.userImageFolder, fullPath: url)
                                            let str = NSLocalizedString("Failed to update profile", comment: "Failed to update profile")
                                            self.displayMessage(str)
                                            return
                                        }
                                        guard let password = data["password"] as? String else {
                                             deleteAwsFile(folderName: appDel.userImageFolder, fullPath: url)
                                            let str = NSLocalizedString("Failed to update profile", comment: "Failed to update profile")
                                            self.displayMessage(str)
                                            return
                                        }
                                        guard let profilePhoto = data["profilePhoto"] as? String else {
                                             deleteAwsFile(folderName: appDel.userImageFolder, fullPath: url)
                                            let str = NSLocalizedString("Failed to update profile", comment: "Failed to update profile")
                                            self.displayMessage(str)
                                            return
                                        }
                                        guard let description = data["description"] as? String else {
                                             deleteAwsFile(folderName: appDel.userImageFolder, fullPath: url)
                                            let str = NSLocalizedString("Failed to update profile", comment: "Failed to update profile")
                                            self.displayMessage(str)
                                            return
                                        }
                                        
                                        MyProfile.userName = username
                                        MyProfile.password = password
                                        MyProfile.userProfilePhoto = profilePhoto
                                        MyProfile.description = description
                                        
                                        let str = NSLocalizedString("Profile Updated Successfully", comment: "Profile Updated Successfully")
                                        self.displayMessage(str, callback: { () -> (Void) in
                                            self.navigationController?.popViewController(animated: true)
                                            //remove the old image from aws
                                            if let url = oldUrl{
                                                deleteAwsFile(folderName: url, fullPath: appDel.userImageFolder)
                                            }
                                        })
                                    }
                                    
                                }else if let msg = data["message"] as? String{
                                     deleteAwsFile(folderName: appDel.userImageFolder, fullPath: url)
                                    let str = NSLocalizedString(msg, comment: msg)
                                    self.displayMessage(str)
                                }
                            }else{
                                deleteAwsFile(folderName: appDel.userImageFolder, fullPath: url)
                                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                                self.displayMessage(str)
                            }
                        }
                    }
                }
                else{
                    
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    
                    let str = NSLocalizedString("Failed to upload image", comment: "Failed to upload image")
                    self.displayMessage(str)
                }
            }
        }
    }

    func showImagePicker(){
        
        let alertController = getAlertController(title: "Select Source", message: nil)
        alertController.view.tintColor = .black
        
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.imagePickerSourceType(source: .camera)
        }))
        
        alertController.addAction(UIAlertAction(title: "Gallary", style: .default, handler: { (_) in
            self.imagePickerSourceType(source: .photoLibrary)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerSourceType(source: UIImagePickerControllerSourceType){
        let imagePicker = UIImagePickerController()
        imagePicker.navigationBar.tintColor = .white
        imagePicker.allowsEditing = true
        imagePicker.sourceType = source
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImage: UIImage!
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            selectedImage = editedImage
        }
        else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            selectedImage = originalImage
        }
        self.profileImageView.image = selectedImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func validate() -> Bool {
        if self.profileImageView.image == nil{
            let str = NSLocalizedString("Please select image", comment: "Please select image")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtName.text!){
            let str = NSLocalizedString("Please enter user name", comment: "Please enter user name")
            self.displayMessage(str)
            return false
        }
        if isEmpty(txtPassword.text!){
            let str = NSLocalizedString("Please enter password", comment: "Please enter password")
            self.displayMessage(str)
            return false
        }
        if txtPassword.text!.count < 8{
            let str = NSLocalizedString("Password should be 8 characters long", comment: "Password should be 8 characters long")
            self.displayMessage(str)
            return false
        }
        if isEmpty(txtConfirmPassword.text!){
            let str = NSLocalizedString("Please confirm password", comment: "Please confirm password")
            self.displayMessage(str)
            return false
        }
        if txtPassword.text! != txtConfirmPassword.text!{
            let str = NSLocalizedString("Password mismatch", comment: "Password mismatch")
            self.displayMessage(str)
            return false
        }
        
        return true
    }
}
