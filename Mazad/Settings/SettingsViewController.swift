

import UIKit
import Contacts

fileprivate enum MenuItem: String {
    case invite                 = "Invite"
    case followPeople           = "Follow People"
    case ecommerceSettings      = "Ecommerce Settings"
    case notifications          = "Notifications"
    case privacyAndSecurity     = "Privacy & Security"
    case support                = "Support"
    case sellerAccount          = "Switch to Buyer Account"
    case buyerAccount           = "Switch to Seller Account"
    case logout                 = "Logout"
    case login                  = "Login"
    case account                = "Account"
    case about                  = "About"
    case changeLanguage         = "Change Language"
}

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    fileprivate var options: [(MenuItem, UIImage)] =
        
        [
            (.invite,                #imageLiteral(resourceName: "icon_home_grey2")),
            (.followPeople,          #imageLiteral(resourceName: "icon_profile_connect")),
            (.account,               #imageLiteral(resourceName: "icon_profile_grey").withRenderingMode(.alwaysOriginal)),
            (.ecommerceSettings,     #imageLiteral(resourceName: "icon_dollar_screen")),
            (.notifications,         #imageLiteral(resourceName: "icon_bell_grey")),
            (.privacyAndSecurity,    #imageLiteral(resourceName: "icon_privacy_grey")),
            (.support,               #imageLiteral(resourceName: "icon_support")),
            (.about,                 #imageLiteral(resourceName: "icon_info")),
            (.changeLanguage,        #imageLiteral(resourceName: "changeLanguage")),
            (.sellerAccount,         #imageLiteral(resourceName: "icon_profileDual_grey")),
            (.logout,                #imageLiteral(resourceName: "icon_shutDown_red"))
    ]
    
    var isExpand = false
    
    @IBOutlet weak var tableView: UITableView!
    
    let btn = UIButton()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Settings", comment: "Settings")
        
        let v = UIView()
        v.backgroundColor = .clear
        tableView.tableFooterView = v
        
        if MyProfile.isSeller == false{
            self.options[9] = (.buyerAccount, #imageLiteral(resourceName: "icon_profileDual_grey"))
        }
        if MyProfile.userId == nil || MyProfile.userId == "" {
            self.options[10] = (.login, #imageLiteral(resourceName: "icon_shutDown_red"))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if isExpand{
                return 3
            }
            return 1
        }
        return self.options.count-1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
                if let mainView = cell.contentView.viewWithTag(1){
                    mainView.addShadow()
                }
                if let img = cell.contentView.viewWithTag(2) as? UIImageView{
                    if let label = cell.contentView.viewWithTag(3) as? UILabel{
                        let data = options[indexPath.row]
                        img.image = data.1
                        label.text = NSLocalizedString(data.0.rawValue, comment: data.0.rawValue)
                    }
                }
                if let img = cell.contentView.viewWithTag(4) as? UIImageView{
                    
                    if isExpand{
                        UIView.animate(withDuration: 0.3) {
                            img.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
                        }
                    }else{
                        UIView.animate(withDuration: 0.3) {
                            img.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
                        }
                    }
                    
                    if indexPath.row == 8 || indexPath.row == 9 {
                        img.isHidden = true
                    }else{
                        img.isHidden = false
                    }
                }
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2")!
                if let mainView = cell.contentView.viewWithTag(1){
                    mainView.addShadow()
                }
                if let label = cell.contentView.viewWithTag(3) as? UILabel{
                    if indexPath.row == 1{
                        label.text = NSLocalizedString("Contacts", comment: "Contacts")
                    }else{
                        label.text = NSLocalizedString("Facebook Friends", comment: "Facebook Friends")
                    }
                }
                return cell
            }
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            if let mainView = cell.contentView.viewWithTag(1){
                mainView.addShadow()
            }
            if let img = cell.contentView.viewWithTag(2) as? UIImageView{
                if let label = cell.contentView.viewWithTag(3) as? UILabel{
                    let data = options[indexPath.row+1]
                    img.image = data.1
                    label.text = NSLocalizedString(data.0.rawValue, comment: data.0.rawValue)
                }
            }
            if let img = cell.contentView.viewWithTag(4) as? UIImageView{
                img.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
                
                if indexPath.row == 8 || indexPath.row == 9 {
                    img.isHidden = true
                }else{
                    img.isHidden = false
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            if indexPath.row == 0{
                
                let results = fetchContact()
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as? ContactViewController {
                    vc.contacts = results
                    self.getUsersListFromServer(showWait: true) { (users, msg) in
                        if let usrs = users{
                            vc.userList = usrs
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
//                self.isExpand = !self.isExpand
//                let range = NSMakeRange(0, self.tableView.numberOfSections)
//                let sections = NSIndexSet(indexesIn: range)
//                self.tableView.reloadSections(sections as IndexSet, with: .automatic)
            }
//            else if indexPath.row == 1{
//                //contact
//                self.fetchContact()
//            }
//            else{
//                //facebook
//            }
        }
        if indexPath.section != 0{
            let data = self.options[indexPath.row+1]
            self.handleSelectedRow(type: data.0)
        }
    }
    
    fileprivate func handleSelectedRow(type: MenuItem){
        
        switch type {
            
        case .account:
            guard let _ = MyProfile.userId else{
                getLoginDialouge()
                return;
            }
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountViewController") as? AccountViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case .followPeople:
            guard let _ = MyProfile.userId else{
                getLoginDialouge()
                return;
            }
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowPeopleViewController") as? FollowPeopleViewController{
                
                vc.actionType = .FollowPeople
                vc.userId = MyProfile.userId
                followVC = vc
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case .ecommerceSettings:
            guard let _ = MyProfile.userId else{
                getLoginDialouge()
                return;
            }
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ECommerceSettingsViewController"){
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case .about:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as? AboutViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case .privacyAndSecurity:
            guard let _ = MyProfile.userId else{
                getLoginDialouge()
                return;
            }
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacySettingsViewController") as? PrivacySettingsViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case .support:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SupportViewController") as? SupportViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case .buyerAccount, .sellerAccount:
            
            guard let _ = MyProfile.userId else{
                getLoginDialouge()
                return;
            }
            
            MyProfile.isSeller = !MyProfile.isSeller
            
            NotificationCenter.default.post(name: NSNotification.Name("changeUser"), object: nil, userInfo: ["user":MyProfile.isSeller])
            
            if MyProfile.isSeller == false{
                self.options[9] = (.buyerAccount, #imageLiteral(resourceName: "icon_profileDual_grey"))
            }
            else{
                self.options[9] = (.sellerAccount, #imageLiteral(resourceName: "icon_profileDual_grey"))
            }
            self.tableView.reloadData()
            
        case .notifications:
            guard let _ = MyProfile.userId else{
                getLoginDialouge()
                return;
            }
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsSettingViewController") as? NotificationsSettingViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case .logout , .login:
            
            messageCenter?.updateOnlineStatus(isOnline: false, playerId: "")
            messageCenter?.removeOldDatabase()
            messageCenter?.stop()
            appDel.adTimer?.invalidate()
            MyProfile.logOutUser()
            MyProfile.showVerifiedScreen = true
            
            appDel.topViewController?.view.window?.rootViewController?.dismiss(animated: true, completion: {
                //appDel.topViewController?.navigationController?.popViewController(animated: true)
            })
            
        case .changeLanguage:
            
            if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "LanguagesViewController") as? LanguagesViewController {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        default: break
            
        }
    }
    
    func getUsersListFromServer(showWait:Bool, callback:@escaping (([UserModel]?, String?)->())){
        
        guard let top = appDel.topViewController else {
            let str = NSLocalizedString("There is some error", comment: "There is some error")
            callback(nil,str)
            return
        }
        
        var wait:WaitView!
        
        if showWait{
            wait = top.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kSearchUserAll, parameters: ["text": "", "user_id": MyProfile.userId ?? ""]) { (data, error) in
            
            if showWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err{
                    if let array = data["description"] as? [[String:Any]]{
                        autoreleasepool {
                            var users = [UserModel]()
                            
                            for item in array{
                                users.append(UserModel(data: item))
                            }
                            
                            if let _ = MyProfile.userId {
                                users = users.filter({ (user) -> Bool in
                                    return user._id != MyProfile.userId
                                })
                            }
                            callback(users, nil)
                            return;
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
                else{
                    let msg = NSLocalizedString("No users found!!", comment: "No users found!!")
                    callback(nil, msg)
                }
            }
            else{
                let msg = NSLocalizedString("No users found!![1]", comment: "No users found!![1]")
                callback(nil, msg)
            }
        }
    }
}


extension UINavigationController {
    func popViewControllerWithHandler(completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.popViewController(animated: true)
        CATransaction.commit()
    }
    func pushViewController(viewController: UIViewController, completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.pushViewController(viewController, animated: true)
        CATransaction.commit()
    }
    
    func popToViewController(viewController: UIViewController, completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        popToViewController(viewController, animated: true)
        CATransaction.commit()
    }
}
