

import UIKit

class StorySettingsViewController: UIViewController {

    @IBOutlet weak var btnAuction: UIButton!
    @IBOutlet weak var btnBuyNow: UIButton!
    @IBOutlet weak var btnWhatsappOnly: UIButton!
    @IBOutlet weak var btnWhatsappAndCall: UIButton!
    
    var isAuction:Bool!
    var isBuyNow:Bool!
    var isWhatsappOnly:Bool!
    var isWhatsappAndCall:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Story Settings", comment: "Story Settings")
        
        self.updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
        if btnAuction.currentImage == #imageLiteral(resourceName: "icon_switch_on") {
            self.isAuction = true
            self.isBuyNow = false
        }else {
            self.isAuction = false
            self.isBuyNow = true
        }
        if btnWhatsappOnly.currentImage ==  #imageLiteral(resourceName: "icon_switch_on") {
            self.isWhatsappOnly = true
            self.isWhatsappAndCall = false
        }else {
            self.isWhatsappOnly = false
            self.isWhatsappAndCall = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onSwitchBtnClick(_ sender: UIButton) {
        
        if btnAuction.currentImage ==  #imageLiteral(resourceName: "icon_switch_on") {
            self.isAuction = false
            self.isBuyNow = true
            self.callUpdateStorySettings { (res) in
                if res {
                    self.btnAuction.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
                    self.btnBuyNow.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
                }
            }
        }else {
            self.isAuction = true
            self.isBuyNow = false
            self.callUpdateStorySettings { (res) in
                if res {
                    self.btnAuction.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
                    self.btnBuyNow.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
                }
            }
        }
    }
    
    @IBAction func onWhatsAppSwitchBtnClick(_ sender: UIButton) {
        
        if btnWhatsappOnly.currentImage ==  #imageLiteral(resourceName: "icon_switch_on") {
            self.isWhatsappOnly = false
            self.isWhatsappAndCall = true
            self.callUpdateStorySettings { (res) in
                if res {
                    self.btnWhatsappOnly.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
                    self.btnWhatsappAndCall.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
                }
            }
        }else {
            self.isWhatsappOnly = true
            self.isWhatsappAndCall = false
            self.callUpdateStorySettings { (res) in
                if res {
                    self.btnWhatsappOnly.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
                    self.btnWhatsappAndCall.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
                }
            }
        }
    }
    
    func callUpdateStorySettings(callback:@escaping ((Bool)->())) {
    
        guard let userId = MyProfile.userId else {
            return
        }
        
        let params = ["is_auction":"\(self.isAuction!)",
                      "is_buyNow":"\(self.isBuyNow!)",
                      "is_whatsappOnly":"\(self.isWhatsappOnly!)",
                      "is_whatsappAndCall":"\(self.isWhatsappAndCall!)",
                      "user_id":userId]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kUpdateStorySettings, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [String:Any] {
                        
                        if let is_auction = des["is_auction"] as? Bool {
                            MyProfile.isAuction = is_auction
                        }
                        if let is_buyNow = des["is_buyNow"] as? Bool {
                            MyProfile.isBuyNow = is_buyNow
                        }
                        if let is_whatsappOnly = des["is_whatsappOnly"] as? Bool {
                            MyProfile.isWhatsappOnly = is_whatsappOnly
                        }
                        if let is_whatsappAndCall = des["is_whatsappAndCall"] as? Bool {
                            MyProfile.isWhatsappAndCall = is_whatsappAndCall
                        }
                        callback(true)
                        let _ = NSLocalizedString("Setting updated successfully", comment: "Setting updated successfully")
                        //self.displayMessage(str, callback: nil)
                    }
                    else{
                        callback(false)
                        let _ = NSLocalizedString("Failed to change setting. Please try again", comment: "Failed to change setting. Please try again")
                        //self.displayMessage(str)
                    }
                }
                else if let msg = data["message"] as? String{
                    callback(false)
                    let _ = NSLocalizedString(msg, comment: msg)
                    //self.displayMessage(str)
                }
            }
            else{
                callback(false)
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                //self.displayMessage(str)
            }
        }
    }
    
    func updateUI() {
        
        if MyProfile.isBuyNow! {
            self.btnBuyNow.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            self.btnAuction.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
        }else {
            self.btnBuyNow.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            self.btnAuction.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
        }
        
        if MyProfile.isWhatsappOnly! {
            btnWhatsappOnly.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            btnWhatsappAndCall.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
        }else {
            btnWhatsappOnly.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            btnWhatsappAndCall.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
        }
    }
    
}
