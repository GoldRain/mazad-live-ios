

import UIKit

class CurrencySelectionViewController:UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    
    @IBOutlet weak var lblCurrencyCode: UILabel!
    @IBOutlet weak var lblCurrencyName: UILabel!
    @IBOutlet weak var listMainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var countriesList = [Currency]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Select Currency", comment: "Select Currency")
        
        if let path = Bundle.main.path(forResource: "Common-Currency", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let dataArray = jsonResult as? [String:Any]{
                    var dataCun = [Currency]()
                    for dataDict in dataArray{
                        if let dict = dataDict.value as? [String:Any]{
                            let d = Currency(dict: dict)
                            dataCun.append(d)
                        }
                    }
                    self.countriesList = dataCun
                    self.tableView.reloadData()
                }
            } catch {
               
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let data = countriesList[indexPath.row]
        if let lblCountry = cell.contentView.viewWithTag(3) as? UILabel{
            if let lblName = cell.contentView.viewWithTag(2) as? UILabel{
                lblCountry.text = "(\(data.symbol!))"
                lblName.text = data.name
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let data = self.countriesList[indexPath.row]
        self.lblCurrencyCode.text = "(\(data.symbol!))"
        self.lblCurrencyName.text = data.name
    }
    
}


class Currency: NSObject {
//    "USD": {
//    "symbol": "$",
//    "name": "US Dollar",
//    "symbol_native": "$",
//    "decimal_digits": 2,
//    "rounding": 0,
//    "code": "USD",
//    "name_plural": "US dollars"
//    }
    
    var name: String?
    var symbol: String?
    
    init(dict: [String: Any]) {
        if let code = dict["code"] as? String{
            self.symbol = code
        }
        if let name = dict["name"] as? String{
            self.name = name
        }
    }
}
