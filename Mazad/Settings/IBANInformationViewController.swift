

import UIKit
import SDWebImage

class IBANInformationViewController: UIViewController {

    @IBOutlet weak var txtCountry: CustomTextField!
    @IBOutlet weak var txtBanNumber: CustomTextField!
    @IBOutlet weak var txtBankName: CustomTextField!
    @IBOutlet weak var txtPhoneNumber: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = NSLocalizedString("IBAN Information", comment: "IBAN Information")
        
        self.txtCountry.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCountryClick(_:))))
        
        self.UpdateFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func onCountryClick(_ sender: Any) {
        
        appDel.getAllCountries { (countries, msg) in
            
            if let countries = countries{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountriesSelectionViewController") as? CountriesSelectionViewController{
                    
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    
                    vc.countriesList = countries
                    vc.callback = {  data in
                        
                        self.txtCountry.showTitleLable(isHide: false)
                        self.txtCountry.text = data.countryCode!
                        
                    }
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if let msg = msg{
                self.displayMessage(msg)
            }
        }
    }
    
    @IBAction func onSaveClick(_ sender: Any) {
        
        if validate() {
            
            guard let userId = MyProfile.userId else {
                return
            }
            
            let params = ["user_id":userId,
                          "country":self.txtCountry.text!,
                          "banNumber":self.txtBanNumber.text!,
                          "bankName":self.txtBankName.text!,
                          "phoneNumber":self.txtPhoneNumber.text!]
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            JSONRequest.makeRequest(kIBAN, parameters: params) { (data, error) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data as? [String:Any]{
                    
                    if let err = data["error"] as? Bool, !err{
                        
                        if let description = data["description"] as? [String:Any] {
                            
                            if let des = description["IBAN"] as? [String:Any]{
                                
                                MyProfile.set(rawData: des)
                                
                                let str = NSLocalizedString("Saved successfully", comment: "Saved successfully")
                                self.displayMessage(str, callback: { 
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                        else{
                            let str = NSLocalizedString("Failed to save. Please try again", comment: "Failed to save. Please try again")
                            self.displayMessage(str)
                        }
                    }
                    else if let msg = data["message"] as? String{
                        let str = NSLocalizedString(msg, comment: msg)
                        self.displayMessage(str)
                    }
                }
                else{
                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                    self.displayMessage(str)
                }
            }
        }
    }
    
    func UpdateFields() {
        
        guard let countryCode = MyProfile.ibanCountryCode else {
            return
        }
        guard let banNumber = MyProfile.ibanBanNumber else {
            return
        }
        guard let bankName = MyProfile.ibanBankName else {
            return
        }
        guard let phoneNumber = MyProfile.ibanPhoneNumber else {
            return
        }
        
        self.txtCountry.showTitleLable(isHide: false)
        self.txtCountry.text = "+"+countryCode.trim()
        
        self.txtBanNumber.showTitleLable(isHide: false)
        self.txtBanNumber.text = banNumber
        
        self.txtBankName.showTitleLable(isHide: false)
        self.txtBankName.text = bankName
        
        self.txtPhoneNumber.showTitleLable(isHide: false)
        self.txtPhoneNumber.text = phoneNumber
        
    }
    
    func validate() -> Bool {
        if isEmpty(self.txtCountry.text!) {
            let str = NSLocalizedString("Please select Country", comment: "Please select Country")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtBanNumber.text!){
            let str = NSLocalizedString("Please enter IBAN Number", comment: "Please enter IBAN Number")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtBankName.text!){
            let str = NSLocalizedString("Please enter Bank Name", comment: "Please enter Bank Name")
            self.displayMessage(str)
            return false
        }
        
        if let phoneNumber = self.txtPhoneNumber.text?.trim(), Double(phoneNumber) == nil {
            let str = NSLocalizedString("Please enter Phone Number ", comment: "Please enter Phone Number")
            self.displayMessage(str)
            return false
        }
        
        return true
    }
}
