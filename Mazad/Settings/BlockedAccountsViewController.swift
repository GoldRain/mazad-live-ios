

import UIKit

class BlockedAccountsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, BlockUserDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var userData = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Blocked Users", comment: "Blocked Users")
        
        let v = UIView()
        v.backgroundColor = .clear
        self.tableView.tableFooterView = v
        
        tabBarVC?.callBlockListApi(showWait: true) { (users) in
            if let users = users {
                self.userData.removeAll()
                self.userData = users
            }
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.count > 0 ? userData.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.userData.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = NSLocalizedString("No Users Found !!", comment: "No Users Found !!")
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BlockUserTableViewCell
        cell.lblUserName.text = self.userData[indexPath.row].username
        if let profileImage = self.userData[indexPath.row].profilePhoto {
            cell.imgUserImage.sd_setImage(with: URL(string: profileImage), placeholderImage: #imageLiteral(resourceName: "user"), options: [], completed: nil)
        }
        cell.delegate = self
        cell.userId = self.userData[indexPath.row]._id
        return cell
    }

    //MARK: BlockUserDelegate
    func didSelectData(userId: String){
        
        let alert = getAlertController(title: nil, message: NSLocalizedString("Unblock", comment: "Unblock"))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: "Yes"), style: .default, handler: { (_) in
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            tabBarVC?.callBlockApi(user_id: userId, isBlock: 0, callback: { (msg) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let message = msg {
                    
                    tabBarVC?.callBlockListApi(showWait: true, callback: { (users) in
                        if let users = users {
                            self.userData.removeAll()
                            self.userData = users
                        }
                        self.tableView.reloadData()
                    })
                    categoryVC?.getDataFromApiAndReloadTable()
                    self.displayMessage(message)
                }
            })
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: "No"), style: .destructive, handler: { (_) in
        }))
        
        appDel.topViewController?.present(alert, animated: true, completion: nil)
    }
}

protocol BlockUserDelegate {
    func didSelectData(userId: String)
}

class BlockUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserImage: UIImageView!
    
    var delegate: BlockUserDelegate!
    var userId: String!
    
    @IBAction func onUnblockClick(_ sender: Any) {
        self.delegate.didSelectData(userId: self.userId)
    }
    
}
