//
//  PlaceMazad.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 22, 2018

import Foundation


class MazadPlace : NSObject, NSCoding{

    var latitude : String!
    var longitude : String!
    var placeName : String!
    var postImages : [MazadPlacePostImage]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        latitude = dictionary["latitude"] as? String
        longitude = dictionary["longitude"] as? String
        placeName = dictionary["place_name"] as? String
        postImages = [MazadPlacePostImage]()
        if let postImagesArray = dictionary["post_images"] as? [[String:Any]]{
            for dic in postImagesArray{
                let value = MazadPlacePostImage(fromDictionary: dic)
                postImages.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if placeName != nil{
            dictionary["place_name"] = placeName
        }
        if postImages != nil{
            var dictionaryElements = [[String:Any]]()
            for postImagesElement in postImages {
                dictionaryElements.append(postImagesElement.toDictionary())
            }
            dictionary["postImages"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        latitude = aDecoder.decodeObject(forKey: "latitude") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        placeName = aDecoder.decodeObject(forKey: "place_name") as? String
        postImages = aDecoder.decodeObject(forKey: "post_images") as? [MazadPlacePostImage]
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if placeName != nil{
            aCoder.encode(placeName, forKey: "place_name")
        }
        if postImages != nil{
            aCoder.encode(postImages, forKey: "post_images")
        }
    }
}
