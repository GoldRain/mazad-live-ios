//
//  MyProfile.swift
import UIKit

fileprivate extension UserDefaults{
    
    fileprivate func set(_ value: Any?, forKey key: Key){
        self.set(value, forKey: key.rawValue)
    }
    
    fileprivate func string(forKey key: Key) -> String{
        return self.string(forKey: key.rawValue) ?? ""
    }
    
    fileprivate func nullableString(forKey key: Key) -> String?{
        return self.string(forKey: key.rawValue)
    }
    
    fileprivate func bool(forKey key: Key) -> Bool{
        return self.bool(forKey: key.rawValue)
    }
}


public enum Key: String {
    
    case kId                     = "Id"
    case kIsSeller               = "IsSeller"
    case kUserEmail              = "Email"
    case kUserName               = "Name"
    case kMobile                 = "Mobile"
    case kEmailVerification      = "EmailVerification"
    case kMobileVerification     = "MobileVerification"
    case kpassword               = "Password"
    case kCountryCode            = "CountryCode"
    case kProfilePhoto           = "ProfilePhoto"
    case kUserType               = "UserType"
    case kCountryName            = "CountryName"
    case kCountryFlagImageUrl    = "flagImageUrl"
    case kCountryId              = "countryId"
    case kProfileDesc            = "profileDescription"
    case kDescription            = "Description"
    case kFavPostId              = "FavPostId"
    case kIsAuction              = "IsAuction"
    case kIsBuyNow               = "IsBuyNow"
    case kIsWhatsappOnly         = "IsWhatsappOnly"
    case kIsWhatsappAndCall      = "IsWhatsappAndCall"
    
    case kIbanCountryCode        = "IbanCountryCode"
    case kIbanBanNumber          = "IbanBanNumber"
    case kIbanBankNumber         = "IbanBankName"
    case kIbanPhoneNumber        = "IbanPhonenumber"
    case kPlayerId               = "PlayerId"
    case kActivityStatus         = "ActivityStatus"
    case kCustomerId             = "CustomerId"
    case kLastLoginTime          = "LastLoginTime"
    
    case kShowVerifiedScreen     = "ShowVerifiedScreen"
    case kIsVerified             = "IsVerified"
    case kVerification           = "Verification"
    
    case kShowAds                = "ShowAds"
    case kIsPromoted             = "IsPromoted"
    case kAvailablePostCount     = "AvailablePostCount"
    case kAvailableLiveCount     = "AvailableLiveCount"
    case kAvailableStoryCount    = "AvailableStoryCount"
    
    case kPushNotification        = "PushNotification"
    case kEmailNotification       = "EmailNotification"
    
    case kTwitterKey              = "TwitterKey"
    case kTwitterSecret           = "TwitterSecret"
    
//    case kLastLoginUserId         = "lastLoginUserId"
//    case kisSubcribedUser         = "isSubscribedUser"
}

internal final class MyProfile {
    
    ///Private initiliser to prevent object creation
    private init(){}
    
    internal static func set(rawData: [String: Any]) {
        
        if let value = rawData["username"] as? String {
            userName = value
        }
        
        if let countryArr = rawData["countryDetails"] as? [Any]{
            
            if let countryData = countryArr.first as? [String:Any] {
                
                if let value = countryData["_id"] as? String {
                    countryId = value
                }
                if let value = countryData["countryCode"] as? String {
                    countryCode = value
                }
                if let value = countryData["countryName"] as? String {
                    countryName = value
                }
                if let value = countryData["flagImageUrl"] as? String {
                    countryFlagUrl = value
                }
            }
        }
        
        if let value = rawData["phone"] as? String {
            phone = value
        }
        
        if let value = rawData["email"] as? String {
            userEmail = value
        }
        
        if let value = rawData["password"] as? String {
            password = value
        }
        
        if let value = rawData["type"] as? String {
            userType = value
        }
        
        if let value = rawData["profilePhoto"] as? String {
            userProfilePhoto = value
        }
        
        if let value = rawData["_id"] as? String {
            userId = value
        }
        
        if let value = rawData["IsSeller"] as? Bool {
            isSeller = value
        }
        
        if let value = rawData["EmailVerification"] as? String {
            emailVerification = value
        }
        
        if let value = rawData["MobileVerification"] as? String {
            mobileVerification = value
        }
        if let value = rawData["description"] as? String {
            description = value
        }
        if let favPostArr = rawData["favPost"] as? [[String:Any]]{
            var arr = [String]()
            for favPost in favPostArr {
                if let value = favPost["post_id"] as? String {
                    arr.append(value)
                }
            }
            favPostId = arr
        }
        
        if let value = rawData["is_activity"] as? Bool{
            activityStatus = value
        }
        if let value = rawData["is_auction"] as? Bool {
            isAuction = value
        }
        if let value = rawData["is_buyNow"] as? Bool {
            isBuyNow = value
        }
        if let value = rawData["is_whatsappOnly"] as? Bool {
            isWhatsappOnly = value
        }
        if let value = rawData["is_whatsappAndCall"] as? Bool {
            isWhatsappAndCall = value
        }
        
        if let value = rawData["country_code"] as? String {
            ibanCountryCode = value
        }
        if let value = rawData["banNumber"] as? String {
            ibanBanNumber = value
        }
        if let value = rawData["bankName"] as? String {
            ibanBankName = value
        }
        if let value = rawData["phoneNumber"] as? String {
            ibanPhoneNumber = value
        }
        
        if let value = rawData["tap_customer_id"] as? String {
            tapCustomerId = value
        }
        
        if let value = rawData["last_login"] as? String {
            lastLoginTime = value
        }
        
        if let value = rawData["is_verified"] as? Bool {
            isVerified = value
        }
        if let value = rawData["show_ads"] as? Bool {
             showAds = value
        }
        if let value = rawData["is_promoted"] as? Bool {
            isPromoted = value
        }
        if let value = rawData["unlimited_post_count"] as? [String:Any] {
            AvailablePostCount = CountModel(data: value)
        }
        if let value = rawData["unlimited_story_count"] as? [String:Any] {
            AvailableStoryCount = CountModel(data: value)
        }
        if let value = rawData["unlimited_live_count"] as? [String:Any] {
            AvailableLiveCount = CountModel(data: value)
        }
        if let value = rawData["push_notification"] as? Bool {
            push_notification = value
        }
        if let value = rawData["email_and_notification"] as? Bool {
            email_and_notification = value
        }
    }
    
    //remove user data from phone user defaults
    
    static func logOutUser(){
        
//        if self.isSubscribedUser {
//            lastLoginUserId = self.userId
//        }
        
        UserDefaults.standard.set(nil, forKey: CurrentLanguageKey)
        
        twitterKey = nil
        twitterSecred = nil
        
        userId = nil
        userType = nil
        isSeller = false
        userEmail = nil
        userName = nil
        phone = nil
        emailVerification = nil
        mobileVerification = nil
        password = nil
        countryCode = nil
        userProfilePhoto = nil
        countryId = nil
        countryName = nil
        countryCode = nil
        countryFlagUrl = nil
        description = nil
        favPostId?.removeAll()
        
        messageCenter?.stop()
        
        ibanPhoneNumber = nil
        ibanCountryCode = nil
        ibanBanNumber = nil
        ibanBankName = nil
        
        tapCustomerId = nil
        lastLoginTime = nil
        activityStatus = false
        isBuyNow = false
        isAuction = false
        isWhatsappOnly = false
        isWhatsappAndCall = false
        
        showVerifiedScreen = false
        isVerified = false
        
//        AvailableLiveCount = 0
        AvailableLiveCount = nil
//        AvailablePostCount = 0
        AvailablePostCount = nil
//        AvailableStoryCount = 0
        AvailableStoryCount = nil
        
        verification = nil
        
        push_notification = false
        email_and_notification = false
        
        showAds = true
    }
    
//    internal static var lastLoginUserId: String? {
//        set{
//            let preferences = UserDefaults.standard
//            preferences.set(newValue, forKey: .kLastLoginUserId)
//            preferences.synchronize()
//        }
//
//        get{
//            return UserDefaults.standard.value(forKey: Key.kLastLoginUserId.rawValue) as? String
//        }
//    }
//
//    internal static var isSubscribedUser: Bool{
//        set{
//            let preferences = UserDefaults.standard
//            preferences.set(newValue, forKey: Key.kisSubcribedUser)
//            preferences.synchronize()
//        }
//        get{
//            if let value = UserDefaults.standard.object(forKey: Key.kisSubcribedUser.rawValue) as? Bool{
//                return value
//            }
//            return false
//        }
//    }
    
    internal static var userId: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kId)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kId.rawValue) as? String
        }
    }
    internal static var twitterKey: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kTwitterKey)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kTwitterKey.rawValue) as? String
        }
    }
    internal static var twitterSecred: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kTwitterSecret)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kTwitterSecret.rawValue) as? String
        }
    }
    
    internal static var userType: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kUserType)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kUserType.rawValue) as? String
        }
    }
    
    internal static var isSeller: Bool{
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: Key.kIsSeller)
            preferences.synchronize()
        }
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kIsSeller.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var userEmail : String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kUserEmail)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kUserEmail.rawValue) as? String
        }
    }
    
    internal static var userName: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kUserName)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kUserName.rawValue) as? String
        }
    }
    
    internal static var phone: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kMobile)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kMobile.rawValue) as? String
        }
    }
    
    internal static var password: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kpassword)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kpassword.rawValue) as? String
        }
    }
    
    internal static var emailVerification: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kEmailVerification)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kEmailVerification.rawValue) as? String
        }
    }
    
    internal static var mobileVerification: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kMobileVerification)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kMobileVerification.rawValue) as? String
        }
    }
    
    internal static var profileDescription: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kProfileDesc)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kProfileDesc.rawValue) as? String
        }
    }
    internal static var countryCode: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kCountryCode)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kCountryCode.rawValue) as? String
        }
    }
    internal static var countryName: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kCountryName)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kCountryName.rawValue) as? String
        }
    }
    internal static var countryFlagUrl: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kCountryFlagImageUrl)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kCountryFlagImageUrl.rawValue) as? String
        }
    }
    internal static var countryId: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kCountryId)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kCountryId.rawValue) as? String
        }
    }
    
    internal static var userProfilePhoto: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kProfilePhoto)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kProfilePhoto.rawValue) as? String
        }
    }
    
    internal static var description: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kDescription)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.value(forKey: Key.kDescription.rawValue) as? String
        }
    }
    
    internal static var favPostId: [String]? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kFavPostId)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.value(forKey: Key.kFavPostId.rawValue) as? [String]
        }
    }
    
    internal static var isAuction: Bool? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: Key.kIsAuction)
            preferences.synchronize()
        }
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kIsAuction.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var isBuyNow: Bool? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: Key.kIsBuyNow)
            preferences.synchronize()
        }
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kIsBuyNow.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var isWhatsappOnly: Bool? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: Key.kIsWhatsappOnly)
            preferences.synchronize()
        }
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kIsWhatsappOnly.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var isWhatsappAndCall: Bool? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: Key.kIsWhatsappAndCall)
            preferences.synchronize()
        }
        get{
            if let value = UserDefaults.standard.object(forKey: Key.kIsWhatsappAndCall.rawValue) as? Bool{
                return value
            }
            return false
        }
    }
    
    internal static var ibanCountryCode: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kIbanCountryCode)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.object(forKey: Key.kIbanCountryCode.rawValue) as? String
        }
    }
    
    
    internal static var ibanBanNumber: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kIbanBanNumber)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.object(forKey: Key.kIbanBanNumber.rawValue) as? String
        }
    }
    
    
    internal static var ibanBankName: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kIbanBankNumber)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.object(forKey: Key.kIbanBankNumber.rawValue) as? String
        }
    }
    
    
    internal static var ibanPhoneNumber: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kIbanPhoneNumber)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.object(forKey: Key.kIbanPhoneNumber.rawValue) as? String
        }
    }
    internal static var playerId: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kPlayerId)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.object(forKey: Key.kPlayerId.rawValue) as? String
        }
    }
    internal static var activityStatus: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kActivityStatus)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: Key.kActivityStatus.rawValue)
        }
    }
    
    internal static var tapCustomerId: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kCustomerId)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kCustomerId.rawValue) as? String
        }
    }
    
    internal static var lastLoginTime: String? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kLastLoginTime)
            preferences.synchronize()
        }
        
        get{
            return UserDefaults.standard.value(forKey: Key.kLastLoginTime.rawValue) as? String
        }
    }
    
//    internal static var Address: AddressModel? {
//
//        set{
//            if let value = newValue{
//                let userDefaults = UserDefaults.standard
//                userDefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: value), forKey: Key.kAddress.rawValue)
//                userDefaults.synchronize()
//            }
//            else{
//                let userDefaults = UserDefaults.standard
//                userDefaults.set(nil, forKey: Key.kAddress.rawValue)
//                userDefaults.synchronize()
//            }
//        }
//        get{
//            if let data = (UserDefaults.standard.object(forKey: Key.kAddress.rawValue) as? Data){
//                return NSKeyedUnarchiver.unarchiveObject(with: data) as? AddressModel
//            }
//            else{
//                return nil
//            }
//        }
//    }
    
    internal static var showVerifiedScreen: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kShowVerifiedScreen)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: Key.kShowVerifiedScreen.rawValue)
        }
    }
    
    internal static var isVerified: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kIsVerified)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: Key.kIsVerified.rawValue)
        }
    }
    
    internal static var showAds: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kShowAds)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: Key.kShowAds.rawValue)
        }
    }
    internal static var isPromoted: Bool {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kIsPromoted)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: Key.kIsPromoted.rawValue)
        }
    }
//    internal static var AvailablePostCount: Int? {
//        set{
//            let preferences = UserDefaults.standard
//            preferences.set(newValue, forKey: .kAvailablePostCount)
//            preferences.synchronize()
//        }
//        get{
//            return UserDefaults.standard.value(forKey: Key.kAvailablePostCount.rawValue) as? Int
//        }
//    }
    internal static var AvailablePostCount:CountModel? {
        
        set{
            if let value = newValue{
                let userDefaults = UserDefaults.standard
                userDefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: value), forKey: Key.kAvailablePostCount.rawValue)
                userDefaults.synchronize()
            }
            else{
                let userDefaults = UserDefaults.standard
                userDefaults.set(nil, forKey: Key.kAvailablePostCount.rawValue)
                userDefaults.synchronize()
            }
        }
        get{
            if let data = (UserDefaults.standard.object(forKey: Key.kAvailablePostCount.rawValue) as? Data){
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? CountModel
            }
            else{
                return nil
            }
        }
    }
//    internal static var AvailableStoryCount: Int? {
//        set{
//            let preferences = UserDefaults.standard
//            preferences.set(newValue, forKey: .kAvailableStoryCount)
//            preferences.synchronize()
//        }
//        get{
//            return UserDefaults.standard.value(forKey: Key.kAvailableStoryCount.rawValue) as? Int
//        }
//    }
    internal static var AvailableStoryCount:CountModel? {
        
        set{
            if let value = newValue{
                let userDefaults = UserDefaults.standard
                userDefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: value), forKey: Key.kAvailableStoryCount.rawValue)
                userDefaults.synchronize()
            }
            else{
                let userDefaults = UserDefaults.standard
                userDefaults.set(nil, forKey: Key.kAvailableStoryCount.rawValue)
                userDefaults.synchronize()
            }
        }
        get{
            if let data = (UserDefaults.standard.object(forKey: Key.kAvailableStoryCount.rawValue) as? Data){
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? CountModel
            }
            else{
                return nil
            }
        }
    }
//    internal static var AvailableLiveCount: Int? {
//        set{
//            let preferences = UserDefaults.standard
//            preferences.set(newValue, forKey: .kAvailableLiveCount)
//            preferences.synchronize()
//        }
//        get{
//            return UserDefaults.standard.value(forKey: Key.kAvailableLiveCount.rawValue) as? Int
//        }
//    }
    internal static var AvailableLiveCount:CountModel? {
        
        set{
            if let value = newValue{
                let userDefaults = UserDefaults.standard
                userDefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: value), forKey: Key.kAvailableLiveCount.rawValue)
                userDefaults.synchronize()
            }
            else{
                let userDefaults = UserDefaults.standard
                userDefaults.set(nil, forKey: Key.kAvailableLiveCount.rawValue)
                userDefaults.synchronize()
            }
        }
        get{
            if let data = (UserDefaults.standard.object(forKey: Key.kAvailableLiveCount.rawValue) as? Data){
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? CountModel
            }
            else{
                return nil
            }
        }
    }
    
    internal static var verification:VerificationModel? {
        
        set{
            if let value = newValue{
                let userDefaults = UserDefaults.standard
                userDefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: value), forKey: Key.kVerification.rawValue)
                userDefaults.synchronize()
            }
            else{
                let userDefaults = UserDefaults.standard
                userDefaults.set(nil, forKey: Key.kVerification.rawValue)
                userDefaults.synchronize()
            }
        }
        get{
            if let data = (UserDefaults.standard.object(forKey: Key.kVerification.rawValue) as? Data){
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? VerificationModel
            }
            else{
                return nil
            }
        }
    }
    
    internal static var push_notification: Bool? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kPushNotification)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: Key.kPushNotification.rawValue)
        }
    }
    internal static var email_and_notification: Bool? {
        set{
            let preferences = UserDefaults.standard
            preferences.set(newValue, forKey: .kEmailNotification)
            preferences.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: Key.kEmailNotification.rawValue)
        }
    }
}
