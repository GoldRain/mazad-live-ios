//
//  StoryCell.swift
//  Mazad Live
//
//  Created by MAC_2 on 17/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import SDWebImage
import DropDown
import Firebase

class StoryCell: UITableViewCell {
    
    var index:IndexPath!
    
    var story:StoryModel!
    
    @IBOutlet weak var mainContainerView: UIView!
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblStoryTime: UILabel!
    @IBOutlet weak var imgStoryImage: UIImageView!
    @IBOutlet weak var imgPlayVideo: UIImageView!
    @IBOutlet weak var imageMainVew: UIView!
    
    @IBOutlet weak var imgVerifiedIcon: UIImageView!
    
    @IBOutlet weak var btnWhatsapp: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var soldView: UIView!
    @IBOutlet weak var lblSold: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    let menu = ["Delete", "Share"]
    let menu2 = ["Report", "Share"]
    
    let menuDropDown = DropDown()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.mainContainerView.addShadow()
        self.imageMainVew.addShadow()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSoldProductNotification(not:)), name: .OnSoldProduct, object: nil)
    }
    
    @objc func onSoldProductNotification(not:Notification){
        
        if let type = not.userInfo?["type"] as? String{
            if type.lowercased() == "story" {
                if let storyId = not.userInfo?["typeId"] as? String {
                    if self.story._id == storyId {
                        self.story.is_sold = true
                    }
                }
            }
        }
    }
    
    override func prepareForReuse() {
        self.btnWhatsapp.isHidden = true
        self.btnCall.isHidden = true
    }
    
    func setupUI() {
        
        if let isVideo = story.is_video, isVideo {
            self.imgPlayVideo.isHidden = false
        }else {
            self.imgPlayVideo.isHidden = true
        }
        
        if let url = self.story.userDetails.first?.profilePhoto{
            self.btnProfileImage.sd_setImage(with: URL(string: url), for: .normal, completed: nil)
        }
        if let name = self.story.userDetails.first?.username{
            self.lblUserName.text = name
        }
        if let storyImageUrl = self.story.url{
            
            self.imgStoryImage.addActivityIndicator(style: .gray)
            
            self.imgStoryImage.sd_setImage(with: URL(string: storyImageUrl.replacingOccurrences(of: ",", with: ""))) { (img, err, cache, url) in
                
                self.imgStoryImage.removeActivityIndicator()
                
                if img == nil{
                    self.imgStoryImage.image = UIImage(named: "ic_placeholder")
                }
            }
        }
        if let date = self.story.created_at{
            self.lblStoryTime.text = String().elapsedTimeFrom(dateValue: date)
        }
        
        if story.userId == MyProfile.userId {
            self.btnWhatsapp.isHidden = true
            self.btnCall.isHidden = true
            
        }else{
            
            if let whatsappOnly = self.story.whatsapp_only, whatsappOnly {
                self.btnWhatsapp.isHidden = false
                self.btnCall.isHidden = true
            }else if let whatsappAndCall = self.story.whatsapp_and_call, whatsappAndCall {
                self.btnWhatsapp.isHidden = false
                self.btnCall.isHidden = false
            }
        }
        
        if let user = story.userDetails.first{
            if user.isVerified {
                self.imgVerifiedIcon.image = #imageLiteral(resourceName: "icon_privacy_yellow")
            }else{
                self.imgVerifiedIcon.image = #imageLiteral(resourceName: "icon_privacy_grey")
            }
        }
        
        if let isSold = story.is_sold, isSold {
            self.soldView.isHidden = false
            self.lblSold.isHidden = false
        }else{
            self.soldView.isHidden = true
            self.lblSold.isHidden = true
        }
        
        if let des = story.storyDescription, des != "" {
            self.lblDescription.text = des
        }else{
            self.lblDescription.text = "NA"
        }
    }
    
    @IBAction func onWhatsappClick(_ sender: Any) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        guard let phone = self.story.userDetails.first?.phone, let countryCode = self.story.userDetails.first?.country_code else{
            let str = NSLocalizedString("Phone number Not Found!", comment: "Phone number Not Found!")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        openWhatsApp(phoneNumber: countryCode + phone, message: "Hello")
    }
    
    @IBAction func onCallClick(_ sender: Any) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge ()
            return;
        }
        
        guard let phone = self.story.userDetails.first?.phone, let countryCode = self.story.userDetails.first?.country_code else{
            let str = NSLocalizedString("Phone number Not Found!", comment: "Phone number Not Found!")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        openCallingApp(phoneNumber:countryCode + phone)
    }
    
    @IBAction func onMenuClick(_ sender: Any) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        if let userId = self.story.userId {
            if let id = MyProfile.userId {
                if userId == id {
                    self.menuDropDown.dataSource = self.menu
                }else{
                    self.menuDropDown.dataSource = self.menu2
                }
            }
        }
        
        self.menuDropDown.anchorView = sender as! UIView
        
        self.menuDropDown.backgroundColor = .white
        self.menuDropDown.textColor = BLUECOLOR
        
        self.menuDropDown.bottomOffset = CGPoint(x: 0, y:(self.menuDropDown.anchorView?.plainView.bounds.height)!)
        self.menuDropDown.topOffset = CGPoint(x: 0, y: -(self.menuDropDown.anchorView?.plainView.bounds.height)!)
        
        self.menuDropDown.selectionAction = { (index: Int, item: String)  in
            
            if item == "Delete" {
                
                if let isSold = self.story.is_sold, isSold {
                    let msg = NSLocalizedString("Sold Story can not be Deleted!!", comment: "Sold Story can not be Deleted!!")
                    appDel.topViewController?.displayMessage(msg)
                    return;
                }
                
                let wait = appDel.topViewController?.addWaitSpinner()
                
                guard let storyId = self.story._id else{
                    let str = NSLocalizedString("Failed to Get Story Id", comment: "Failed to Get Story Id")
                    appDel.topViewController?.displayMessage(str)
                    return
                }
                JSONRequest.makeRequest(kDeleteStory, parameters: ["story_id":storyId], callback: { (data, error) in
                    
                    if let wait = wait{
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    
                    if let data = data as? [String:Any] {
                        if let err = data["error"] as? Bool, !err{
                            NotificationCenter.default.post(name: NSNotification.Name("ReloadStoryViewController"), object: nil)
                        }else{
                            let str = NSLocalizedString("Failed to Delete Story", comment: "Failed to Delete Story")
                            appDel.topViewController?.displayMessage(str)
                        }
                    }
                })
             }
            
            if item == "Report" {
                
                let userDetail = self.story.userDetails.first
                
                if let id = userDetail?._id {
                    if let userId = MyProfile.userId {
                        if id != userId {
                            
                            if let storyId = self.story._id {
                                
                                if let rvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReportPostViewController") as? ReportPostViewController {
                                    
                                    rvc.storyId = storyId
                                    
                                    rvc.modalTransitionStyle = .crossDissolve
                                    rvc.modalPresentationStyle = .overCurrentContext
                                    
                                    appDel.topViewController?.present(rvc, animated: true, completion: nil)
                                    
                                }
                            }
                        }
                    }
                }
            }
            
            if item == "Share" {
                
                guard let storyId = self.story._id  else {
                    let str = NSLocalizedString("Failed to get Story Id", comment: "Failed to get Story Id")
                    appDel.topViewController?.displayMessage(str)
                    return
                }
                
                guard let link = URL(string: "https://mazadlive.page.link?type=story&id=\(storyId)") else { return }
                let dynamicLinksDomainURIPrefix = "https://mazadlive.page.link"
                guard let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix) else{
                    print("wierd!! link builder is not created")
                    return
                }
                linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.itwhiz4u.q8mercato.mazadlive")
                linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: "com.mazadlive")
                linkBuilder.iOSParameters?.appStoreID = "1450514377"
                linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
                if let storyDetails = self.story.storyDescription{
                    linkBuilder.socialMetaTagParameters?.title = ""
                    linkBuilder.socialMetaTagParameters?.descriptionText = storyDetails
                }
                if let storyImageUrl = self.story.url, storyImageUrl != ""{
                    linkBuilder.socialMetaTagParameters?.imageURL = URL(string: storyImageUrl)
                }
                
                guard let longDynamicLink = linkBuilder.url else { return }
                print("The long URL is: \(longDynamicLink)")
                
                
                linkBuilder.shorten() { url, warnings, error in
                    
                    guard let url = url, error == nil else { return }
                    
                    print("The short URL is: \(url)")
                    
                    let textToShare = [url]
                    
                    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = appDel.topViewController!.view // so that iPads won't crash
                    
                    appDel.topViewController!.present(activityViewController, animated: true, completion: {
                        
                    })
                    
                }
                
            }
        }
        self.menuDropDown.show()
    }
}
