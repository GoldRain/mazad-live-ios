
import UIKit
import SystemConfiguration
import Foundation
import NVActivityIndicatorView

public typealias WaitView = (first: UIView, second: UIView)

enum UserDefaultsKeys : String {
    case isLoggedIn
    case userID
}

class Extensions: UIViewController, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension UserDefaults{
    
    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    func isLoggedIn()-> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    //MARK: Save User Data
    func setUserID(value: Int){
        set(value, forKey: UserDefaultsKeys.userID.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getUserID() -> Int{
        return integer(forKey: UserDefaultsKeys.userID.rawValue)
    }
    
    func removeIsLogin(){
        
        removeObject(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    func removeUserID(){
        
        removeObject(forKey: UserDefaultsKeys.userID.rawValue)
    }
    
    func resetDefaults() {
        let dictionary = dictionaryRepresentation()
        dictionary.keys.forEach { key in
            removeObject(forKey: key)
        }
    }
    
    
}


extension UIColor {
    
    
    static let buttonColor  = UIColor(red:0.57999998330000002, green:0.80000001190000003 ,blue:0.33300000429999999, alpha:1.00)
    static let loginColor   = UIColor(red:0.31000000238418579, green:0.3449999988079071 ,blue:0.34900000691413879, alpha:1.00)
    static let imageGrayColor = UIColor.lightGray//UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0)
    static let purpleColor = UIColor(red: 42/255, green: 2/255, blue: 85/255, alpha: 1.0)
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    
}


extension UITextField{
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
    
    func setPlaceholder(placeholder: String, color: UIColor){
    
        self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedStringKey.foregroundColor: color])
    
    }
    
    func isValidEmail() -> Bool{
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: self.text)
        
    }
    
    func addRightView(image:UIImage){
        
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        let imageView = UIImageView(image:image)
        
        imageView.frame = CGRect(x: 0, y: 0, width: 28, height: 28)
        imageView.frame.origin.y = 7
        
        v.addSubview(imageView)
        
        self.rightViewMode = .always
        self.rightView = v
        
    }
    
}

extension UIImage{
    
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func invertedImage(callback:@escaping ((UIImage?)->())){
        guard let cgImage = self.cgImage else {
            callback(nil)
            return
            
        }

        DispatchQueue(label:"invertImage").async {
            
            let ciImage = CoreImage.CIImage(cgImage: cgImage)
            guard let filter = CIFilter(name: "CIColorInvert") else { DispatchQueue.main.async {
                
                callback(nil)
                }
                return
            }
            filter.setDefaults()
            filter.setValue(ciImage, forKey: kCIInputImageKey)
            let context = CIContext(options: nil)
            guard let outputImage = filter.outputImage else { DispatchQueue.main.async {
                
                callback(nil)
                
                }
                return
            }
            guard let outputImageCopy = context.createCGImage(outputImage, from: outputImage.extent) else {
                DispatchQueue.main.async {
                    
                    callback(nil)
                   
                }
                return
                
            }
            
            DispatchQueue.main.async {
                
                callback(UIImage(cgImage: outputImageCopy))
                
            }
        }
        
        
    }
    
}

extension UIImageView{
    
    
    
}

extension UITableView{
    
    
    
}

extension UILabel{
    
    
    
}

extension UITextView{
    
    
    
}

extension UITableViewCell{
    
    
    
}

extension UIScrollView{
    
    public func addSubViewInScrollViewWithConstraint(view childView: UIView){
        
        childView.frame = self.bounds
        self.addSubview(childView)
        self.bringSubview(toFront: childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let right = NSLayoutConstraint(item: childView, attribute: .right , relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0)
        let bottom = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let left = NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        
        self.addConstraints([top, right, bottom, left])
    }
    
    // Scroll to a particular view
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x: 0, y: childStartPoint.y, width: 1, height: frame.height), animated: animated)
        }
    }
    
    // Scroll to top
    func scrollToTop(animated: Bool) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
    }
    
    // Scroll to bottom
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }
    
    
}

extension UIView {
    
    @IBInspectable var BorderColor: UIColor?{
        set{
            layer.borderColor = newValue?.cgColor
        }
        
        get{
            return nil
        }
    }
    
    @IBInspectable var BorderWidth: CGFloat{
        set{
            layer.borderWidth = newValue
        }
        
        get  {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var CornerRadious: CGFloat{
        set{
            layer.cornerRadius = newValue
            if newValue > 0{
                layer.masksToBounds = true
            }else{
                layer.masksToBounds = false
            }
        }
        
        get{
            return layer.cornerRadius
        }
    }
    
    
    public func addSubViewWithConstraints(view childView: UIView){
        
        childView.frame = self.bounds
        self.addSubview(childView)
        self.bringSubview(toFront: childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let right = NSLayoutConstraint(item: childView, attribute: .right , relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0)
        let bottom = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let left = NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        
        self.addConstraints([top, right, bottom, left])
    }
    
    public func addSubViewFromRightAnimated(view childView: UIView){
        
        childView.frame = self.bounds
        self.addSubview(childView)
        self.bringSubview(toFront: childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let right = NSLayoutConstraint(item: childView, attribute: .right , relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0)
        let bottom = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let left = NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        
        self.addConstraints([top, right, bottom, left])
        
        childView.frame.origin.x += self.frame.width
        
        UIView.animate(withDuration: 0.5) {
            
            childView.frame.origin.x -= self.frame.width
            
            
        }
        
        
    }
    
    public func addSubViewFromLeftAnimated(view childView: UIView){
        
        childView.frame = self.bounds
        self.addSubview(childView)
        self.bringSubview(toFront: childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let right = NSLayoutConstraint(item: childView, attribute: .right , relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0)
        let bottom = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let left = NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        
        self.addConstraints([top, right, bottom, left])
        
        childView.frame.origin.x -= self.frame.width
        
        UIView.animate(withDuration: 0.5) {
            
            childView.frame.origin.x += self.frame.width
            
        }
        
    }
    
    public func addSubViewFromBottomAnimated(view childView: UIView){
        
        childView.frame = self.bounds
        self.addSubview(childView)
        self.bringSubview(toFront: childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
//        let top = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let right = NSLayoutConstraint(item: childView, attribute: .right , relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 10.0)
        let bottom = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 10.0)
        let left = NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 10.0)
        
        self.addConstraints([right, bottom, left])
        
//        childView.frame.origin.x -= self.frame.width
        
        childView.frame.origin.y = self.frame.height + childView.frame.height
        
        UIView.animate(withDuration: 0.5) {
            
            childView.frame.origin.y = self.frame.height
        }
    }
    
    public func removeSubViewFromRightAnimated(){
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.frame.origin.x += self.frame.width
            
        }) { (true) in
            
            self.removeFromSuperview()
        }
        
        //self.removeFromSuperview()
        
    }
    
    public func removeSubViewFromLeftAnimated(){
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.frame.origin.x -= self.frame.width
            
        }) { (true) in
            
            self.removeFromSuperview()
        }
        //self.removeFromSuperview()
        
    }
    
    func addShadow(shadowColor:UIColor = UIColor.gray, size: CGSize = CGSize.zero) {
        
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = size
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 8.0
        
        
    }
    
    func addGradient() {
        
        let v = UIView()
        v.tag = 12255
        v.frame = self.bounds
        let thisLayer = CAGradientLayer()
        thisLayer.frame = self.bounds
        //thisLayer.colors = [redColor.cgColor, purpleColor.cgColor];
        //thisLayer.colors = [goldenColor, goldenDark]
        thisLayer.startPoint = CGPoint(x: 0.5, y: 0.5)
        thisLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        v.layer.insertSublayer(thisLayer, at: 0)
        v.CornerRadious = 3.0
        v.layer.masksToBounds = true
        self.addSubview(v)
        self.sendSubview(toBack: v)
        
    }
    func removeGradientLayer()
    {
        if let v = self.viewWithTag(12255){
            
            v.removeFromSuperview()
        }
        
    }
    
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        layer.add(animation, forKey: kCATransitionFade)
    }
    
    func setBackGroundImage(img:UIImage){
        
        UIGraphicsBeginImageContext(self.frame.size);
        
        img.draw(in: self.bounds)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext();
        
        self.backgroundColor = UIColor.init(patternImage: image!)
        
    }
    func drawInnerShadow(color:UIColor) {
        
        let innerShadowView = UIImageView(frame: bounds)
        innerShadowView.contentMode = .scaleToFill
        innerShadowView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(innerShadowView)
        innerShadowView.layer.masksToBounds = true
        innerShadowView.layer.borderColor = color.cgColor
        innerShadowView.layer.shadowColor = color.cgColor
        innerShadowView.layer.borderWidth = 1.0
        innerShadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
        innerShadowView.layer.shadowOpacity = 1.0
        // this is the inner shadow thickness
        innerShadowView.layer.shadowRadius = 5.0
    }
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func addWaitView(height:Int = 50, width:Int = 50){
        
        if let v = self.viewWithTag(500) as? NVActivityIndicatorView{
            self.bringSubview(toFront: v)
            v.startAnimating()
            return
        }
        
        assert(self.viewWithTag(500) == nil, "Another view having tag 500")
        
        let indicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: height, height: width), type: .circleStrokeSpin, color: BLUECOLOR)
        indicatorView.tag = 500
        
        self.addSubview(indicatorView)
        
        indicatorView.translatesAutoresizingMaskIntoConstraints = false
        
        indicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        indicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        indicatorView.widthAnchor.constraint(equalToConstant: CGFloat(width)).isActive = true
        indicatorView.heightAnchor.constraint(equalToConstant: CGFloat(height)).isActive = true
        
        self.bringSubview(toFront: indicatorView)
        
        indicatorView.startAnimating()
        
    }
    
    func removeWaitView(){
        
        if let v = self.viewWithTag(500) as? NVActivityIndicatorView{
            v.stopAnimating()
            self.viewWithTag(500)?.removeFromSuperview()
        }
    }
    
    func addActivityIndicator(style:UIActivityIndicatorViewStyle = .whiteLarge) {
        
        self.removeActivityIndicator()
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: style)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.tag = 6912
        self.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        activityIndicator.startAnimating()
    }
    
    func removeActivityIndicator() {
        if let activityIndicator = self.viewWithTag(6912) as? UIActivityIndicatorView {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
}

extension UIViewController{
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let v = touches.first?.view{
            if v == self.view{
                self.view.endEditing(true)
            }
        }
    }
    
    // MARK: Check Internet Connection
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    public func displayMessage(_ msg: String?, callback:(()->(Void))? = nil){
        
        self.displayMessage(title: nil, msg: msg, callback: callback)
        
    }
    public func displayMessage(title: String?, msg: String?, callback:(()->(Void))? = nil){
        
        if parent != nil{
            parent!.displayMessage(title: title, msg: msg, callback: callback)
            return
        }
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        
        if let title = title {
            vc.titleText = title
        }
        if let msg = msg{
            vc.alertText = msg
        }
        vc.callback = {
            callback?()
        }
        
        present(vc, animated: true, completion: nil)

//        let alertView:UIAlertController!
//
//        if UIDevice.current.userInterfaceIdiom == .pad{
//            alertView = UIAlertController(title: msg, message: title, preferredStyle: .alert)
//        }
//        else{
//            alertView = UIAlertController(title: msg, message: title, preferredStyle: .actionSheet)
//        }
//
//        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: { _ in
//            alertView.dismiss(animated: true, completion: nil)
//            callback?()
//        })
//        alertView.addAction(alertAction)
//        present(alertView, animated: true, completion: nil)
    }
    
//    public func addWaitSpinner()->WaitView{
//
//        let backView = UIView(frame: view.bounds)
//        //        let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
//        //        indicatorView.color = UIColor.red
//        //        view.addSubview(backView)
    
    public func addWaitSpinner()->WaitView {
        
        let backView = UIView(frame: view.bounds)
//        let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
//        indicatorView.color = .white
//        view.addSubview(backView)
//        view.bringSubview(toFront: backView)
        
        let indicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .circleStrokeSpin, color: BLUECOLOR)
        view.addSubview(backView)
        
        
        backView.addSubview(indicatorView)
        indicatorView.center = backView.center
        backView.bringSubview(toFront: indicatorView)
        indicatorView.startAnimating()
        backView.alpha = 0.0
        backView.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
        
        UIView.animate(withDuration: 0.25, animations: {
            backView.alpha = 1.0
        })
        
        return (backView, indicatorView)
    }
    
    public func removeWaitSpinner(waitView: WaitView){
        UIView.animate(withDuration: 0.25, animations: {
            waitView.first.alpha = 0.0
        }, completion: { _ in
            waitView.second.removeFromSuperview()
            waitView.first.removeFromSuperview()
        })
    }
    

    func setNavigationBar(){
        
        self.navigationController?.isNavigationBarHidden = false
        let nav = self.navigationController?.navigationBar
        nav?.barTintColor = UIColor(red: 79.0/255.0, green: 88.0/255.0, blue: 89.0/255.0, alpha: 1.0)
        nav?.barStyle = UIBarStyle.black
        //nav?.backgroundColor = UIColor(named: "LoginColor")
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
}

extension UINavigationController {
    
    ///Get previous view controller of the navigation stack
    func previousViewController() -> UIViewController?{
        
        let lenght = self.viewControllers.count
        
        let previousViewController: UIViewController? = lenght >= 2 ? self.viewControllers[lenght-2] : nil
        
        return previousViewController
    }
    
    func popViewController(completion:@escaping (()->()), animation:Bool){
        
        CATransaction.begin()
        
        CATransaction.setCompletionBlock({
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                completion()
            })
        })//**/*//
        
        self.popViewController(animated: animation)
        
        CATransaction.commit()
    }
    
}
extension UIButton{
    @IBInspectable var setContentMode:Bool{
        
        get{
            return self.imageView?.contentMode == .scaleAspectFit
        }
        set(value){
            if value == true{
                self.imageView?.contentMode = .scaleAspectFit
            }
        }
        
    }
}
