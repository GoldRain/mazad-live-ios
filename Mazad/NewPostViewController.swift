

import UIKit
import YPImagePicker
import SDWebImage
import AWSS3
import AWSCore
import GooglePlacePicker
import GoogleMaps
import GooglePlaces

class NewPostViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UITextFieldDelegate {
    
    //var countries = [Country?]()
    
    var countryList = [CountryModel]()
    
//    var selectedPhoto:YPMediaPhoto!
    
//    var selectedPhoto = [YPMediaPhoto]()
    
    var selectedPhotos = [UIImage]()
    var selectedVideosUrl = [URL]()
    
    let MAXIMUM_CHARACTER = 200
    
    @IBOutlet weak var imgUserImage: UIImageView!
    
    @IBOutlet weak var listMainView: UIView!
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var locationMain: UIView!
    @IBOutlet weak var tagsView: UIView!
    
    @IBOutlet weak var txtViewDetails: UITextView!
    
    @IBOutlet weak var lblTags: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnWhatsappOnly: UIButton!
    @IBOutlet weak var btnWhatsappCall: UIButton!
    @IBOutlet weak var btnCheckBoxStatus: UIButton!
    @IBOutlet weak var imgCheckImage: UIImageView!
    
    
    @IBOutlet weak var selectLanguageView: UIView!
    @IBOutlet weak var imgLang: UIImageView!
    @IBOutlet weak var lblLang: UILabel!
    
    @IBOutlet weak var imageMainView: UIView!
    @IBOutlet weak var selectedImg1: UIImageView!
    @IBOutlet weak var selectedImg2: UIImageView!
    @IBOutlet weak var selectedImg3: UIImageView!
    @IBOutlet weak var lblForImage: UILabel!
    
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var btnInstagram: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var btnAuction: UIButton!
    @IBOutlet weak var btnBuyNow: UIButton!
    @IBOutlet weak var btnCreditCard: UIButton!
    @IBOutlet weak var btnCash: UIButton!
    
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var buyNowView: UIView!
    @IBOutlet weak var auctionView: UIView!
    @IBOutlet weak var whatsappOnlyView: UIView!
    @IBOutlet weak var whatsappAndCallView: UIView!
    @IBOutlet weak var creditCardView: UIView!
    @IBOutlet weak var cashView: UIView!
    @IBOutlet weak var btnCreateNewPost: UIButton!
    
    @IBOutlet weak var lblUserDescription: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtProductSerial: UITextField!
    @IBOutlet weak var productSerialView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("New Post", comment: "New Post")

        locationMain.addShadow()
        tagsView.addShadow()
        amountView.addShadow()
        buyNowView.addShadow()
        auctionView.addShadow()
        whatsappAndCallView.addShadow()
        creditCardView.addShadow()
        cashView.addShadow()
        btnCreateNewPost.addShadow()
        listMainView.addShadow()
        txtViewDetails.addShadow()
        btnCreateNewPost.addShadow()
        imageMainView.addShadow()
        whatsappOnlyView.addShadow()
        whatsappAndCallView.addShadow()
        
        self.updateImagesView()
        
        self.tagsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTagsViewClick)))
        self.selectLanguageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onSelectLanguageClick)))
        self.locationMain.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onLocationClick)))
        
        self.txtViewDetails.delegate = self
        self.txtProductSerial.delegate = self
        self.txtAmount.delegate = self
        
        self.productSerialView.addShadow()
        
        let attStr = NSAttributedString(string: "Product Serial", attributes: [NSAttributedStringKey.foregroundColor : UIColor.lightGray])
        self.txtProductSerial.attributedPlaceholder = attStr
    }
    
    fileprivate func updateImagesView() {
        
        if self.selectedPhotos.count == 1 {
            self.lblForImage.isHidden = true
            self.selectedImg2.isHidden = true
            self.selectedImg3.isHidden = true
            //self.selectedImg1.image = self.selectedPhoto[0].image
            self.selectedImg1.image = self.selectedPhotos[0]
            
        }else if self.selectedPhotos.count == 2 {
            self.lblForImage.isHidden = true
            self.selectedImg3.isHidden = true
            self.selectedImg2.image = self.selectedPhotos[0]//.image
            self.selectedImg1.image = self.selectedPhotos[1]//.image
            
        }else if self.selectedPhotos.count == 3 {
            self.lblForImage.isHidden = true
            self.selectedImg3.image = self.selectedPhotos[0]//.image
            self.selectedImg2.image = self.selectedPhotos[1]//.image
            self.selectedImg1.image = self.selectedPhotos[2]//.image
            
        }else if self.selectedPhotos.count == 4 {
            self.selectedImg1.image = self.selectedPhotos[0]//.image
            self.selectedImg2.image = self.selectedPhotos[1]//.image
            self.selectedImg3.image = self.selectedPhotos[2]//.image
            self.lblForImage.text = "+1 more..."
            
        }else if self.selectedPhotos.count == 5 {
            
            self.selectedImg1.image = self.selectedPhotos[0]//.image
            self.selectedImg2.image = self.selectedPhotos[1]//.image
            self.selectedImg3.image = self.selectedPhotos[2]//.image
            self.lblForImage.text = "+2 more..."
        } else {
            self.selectedPhotos.removeAll()
            self.imageMainView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
        if let userImage = MyProfile.userProfilePhoto {
            self.imgUserImage.sd_setImage(with: URL(string: userImage), completed: nil)
        }
        
        if let userName = MyProfile.userName {
            self.lblUserName.text = userName
        }else{
            self.lblUserName.text = ""
        }
        
        if let description = MyProfile.description {
            self.lblUserDescription.text = description
        }else {
            self.lblUserDescription.text = ""
        }
    
        NotificationCenter.default.removeObserver(self, name: .imageDeleted, object: nil)
        
        if selectedPhotos.count == 0 {
            self.displayMessage("Post is Cancelled") { () -> (Void) in
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    let ROWHEIGHT:CGFloat = 30
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let height = self.listMainView.frame.size.height
        
        let newHeight = CGFloat(self.countryList.count) * ROWHEIGHT
        
        if newHeight <= height {
            tableView.isScrollEnabled = false
            UIView.animate(withDuration: 0.25) {
                self.tableViewHeight.constant = newHeight
            }
        }
        else{
            tableView.isScrollEnabled = true
        }
        
        return countryList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROWHEIGHT
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! countrySelectCell
        cell.selectionStyle = .none
        let data = self.countryList[indexPath.row]
        
        let imageUrl = data.flagImageUrl!
        var completeUrl =  "\(apiBaseUrl)\(imageUrl)"
        completeUrl = completeUrl.replacingOccurrences(of: " ", with: "%20")
        
        if let imgCountry = cell.contentView.viewWithTag(1) as? UIImageView{
            if let lblName = cell.contentView.viewWithTag(2) as? UILabel{
                
                imgCountry.image = nil
                imgCountry.sd_setImage(with: URL(string: completeUrl), completed: nil)
                lblName.text = data.countryName
            }
        }

        if data.countryName == MyProfile.countryName {
            cell.btnSelectCountry.setImage(#imageLiteral(resourceName: "onRadioButton"), for: .normal)
            self.selectedCountry.append(data)
        }
        return cell
    }
    
    var selectedCountry = [CountryModel]()
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! countrySelectCell
        
        if cell.btnSelectCountry.currentImage == UIImage(named: "onRadioButton") {
            
            cell.btnSelectCountry.setImage(#imageLiteral(resourceName: "offRadioButton"), for: .normal)
            self.addCountry(country: countryList[indexPath.row])
        }else {
            cell.btnSelectCountry.setImage(#imageLiteral(resourceName: "onRadioButton"), for: .normal)
            self.addCountry(country: countryList[indexPath.row])
        }
    }
    
    func addCountry(country: CountryModel) {
        
        if let index = self.selectedCountry.firstIndex(where: { (t) -> Bool in
            return t.countryName == country.countryName
        }){
            self.selectedCountry.remove(at: index)
        }
        else {
            self.selectedCountry.append(country)
        }
    }
    
    @IBAction func onSliderValueChange(_ sender: UISlider){
//        let mainTableViewOffSet = self.tableView.contentSize.height - self.tableView.frame.height + self.tableView.contentInset.bottom
//
//        let valueY = mainTableViewOffSet*CGFloat(sender.value/100)
//        self.tableView.scrollRectToVisible(CGRect(origin: CGPoint(x: 0, y: valueY), size: self.tableView.bounds.size), animated: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == tableView else { return }
//        let mainTableViewOffSet = self.tableView.contentSize.height - self.tableView.frame.height + self.tableView.contentInset.bottom
//
//        let value = self.tableView.contentOffset.y/(mainTableViewOffSet/100)
//        self.sliderBar.value = Float(value)
    }
    
    lazy var sliderBar: CustomSlider = {
        let slider = CustomSlider()
        slider.addTarget(self, action: #selector(onSliderValueChange(_:)), for: .valueChanged)
        return slider
    }()
    
    private func setSliderBar(){
        
        self.listMainView.addSubview(sliderBar)
        self.listMainView.bringSubview(toFront: sliderBar)
        sliderBar.trailingAnchor.constraint(equalTo: listMainView.trailingAnchor, constant: self.listMainView.frame.width*0.19).isActive = true
        sliderBar.centerYAnchor.constraint(equalTo: listMainView.centerYAnchor, constant: 0.0).isActive = true
        let height = NSLayoutConstraint(item: sliderBar, attribute: .width, relatedBy: .equal, toItem: listMainView, attribute: .height, multiplier: 0.8, constant: 0.0)
        NSLayoutConstraint.activate([height])
        sliderBar.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
    
    }
    
    var isInstagram:Bool = false
    var isTwitter:Bool = false
    
    var paymentType = ["cc"]
    
    @IBAction func onSwitchBtnClick(_ sender: UIButton) {
        
        if sender.currentImage == #imageLiteral(resourceName: "icon_switch_off"){
            sender.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            
            if sender == self.btnInstagram {
                self.isInstagram = true
            }else if sender == self.btnTwitter {
                self.isTwitter = true
            }else if sender == self.btnCreditCard {
                self.paymentType.append("cc")
            }else if sender == self.btnCash {
                self.paymentType.append("cash")
            }
        }else{
            sender.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            
            if sender == self.btnInstagram {
                self.isInstagram = false
            }else if sender == self.btnTwitter {
                self.isTwitter = false
            }else if sender == self.btnCreditCard {
                if let index = self.paymentType.firstIndex(of: "cc") {
                    self.paymentType.remove(at: index)
                }
            }else if sender == self.btnCash {
                if let index = self.paymentType.firstIndex(of: "cash") {
                    self.paymentType.remove(at: index)
                }
            }
        }
    }
    
    var isWhatsappOnly:Bool = false
    var isWhatsappAndCall:Bool = true
    
    @IBAction func onWhatsappOnlyClick(_ sender: Any) {
        
        if self.btnWhatsappOnly.currentImage == UIImage(named: "icon_switch_off") {
            self.btnWhatsappOnly.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            self.btnWhatsappCall.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            self.isWhatsappOnly = true
            self.isWhatsappAndCall = false
        }else {
            self.btnWhatsappOnly.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            self.btnWhatsappCall.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            self.isWhatsappOnly = false
            self.isWhatsappAndCall = true
        }
    }
    
    var isAuction:Bool = true
    var isBuyNow:Bool = false
    
    
    @IBAction func onBuyNowClick(_ sender: Any) {
        
        if self.btnBuyNow.currentImage ==  UIImage(named: "icon_switch_off")  {
            self.btnBuyNow.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            self.isBuyNow = true
            //self.btnAuction.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            //self.isAuction = false
            
        } else {
            self.btnBuyNow.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            self.isBuyNow = false
        }
    }
    @IBAction func onAuctionClick(_ sender: Any) {
        
        if self.btnAuction.currentImage == UIImage(named: "icon_switch_off") {
            self.btnAuction.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            self.isAuction = true
            //self.btnBuyNow.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            //self.isBuyNow = false
        }else {
            self.btnAuction.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            self.isAuction = false
        }
        
    }
    
    var isBoxChecked = false
    
    @IBAction func onCheckBoxClick(_ sender: UIButton)  {
        
        if self.imgCheckImage.image == #imageLiteral(resourceName: "tick.png") {
            self.imgCheckImage.image = nil
            self.isBoxChecked = false
        }else{
            self.imgCheckImage.image = #imageLiteral(resourceName: "tick.png")
            self.isBoxChecked = true
        }
    }
    
    var tags = [TagModel]()
    var selectedTags = [TagModel]()
    
    @objc func onTagsViewClick() {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagSelectViewController") as? TagSelectViewController {
            
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            
            if self.tags.count != 0 {
                vc.tagList = self.tags
                vc.previousController = self
                vc.selectedTags = self.selectedTags
                vc.tempTags = self.selectedTags
                
                self.present(vc, animated: true, completion: nil)
            }else {
                tabBarVC!.getTagList { (tags) in
                    if tags.count > 0 {
                        self.tags = tags
                        vc.tagList = self.tags
                        vc.previousController = self
                        vc.selectedTags = self.selectedTags
                        vc.tempTags = self.selectedTags
                        
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    // this function is used in tagSelectViewController
    func addTag(tag:TagModel) {
        
        if let index = self.selectedTags.firstIndex(where: { (t) -> Bool in
            return t._id == tag._id
        }){
            self.selectedTags.remove(at: index)
        }
        else {
            self.selectedTags.append(tag)
        }
    }
    
    @objc func onSelectLanguageClick() {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLanguageViewController") as? SelectLanguageViewController {
            
            vc.callback = {  lang in
                
                if lang == "ENGLISH" {
                    self.lblLang.text = "ENGLISH"
                    self.imgLang.image = #imageLiteral(resourceName: "icon_6")
                }else{
                    self.lblLang.text = "ARABIC"
                    self.imgLang.image = UIImage(named: "icon_5")
                }
            }
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onSelectedImageClick(_ sender: Any) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleImageDelete(not:)), name: .imageDeleted, object: nil)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectedImageViewerViewController") as? SelectedImageViewerViewController {
            
            vc.selectedPhoto = self.selectedPhotos
            vc.selectedVideoUrl = self.selectedVideosUrl
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func handleImageDelete(not:Notification) {
        
        if let indexPath = not.userInfo?["indexPath"] as? IndexPath{
            
            self.selectedPhotos.remove(at: indexPath.row)
            self.updateImagesView()
            if self.selectedVideosUrl.count > 0 {
                self.selectedVideosUrl.remove(at: indexPath.row)
            }
        }
    }
    
    var location:(String,String,String,String,String)?
    var Address = ""
    
    @objc func onLocationClick() {
        
        if self.location != nil {
            self.location = nil
            self.lblLocation.font = UIFont.systemFont(ofSize: 13.0)
        }
        else{
            
            let config = GMSPlacePickerConfig(viewport: nil)
            
            let placePicker = GMSPlacePickerViewController(config: config)
            
            
            
            placePicker.delegate = self
            
            present(placePicker, animated: true, completion: nil)
           
        }
    }
    
    // MARK:- TextView Delegate
    @IBOutlet weak var lblTextViewCount:UILabel!
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Details about the products..." {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        
        guard let stringRange = Range(range, in: currentText) else {
            return false
        }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return updatedText.count <= MAXIMUM_CHARACTER
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.lblTextViewCount.text = "\(textView.text.count)" + "/" + "\(MAXIMUM_CHARACTER)"
        
        if textView.text.count < MAXIMUM_CHARACTER {
            self.lblTextViewCount.textColor = .black
        }else{
            self.lblTextViewCount.textColor = .red
        }
    }
    // MARK:- TextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if self.txtProductSerial.isEditing {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 30
        }
        if self.txtAmount.isEditing {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 10
        }
        return false
    }
    
    //MARK:- onCreateNewPostClick
    
    var randomName:String!
    
    @IBAction func onCreateNewPostClick(_ sender: Any) {
        
        self.view.endEditing(true)
        
        var tags = ""
        var countries = ""
        let paymentType = self.paymentType.joined(separator: ",")
        var isVideo:Bool?
        
        if selectedVideosUrl.count > 0 {
            isVideo = true
        }else {
            isVideo = false
        }
        
        for tag in self.selectedTags {
            if let id = tag._id{
                if tags == ""{
                    tags = id
                }
                else{
                    tags = tags + "," + id
                }
            }
        }
        
        for country in self.selectedCountry {
            if let id = country._id {
                if countries == "" {
                    countries = id
                }else {
                    countries = countries + "," + id
                }
            }
        }
        
        if self.validate() {
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            var images = [UIImage]()
            for item in self.selectedPhotos{
                images.append(item)
            }
            
            var videosData = [Data]()
            
            for url in selectedVideosUrl {
                do  {
                    let data = try Data(contentsOf: url)
                    videosData.append(data)
                }catch {
                    print("error while uploading video \(error)")
                }
            }
            
            self.randomName = randomString(length: 20)
            
            uploadImagesToAws(images, folderName: appDel.postImagesFolder!, randomName: self.randomName) { (urls) in

                if let urls = urls {
                    
                    if videosData.count > 0 {
                        
                        uploadVideosToAws(videosData, folderName: appDel.postImagesFolder! , randomName: self.randomName, callback: {  (videoUrls) in
 
                            if let _ = videoUrls {
                                
                                DispatchQueue.main.async {
                                    self.callCreatePostApi(urls: urls, isVideo: isVideo!, countries: countries, paymentType: paymentType, tags:tags)
                                    if let wait = wait {
                                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                                    }
                                }
                            }else {
                                mainQueue.async {
                                    let str = NSLocalizedString("Failed to create post", comment: "Failed to create post")
                                    self.displayMessage(str)
                                    if let wait = wait {
                                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                                    }
                                }
                            }
                        })
                    }else {
                        DispatchQueue.main.async {
                            self.callCreatePostApi(urls: urls, isVideo: isVideo!, countries: countries, paymentType :paymentType, tags:tags)
                            if let wait = wait {
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                        }
                    }
                }else {
                    mainQueue.async {
                        let str = NSLocalizedString("Failed to create post", comment: "Failed to create post")
                        self.displayMessage(str)
                        if let wait = wait {
                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func callCreatePostApi(urls:[String],isVideo:Bool,countries:String, paymentType:String, tags:String) {
        
        var newAmount:String!
        
        if let amount = self.txtAmount.text, let doubleAmount = Double(amount) {
            newAmount = String(format: "%.2f", doubleAmount)
        }else{
            newAmount = self.txtAmount.text
        }
        
        let params = [
            "version": VERSION,
            "description" : self.txtViewDetails.text.trim() ,
            "latitude" : "\(self.location?.0 ?? "")" ,
            "longitude" : "\(self.location?.1 ?? "")" ,
            "place_name" : self.location?.2 ?? "",
            "post_user_id" : MyProfile.userId! ,
            "is_instagram": "\(self.isInstagram)" ,
            "is_twitter":  "\(self.isTwitter)" ,
            "amount" : newAmount!,
            "is_auction" : "\(self.isAuction)" ,
            "buy_now" : "\(self.isBuyNow)" ,
            "whatsapp_only" : "\(self.isWhatsappOnly)" ,
            "whatsapp_and_call" : "\(self.isWhatsappAndCall)" ,
            "payment_type" : paymentType,
            "language" : self.lblLang.text!,
            "only_user_from_selected_country" : "\(self.isBoxChecked)" ,
            "is_deleted" : "false",
            "status" : "Available",
            "is_sold" : "\(false)" ,
            "is_story" : "\(false)" ,
            "image_url" :  urls.joined(separator: ","),
            "is_video" : "\(isVideo)" ,
            "country" : countries,
            "tag" : tags,
            "post_id" : "",
            "bid_user_id" :"",
            "bid_amount": "",
            "bid_created_at" : "",
            "product_serial":self.txtProductSerial.text!
            ]
        
        JSONRequest.makeRequest(kCreatePost, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [Any] {
                        
                        if let _ = des.first as? [String:Any] {
                            
                            self.navigationController?.popViewControllerWithHandler {
                        
                                if self.isInstagram || self.isTwitter{
                                    if let vc = UIStoryboard(name: "ShareSB", bundle: nil).instantiateViewController(withIdentifier: "SharePostViewController") as? SharePostViewController{
                                        
                                        if self.isInstagram && self.isTwitter{
                                            vc.shareType = .all
                                        }
                                        else if self.isInstagram{
                                            vc.shareType = .instagram
                                        }
                                        else if self.isTwitter{
                                            vc.shareType = .twitter
                                        }
                                        
                                        vc.shareImage = self.selectedPhotos.first!
                                        vc.shareDescription = self.txtViewDetails.text!
                                        
                                        let nvc = UINavigationController(rootViewController: vc)
                                        appDel.topViewController?.present(nvc, animated: true, completion: {
                                            categoryVC?.getDataFromApiAndReloadTable()
                                            NotificationCenter.default.post(name: .ReloadProfileViewControllerAfterPostUpload, object: nil)
                                        })
                                    }
                                }
                                else{
                                    categoryVC?.getDataFromApiAndReloadTable()
                                    NotificationCenter.default.post(name: .ReloadProfileViewControllerAfterPostUpload, object: nil)
                                }
                                //this is sample change
                            }
                        }
                    }
                    else{
                        let str = NSLocalizedString("Failed to create post", comment: "Failed to create post")
                        self.displayMessage(str)
                    }
                }
                else if let msg = data["message"] as? String{
                    let str = NSLocalizedString(msg, comment: msg)
                    self.displayMessage(str)
                }
            }
            else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                self.displayMessage(str)
            }
        }
    }
    
    func validate() -> Bool {
        
        if isEmpty(self.txtProductSerial.text!){
            let str = NSLocalizedString("Please enter Product Serial", comment: "Please enter Product Serial")
            self.displayMessage(str)
            return false
        }
        if self.txtViewDetails.text == "Details about the products..." || self.txtViewDetails.text == "" {
            let str = NSLocalizedString("Please enter Details about the Product", comment: "Please enter Details about the Product")
            self.displayMessage(str)
            return false
        }
        if self.selectedTags.count <= 0{
            let str = NSLocalizedString("Please Select Tags", comment: "Please Select Tags")
            self.displayMessage(str)
            return false
        }
//        if isEmpty(self.location?.0) && isEmpty(self.location?.1) && isEmpty(self.location?.2) && isEmpty(self.location?.3){
//            let str = NSLocalizedString("Please Select Location", comment: "Please Select Location")
//            self.displayMessage(str)
//            return false
//        }
        if let amount = self.txtAmount.text?.trim(), let doubleAmount = amount.doubleValue, doubleAmount > 0.0{
            if doubleAmount > AMOUNT {
                let str = NSLocalizedString("Maximum Amount can not be more than 1000", comment: "Maximum Amount can not be more than 1000")
                self.displayMessage(str)
                return false
            }
        }else{
            let str = NSLocalizedString("Please enter valid Amount", comment: "Please enter valid Amount")
            self.displayMessage(str)
            return false
        }
        if self.btnCreditCard.currentImage == UIImage(named: "icon_switch_off") && self.btnCash.currentImage == UIImage(named: "icon_switch_off") {
            let str = NSLocalizedString("Please Select Credit Card or Cash Payment", comment: "Please Select Credit Card or Cash Payment")
            self.displayMessage(str)
            return false
        }
        if self.btnAuction.currentImage == UIImage(named: "icon_switch_off") && self.btnBuyNow.currentImage == UIImage(named: "icon_switch_off") {
            let str = NSLocalizedString("Please Select either Auction or BuyNow", comment: "Please Select either Auction or BuyNow")
            self.displayMessage(str)
            return false
        }
        if self.selectedCountry.count == 0 {
            let str = NSLocalizedString("Please Select Country", comment: "Please Select Country")
            self.displayMessage(str)
            return false
        }
        return true
    }
}
class countrySelectCell:UITableViewCell {
    
    @IBOutlet weak var btnSelectCountry: UIButton!
    
    override func prepareForReuse() {
        self.btnSelectCountry.setImage(#imageLiteral(resourceName: "offRadioButton.png"), for: .normal)
    }
}

extension NewPostViewController:GMSPlacePickerViewControllerDelegate{
    
    // MARK:- Place Picker Delegate
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        
        //place.coordinate
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        
        let latt = place.coordinate.latitude
        let long = place.coordinate.longitude
        let city = place.name
        let address = place.formattedAddress ?? ""
        let country = (place.addressComponents?.first(where: { (component) -> Bool in
            return component.type == "administrativeLevel1"
        })?.name) ?? ""
        
        self.Address = address
        self.location = ("\(latt)", "\(long)", city ,address, country)
        
        self.lblLocation.font = UIFont.boldSystemFont(ofSize: 13)
        
        viewController.dismiss(animated: true, completion: {
            
        })
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
    }
}
