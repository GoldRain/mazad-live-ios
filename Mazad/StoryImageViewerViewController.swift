

import UIKit
import SDWebImage
import Pages
import AVKit
import AVFoundation

class StoryImageViewerViewController: UIViewController {
    
    weak var storiesVC:StoryViewController?
    
    @IBOutlet weak var storyProgressView: UIProgressView!
    
    var player: AVPlayer?
    var playerLayer:AVPlayerLayer?
    var timer:Timer?
    var duration:Float?
    
    weak var pages:PagesController!
    var hasNext = false
    
    var story:StoryModel!
    
    var keyboardHeight = 0.0
    
    var counter:Float = 0
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgStory: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var btnAddBid: UIButton!
    
    @IBOutlet weak var btnWhatsapp: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    var frame:CGRect!
    
    //MARK:- Class View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.frame = self.imgStory.frame
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.storyProgressView.progress = 0.0
        self.counter = 0.0
        
        self.setUpData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.addGestures()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        
        self.setupDataForVideoStory()
    }
    
    func addGestures() {
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.onCloseClick(_:)))
        swipeGesture.direction = .down
        self.view.addGestureRecognizer(swipeGesture)
        
        let logPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(_:)))
        logPressGesture.minimumPressDuration = 0.2
        self.view.addGestureRecognizer(logPressGesture)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.resetAllBeforeDismiss()
    }
    
    //MARK:-
    @objc func handleLongPress(_ sender: UILongPressGestureRecognizer){
        
        switch sender.state {
            
        case .began, .changed:
            
            self.stopTimerOrPauseVideo()
            
        case .ended:
            
            self.startTimerOrPlayVideo()
            
        default:
            break
        }
    }
    
    @objc func playerItemDidReachEnd() {
        
        if self.hasNext{
            self.player?.pause()
            self.player = nil
            self.pages.moveForward()
        }
    }
    
    func setUpData() {
        
        // setting up add bid and buy now btn
        if MyProfile.userId == story.userId {
            self.btnAddBid.fd_collapsed = true
            self.btnAddBid.isUserInteractionEnabled = false
        }else {
            if let isSold = self.story.is_sold {
                self.btnAddBid.fd_collapsed = isSold
            }
            if let value = self.story.is_buyNow, value{
                self.btnAddBid.setTitle("Buy Now", for: .normal)
            }
            self.btnAddBid.isUserInteractionEnabled = true
        }
        
        // setting up name, profile image, story image, price, product description
        if let url = self.story.userDetails.first?.profilePhoto{
            self.userImage.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "icon_profile_grey"), options: [], completed: nil)
        }
        if let name = self.story.userDetails.first?.username{
            self.lblUserName.text = name
        }else{
            self.lblUserName.text = ""
        }
        if let storyImageUrl = self.story.url{
            self.view.addActivityIndicator()
            self.imgStory.sd_setImage(with: URL(string: storyImageUrl.replacingOccurrences(of: ",", with: "")), placeholderImage: #imageLiteral(resourceName: "ic_placeholder.png"), options: []) { (_, _, _, _) in
                self.view.removeActivityIndicator()
            }
        }
        if let date = self.story.created_at{
            self.lblTime.text = String().elapsedTimeFrom(dateValue: date)
        }else{
            self.lblTime.text = ""
        }
        if let price = self.story.buyNowPrice?.trim(), price != "", let doublePrice = Double(price) {
            let newPrice = String(format: "%.2f", doublePrice)
            self.lblPrice.text = "Price KD \(newPrice)"
        }
        else{
            self.lblPrice.text = "Price not available"
        }
        if let des = self.story.storyDescription, des != "" {
            self.lblDescription.text = des
        }else{
            self.lblDescription.text = "NA"
        }
        
        //Setting up whatsapp and call btn
        if story.userId == MyProfile.userId {
            self.btnWhatsapp.isHidden = true
            self.btnCall.isHidden = true
        }else{
            if let whatsappOnly = self.story.whatsapp_only, whatsappOnly {
                self.btnWhatsapp.isHidden = false
                self.btnCall.isHidden = true
            }else if let whatsappAndCall = self.story.whatsapp_and_call, whatsappAndCall {
                self.btnWhatsapp.isHidden = false
                self.btnCall.isHidden = false
            }
        }
    }
    
    func setupDataForVideoStory() {
        
        self.view.addActivityIndicator()
        
        // if story is video type
        if self.story.is_video ?? false,  let url = self.story.url?.replacingOccurrences(of: ".jpg", with: ".mp4"){
            
            CacheManager.shared.getFileWith(stringUrl: url) { [weak self] (result) in
                
                switch result {
                    
                case .success(let url):
                    
                        guard self != nil else{
                            return
                        }
                        
                        self?.player = AVPlayer(url: url)
                        self?.playerLayer = AVPlayerLayer(player: self!.player)
                        self?.playerLayer!.frame = self!.imgStory.bounds
                        self?.imgStory.layer.addSublayer(self!.playerLayer!)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now()+0.01, execute: {
                            guard self != nil else{
                                return
                            }
                            guard self?.playerLayer != nil else {
                                return
                            }
                            self?.playerLayer!.frame = self!.imgStory.bounds
                        })
                        
                        self?.playerLayer!.videoGravity = AVLayerVideoGravity.resizeAspect
                        
                        self?.player?.play()
                        
                        let interval = CMTime(value: 1, timescale: 2)
                        
                        self?.player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
                            
                            let seconds = CMTimeGetSeconds(progressTime)
                            if seconds > 0 {
                                self?.view.removeActivityIndicator()
                            }
                            if let duration = self?.player?.currentItem?.duration {
                                let durationSecond = CMTimeGetSeconds(duration)
                                let progressDuration = Float (seconds / durationSecond)
                                
                                UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseInOut, animations: {
                                    self?.storyProgressView.progress = progressDuration
                                })
                                
                                if progressDuration >= 1.0 {
                                    if self?.hasNext ?? false {
                                        self?.pages.moveForward()
                                    }else {
                                        self?.resetAllBeforeDismiss()
                                        self?.dismiss(animated: true, completion: nil)
                                    }
                                }
                            }
                        })
                    
                case .failure(let error):
                    print(error)
                }
                
            }
            
        }else { // if story is Image, start timer
            self.view.removeActivityIndicator()
            startTimerOrPlayVideo()
        }
    }
    
    @objc private func storyImageTimerController(_ sender: Timer){
        
        counter += 0.1
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseInOut, animations: { [unowned self] in
            self.storyProgressView.progress = (self.counter/5)
        }, completion: nil)
        
        if counter >= Float(5.0){
            
            self.stopTimerOrPauseVideo()
            
            if self.hasNext {
                self.pages.moveForward()
            }else {
                self.resetAllBeforeDismiss()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    deinit {
        print("deinit")
        self.stopTimerOrPauseVideo()
    }
    
    @IBAction func onCloseClick(_ sender: UIButton) {
        
        self.resetAllBeforeDismiss()
        self.storiesVC?.pages = nil
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func onBidClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        self.view.endEditing(true)
        
        self.stopTimerOrPauseVideo()
        
        if let value = self.story.is_buyNow, value { // if story is available for buy now
            
            self.resetAllBeforeDismiss()
            
            self.dismiss(animated: true) {
                
                if let top = appDel.topViewController {
                    
                    if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateOrderViewController") as? CreateOrderViewController {
                        
                        if let amount = self.story.buyNowPrice, let storyId = self.story._id, let sellerId = self.story.userId, let paymentMode = self.story.payment_type?.components(separatedBy: ",") {
                            vc.amount = amount
                            vc.type_id = storyId
                            vc.sellerId = sellerId
                            vc.type = "story"
                            vc.paymentType = paymentMode
                        }
                        top.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
        else { // if story is available for auction
            NotificationCenter.default.addObserver(self, selector: #selector(handleTimerNotification), name: NSNotification.Name(rawValue: "startTimerAfterDismiss"), object: nil)
            
            if let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "CustomAlertMakeBidViewController") as? CustomAlertMakeBidViewController{
                
//                if let amount = story.buyNowPrice {
//                    vc.minPrice = Double(amount)
//                }
                vc.minPrice = 1.0
                vc.type = .story
                vc.id = self.story._id
                
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.callback = { amount in
                    
                    var newAmount:String!
                    
                    if let doubleAmount = Double(amount) {
                        newAmount = String(format: "%.2f", doubleAmount)
                    }else{
                        newAmount = amount
                    }
                    
                    self.addbidOnStory(amount: newAmount)
                }
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func handleTimerNotification() {
        self.view.endEditing(true)
        
        self.startTimerOrPlayVideo()
    }
    
    func startTimerOrPlayVideo(){
        
        if let isVideo = self.story.is_video, isVideo {
            self.player?.play()
        }else{// if it is Image type Story
            if let time = self.timer{
                time.invalidate()
            }
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.storyImageTimerController(_:)), userInfo: "selectorAsString", repeats: true)
        }
    }
    
    func stopTimerOrPauseVideo(){
        
        if let isVideo = self.story.is_video, isVideo {
            self.player?.pause()
        }else{// if it is Image type Story
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    //MARK:- Keyboard Methods
    @objc fileprivate func keyboardWillShow(notification:NSNotification) {
        
        self.stopTimerOrPauseVideo()
        
        if let keyboardRectValue = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.keyboardHeight = Double(keyboardRectValue.height)
            
            //self.tableView.alpha = 0.0
            
            UIView.animate(withDuration: 0.25) {
                
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc fileprivate func keyboardWillHide(notification:NSNotification) {
        
        if let _ = appDel.topViewController as? StoryImageViewerViewController{
            self.startTimerOrPlayVideo()
        }
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    func addbidOnStory(amount:String) {
        
        self.stopTimerOrPauseVideo()
        
        let params = ["user_id": MyProfile.userId!,
                      "story_id": self.story._id!,
                      "text": amount,
                      "type":"amount"]
        
        let wait = addWaitSpinner()
        
        JSONRequest.makeRequest(kBidOnStory, parameters: params) { (data, error) in
            self.removeWaitSpinner(waitView: wait)
            
            self.startTimerOrPlayVideo()
        }
    }
    
    @IBAction func onWhatsappClick(_ sender: Any) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge ()
            return;
        }
        
        guard let phone = self.story.userDetails.first?.phone, let countryCode = self.story.userDetails.first?.country_code else{
            let str = NSLocalizedString("Phone number Not Found!", comment: "Phone number Not Found!")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        openWhatsApp(phoneNumber: countryCode + phone, message: "Hello")
    }
    
    @IBAction func onCallClick(_ sender: Any) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        guard let phone = self.story.userDetails.first?.phone, let countryCode = self.story.userDetails.first?.country_code else{
            let str = NSLocalizedString("Phone number Not Found!", comment: "Phone number Not Found!")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        openCallingApp(phoneNumber:countryCode + phone)
    }
    
    //MARK:- Essential Method
    
    
    func resetAllBeforeDismiss() {
        
        print("reseting")
        
        self.stopTimerOrPauseVideo()
        self.playerLayer?.removeFromSuperlayer()
        self.playerLayer = nil
        self.player = nil
        
        self.view.removeActivityIndicator()
        
        self.storyProgressView.progress = 0.0
        
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object:nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardDidShow, object: nil)
    }
}

public enum Result<T> {
    case success(T)
    case failure(NSError)
}

class CacheManager {
    
    static let shared = CacheManager()
    
    private lazy var mainDirectoryUrl: URL = {
        
        let documentsUrl = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        return documentsUrl
    }()
    
    func getFileWith(stringUrl: String, completionHandler: @escaping (Result<URL>) -> Void ) {
        
        let file = directoryFor(stringUrl: stringUrl)
        
        //return file path if already exists in cache directory
        guard !FileManager.default.fileExists(atPath: file.path)  else {
            completionHandler(Result.success(file))
            return
        }
        
        DispatchQueue.global().async {
            
            if let videoData = NSData(contentsOf: URL(string: stringUrl)!) {
                videoData.write(to: file, atomically: true)
                
                DispatchQueue.main.async {
                    completionHandler(Result.success(file))
                }
            } else {
                DispatchQueue.main.async {
                    completionHandler(Result.failure(NSError(domain: "Can't download video", code: 0, userInfo: nil)))
                }
            }
        }
    }
    
    private func directoryFor(stringUrl: String) -> URL {
        
        let fileURL = URL(string: stringUrl)!.lastPathComponent
        
        let file = self.mainDirectoryUrl.appendingPathComponent(fileURL)
        
        return file
    }
}
