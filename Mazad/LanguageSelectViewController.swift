

import UIKit

class LanguageSelectViewController: UIViewController {

    @IBOutlet weak var imgMain: UIImageView!
    
    var langNames = [String]()
    var langCodes = [String]()
    
    @IBOutlet weak var btnEngligh:UIButton!
    @IBOutlet weak var btnArabic:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for languageCode in Bundle.main.localizations.filter({ $0 != "Base" }) {
            if let langName = Locale.current.localizedString(forLanguageCode: languageCode){
                self.langNames.append(langName)
            }
            self.langCodes.append(languageCode)
        }
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let view = touches.first?.view{
//            if view == imgMain{
//                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController"){
//                    let nvc = UINavigationController(rootViewController: vc)
//                    self.present(nvc, animated: true, completion: nil)
//                }
//            }
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
  
    @IBAction func onLanguageClick(_ sender: UIButton) {
        
//        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationMainViewController"){
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        

//        if sender.title(for: .normal) == "ENGLISH" {
//            self.changeToLanguage("en")
//        }else{
//            self.changeToLanguage("ar")

        
        if sender == self.btnEngligh{
            self.changeToLanguage("en")
        }else if sender == self.btnArabic{
            self.changeToLanguage("ar")
        }
   }
    
    private func changeToLanguage(_ langCode: String) {
        
        
        if Bundle.main.preferredLocalizations.first != langCode {
            
            let message = "In order to change the language, the App must be closed and reopened by you."
            
            let confirmAlertCtrl = UIAlertController(title: "App restart required", message: message, preferredStyle: .alert)
            
            let confirmAction = UIAlertAction(title: "Close now", style: .destructive) { _ in
                UserDefaults.standard.set([langCode], forKey: AppleLanguageKey)
                UserDefaults.standard.set(langCode, forKey: CurrentLanguageKey)
                UserDefaults.standard.synchronize()
                print("Saved code ==> \(langCode)")
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                    exit(EXIT_SUCCESS)
                })
            }
            confirmAlertCtrl.addAction(confirmAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            confirmAlertCtrl.addAction(cancelAction)
            
            present(confirmAlertCtrl, animated: true, completion: nil)
        }
        else{
            
            UserDefaults.standard.set([langCode], forKey: AppleLanguageKey)
            UserDefaults.standard.set(langCode, forKey: CurrentLanguageKey)
            UserDefaults.standard.synchronize()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}

