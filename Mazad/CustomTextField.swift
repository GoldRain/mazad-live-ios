

import UIKit

class CustomTextField: UITextField, UITextFieldDelegate {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.10) {
            self.addSubView()
        }
    }
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Lato-Regular", size: 12.0)
        label.textColor = UIColor(red: 179/255, green: 179/255, blue: 179/255, alpha: 1.0)
        return label
    }()
    
    private var bottomView = UIView()
    
    private var placeHolderStr: String?
    
    private func addSubView(){
        
        borderStyle = .none
        delegate = self
        
        textColor = UIColor(red: 54/255, green: 54/255, blue: 54/255, alpha: 1.0)
        font = UIFont(name: "Lato-Regular", size: 14.0)
        
        placeHolderStr = placeholder
        placeholder = nil
        
        titleLabel.frame =  CGRect(x: bounds.minX, y: bounds.midY-8.0, width: frame.width, height: 14.0)
        
        
        bottomView.frame = CGRect(x: bounds.minX, y: bounds.maxY-8.0, width: frame.width, height: 0.5)
        bottomView.backgroundColor = offLightGrayColor
        addSubview(bottomView)
        addSubview(titleLabel)
        
        
        textLayer.string = placeHolderStr
        textLayer.alignmentMode = textAlignment == .center ? kCAAlignmentCenter : kCAAlignmentLeft
        textLayer.font = self.titleLabel.font.fontName as CFTypeRef?
        textLayer.fontSize = startFontSize
        textLayer.foregroundColor = offLightGrayColor.cgColor
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.frame = self.titleLabel.bounds
        
        self.titleLabel.layer.addSublayer(textLayer)
        
        //animation:
        let duration: TimeInterval = 0.3
        fontSizeAnimation.duration = duration
        fontSizeAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        if text != ""{
            self.textLayer.fontSize = self.endFontSize
            self.titleLabel.frame =  CGRect(x: self.bounds.minX, y: self.bounds.minY-8.0, width: self.frame.width, height: 14.0)
            self.fontSizeAnimation.fromValue = self.startFontSize
            self.fontSizeAnimation.toValue = self.endFontSize
            self.textLayer.foregroundColor = self.darkLightGrayColor.cgColor
            self.bottomView.backgroundColor = self.darkLightGrayColor
            self.textLayer.add(self.fontSizeAnimation, forKey: "fontSize")
        }
    }
    
    
    let textLayer = CATextLayer()
    private let fontSizeAnimation = CABasicAnimation(keyPath: "fontSize")
    
    private let offLightGrayColor = UIColor(red: 179/255, green: 179/255, blue: 179/255, alpha: 1.0)
    private let darkLightGrayColor = UIColor(red: 117/255, green: 117/255, blue: 117/255, alpha: 1.0)
    
    private let startFontSize: CGFloat = 12
    private let endFontSize: CGFloat = 10
    
    func showTitleLable(isHide: Bool) {
        
        UIView.animate(withDuration: 0.3) {
            
            if isHide{
                self.textLayer.fontSize = self.startFontSize
                
                self.titleLabel.frame =  CGRect(x: self.bounds.minX, y: self.bounds.midY-8.0, width: self.frame.width, height: 14.0)
                self.fontSizeAnimation.fromValue = self.endFontSize
                self.fontSizeAnimation.toValue = self.startFontSize
                self.textLayer.add(self.fontSizeAnimation, forKey: "fontSize")
                self.textLayer.foregroundColor = self.offLightGrayColor.cgColor
                self.bottomView.backgroundColor = self.offLightGrayColor
            }else{
                self.textLayer.fontSize = self.endFontSize
                self.titleLabel.frame =  CGRect(x: self.bounds.minX, y: self.bounds.minY-8.0, width: self.frame.width, height: 14.0)
                self.fontSizeAnimation.fromValue = self.startFontSize
                self.fontSizeAnimation.toValue = self.endFontSize
                self.textLayer.foregroundColor = self.darkLightGrayColor.cgColor
                self.bottomView.backgroundColor = self.darkLightGrayColor
                self.textLayer.add(self.fontSizeAnimation, forKey: "fontSize")
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if text!.trim().isEmpty{
            self.showTitleLable(isHide: false)
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if text!.trim().isEmpty{
            self.showTitleLable(isHide: true)
        }
        return true
    }
    
}
