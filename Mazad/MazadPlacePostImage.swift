//
//  PlacePostImage.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 22, 2018

import Foundation


class MazadPlacePostImage : NSObject, NSCoding{

    var id : String!
    var imageUrl : String!
    var isVideo : Bool!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["_id"] as? String
        imageUrl = dictionary["image_url"] as? String
        isVideo = dictionary["is_video"] as? Bool
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["_id"] = id
        }
        if imageUrl != nil{
            dictionary["image_url"] = imageUrl
        }
        if isVideo != nil{
            dictionary["is_video"] = isVideo
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "_id") as? String
        imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
        isVideo = aDecoder.decodeObject(forKey: "is_video") as? Bool
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "_id")
        }
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
        if isVideo != nil{
            aCoder.encode(isVideo, forKey: "is_video")
        }
    }
}
