//
//  API.swift
//  Mazad Live
//
//  Created by Mac-3 on 21/11/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

let VERSION = "1.1"

let R5LiscenceKey = "EU2B-2BTM-K455-ZPHK"
let ContextName = "live"
let ServerPort:Int32 = 8554

//for local
//let apiBaseUrl = "http://192.168.0.120:8082"
//let ServerIPLiveStream = "192.168.0.129"

//for server
let apiBaseUrl = "http://18.218.27.17:8082"
let ServerIPLiveStream = "18.218.27.17"

//for staging
//let apiBaseUrl = "https://mazadlive.com:9000"

let kLoginApi               = "\(apiBaseUrl)/login"
let kRegister               = "\(apiBaseUrl)/register"
let kForgotPassword         = "\(apiBaseUrl)/forgotPassword"
let kVerifyOtp              = "\(apiBaseUrl)/verifyCode"
let kChangePassword         = "\(apiBaseUrl)/changePassword"
let kCountries              = "\(apiBaseUrl)/countries"
let kTags                   = "\(apiBaseUrl)/tags"
let kCreatePost             = "\(apiBaseUrl)/userPost"
let kRaiseMyBid             = "\(apiBaseUrl)/raiseMyBid"
let kGetPost                = "\(apiBaseUrl)/getPost"
let kGetMyPost              = "\(apiBaseUrl)/getMyPosts"

let kGetUserDetails         = "\(apiBaseUrl)/getUserDetails"
let kStreamDetails          = "\(apiBaseUrl)/streamDetails"
let kStopMyStream           = "\(apiBaseUrl)/stopMyStream"

let kPostLike               = "\(apiBaseUrl)/postLike"
let kLike                   = "\(apiBaseUrl)/like"
let kUserData               = "\(apiBaseUrl)/userData"
let kUpdateProfile          = "\(apiBaseUrl)/updateProfile"
let kFollow                 = "\(apiBaseUrl)/follow"
let kFavPost                = "\(apiBaseUrl)/favPost"
let kIBAN                   = "\(apiBaseUrl)/iban"
let kUpdateStorySettings    = "\(apiBaseUrl)/updateStorySettings"
let kVote                   = "\(apiBaseUrl)/vote"
let kCreateLiveStream       = "\(apiBaseUrl)/createStream"
let kBookmarkBid            = "\(apiBaseUrl)/bookMark"
let kBlock                  = "\(apiBaseUrl)/block"
let kBlockList              = "\(apiBaseUrl)/blockList"
let kReportUser             = "\(apiBaseUrl)/reportUser"
let kReportPost             = "\(apiBaseUrl)/reportPost"
let kGetMyFavPost           = "\(apiBaseUrl)/getMyFavPost"
let kGetMyStories           = "\(apiBaseUrl)/getMyStories"
let kCreateStory            = "\(apiBaseUrl)/storyPost"
let kBidOnStory           = "\(apiBaseUrl)/bidOnStory"
let kSearchUser             = "\(apiBaseUrl)/search"
let kHighestBid             = "\(apiBaseUrl)/highestBid"
let kSendOtp                = "\(apiBaseUrl)/verifyPhone"

let kSearchUserAll          = "\(apiBaseUrl)/searchUser"
let kSearchCategories       = "\(apiBaseUrl)/searchCategories"
let kSearchByPlace          = "\(apiBaseUrl)/searchByPlace"
let kCreateRoom             = "\(apiBaseUrl)/createRoom"
let kSendMessage            = "\(apiBaseUrl)/sendMessage"
let kGetRooms               = "\(apiBaseUrl)/getRooms"
let kAddAddress             = "\(apiBaseUrl)/addAddress"
let kCreateOrder            = "\(apiBaseUrl)/createOrder"
let kOrderOTP               = "\(apiBaseUrl)/orderOtp"
let kVerifyOrderOtp         = "\(apiBaseUrl)/verifyOrderOtp"
let kFetchBrainTreeToken    = "\(apiBaseUrl)/client_token"
let kCheckout               = "\(apiBaseUrl)/checkout"
let kOrderList              = "\(apiBaseUrl)/orderList"
let kActivities             = "\(apiBaseUrl)/activities"
let kPostBidDetails         = "\(apiBaseUrl)/postBidDetails"
let kChangeBidStatus        = "\(apiBaseUrl)/changeBidStatus"
let kPendingFeedbackOrder   = "\(apiBaseUrl)/pendingFeedbackOrder"
let kAddFeedbackOnPost      = "\(apiBaseUrl)/addFeedbackOnPost"
let kAddFeedbackOnStory     = "\(apiBaseUrl)/addFeedbackOnStory"
let kWalletList             = "\(apiBaseUrl)/walletList"
let kDeletePost             = "\(apiBaseUrl)/deletePost"
let kIsDeletedStatus        = "\(apiBaseUrl)/is_deletedStatus"
let kUpdateActivityStatus   = "\(apiBaseUrl)/activityStatus"
let kUpdateThumb            = "\(apiBaseUrl)/updateThumb"
let kFeedbackReceived       = "\(apiBaseUrl)/feedbackReceived"
let kSellerOrder            = "\(apiBaseUrl)/sellerOrder"

let kAcceptOrderRequestBySeller = "\(apiBaseUrl)/acceptOrderRequestBySeller"
let kRequestPaymentToBuyer      = "\(apiBaseUrl)/requestPaymentToAdmin"
let kDeliveryConfirmed          = "\(apiBaseUrl)/deliveryConfirmed"
let kAutoConfirmed              = "\(apiBaseUrl)/autoConfirmed"
let kAwardBid                   = "\(apiBaseUrl)/awardBid"
let kTapCustomerId              = "\(apiBaseUrl)/tapCustomerId"
let kCheckLastLogin             = "\(apiBaseUrl)/checkLastLogin"
let kUpdateStreamProduct        = "\(apiBaseUrl)/updateProduct"
let kGetCurrentStreamProduct    = "\(apiBaseUrl)/getCurrentProduct"
let kDeleteStory                = "\(apiBaseUrl)/deleteStory"
let kCheckEmail                 = "\(apiBaseUrl)/checkEmail"

let kShowCounts                 = "\(apiBaseUrl)/showCounts"
let kUserVerification           = "\(apiBaseUrl)/userVerification"
let kUpdatePostCount            = "\(apiBaseUrl)/update_post_count"
let kUpdateLiveCount            = "\(apiBaseUrl)/update_live_count"

let kUpdatePromote              = "\(apiBaseUrl)/updatePromote"
let kPopularPost                = "\(apiBaseUrl)/popularPost"
let kNotificationAndEmail       = "\(apiBaseUrl)/notificationAndEmail"
let kVerificationPayment        = "\(apiBaseUrl)/verificationPayment"
let kVerificationDetails        = "\(apiBaseUrl)/verificationDetails"
let kUpdateAllCount             = "\(apiBaseUrl)/updateAllCount"
let kNoMoreAds                  = "\(apiBaseUrl)/noMoreAds"

let kCategoryAndCount           = "\(apiBaseUrl)/categoryAndCount"
let kPlaceAndCount              = "\(apiBaseUrl)/placeAndCount"
let kRepost                     = "\(apiBaseUrl)/repost"
let kGetOrderDetails            = "\(apiBaseUrl)/getOrderDetails"
let kReportStory                = "\(apiBaseUrl)/reportStory"

let kFollowerList               = "\(apiBaseUrl)/followerList"
let kFollowingList              = "\(apiBaseUrl)/followingList"

//////
let kTermsAndConditionEn          = "\(apiBaseUrl)/public/mazadTerms.html"
let kDataPrivacyEn                = "\(apiBaseUrl)/public/mazadPrivacy.html"

let kTermsAndConditionAr          = "\(apiBaseUrl)/public/mazadTermsArabic.html"
let kDataPrivacyAr                = "\(apiBaseUrl)/public/mazadPrivacyArabic.html"
////// FOR IN APP PURCHASE
let kTest                         = "\(apiBaseUrl)/test"
let kCheckReceipt                 = "\(apiBaseUrl)/checkReceipt"
let kRestorePurchase              = "\(apiBaseUrl)/restorePurchase"

// for test only
//let kobserveSubscriptionStatus = "\(apiBaseUrl)/observeSubscriptionStatus"
