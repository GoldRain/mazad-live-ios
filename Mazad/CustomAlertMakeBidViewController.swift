import UIKit

enum viewControllerType2 {
    case stream
    case post
    case story
}

class CustomAlertMakeBidViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var lblCountryIcon: UILabel!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var txtPrice: CustomTextField!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var lblMinBidAmount: UILabel!
    
    var type:viewControllerType2 = .post
    
    var id:String?
    
    var minPrice:Double?
    
    var callback:((String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if let countryName = MyProfile.countryName {
            self.lblCountryName.text = countryName
            if let countryFlag = MyProfile.countryFlagUrl {
                self.countryImage.sd_setImage(with: URL(string: countryFlag), completed: nil)
            }
        }
        if let minPrice = self.minPrice {
            let newPrice = String(format: "%.2f", minPrice)
            self.lblMinBidAmount.text = "The last Bid was (KD \(newPrice))"
        }
        
//        self.minPrice = 1.0
//        let newPrice = String(format: "%.2f", minPrice!)
//        self.lblMinBidAmount.text = "Initial Bid is (KD \(newPrice))" //"The last Bid was (KD \(newPrice))"
        
        self.txtPrice.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.countryView.isUserInteractionEnabled = false
        
        self.getHighestBid()
    }
    
    func getHighestBid() {
        
        var params = [String:String]()
        
        if type == .post {
            params["type"] = "post"
            if let id = self.id {
                params["post_id"] = id
            }
        }else if type == .story{
            params["type"] = "story"
            if let id = self.id {
                params["story_id"] = id
            }
        }else if type == .stream {
            params["type"] = "stream"
            if let id = self.id {
                params["stream_id"] = id
            }
        }
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kHighestBid, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let description = data["description"] as? [String:Any] {
                        
                        if self.type == .story || self.type == .stream {
                            
                            if let amount = description["amount"] as? String {
                                
                                self.minPrice = Double(amount)
                                self.lblMinBidAmount.text = "The last Bid was (KD \(self.minPrice!))"
                            }
                        }
                        if self.type == .post {
                            
                            if let amount = description["bid_amount"] as? String {
                                
                                self.minPrice = Double(amount)
                                self.lblMinBidAmount.text = "The last Bid was (KD \(self.minPrice!))"
                            }
                        }
                    }
                }
                else if let _ = data["message"] as? String{
                }
            }
            else{
                let str = NSLocalizedString("Failed to get Last Bid, Please try again later", comment: "Failed to get Last Bid, Please try again later")
                self.displayMessage(str, callback: { () -> (Void) in
                    self.dismiss(animated: true, completion: nil)
                })
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let firstView = touches.first?.view{
            if firstView != alertView {
                self.dismiss(animated: true) {
                    if self.type == .story {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "startTimerAfterDismiss"), object: nil)
                    }
                }
            }
        }
    }
    
    @IBAction func onContinueClick(_ sender: UIButton) {
       
        if validate() {
            self.dismiss(animated: true, completion: {
                self.callback?(self.txtPrice.text!)
            })
        }
    }
    
    func validate() -> Bool {
       
        if let amount = self.txtPrice.text, !amount.isEmpty, let doubleAmount = amount.doubleValue{
            if doubleAmount <= self.minPrice! {
                var str = NSLocalizedString("Not a valid bid, Last bid was KD", comment: "Not a valid bid, Last bid was KD")
                str = str + "\(minPrice!)"
                self.displayMessage(str)
                return false
            }
        }else{
            let str = NSLocalizedString("Not a valid bid", comment: "Not a valid bid")
            self.displayMessage(str)
            return false
        }
        
        return true
    }
    
    //MARK:- TextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return updatedText.count <= 6
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let textField = textField as? CustomTextField{
            return textField.textFieldShouldBeginEditing(_:textField)
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let textField = textField as? CustomTextField{
            return textField.textFieldShouldEndEditing(_:textField)
        }
        return true
    }
}
