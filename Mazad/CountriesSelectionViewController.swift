import UIKit

enum countrySelectionFrom {
    case tabBarVC
    case otherVC
}

class CountriesSelectionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let ROWHEIGHT:CGFloat = 60
    
    var type:countrySelectionFrom = .otherVC
    
    var callback: (((CountryModel))->())?
    
    @IBOutlet weak var listMainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var countriesList = [CountryModel]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        if self.type == .tabBarVC {
            self.countriesList.insert(CountryModel(data: ["countryName":"All"]), at:0)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let view = touches.first?.view{
            if view != listMainView || view != tableView{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROWHEIGHT
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let height = self.view.frame.size.height
        
        var newHeight = CGFloat(self.countriesList.count) * ROWHEIGHT
        
        if newHeight >= height{
            newHeight = height - ROWHEIGHT
            tableView.isScrollEnabled = true
        }
        else{
            tableView.isScrollEnabled = false
        }
       
        UIView.animate(withDuration: 0.25) {
            self.tableViewHeight.constant = newHeight
        }
        
        return countriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let data = countriesList[indexPath.row]
        
        if let imgCountry = cell.contentView.viewWithTag(1) as? UIImageView {
            if let lblName = cell.contentView.viewWithTag(2) as? UILabel{
                
                imgCountry.image = UIImage(named: "world (1)")
                if let imageUrl = data.flagImageUrl {
                    var completeUrl =  "\(apiBaseUrl)\(imageUrl)"
                    completeUrl = completeUrl.replacingOccurrences(of: " ", with: "%20")
                    imgCountry.sd_setImage(with: URL(string: completeUrl), completed: nil)
                }
                lblName.text = data.countryName
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.dismiss(animated: true, completion: {
            
            let data = self.countriesList[indexPath.row]
            if let callback = self.callback{
                callback(data)
            }
        })
    }
}
