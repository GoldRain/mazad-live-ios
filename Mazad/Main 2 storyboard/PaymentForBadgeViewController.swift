//
//  PaymentForBadgeViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 23/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class PaymentForBadgeViewController: UIViewController, PayementViewControllerDelegate {

    @IBOutlet weak var imgProfileImage: UIImageView!
    @IBOutlet weak var imgVerifiedImage: UIImageView!
    @IBOutlet weak var imgDownHand: UIImageView!
    @IBOutlet weak var alertView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let url = MyProfile.userProfilePhoto {
            self.imgProfileImage.sd_setImage(with: URL(string: url), completed: nil)
        }
        setupUI()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view != self.alertView {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func setupUI(){
        
        UIView.animate(withDuration: 0.6, delay: 0.0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
            self.imgDownHand.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -25)
        }){ (fin) in
            self.imgDownHand.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 0)
        }
        
        let animation1 = CABasicAnimation(keyPath: "transform.scale")
        animation1.duration = 0.6
        animation1.repeatCount = .infinity
        animation1.fromValue = 1.3
        animation1.toValue = 0.6
        animation1.autoreverses = true
        self.imgVerifiedImage.layer.add(animation1, forKey: "transform.scale")
    }

    @IBAction func onProceedToPayClick(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
            guard let fullName = MyProfile.userName else {
                return;
            }
            var firstName = ""
            var lastName = ""
            var components = fullName.components(separatedBy: " ")
            if(components.count > 0)
            {
                firstName = components.removeFirst()
                lastName = components.joined(separator: " ")
            }
            guard let email = MyProfile.userEmail else{
                return;
            }
            guard let number = MyProfile.phone else{
                return;
            }
            
            _ = PaymentViewController.showPaymentWithData(amount: 1100.0,
                                                          currency: "kwd",
                                                          customerFirstName: firstName,
                                                          customerLastName: lastName,
                                                          customerEmail: email,
                                                          customerPhone: number,
                                                          delegate: self)
        }
    }
    
    @IBAction func onDoThisLaterClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Payment ViewController delegate Method
    func paymentWebViewDidCancel(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
            
        }
    }
    
    func paymentWebViewDidFinish(_ paymentViewController: PaymentViewController, paymentInfo: PaymentData) {
        
        paymentViewController.dismiss(animated: true) {
            
            print(paymentInfo)
            
            self.createOrder(paymentData: paymentInfo)
            
        }
    }
    
    func paymentWebViewDidFail(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
            
        }
    }
    
    //MARK:- calling Create order Api
    fileprivate func createOrder(paymentData:PaymentData) {
        
        guard let userId = MyProfile.userId else{
            self.displayMessage("User Id Not Found!!")
            return
        }
        
        let params = ["user_id":userId,
                      "chargeid":paymentData.chargeid!,
                      "crd":paymentData.crd!,
                      "crdtype":paymentData.crdtype!,
                      "hash":paymentData.hash!,
                      "payid":paymentData.payid!,
                      "ref":paymentData.ref!,
                      "result":paymentData.result!,
                      "trackid":paymentData.trackid!
                    ]
        
        let txnResponse = TransactionResponse(data: ["transaction_id":paymentData.payid])
        
        if paymentData.result == "SUCCESS" {
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            JSONRequest.makeRequest(kVerificationPayment, parameters: params) { (data, error) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data as? [String:Any] {
                    
                    if let err = data["error"] as? Bool, !err {
                        
                        let str1 = NSLocalizedString("Congratulation!!", comment: "Congratulation!!")
                        
                        let str = NSLocalizedString("You got a Verified Badge for your Seller Profile", comment: "You got a Verified Badge for your Seller Profile")
                        
                        appDel.topViewController?.displayMessage(title: str1, msg: str)
                        
                        MyProfile.isVerified = true
                        
                        profileVC?.collectionView.reloadData()
                        
                        return;
                    }
                }
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionFailedViewController") as? TransactionFailedViewController {
                    
                    vc.txnResponse = txnResponse
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }else{
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionFailedViewController") as? TransactionFailedViewController {
                
                vc.txnResponse = txnResponse
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
}
