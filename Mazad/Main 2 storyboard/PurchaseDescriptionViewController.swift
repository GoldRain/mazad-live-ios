//
//  PaymentForBadgeViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 23/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class PurchaseData{
    
    var title:String?
    var amount:String?
    var image:UIImage?
    var descriptionText:String?
    
    init(title:String, amount:String, image:UIImage, descText:String){
        self.title = title
        self.amount = amount
        self.image = image
        self.descriptionText = descText
    }
}

class PurchaseDescriptionViewController: UIViewController{
    
    var localize = true
    
    var purchaseData:PurchaseData!
    
    var callback:(()->())!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var imgTitle:UIImageView!
    
    @IBOutlet weak var imgDownHand: UIImageView!
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var btnPurchase: UIButton!
    @IBOutlet weak var btnLater: UIButton!
    
    var btnTitle:String = "Proceed to Pay"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.lblTitle.text = self.purchaseData.title
        self.lblAmount.text = self.purchaseData.amount!
        self.lblDescription.text = self.purchaseData.descriptionText
        self.imgTitle.image = self.purchaseData.image
        
        if !self.localize{
            self.btnPurchase.setTitle("Proceed to Pay", for: .normal)
            self.btnLater.setTitle("I will do this later", for: .normal)
        }
        
        self.btnPurchase.setTitle(self.btnTitle, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        setupUI()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view != self.alertView {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func setupUI(){
        
        UIView.animate(withDuration: 0.6, delay: 0.0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
            self.imgDownHand.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -25)
        }){ (fin) in
            self.imgDownHand.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 0)
        }
    }
    
    @IBAction func onProceedToPayClick(_ sender: Any) {
        
        self.dismiss(animated: true) {
            self.callback()
        }
    }
    
    @IBAction func onDoThisLaterClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
