//
//  PopUpPostViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 12/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit
import SDWebImage
import ImageViewer
import AVKit
import AVFoundation
import CoreLocation
import DropDown

import Firebase

class PopUpPostViewController: UIViewController, GalleryItemsDataSource, GalleryItemsDelegate {

    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var subViewOfMainContainerView: UIView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnUserImage: UIButton!
    @IBOutlet weak var imgVerifiedIImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPostLocation: UILabel!
    @IBOutlet weak var btnThumbsUp: UIButton!
    @IBOutlet weak var btnThumbsDown: UIButton!
    @IBOutlet weak var btnMenuOption: UIButton!
    @IBOutlet weak var lblVotePercentage: UILabel!
    
    @IBOutlet weak var imageMainView: UIView!
    @IBOutlet weak var imageTopView: UIView!
    @IBOutlet weak var imgPostImage: UIImageView!
    @IBOutlet weak var btnPlayVideo: UIButton!
    
    @IBOutlet weak var btnWhatsapp: UIButton!
    @IBOutlet weak var btnStar: UIButton!
    @IBOutlet weak var btnHeart: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnPriceForBuyNow: UIButton!
    
    @IBOutlet weak var lblPostDescription: UILabel!
    
    @IBOutlet weak var buySellContainerView: UIView!
    
    @IBOutlet weak var buyNowView: UIView!
    @IBOutlet weak var lblBuyNow: UILabel!
    @IBOutlet weak var btnBuyNow: UIButton!
    
    @IBOutlet weak var makeABidView: UIView!
    @IBOutlet weak var lblMakeABid: UILabel!
    @IBOutlet weak var btnMakeABid: UIButton!
    
    @IBOutlet weak var feedbackMainView: UIView!
    @IBOutlet weak var lblFeedback: UILabel!
    
    @IBOutlet weak var soldVIew: UIView!
    @IBOutlet weak var lblSold: UILabel!
    
    
    var post:BuyerSellerModel!
    
    lazy var geocoder = CLGeocoder()
    
    var postId = ""
    
    var dataImage: [GalleryItem] = [GalleryItem]()
    var galleryController:GalleryViewController?
    
    var postImages = [PostImageModel]()
    
    //MARK:- View did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let view = touches.first?.view{
            if view != mainContainerView && view != subViewOfMainContainerView && view != topView && view != buySellContainerView  && view != self.imageMainView && view != imageTopView && view != imgPostImage && view != self.buyNowView && view != self.makeABidView && view != self.soldVIew {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //MARK:-
    func setupData() {
        
        NotificationCenter.default.removeObserver(self, name: .UpdateVotePercent, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateVoteNotification(not:)), name: .UpdateVotePercent, object: nil)
        
        self.imageMainView.addShadow()
        self.mainContainerView.addShadow()
        self.imageTopView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePostImageViewer)))
        
        let userDetails = self.post.userDetails
        let postImages = self.post.post_images
        let postLike = self.post.post_like
        let votes = self.post.vote
        
        self.postImages = postImages
        //
        if self.btnPlayVideo != nil {
            if let isVideo = postImages.first?.is_video, isVideo {
                self.btnPlayVideo.isHidden = false
                self.btnPlayVideo.isUserInteractionEnabled = true
            }else {
                self.btnPlayVideo.isHidden = true
                self.btnPlayVideo.isUserInteractionEnabled = false
            }
        }
        if let userName = userDetails.first?.username {
            self.lblUserName.text = userName
        }
        
        if let latString = self.post.latitude, latString != "" {
            if let lngString = self.post.longitude, lngString != "" {
                let lat = Double(latString)
                let lng = Double(lngString)
                
                // Create Location
                let location = CLLocation(latitude: lat!, longitude: lng!)
                
                // Geocode Location
                geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                    // Process Response
                    
                    if let error = error {
                        print("Unable to Reverse Geocode Location (\(error))")
                    } else {
                        if let placemarks = placemarks, let placemark = placemarks.first {
                            self.lblPostLocation.text = placemark.compactAddress
                        }
                    }
                }
            }
        }else {
            self.lblPostLocation.text = ""
        }
        
        if let profileImage = userDetails.first?.profilePhoto {
            self.btnUserImage.sd_setImage(with: URL(string: profileImage), for: .normal, placeholderImage: #imageLiteral(resourceName: "icon_profile_grey2"), options: [], completed: nil)
        }
        
        if let url = postImages.first?.image_url {
            self.imgPostImage.sd_setImage(with: URL(string: url)) { (img, err, cache, url) in
                if img == nil{
                    self.imgPostImage.image = UIImage(named: "ic_placeholder")
                }
                self.imgPostImage.clipsToBounds = true
            }
        }
        for like in postLike {
            if like._id == MyProfile.userId {
                self.btnHeart.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            }
        }
        if let favPosts = MyProfile.favPostId {
            if favPosts.contains(where: { (favPostId) -> Bool in
                return favPostId == self.post._id
            }){
                self.btnStar.setImage(#imageLiteral(resourceName: "ic_star_fill"), for: .normal)
            }
        }
        if votes.count > 0 {
            
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "1"
            }){
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "ic_like_fill"), for: .normal)
            }
            
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "0"
            }){
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "ic_dislike_fill"), for: .normal)
            }
        }
        
        if let amount = self.post.amount, let doubleAmount = Double(amount) {
            let newAmount = String(format: "%.2f", doubleAmount)
            self.btnPriceForBuyNow.setTitle("Price KD \(newAmount)", for: .normal)
        }
        
        //check for buynow
        
        if let value = self.post.buy_now, value {
            if buyNowView != nil && btnBuyNow != nil{
                self.buyNowView.backgroundColor = BLUECOLOR
                self.btnBuyNow.isUserInteractionEnabled = true
            }
        }else {
            if buyNowView != nil && btnBuyNow != nil {
                self.buyNowView.backgroundColor = UIColor.gray
                self.btnBuyNow.isUserInteractionEnabled = false
            }
        }
        
        //check for Auction
        
        if let value = self.post.is_auction, value {
            if makeABidView != nil && btnMakeABid != nil {
                self.makeABidView.backgroundColor = BLUECOLOR
                self.btnMakeABid.isUserInteractionEnabled = true
            }
        }else {
            if makeABidView != nil && btnMakeABid != nil {
                self.makeABidView.backgroundColor = UIColor.gray
                self.btnMakeABid.isUserInteractionEnabled = false
            }
        }
        
        if let user = userDetails.first {
            if let userId = MyProfile.userId {
                if user._id == userId {
                    self.buyNowView.isHidden = true
                    self.makeABidView.isHidden = true
                    self.buyNowView.isUserInteractionEnabled = false
                    self.makeABidView.isUserInteractionEnabled = false
                    self.buySellContainerView.fd_collapsed = true
                }else {
                    if let isSold = self.post.is_sold, !isSold {
                        self.buyNowView.isHidden = false
                        self.makeABidView.isHidden = false
                        self.buyNowView.isUserInteractionEnabled = true
                        self.makeABidView.isUserInteractionEnabled = true
                        self.buySellContainerView.fd_collapsed = false
                    }else{
                        self.buyNowView.isHidden = true
                        self.makeABidView.isHidden = true
                        self.buyNowView.isUserInteractionEnabled = false
                        self.makeABidView.isUserInteractionEnabled = false
                        self.buySellContainerView.fd_collapsed = true
                    }
                }
            }
        }
        
        if let percent = self.post.percent, percent != "" {
            //print("percent value \(percent)")
            self.lblVotePercentage.text = percent
        }else {
            self.lblVotePercentage.text = "0%"
        }
        
        if let isWhatsaappOnly = self.post.whatsapp_only, isWhatsaappOnly {
            if let userId = MyProfile.userId {
                if userDetails.first?._id == userId {
                    self.btnWhatsapp.isHidden = true
                    self.btnCall.isHidden = true
                }else {
                    self.btnWhatsapp.isHidden = false
                    self.btnCall.isHidden = true
                }
            }
        }
        
        if let isWhatsappAndCall = self.post.whatsapp_and_call, isWhatsappAndCall {
            if let userId = MyProfile.userId {
                if userDetails.first?._id == userId {
                    self.btnWhatsapp.isHidden = true
                    self.btnCall.isHidden = true
                }else {
                    self.btnWhatsapp.isHidden = false
                    self.btnCall.isHidden = false
                }
            }
        }
        
        if let userId = MyProfile.userId {
            if userDetails.first?._id == userId {
                self.btnMessage.isHidden = true
            }else {
                self.btnMessage.isHidden = false
            }
        }
        
        if let postDetails = self.post.details, postDetails != "" {
            self.lblPostDescription.text = postDetails
        }else{
            self.lblPostDescription.text = ""
        }
        
        if let isVerified = userDetails.first?.isVerified, isVerified{
            self.imgVerifiedIImage.image = #imageLiteral(resourceName: "icon_privacy_yellow")
        }else{
            self.imgVerifiedIImage.image = #imageLiteral(resourceName: "icon_privacy_grey")
        }
        
        let feedbacks = self.post.feedback
        
        if feedbacks.count == 0 {
            self.feedbackMainView.fd_collapsed = true
        }else{
            self.feedbackMainView.fd_collapsed = false
            self.lblFeedback.text = NSLocalizedString("Feedback", comment: "Feedback")
        }
        
        if let isSold = self.post.is_sold {
            self.soldVIew.isHidden = !isSold
            self.lblSold.isHidden = !isSold
        }
    }
    
    @objc func handlePostImageViewer() {
        self.showImagesPreview(index: 0)
    }
    
    @objc func onUpdateVoteNotification(not:Notification){
        if let percent = not.userInfo?["percent"] as? String{
            if let postId = not.userInfo?["postId"] as? String{
                if postId == self.post._id{
                    self.lblVotePercentage.text = percent
                    self.post.percent = percent
                }
            }
        }
    }
    
    // MARK:- VoteClick
    
    var workItem3:DispatchWorkItem?
    
    var isThumbsUp = 0
    var isThumbsDown = 0
    
    @IBAction func onVoteClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        if MyProfile.isSeller {
            
            let votes = self.post.vote
            
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "1"
            }){
                self.isThumbsUp = 1
            }
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "0"
            }){
                self.isThumbsDown = 0
            }
            
        }else {
            let votes = self.post.vote
            
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "1"
            }){
                self.isThumbsUp = 1
            }
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "0"
            }){
                self.isThumbsDown = 0
            }
        }
        
        workItem3?.cancel()
        
        if sender == self.btnThumbsUp {
            
            if self.btnThumbsUp.image(for: .normal) == #imageLiteral(resourceName: "icon_like_green") {
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "ic_like_fill"), for: .normal)
                if btnThumbsDown.image(for: .normal) == #imageLiteral(resourceName: "ic_dislike_fill") {
                    self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
                }
                self.isThumbsUp = 1
                
            }else {
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
                
                self.isThumbsUp = 1
            }
            
            self.workItem3 = DispatchWorkItem(block: {
                self.callVotePost(isVote: self.isThumbsUp)
            })
        }
        if sender == self.btnThumbsDown {
            
            if self.btnThumbsDown.image(for: .normal) == #imageLiteral(resourceName: "icon_dislike_red") {
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "ic_dislike_fill"), for: .normal)
                if btnThumbsUp.image(for: .normal) == #imageLiteral(resourceName: "ic_like_fill") {
                    btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
                }
                self.isThumbsDown = 0
                
            }else {
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
                
                self.isThumbsDown = 0
            }
            
            self.workItem3 = DispatchWorkItem(block: {
                self.callVotePost(isVote: self.isThumbsDown)
            })
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: self.workItem3!)
    }
    
    func callVotePost(isVote:Int) {
        
        if MyProfile.isSeller {
            guard let postId = self.post._id else {
                return
            }
            self.postId = postId
        }else {
            guard let postId = self.post._id else {
                return
            }
            self.postId = postId
        }
        
        let params = ["post_id":postId,
                      "user_id":MyProfile.userId!,
                      "is_vote":"\(isVote)"]
        
        JSONRequest.makeRequest(kVote, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [String:Any] {
                        
                        if let votes = des["vote"] as? [[String:Any]] {
                            
                            if MyProfile.isSeller {
                                if self.post.vote.count > 0 {
                                    self.post.vote.removeAll()
                                }
                            }else {
                                if self.post.vote.count > 0 {
                                    self.post.vote.removeAll()
                                }
                            }
                            
                            for vote in votes {
                                if MyProfile.isSeller {
                                    
                                    self.post.vote.append(VoteModel(data: vote))
                                }else {
                                    
                                    self.post.vote.append(VoteModel(data: vote))
                                }
                            }
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                    // add toast
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                // add toast
            }
        }
    }
    
    //MARK:- Menu option click
    
    var menuOptionDropDown = DropDown()
    var menuOption = ["Report Post", "Share"]
    
    @IBAction func onMenuOptionClick(_ sender: UIButton) {
        
        guard let userId = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        guard let postId = self.post._id else {
            self.displayMessage("Failed to get Post Id")
            return
        }
        
        if post.post_user_id == userId {
            self.menuOption[0] = "Delete"
            
        }else {
            self.menuOption[0] = "Report Post"
        }
        
        self.menuOptionDropDown.dataSource = self.menuOption
        self.menuOptionDropDown.anchorView = self.btnMenuOption
        self.menuOptionDropDown.backgroundColor = .white
        self.menuOptionDropDown.textColor = BLUECOLOR
        
        self.menuOptionDropDown.bottomOffset = CGPoint(x: 0, y:(self.menuOptionDropDown.anchorView?.plainView.bounds.height)!)
        self.menuOptionDropDown.topOffset = CGPoint(x: 0, y: -(self.menuOptionDropDown.anchorView?.plainView.bounds.height)!)
        
        self.menuOptionDropDown.selectionAction = { (index: Int, item: String)  in
            
            if item == "Delete" {
                
                guard let postId = self.post._id else {
                    self.displayMessage("Failed to get Post Id")
                    return
                }
                
                let params = ["post_id":postId,
                              "is_deleted":"\(true)"]
                
                let wait = appDel.topViewController?.addWaitSpinner()
                
                JSONRequest.makeRequest(kDeletePost, parameters: params, callback: {  (data, error) in
                    
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    
                    if let data = data as? [String:Any] {
                        
                        if let err = data["error"] as? Bool, !err {
                            
                            self.dismiss(animated: true, completion: {
                                // reload View Controller
                                profileVC?.reloadingDataAfterUpdate()
                                categoryVC?.getDataFromApiAndReloadTable()
                            })
                        }else if let msg = data["message"] as? String{
                            let str = NSLocalizedString(msg, comment: msg)
                            self.displayMessage(str)
                        }
                    }else {
                        let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                        self.displayMessage(str)
                    }
                })
            }
            
            if item == "Report Post" {
                
                let userDetail = self.post.userDetails.first
                
                if let id = userDetail?._id {
                    if let userId = MyProfile.userId {
                        if id != userId {
                            
                            if let postId = self.post._id {
                                
                                if let rvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReportPostViewController") as? ReportPostViewController {
                                    
                                    rvc.postId = postId
                                    
                                    rvc.modalTransitionStyle = .crossDissolve
                                    rvc.modalPresentationStyle = .overCurrentContext
                                    self.present(rvc, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }
            }
            
            if item == "Share" {
                guard let link = URL(string: "https://mazadlive.page.link?type=post&id=\(postId)") else { return }
                let dynamicLinksDomainURIPrefix = "https://mazadlive.page.link"
                guard let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix) else{
                    print("wierd!! link builder is not created")
                    return
                }
                linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.itwhiz4u.q8mercato.mazadlive")
                linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: "com.mazadlive")
                linkBuilder.iOSParameters?.appStoreID = "1450514377"
                linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
                if let postDetails = self.post.details{
                    linkBuilder.socialMetaTagParameters?.title = ""
                    linkBuilder.socialMetaTagParameters?.descriptionText = postDetails
                }
                if let imageUrl = self.post.post_images.first?.image_url, imageUrl != ""{
                    linkBuilder.socialMetaTagParameters?.imageURL = URL(string: imageUrl)
                }
                
                guard let longDynamicLink = linkBuilder.url else { return }
                print("The long URL is: \(longDynamicLink)")
                
                
                linkBuilder.shorten() { url, warnings, error in
                    
                    guard let url = url, error == nil else { return }
                    
                    print("The short URL is: \(url)")
                    
                    let textToShare = [url]
                    
                    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = appDel.topViewController!.view // so that iPads won't crash
                    
                    // exclude some activity types from the list (optional)
//                    activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                    
                    // present the view controller
                    appDel.topViewController!.present(activityViewController, animated: true, completion: {
                        
                    })
                    
                }
                
            }
        }
        self.menuOptionDropDown.show()
    }
    
    //MARK:- PlayVideo
    
    @IBAction func onPlayVideoClick(_ sender: UIButton) {
        
        guard let postImage = self.post.post_images.first else {
            return
        }
        guard let imageUrl = postImage.image_url else {
            return
        }
        let videoUrl = imageUrl.replacingOccurrences(of: ".jpg", with: ".mp4")
        
        let player = AVPlayer(url: URL(string: videoUrl)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        
        if let top = appDel.topViewController {
            top.present(playerController, animated: true) {
                player.play()
            }
        }
    }
    
    // MARK:- StarClick
    
    var workItem2:DispatchWorkItem?
    var isFav = false
    
    @IBAction func onStarClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        workItem2?.cancel()
        
        if let postIds = MyProfile.favPostId {
            if postIds.contains(where: { (postId) -> Bool in
                if MyProfile.isSeller {
                    if let id = post._id {
                        return self.postId == id
                    }
                    return false
                }else {
                    if let id = self.post._id {
                        return postId == id
                    }
                    return false
                }
            }) {
                self.isFav = true
            }else {
                self.isFav = false
            }
        }
        
        var action = false
        
        if sender.image(for: .normal) == #imageLiteral(resourceName: "icon_star_yellow"){
            sender.setImage(#imageLiteral(resourceName: "ic_star_fill"), for: .normal)
            action = true
        }else{
            sender.setImage(#imageLiteral(resourceName: "icon_star_yellow"), for: .normal)
            action = false
        }
        
        self.workItem2 = DispatchWorkItem(block: {
            if self.isFav != action{
                self.callFavPost(isFav: action)
            }
            else{
                print("no action required")
            }
        })
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: self.workItem2!)
    }
    
    var favPostId = [String]()
    
    func callFavPost(isFav:Bool) {
        
        if MyProfile.isSeller {
            guard let postId = self.post._id else {
                return
            }
            self.postId = postId
        }else {
            guard let postId = self.post._id else {
                return
            }
            self.postId = postId
        }
        
        let params = ["post_id":postId,
                      "user_id":MyProfile.userId!,
                      "is_fav":"\(isFav)"]
        
        JSONRequest.makeRequest(kFavPost, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [String:Any] {
                        
                        if let favPosts = des["favPost"] as? [[String:Any]] {
                            
                            self.favPostId.removeAll()
                            
                            for favPost in favPosts {
                                autoreleasepool {
                                    if let fPost = favPost["post_id"] as? String {
                                        self.favPostId.append(fPost)
                                    }
                                }
                            }
                            MyProfile.favPostId?.removeAll()
                            MyProfile.favPostId = self.favPostId
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                    // add toast
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                // add toast
            }
        }
    }
    
    // MARK:- HeartClick
    
    var workItem1:DispatchWorkItem?
    var isLiked = 0
    
    @IBAction func onHeartClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        let postLikes = self.post.post_like
        
        for postLike in postLikes {
            
            // if post user id is equal to myprofile user id
            // user already liked the post so dislike it
            // remove user from postLike
            
            if postLike._id == MyProfile.userId {
                self.isLiked = 1
            }
        }
        
        workItem1?.cancel()
        var action = 0
        
        if sender.image(for: .normal) == #imageLiteral(resourceName: "icon_like_red") {
            sender.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            action = 1
            
        }else {
            sender.setImage(#imageLiteral(resourceName: "icon_like_red"), for: .normal)
            action = 0
        }
        
        self.workItem1 = DispatchWorkItem(block: {
            if self.isLiked != action{
                self.callPostLike(isLike: action)
            }
            else{
                print("no action required")
            }
        })
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: self.workItem1!)
    }
    
    func callPostLike(isLike:Int) {
        
        if MyProfile.isSeller {
            guard let postId = self.post._id else {
                return
            }
            self.postId = postId
        }else {
            guard let postId = self.post._id else {
                return
            }
            self.postId = postId
        }
        
        let params = ["post_id":postId,
                      "user_id":MyProfile.userId!,
                      "is_like":"\(isLike)"]
        
        JSONRequest.makeRequest(kPostLike, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [Any] {
                        
                        if let data = des.first as? [String:Any] {
                            
                            if let postLikeArr = data["post_like"] as? [[String:Any]] {
                                
                                for data in postLikeArr {
                                    
                                    autoreleasepool {
                                        
                                        if let id = data["_id"] as? String {
                                            
                                            if MyProfile.isSeller {
                                                
                                                if id == MyProfile.userId {
                                                    
                                                    self.post.post_like.append(PostLikeModel(data: data))
                                                }else {
                                                    if let i = self.post.post_like.firstIndex(where: { (post) -> Bool in
                                                        return post._id == id
                                                    }) {
                                                        self.post.post_like.remove(at: i)
                                                    }
                                                }
                                            }else {
                                                if id == MyProfile.userId {
                                                    self.post.post_like.append(PostLikeModel(data: data))
                                                }else {
                                                    if let i = self.post.post_like.firstIndex(where: { (post) -> Bool in
                                                        return post._id == id
                                                    }) {
                                                        self.post.post_like.remove(at: i)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                    // add toast
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                // add toast
            }
        }
    }
    
    //MARK:- Whatsapp,call,Message
    
    @IBAction func onClickAction(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        guard let phone = self.post.userDetails.first?.phone, let countryCode = self.post.userDetails.first?.country_code else{
            let str = NSLocalizedString("Phone number Not Found!", comment: "Phone number Not Found!")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        switch sender {
        case btnWhatsapp:
            openWhatsApp(phoneNumber: countryCode + phone, message: "Hello")
        case btnMessage:
            self.startChat()
        case btnCall:
            openCallingApp(phoneNumber:countryCode + phone)
        default:
            break
        }
    }
    
    func startChat() {
        
        //UIApplication.shared.open(URL(string: "sms:12345678901")!, options: [:], completionHandler: nil)
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            
            if let otherUserId = self.post.userDetails.first?._id{
                
                messageCenter?.getRoomFromLocal(otherUserId: otherUserId, roomId: nil, callback: {  (room) in
                    
                    vc.room = room
                    vc.otherId = otherUserId
                    vc.userName = self.post.userDetails.first?.username
                    
                    mainQueue.async {
                        self.dismiss(animated: true, completion: {
                            if let top = appDel.topViewController{
                                top.navigationController?.pushViewController(vc, animated: true)
                            }
                        })
                    }
                })
            }
        }
    }
    
    //MARK:- GalleryItemsDataSource
    func itemCount() -> Int {
        
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    //MARK: Image preview
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        for image in self.postImages {
            
            if let img = image.image_url, img != ""{
                
                let galleryItem = GalleryItem.image { imageCompletion in
                    
                    
                    print("Image URl \(img)")
                    
                    SDWebImageDownloader.shared().downloadImage(with: URL(string: img), options: [], progress: nil, completed: { (img, data, err, fin) in
                        
                        if let image = img{
                            imageCompletion(image)
                        }
                    })
                }
                self.dataImage.append(galleryItem)
            }
        }
        
        guard dataImage.count > 0 else{
            if let top = appDel.topViewController{
                top.displayMessage("Image Not Found!!")
            }
            return
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
    }
    
    //MARK:- Buy now click
    
    @IBAction func onBuyNowClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        // Check if product is available for selected country
        if let value = post.only_user_from_selected_country, value {
            
            if !post.available_country_id.contains(where: { (country) -> Bool in
                return country._id == MyProfile.countryId
            }){
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
                
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                
                vc.alertText = "This Product isn't Available for your Country, Keep Searching for other Products!"
                
                appDel.topViewController?.present(vc, animated: true, completion: nil)
                
                return;
            }
        }
        
        // Check if product is deleted or not
        let params = [
            "type_id": post._id!,
            "type": "post"
        ]
        let wait = appDel.topViewController?.addWaitSpinner()
        JSONRequest.makeRequest(kIsDeletedStatus, parameters: params) { (data, error) in
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    if let msg = data["message"] as? Bool, !msg { // not deleted
                        
                        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateOrderViewController") as? CreateOrderViewController {
                            
                            if let amount = self.post.amount, let postId = self.post._id, let sellerId = self.post.post_user_id {
                                vc.amount = amount
                                vc.type_id = postId
                                vc.sellerId = sellerId
                                vc.type = "post"
                                vc.post = self.post
                            }
                            self.dismiss(animated: true) {
                                if let top = appDel.topViewController {
                                    top.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                        
                    }else{ // deleted
                        let str = NSLocalizedString("This Item is no longer available for Purchase", comment: "This Item is no longer available for Purchase")
                        self.displayMessage(str)
                    }
                }else{
                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                    self.displayMessage(str)
                }
            }
            else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                self.displayMessage(str)
            }
        }
    }
    
    //MARK:- Make a Bid Click
    
    @IBAction func onMakeABidClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }

        
        if let value = post.only_user_from_selected_country, value {
            
            if !post.available_country_id.contains(where: { (country) -> Bool in
                return country._id == MyProfile.countryId
            }){
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
                
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                
                vc.alertText = "This Product isn't Available for your Country, Keep Searching for other Products!"
                
                appDel.topViewController?.present(vc, animated: true, completion: nil)
                
                return;
            }
        }
        
        guard let postUserId = self.post.post_user_id else{
            return;
        }
        
        if postUserId != MyProfile.userId {

            guard let postId = post._id else {
                return
            }
            
            if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertMakeBidViewController") as? CustomAlertMakeBidViewController{
                
//                if let amount = post.amount {
//                    vc.minPrice = Double(amount)
//                }
                vc.minPrice = 1.0
                vc.id = postId
                
                vc.callback = {  amount in
                    
                    guard let userId = MyProfile.userId else {
                        return
                    }
                    
                    var newAmount:String!
                    
                    if let doubleAmount = Double(amount) {
                        newAmount = String(format: "%.2f", doubleAmount)
                    }else{
                        newAmount = amount
                    }
                    
                    let wait = appDel.topViewController?.addWaitSpinner()
                    
                    tabBarVC?.callMakeBidApi(userId: userId, postId: postId, amount: newAmount, callback: {  (msg) in
                        
                        if let wait = wait {
                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                        }
                        
                        if let message = msg {
                            self.displayMessage(message)
                        }
                    })
                }
                
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    //MARK:- onFeedback Click
    @IBAction func onFeedbackClick(_ sender: Any) {
        
        if let vc = UIStoryboard(name: "Feedback", bundle: nil).instantiateViewController(withIdentifier: "PostFeedbacksViewController") as? PostFeedbacksViewController {
            
            vc.post = self.post
            
            self.dismiss(animated: true) {
                appDel.topViewController?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func onProfileImageClick(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
            guard let postUserId = self.post.post_user_id else {
                return;
            }
            
            if postUserId != MyProfile.userId {
            
                let userDetails = self.post.userDetails
                
                if let id = userDetails.first?._id {
                    
                    if id == MyProfile.userId {
                        return;
                    }
                    
                    if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
                        
                        vc.userId = id
                        
                        let nav = UINavigationController(rootViewController: vc)
                        //Presenting navigation Controller
                        nav.modalTransitionStyle = .crossDissolve
                        appDel.topViewController?.present(nav, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
}
