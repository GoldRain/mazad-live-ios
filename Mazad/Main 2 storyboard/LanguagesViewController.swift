//
//  LanguagesViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 31/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class LanguagesViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView:UITableView!
    
    var langNames = [String]()
    var langCodes = [String]()
    
    var selectedIndexPath:IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        let str = NSLocalizedString("Languages", comment: "Languages")
        
        self.title = str
        
        self.langNames.removeAll()
        
        for languageCode in Bundle.main.localizations.filter({ $0 != "Base" }) {
            if let langName = Locale.current.localizedString(forLanguageCode: languageCode){
                self.langNames.append(langName)
            }
            self.langCodes.append(languageCode)
        }
        
        if Locale.preferredLanguages.count > 0{
            
            if let index = self.langCodes.index(of: Locale.preferredLanguages[0]){
                self.selectedIndexPath = IndexPath(row: index, section: 0)
            }
        }
        
        let button = UIButton()
        button.setTitle("Done", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(self.onDoneClick(_:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onDoneClick(_ sender:Any){
        
        if let index = self.selectedIndexPath?.row{
            self.changeToLanguage(self.langCodes[index])
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return langNames.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        if let imageView = cell.contentView.viewWithTag(1) as? UIImageView{
            if let currentValue = UserDefaults.standard.value(forKey: CurrentLanguageKey) as? String{
                print("current value = \(currentValue)")
                if currentValue == self.langCodes[indexPath.row]{
                    imageView.image = UIImage(named: "check_box_checked")
                }
                else{
                    imageView.image = UIImage(named: "check_box")
                }
            }
            else{
                imageView.image = UIImage(named: "check_box")
            }
        }
        if let label = cell.contentView.viewWithTag(2) as? UILabel{
            label.text = self.langNames[indexPath.row]
            let font = UIFont(name: "Avenir Next", size: 15.0)!
            label.font = font
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = self.tableView.cellForRow(at: indexPath)
        
        if let path = self.selectedIndexPath{
            let cell = self.tableView.cellForRow(at: path)
            if let imageView = cell?.contentView.viewWithTag(1) as? UIImageView{
                imageView.image = UIImage(named: "check_box")
            }
        }
        
        self.selectedIndexPath = indexPath
        
        if let imageView = cell?.contentView.viewWithTag(1) as? UIImageView{
            imageView.image = UIImage(named: "check_box_checked")
        }
    }
    
    private func changeToLanguage(_ langCode: String) {
        
        if Bundle.main.preferredLocalizations.first != langCode {
            let message = "In order to change the language, the App must be closed and reopened by you."
            let confirmAlertCtrl = UIAlertController(title: "App restart required", message: message, preferredStyle: .alert)
            
            let confirmAction = UIAlertAction(title: "Close now", style: .destructive) { _ in
                UserDefaults.standard.set([langCode], forKey: AppleLanguageKey)
                UserDefaults.standard.set(langCode, forKey: CurrentLanguageKey)
                UserDefaults.standard.synchronize()
                print("Saved code ==> \(langCode)")
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                    exit(EXIT_SUCCESS)
                })
            }
            confirmAlertCtrl.addAction(confirmAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            confirmAlertCtrl.addAction(cancelAction)
            
            present(confirmAlertCtrl, animated: true, completion: nil)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}
