//
//  PopUpStoryViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 12/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SDWebImage
import ImageViewer

class PopUpStoryViewController: UIViewController, GalleryItemsDataSource, GalleryItemsDelegate {

    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnUserImage: UIButton!
    @IBOutlet weak var imgVerifiedIcon: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var imageMainView: UIView!
    @IBOutlet weak var imgStory: UIImageView!
    @IBOutlet weak var btnWhatsapp: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnPlayVideo: UIButton!
    
    @IBOutlet weak var soldView: UIView!
    @IBOutlet weak var lblSold: UILabel!
    
    
    var story:StoryModel!
    
    var dataImage: [GalleryItem] = [GalleryItem]()
    var galleryController:GalleryViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainContainerView.addShadow()
        imageMainView.addShadow()
        imgStory.addShadow()
        
        setupUI()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let view = touches.first?.view{
            if view != mainContainerView && view != topView && view != imageMainView && view != imgStory {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func setupUI() {
        
        self.btnWhatsapp.isHidden = true
        self.btnCall.isHidden = true
        
        if let isVideo = story.is_video, isVideo {
            self.btnPlayVideo.isHidden = false
        }else {
            self.btnPlayVideo.isHidden = true
        }
        
        if let url = self.story.userDetails.first?.profilePhoto{
            self.btnUserImage.sd_setImage(with: URL(string: url), for: .normal, completed: nil)
        }
        if let name = self.story.userDetails.first?.username{
            self.lblUserName.text = name
        }
        if let storyImageUrl = self.story.url{
            
            self.imgStory.sd_addActivityIndicator()
            
            self.imgStory.sd_setImage(with: URL(string: storyImageUrl.replacingOccurrences(of: ",", with: ""))) { (img, err, cache, url) in
                
                self.imgStory.sd_removeActivityIndicator()
                
                if img == nil{
                    self.imgStory.image = UIImage(named: "ic_placeholder")
                }
                self.imgStory.clipsToBounds = true
            }
        }
        if let date = self.story.created_at{
            self.lblTime.text = String().elapsedTimeFrom(dateValue: date)
        }
        
        if story.userId == MyProfile.userId {
            self.btnWhatsapp.isHidden = true
            self.btnCall.isHidden = true
        }else{
            if let whatsappOnly = self.story.whatsapp_only, whatsappOnly {
                self.btnWhatsapp.isHidden = false
                self.btnCall.isHidden = true
            }else if let whatsappAndCall = self.story.whatsapp_and_call, whatsappAndCall {
                self.btnWhatsapp.isHidden = false
                self.btnCall.isHidden = false
            }
        }
        
        if let user = story.userDetails.first{
            if user.isVerified {
                self.imgVerifiedIcon.image = #imageLiteral(resourceName: "icon_privacy_yellow")
            }else{
                self.imgVerifiedIcon.image = #imageLiteral(resourceName: "icon_privacy_grey")
            }
        }
        
        self.mainContainerView.bringSubview(toFront: self.imageMainView)
        self.imageMainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleImageViewer)))
        
        if let isSold = self.story.is_sold {
            self.soldView.isHidden = !isSold
            self.lblSold.isHidden = !isSold
        }
    }
    
    @objc func handleImageViewer() {
        
        self.showImagesPreview(index: 0)
    }
    
    @IBAction func onPlayVideo(_ sender: Any) {
        
        guard let imageUrl = story.url else {
            return
        }
        let videoUrl = imageUrl.replacingOccurrences(of: ".jpg", with: ".mp4")
        
        let player = AVPlayer(url: URL(string: videoUrl)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        
        if let top = appDel.topViewController {
            top.present(playerController, animated: true) {
                player.play()
            }
        }
    }
    
    @IBAction func onClickAction(_ sender: UIButton) {
        
        guard let phone = self.story.userDetails.first?.phone, let countryCode = self.story.userDetails.first?.country_code else{
            let str = NSLocalizedString("Phone number Not Found!", comment: "Phone number Not Found!")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        switch sender {
        case btnWhatsapp:
            openWhatsApp(phoneNumber: countryCode + phone, message: "Hello")
        case btnCall:
            openCallingApp(phoneNumber:countryCode + phone)
        default:
            break
        }
    }
    
    @IBAction func onUserImageClick(_ sender: Any) {
        
        
    }
    
    //MARK:- GalleryItemsDataSource
    func itemCount() -> Int {
        
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    //MARK: Image preview
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        if let img = story.url, img != ""{
            
            let galleryItem = GalleryItem.image { imageCompletion in
                
                SDWebImageDownloader.shared().downloadImage(with: URL(string: img), options: [], progress: nil, completed: { (img, data, err, fin) in
                    
                    if let image = img{
                        imageCompletion(image)
                    }
                })
            }
            self.dataImage.append(galleryItem)
        }
        
        guard dataImage.count > 0 else{
            if let top = appDel.topViewController{
                top.displayMessage("Image Not Found!!")
            }
            return
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
    }
}
