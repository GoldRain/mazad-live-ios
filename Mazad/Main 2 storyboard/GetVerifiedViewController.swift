//
//  GetVerifiedViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 19/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class GetVerifiedViewController: UIViewController {

    @IBOutlet weak var imgDownHand: UIImageView!
    @IBOutlet weak var imgVerified: UIImageView!
    @IBOutlet weak var imgProfileImage: UIImageView!
    
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var btnContinue: UIButton!
    
    var callback:((Bool)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let str = NSLocalizedString("Continue", comment: "Continue")
        
        btnContinue.setTitle(str, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let url = MyProfile.userProfilePhoto {
            self.imgProfileImage.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "icon_profile_grey2"), options: [], completed: nil)
        }
        setupUI()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view != self.alertView {
                callback!(false)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
 
    func setupUI(){
        
        UIView.animate(withDuration: 0.6, delay: 0.0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
            self.imgDownHand.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -25)
        }){ (fin) in
            self.imgDownHand.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 0)
        }
        
        let animation1 = CABasicAnimation(keyPath: "transform.scale")
        animation1.duration = 0.6
        animation1.repeatCount = .infinity
        animation1.fromValue = 1.3
        animation1.toValue = 0.6
        animation1.autoreverses = true
        self.imgVerified.layer.add(animation1, forKey: "transform.scale")
    }
    
    @IBAction func onContinueClick(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifiedDocumentViewController") as? VerifiedDocumentViewController {
                
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.callback = { fin in
                    self.callback!(fin)
                }
                appDel.topViewController?.navigationController?.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onLaterClick(_ sender: Any) {
        self.callback!(false)
        self.dismiss(animated: true, completion: nil)
    }
}
