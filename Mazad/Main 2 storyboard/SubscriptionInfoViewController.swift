//
//  SubscriptionInfoViewController.swift
//  Mazad Live
//
//  Created by alienbrainz on 22/05/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class SubscriptionInfoViewController: UIViewController {

    @IBOutlet weak var lblMessage: UILabel!
    
    var message:String!
    
    var callback:((Bool, String?)->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblMessage.text = self.message
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onCloseClick(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.callback(false, nil)
        })
    }
    
    @IBAction func onPrivacyPolicyClick(_ sender: Any) {
        self.dismiss(animated: true) {
            self.callback(false, "privacy")
        }
    }
    
    @IBAction func onTermsAndConditions(_ sender: Any) {
        self.dismiss(animated: true) {
            self.callback(false, "terms")
        }
    }
    
    @IBAction func onContinueClick(_ sender: Any) {
        self.dismiss(animated: true) {
            self.callback(true, nil)
        }
    }
    
}
