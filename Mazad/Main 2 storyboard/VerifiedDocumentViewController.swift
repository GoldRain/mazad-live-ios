//
//  VerifiedDocumentViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 19/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit
import DropDown

class VerifiedDocumentViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var idTypeView: UIView!
    @IBOutlet weak var txtIDType: UITextField!
    @IBOutlet weak var txtIdNumber: UITextField!
    @IBOutlet weak var btnUploadDocument: UIButton!
    @IBOutlet weak var imgMainView: UIView!
    @IBOutlet weak var imgDocumentImage: UIImageView!
    
    private var documentImage = [UIImage]()
    
    let idTypeDropDown = DropDown()
    let typeID = ["Driving Liscence", "Passport", "Other ID"]
    
    var callback:((Bool)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.idTypeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onIdTypeClick)))
        
        self.imgMainView.addShadow()
    }
    
    @objc func onIdTypeClick() {
        
        idTypeDropDown.dataSource = self.typeID
        idTypeDropDown.anchorView = self.idTypeView
        idTypeDropDown.backgroundColor = UIColor(red:0.91, green:0.94, blue:0.97, alpha:1.0)
        
        self.idTypeDropDown.bottomOffset = CGPoint(x: 0, y:(self.idTypeDropDown.anchorView?.plainView.bounds.height)!)
        self.idTypeDropDown.topOffset = CGPoint(x: 0, y: -(self.idTypeDropDown.anchorView?.plainView.bounds.height)!)
        
        self.idTypeDropDown.selectionAction = { (index: Int, item: String)  in
            if item == "Driving Liscence" {
                self.txtIDType.text = item
            }else if item == "Passport" {
                self.txtIDType.text = item
            }else{
                self.txtIDType.text = item
            }
        }
        self.idTypeDropDown.show()
    }

    @IBAction func onCloseClick(_ sender: Any) {
        callback!(false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onAddImageClick(_ sender: Any) {
        
        let alertViewController:UIAlertController = getAlertController(title: nil, message: "Select image of your Id Document")
        
        let cameraAction = UIAlertAction(title: "Take photo", style: .default, handler: { _ in
            let imagePicker = UIImagePickerController()
            imagePicker.restorationIdentifier = "document"
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            alertViewController.dismiss(animated: true, completion: nil)
            self.present(imagePicker, animated: true, completion: nil)
        })
        
        let photoAction = UIAlertAction(title: "Choose photo", style: .default, handler: { _ in
            let imagePicker = UIImagePickerController()
            imagePicker.restorationIdentifier = "document"
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            alertViewController.dismiss(animated: true, completion: nil)
            self.present(imagePicker, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "I will do this later", style: .destructive, handler: { _ in
            
            alertViewController.dismiss(animated: true, completion: {
                
            })
            self.dismiss(animated: true, completion: nil)
        })
        alertViewController.addAction(cameraAction)
        alertViewController.addAction(photoAction)
        alertViewController.addAction(cancelAction)
        self.present(alertViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func onUploadDocument(_ sender: UIButton) {
        
        if self.validate() {
            // upload image
            guard let userId = MyProfile.userId else{
                return;
            }
            
            let wait = appDel.topViewController?.addWaitSpinner()
            let randomName = randomString(length: 20)
            
            uploadImagesToAws(self.documentImage, folderName: appDel.documentVerificationFolder!, randomName: randomName) { (urls) in
                
                if let url = urls?.first{
                    
                    let params = ["user_id":userId,
                                  "id_type":self.txtIDType.text!,
                                  "id_number":self.txtIdNumber.text!,
                                  "image_url":url,
                                  ]
                    JSONRequest.makeRequest(kUserVerification, parameters: params, callback: {  (data, error) in
                        
                        if let wait = wait {
                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                        }
                        
                        if let data = data as? [String:Any] {
                            if let err = data["error"] as? Bool, !err {
                                if let des = data["description"] as? [String:Any]{
                                    if let verificationData = des["verification"] as? [String:Any] {
                                        let verification = VerificationModel(data: verificationData)
                                        
                                        MyProfile.verification = verification
                                        
                                        self.callback!(true)
                                        self.dismiss(animated: true, completion: {
                                            let str = NSLocalizedString("Document Uploaded Successfully!", comment: "Document Uploaded Successfully!")
                                            appDel.topViewController?.displayMessage(str)
                                        })
                                    }
                                }
                            }else{
                                self.dismiss(animated: true, completion: {
                                    let str = NSLocalizedString("Failed to upload Document", comment: "Failed to upload Document")
                                    appDel.topViewController?.displayMessage(str)
                                })
                            }
                        }else{
                            self.dismiss(animated: true, completion: {
                                let str = NSLocalizedString("Failed to upload Document", comment: "Failed to upload Document")
                                appDel.topViewController?.displayMessage(str)
                            })
                        }
                    })
                }else{
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                }
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImage: UIImage?
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage{
            
            selectedImage = image
            self.imgDocumentImage.image = image
            
        }else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            selectedImage = image
            self.imgDocumentImage.image = image
        }
        
        self.documentImage.append(selectedImage!)
        
        self.btnUploadDocument.setTitle("Upload Document", for: .normal)
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        dismiss(animated: true, completion: nil)
        
    }
    
    func validate() -> Bool{
        
        if let id = self.txtIDType.text, id.trim() == "" {
            self.idTypeView.shake()
            return false
        }
        if let number = self.txtIdNumber.text, number.trim() == "" {
            self.txtIdNumber.shake()
            return false
        }
        return true
    }
}
extension UIView{
    
    func shake() {
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
}
