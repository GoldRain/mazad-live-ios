//
//  SelectLanguageViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 04/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class SelectLanguageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var listMainView: UIView!
    
    var lang = ["ENGLISH", "ARABIC"]
    var callback:((String)->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        
        tableView.separatorStyle = .singleLine
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lang.count
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let view = touches.first?.view{
            if view != listMainView || view != tableView{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SelectLanguageCell
        cell.lblLang.text = lang[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! SelectLanguageCell
        let text = cell.lblLang.text
        callback(text!)
        self.dismiss(animated: true, completion: nil)
    }
}

class SelectLanguageCell: UITableViewCell {
    
    @IBOutlet weak var lblLang:UILabel!
}
