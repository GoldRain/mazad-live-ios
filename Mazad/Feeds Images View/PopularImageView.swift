//
//  FeedsImagesView.swift
//  P2P Trend
//
//  Created by Bodacious on 07/11/17.
//  Copyright © 2017 webblue tech. All rights reserved.
//

import UIKit
import AVFoundation

enum CardImageMode{
    case one
    case two
    case three
    case four
}

class PopularImageView: UIView {
    
    var post:BuyerSellerModel!
    
    var indexPath:IndexPath!
    
    @IBOutlet var cards2:UIView!
    @IBOutlet var cards3:UIView!
    @IBOutlet var cards4:UIView!
    @IBOutlet weak var singleImage: UIImageView!
    
    var videoValue:Int!
    var mode:CardImageMode!
    
    func setCardMode(mode:CardImageMode){
        
        self.mode = mode
        
        switch mode {
        case .two:
            self.addSubViewWithConstraints(view: self.cards2)
            self.bringSubview(toFront: self.cards2)
            self.cards2.isHidden = false
            
        case .three:
            self.addSubViewWithConstraints(view: self.cards3)
            self.bringSubview(toFront: self.cards3)
            self.cards3.isHidden = false
            
        case .four:
            self.addSubViewWithConstraints(view: self.cards4)
            self.bringSubview(toFront: self.cards4)
            self.cards4.isHidden = false
            
        default:
            self.sendSubview(toBack: self.cards2)
            self.sendSubview(toBack: self.cards3)
            self.sendSubview(toBack: self.cards4)
            self.singleImage.isHidden = false
        }
    }
    
    func getImageView(tag:Int) -> UIImageView?{
        
        if self.mode == CardImageMode.one{
            return self.singleImage
        }
        
        var parent:UIView? = self.subviews.first
        switch mode! {
        case .two:
            parent = cards2
        case .three:
            parent = cards3
        case .four:
            parent = cards4
        default: break
            
        }
        
        if let v = parent?.viewWithTag(tag) as? UIImageView{
            return v
        }
        self.backgroundColor = .red
        return nil
    }
    
    func setMoreImage(count:Int){
        
        if let lastImage = self.getImageView(tag: 4){
            
            lastImage.superview?.viewWithTag(1002)?.removeFromSuperview()
            let image = UIImage(named: "more_\(count)")
            let imageView = UIImageView(image: image)
            imageView.tag = 1002
            imageView.contentMode = .scaleAspectFill
            lastImage.superview?.addSubViewWithConstraints(view: imageView)
            
        }
    }
}




