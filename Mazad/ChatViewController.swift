

import UIKit
import IQKeyboardManagerSwift

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tableView:UITableView!
    
    @IBOutlet weak var txtFieldMsg: UITextField!
    
    @IBOutlet weak var txtFieldMainViewBottomConstraint: NSLayoutConstraint!
    
    var room:Room?
    
    var chatMessages = [ChatMessage]()
    
    var otherId:String!
    
    var userName:String!
    
    var keyboardHeight = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "Chat"
        self.title = userName
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        if let _ = self.room{
            self.getMessages()
        }
        else{
            self.getRoom()
        }
        
        self.txtFieldMsg.delegate = self
        
        self.txtFieldMsg.returnKeyType = .send
        
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    func getRoom(){
        
        messageCenter?.getRoomFromLocal(otherUserId: otherId, roomId: nil, callback: {  (room) in
            self.room = room
            self.getMessages()
        })
        
    }
    
    func getMessages(){
        
        if let room = room {
            
            self.chatMessages = (room.messages?.allObjects as? [ChatMessage])!
            
            self.chatMessages.sort(by: { (msg1, msg2) -> Bool in
                return msg1.createdAt?.compare(msg2.createdAt!) == .orderedAscending
            })
            
            mainQueue.async {
                
                self.tableView.reloadData()
                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                    if self.chatMessages.count > 0 {
                        let indexPath = IndexPath(row: self.chatMessages.count-1, section: 0)
                        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    }
                }
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onMessageReceived(notification:)), name: .OnMessageRecieved, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: .UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification:Notification) {
        
        if let keyboardRectValue = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            keyboardHeight = Double(keyboardRectValue.height)
            
            self.txtFieldMainViewBottomConstraint.constant = CGFloat(self.keyboardHeight)
            
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
                if self.chatMessages.count > 0{
                    let indexPath = IndexPath(row: self.chatMessages.count-1, section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
    @objc fileprivate func keyboardWillHide(notification:NSNotification) {
        
        self.txtFieldMainViewBottomConstraint.constant = 0
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    func updateChatMessages() {
        
        messageCenter?.getRoomFromLocal(otherUserId: otherId, roomId: nil, callback: {  (room) in
            
            if let room = room {
                self.chatMessages = (room.messages?.allObjects as? [ChatMessage])!
                self.chatMessages.sort(by: { (msg1, msg2) -> Bool in
                    return msg1.createdAt?.compare(msg2.createdAt!) == .orderedAscending
                })
                mainQueue.async {
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.25) {
                        if self.chatMessages.count > 0 {
                            let indexPath = IndexPath(row: self.chatMessages.count-1, section: 0)
                            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    @objc func onMessageReceived(notification: Notification) {
        
        if let data = notification.userInfo as? [String:ChatMessage]{
            
            if let message = data["message"] {
                
                if chatMessages.contains(where: { (msg) -> Bool in
                    if msg.mongoId == message.mongoId {
                        return true
                    }
                    return false
                }){// do nothing
                    
                }else {
                    // add message
                    self.chatMessages.append(message)
                }
                self.addAnotherRow()
                self.room?.markRead()
            }
        }
    }
    
    func addAnotherRow() {
        let indexPath = IndexPath(item: chatMessages.count - 1, section: 0)
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
        self.updateContentInset(for: tableView, animated: true)
        
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    func updateContentInset(for tableView: UITableView?, animated: Bool) {
        var lastRow: Int? = nil
        if let aView = tableView {
            lastRow = self.tableView(aView, numberOfRowsInSection: 0)
        }
        
        let lastIndex: Int = (lastRow ?? 0) > 0 ? (lastRow ?? 0) - 1 : 0
        
        let lastIndexPath = IndexPath(item: lastIndex, section: 0)
        let lastCellFrame: CGRect = tableView!.rectForRow(at: lastIndexPath)
        
        let _ = max(self.tableView.frame.height - lastCellFrame.origin.y - lastCellFrame.height, 0)
        
        let _: UIEdgeInsets? = tableView?.contentInset
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        NotificationCenter.default.removeObserver(self, name: .OnMessageRecieved, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        
        
        if let room = self.room{
            room.markRead()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message = chatMessages[indexPath.row]
        
//        created at Optional(2018-12-26 04:43:32 +0000)
        if message.senderId == MyProfile.userId {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "senderCell", for: indexPath) as! ChatViewControllerSenderCell
            
            cell.lblMessage.text = message.text
            if let createdAt = message.createdAt {
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm"
                let strDate = formatter.string(from: createdAt)
                cell.lblTime.text = strDate
            }
            return cell
            
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiverCell", for: indexPath) as! ChatViewControllerSenderCell
            
            cell.lblMessage.text = message.text
            if let createdAt = message.createdAt {
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm"
                let strDate = formatter.string(from: createdAt)
                cell.lblTime.text = strDate
            }
            return cell
            
        }
//        else if indexPath.row == 2 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "senderImageCell", for: indexPath)
//            return cell
//        }else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "receiverImageCell", for: indexPath)
//            return cell
//        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //txtFieldMsg.resignFirstResponder()
        onSendClick(nil)
        return true
    }
    
    @IBAction func onSendClick(_ sender:Any?) {
        
        if let room = self.room{
            self.sendMessage(room:room)
        }
        else{
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            messageCenter?.createRoom(name: "Room", otherUserId: self.otherId, callback: {  (room) in
                
                mainQueue.async {
                    
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    
                    if let room = room{
                        self.room = room
                        self.sendMessage(room:room)
                    }
                }
            })
        }
    }
    
    func sendMessage(room:Room){
        
        let tempId = (room.messages?.count ?? 0) + 1
        
        guard let msg = self.txtFieldMsg.text?.trim(), msg != "" else {
            return
        }
        
        if let msg = messageCenter?.sendTextMessage(room: room, tempId: "\(tempId)", sentStatus: "sending", text: msg, contentType: MessageTypeText, callback: { (msg) in
            
            //update message in chat list
            //callback when message is sent
            if let updatedMessage = msg {
                if let index = self.chatMessages.index(where: { (message) -> Bool in
                    return message.mongoId == updatedMessage.mongoId
                }){
                    //update the message data
                    let message = self.chatMessages[index]
                    let indexPath = IndexPath(row: index, section: 0)
                    if let cell = self.tableView.cellForRow(at: indexPath) as? ChatViewControllerSenderCell{
                        if let status = message.sentStatus{
                            if status == "sent"{
                                
                            }
                        }
                        cell.setNeedsLayout()
                    }
                }
            }
        }){
            //add message to chat list
            self.chatMessages.append(msg)
            self.addAnotherRow()
            self.txtFieldMsg.text = ""
        }
    }
}

class ChatViewControllerSenderCell: UITableViewCell {
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
}
