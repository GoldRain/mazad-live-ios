

import UIKit
import SwiftyStoreKit

// Non-Consumable
let RemoveAdsPurchase = "RemoveAds1"

// Consumable
let Extra10Post       = "Extra10Post"
let Extra10Stories    = "Extra10Stories"
let Extra10Streams    = "Extra10Streams"

// Non-Consumable
let UnlimitedPost     = "UnlimitedPost1"
let UnlimitedStories  = "UnlimitedStories1"
let UnlimitedStreams  = "UnlimitedLiveStreams1"

// Non-renewable
//let UnlimitedPostForOneMonth       = "UnlimitedPostForOneMonth"
//let UnlimitedStoriesForOneMonth      = "UnlimitedStoriesForOneMonth"
//let UnlimitedLiveStreamsForOneMonth  = "UnlimitedLiveStreamsForOneMonth"

// Auto-renewablesss
let UnlimitedPostForOneMonth         = "AutoRenewableUnlimitedPostsForOneMonth1"
let UnlimitedStoriesForOneMonth      = "AutoRenewableUnlimitedStoriesForOneMonth1"
let UnlimitedLiveStreamsForOneMonth  = "AutoRenewableUnlimitedLiveStreamsForOneMonth1"


class UpgradeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var removeAdsView: UIView!
    
    @IBOutlet weak var removeAdsPrice: UILabel!
    
    @IBOutlet weak var RemoveAdsText: UILabel!
    
    let imagesArr = [#imageLiteral(resourceName: "img1"),#imageLiteral(resourceName: "img2"),#imageLiteral(resourceName: "img3"),#imageLiteral(resourceName: "img4"),#imageLiteral(resourceName: "img5"),#imageLiteral(resourceName: "img6")]
    
    var typeArr = ["Extra 10 Post",
                   "Unlimited Post",
                   "Extra 10 Stories",
                   "Unlimited Stories",
                   "Extra 10 Streams",
                   "Unlimited Live Streams",
                   "Remove Ads"
                   ]
    
    var descTexts = ["You will get 10 post count after purchase",
                     "You will get unlimited post count",
                     "You will get 10 story count",
                     "You will get unlimited stories count",
                     "You will get 10 LiveStream count",
                     "You will get unlimited LiveStreams Count",
                     "Ads will not be shown"]
    
    var amounts = ["3","29","3","29","3","29", "3"]
    
    var selectionCount = ["10", "-1","10", "-1","10", "-1", ""]
    
    var ProductIds = [Extra10Post, UnlimitedPostForOneMonth, Extra10Stories, UnlimitedStoriesForOneMonth, Extra10Streams, UnlimitedLiveStreamsForOneMonth,                                                                                  RemoveAdsPurchase]
    
    var selectionType = ["post", "post", "story", "story", "live", "live", "ads"]
    
    var itemCounts = [CountModel]()
    
    var removeAds:Decimal = 3.0
    
    var paymentType:upgradeType!
    
    var selectedCount = ""
    var selectedType = ""
    var selectedAmount = ""
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let title = NSLocalizedString("Upgrade", comment: "Upgrade")
        let title = "Upgrade"
        self.title = title
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if MyProfile.showAds {
            removeAdsView.isHidden = false
        }else{
            removeAdsView.isHidden = true
        }
        
        self.navigationController?.isNavigationBarHidden = false
        
        self.callShowCountApiAndReload()
        
        self.removeAdsPrice.text = ""
        self.RemoveAdsText.text = ""
        
        self.removeAdsView.addWaitView(height:20, width: 20)
        
        let ProductId = self.ProductIds[self.ProductIds.count-1]
        SwiftyStoreKit.retrieveProductsInfo([ProductId]) { result in
            self.removeAdsView.removeWaitView()
            
            if let product = result.retrievedProducts.first {
                self.removeAdsPrice.text = product.localizedPrice
                self.RemoveAdsText.text = product.localizedTitle
            }
            else{
                self.displayMessage("Failed to get purchase. Please try again later.", callback: { [unowned self] () -> (Void) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    func callShowCountApiAndReload() {
        
        guard let userId = MyProfile.userId else {
            return;
        }
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kShowCounts, parameters: ["user_id":userId], callback: { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    if let des = data["description"] as? [String:Any] {
                        
                        self.itemCounts.removeAll()
                        
                        if let postDict = des["unlimited_post_count"] as? [String:Any] {
                            self.itemCounts.append(CountModel(data: postDict))
                            self.itemCounts.append(CountModel(data: postDict))
                            
//                            let postCount = CountModel(data: postDict)
//
//                            // Check if user is subscribed or not
//                            if postCount.status{
//                                MyProfile.isSubscribedUser = true
//                            }
                        }
                        
                        if let storyDict = des["unlimited_story_count"] as? [String:Any] {
                            self.itemCounts.append(CountModel(data: storyDict))
                            self.itemCounts.append(CountModel(data: storyDict))
                            
//                            let storyCount = CountModel(data: storyDict)
//
//                            // Check if user is subscribed or not
//                            if storyCount.status{
//                                MyProfile.isSubscribedUser = true
//                            }
                        }
                        
                        if let liveDict = des["unlimited_live_count"] as? [String:Any] {
                            self.itemCounts.append(CountModel(data: liveDict))
                            self.itemCounts.append(CountModel(data: liveDict))
                            
//                            let liveCount = CountModel(data: liveDict)
//
//                            // Check if user is subscribed or not
//                            if liveCount.status{
//                                MyProfile.isSubscribedUser = true
//                            }
                        }
                        
                        self.collectionView.reloadData()
                    }
                }
            }
        })
    }
    
    func updatingLocalArray(index:Int) {
        
        for _ in 0...1 {
            self.typeArr.remove(at: index)
            self.descTexts.remove(at: index)
            self.amounts.remove(at: index)
            self.selectionType.remove(at: index)
            self.selectionCount.remove(at: index)
            self.ProductIds.remove(at: index)
        }
    }
    
    //MARK:- Collection View method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemCounts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        if let lblExpiresOn = cell.contentView.viewWithTag(5) as? UILabel {
            lblExpiresOn.isHidden = true
        }
        
        if let bgImageView = cell.contentView.viewWithTag(1) as? UIImageView{
            bgImageView.image = self.imagesArr[indexPath.row]
        }
        if let title = cell.contentView.viewWithTag(2) as? UILabel{
            title.text = ""
        }
        if let lblCount = cell.contentView.viewWithTag(4) as? UILabel {
            lblCount.text = ""
        }
        
        if let lblAmount = cell.contentView.viewWithTag(3) as? UILabel {
            lblAmount.text = ""
            
            cell.contentView.addWaitView()
            
            let ProductId = self.ProductIds[indexPath.row]
            SwiftyStoreKit.retrieveProductsInfo([ProductId]) { result in
                
                cell.contentView.removeWaitView()
                
                if let product = result.retrievedProducts.first {
                    
                    lblAmount.text = product.localizedPrice
                    
                    if let title = cell.contentView.viewWithTag(2) as? UILabel{
                        title.text = product.localizedTitle
                    }
                    
                    if let lblCount = cell.contentView.viewWithTag(4) as? UILabel {
                        if self.itemCounts.count > 0 {
                            if let count = self.itemCounts[indexPath.row].count {
                                lblCount.text = "Available Count " + "\(count)"
                            }
                        }
                    }
                    
                    if indexPath.row % 2 != 0 {
                        if let lblExpiresOn = cell.contentView.viewWithTag(5) as? UILabel {
                            if let statusOfSubscription = self.itemCounts[indexPath.row].status, statusOfSubscription {
                                if let expiresOn = self.itemCounts[indexPath.row].expiry {
                                    if let doubleDate = Double(expiresOn) {
                                        
                                        let date = Date(timeIntervalSince1970: doubleDate / 1000.0)
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "dd MMM YYYY"
                                        
                                        lblExpiresOn.isHidden = false
                                        lblExpiresOn.text = "Expires on: " + dateFormatter.string(from: date)
                                        
                                        if let lblCount = cell.contentView.viewWithTag(4) as? UILabel {
                                            lblCount.text = "Unlimited"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    self.displayMessage("Failed to get purchase. Please try again later.", callback: { [unowned self] () -> (Void) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (self.view.frame.width-48) / 2
        return CGSize(width: size , height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let ProductId = self.ProductIds[indexPath.row]
        self.selectedCount = self.selectionCount[indexPath.row]
        self.selectedType = self.selectionType[indexPath.row]
        
        switch ProductId {
        case "AutoRenewableUnlimitedPostsForOneMonth1", "AutoRenewableUnlimitedStoriesForOneMonth1", "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
            
            let alert = getAlertController(title: nil, message: nil)
            
            alert.addAction(UIAlertAction(title: "Purchase", style: .default, handler: { (_) in
                
//                if let statusOfSubscription = self.itemCounts[indexPath.row].status, statusOfSubscription {
//                    appDel.topViewController?.displayMessage("You already have Subscription for One Month")
//                    return;
//                }
                
                self.startPurchase(ProductId, indexPath)
            }))
            
            alert.addAction(UIAlertAction(title: "Restore Purchase", style: .default, handler: { (_) in
//                self.startRestorePurchase(ProductId, indexPath)
                
                // For non-renewing subscription case restore purchase should be handle on server side
//                self.callShowCountApiAndReload()
                
                guard let userId = MyProfile.userId else{
                    return;
                }
                let wait = self.addWaitSpinner()
                
                JSONRequest.makeRequest(kRestorePurchase, parameters: ["user_id":userId, "product_id":ProductId], callback: { (data, error) in
                    DispatchQueue.main.async {
                        self.removeWaitSpinner(waitView: wait)
                        self.callShowCountApiAndReload()
                    }
                })
                
//                SwiftyStoreKit.fetchReceipt(forceRefresh: true) { result in
//                    switch result {
//                    case .success(let receiptData):
//                        let encryptedReceipt = receiptData.base64EncodedString(options: [])
//                        appDel.printMessage("RESTORE encryptedReceipt =>> \(encryptedReceipt)")
//                            self.callTest(receipt: encryptedReceipt, transactionId: "", productId: "AutoRenewableUnlimitedPostsForOneMonth1", callback:{ data in
//
////                            if let data = data as? [String:Any] {
////                                if let update = data["update"] as? Bool, update {
////                                    self.callShowCountApiAndReload()
////                                }
////                            }
//                        })
//                    case .error(let error):
//                        appDel.printMessage("error", error)
//                    }
//                }*
            }))
            
            let openSubscriptionInfo = UIAlertAction(title: "Subscription Info", style: .default) { (_) in
                
                var msg = ""
                
                switch ProductId {
                case "AutoRenewableUnlimitedPostsForOneMonth1":
                    msg = "Posts"
                case "AutoRenewableUnlimitedStoriesForOneMonth1":
                    msg = "Stories"
                case "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
                    msg = "Live Streams"
                
                default:
                    appDel.printMessage("Do nothing")
                }
                let title = "Subscription Info"
                let message = "• Get Unlimited \(msg) to create.\n• $4.99 will be charged to iTunes Account at confirmation of purchase.\n• Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period.\n• Account will be charged with $4.99 for auto-renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal.\n• Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user's Account Settings after purchase."
                
                self.displayMessage(title: title, msg: message, callback: { () -> (Void) in
                    
                })
            }
            
            //alert.addAction(openSubscriptionInfo)
            //
            
            let dataPrivacy = UIAlertAction(title: "Privacy Policy", style: .default) { (_) in
                self.openPrivacyPolicy()
            }
            
            //alert.addAction(dataPrivacy)
            
            //
            let termOfUse = UIAlertAction(title: "Terms and Conditions", style: .default) { (_) in
                self.openTermsAndConditions()
            }
            
            //alert.addAction(termOfUse)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
                
            }
            cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        default:
            self.startPurchase(ProductId, indexPath)
        }
    }
    
    func openTermsAndConditions() {
        if let vc = UIStoryboard(name:"Main2", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionViewController") as? TermsAndConditionViewController {
            
            vc.title = "Terms and Conditions"
            
            if let currentValue = UserDefaults.standard.value(forKey: CurrentLanguageKey) as? String{
                appDel.printMessage("current value = \(currentValue)")
                
                if currentValue == "en"{ // for english
                    vc.url = URL(string: kTermsAndConditionEn)
                }else{ // for arabic
                    vc.url = URL(string: kTermsAndConditionAr)
                }
            }else{
                vc.url = URL(string: kTermsAndConditionEn)
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func openPrivacyPolicy() {
        if let vc = UIStoryboard(name:"Main2", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionViewController") as? TermsAndConditionViewController {
            
            vc.title = "Privacy Policy"
            
            if let currentValue = UserDefaults.standard.value(forKey: CurrentLanguageKey) as? String{
                
                if currentValue == "en"{
                    vc.url = URL(string: kDataPrivacyEn)
                }else{
                    vc.url = URL(string: kDataPrivacyAr)
                }
            }else{
                vc.url = URL(string: kDataPrivacyEn)
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    fileprivate func startPurchase(_ ProductId: String, _ indexPath: IndexPath) {
        
        appDel.printMessage("start purchase called")
        
        let wait1 = appDel.topViewController!.addWaitSpinner()
        
        appDel.printMessage("wait 1 added")
        
        SwiftyStoreKit.retrieveProductsInfo([ProductId]) { result in
            
            appDel.printMessage("retrieving product info")
            
            appDel.topViewController?.removeWaitSpinner(waitView: wait1)
            
            appDel.printMessage("wait 1 removed")
            
            if let product = result.retrievedProducts.first {
                
                appDel.printMessage("found first product")
                
                if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PurchaseDescriptionViewController") as? PurchaseDescriptionViewController{
                    
                    appDel.printMessage("purchase des vc presented")
                    
                    vc.localize = false
                    
                    switch ProductId {
                    case "AutoRenewableUnlimitedPostsForOneMonth1", "AutoRenewableUnlimitedStoriesForOneMonth1", "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
                            vc.btnTitle = "Continue"
                    default:
                            vc.btnTitle = "Proceed to Pay"
                    }
                    
                    vc.purchaseData = PurchaseData(title: product.localizedTitle,
                                                   amount: product.localizedPrice ?? "KD\(product.price)",
                                                    image: self.imagesArr[indexPath.row],
                                                    descText: product.localizedDescription)
                    vc.callback = {
                        
                        appDel.printMessage("purchase des vc dismissed")
                        
                        switch ProductId{
                            
                        case "AutoRenewableUnlimitedPostsForOneMonth1", "AutoRenewableUnlimitedStoriesForOneMonth1", "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
                            
                            appDel.printMessage("auto renewable package case entered")
                            
                            var msg = ""
                            
                            switch ProductId {
                            case "AutoRenewableUnlimitedPostsForOneMonth1":
                                msg = "Posts"
                            case "AutoRenewableUnlimitedStoriesForOneMonth1":
                                msg = "Stories"
                            case "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
                                msg = "Live Streams"
                                
                            default:
                                appDel.printMessage("Do nothing")
                            }
                            
                            let message = "• Get Unlimited \(msg) to create.\n• $4.99 will be charged to iTunes Account at confirmation of purchase.\n• Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period.\n• Account will be charged with $4.99 for auto-renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal.\n• Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user's Account Settings after purchase."
                            
                            appDel.printMessage("display message presented")
                            
                            if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionInfoViewController") as? SubscriptionInfoViewController {
                                
                                vc.modalTransitionStyle = .crossDissolve
                                vc.modalPresentationStyle = .overCurrentContext
                                
                                vc.message = message
                                
                                vc.callback = { res, msg in
                                    
                                    if res{
                                        
                                        appDel.printMessage("display message dismissed")
                                        
                                        var wait2:WaitView?
                                        
                                        DispatchQueue.main.async {
                                            appDel.printMessage("wait 2 added")
                                            wait2 = appDel.topViewController?.addWaitSpinner()
                                        }
                                        
                                        self.paymentType = upgradeType.count
                                        appDel.printMessage("SwiftyStoreKit Purchase Product starts ")
                                        SwiftyStoreKit.purchaseProduct(product, completion: { (result:PurchaseResult) in
                                            
                                            appDel.printMessage("SwiftyStoreKit Purchase Product Completed ")
                                            
                                            DispatchQueue.main.async {
                                                if let wait2 = wait2 {
                                                    appDel.printMessage("wait 2 removed")
                                                    appDel.topViewController?.removeWaitSpinner(waitView: wait2)
                                                }
                                            }
                                            
                                            appDel.printMessage("result => \(result)")
                                            
                                            if case .success(let purchase) = result {
                                                
                                                appDel.printMessage("result success")
                                                
                                                let downloads = purchase.transaction.downloads
                                                if !downloads.isEmpty {
                                                    SwiftyStoreKit.start(downloads)
                                                    appDel.printMessage("if download is not empty")
                                                }
                                                // Deliver content from server, then:
                                                if purchase.needsFinishTransaction {
                                                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                                                    appDel.printMessage("if needs finish transaction")
                                                }
                                                
                                                appDel.printMessage("switch for product idd")
                                                
                                                switch ProductId {
                                                    
                                                case "AutoRenewableUnlimitedPostsForOneMonth1", "AutoRenewableUnlimitedStoriesForOneMonth1", "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
                                                    // Retrive receipt data from local if not found in local then fetch receipt data and send it to server for validation
                                                    
                                                    appDel.printMessage("auto renewable case found")
                                                    
                                                    var wait3:WaitView?
                                                    
                                                    DispatchQueue.main.async {
                                                        appDel.printMessage("wait 3 added")
                                                        wait3 = self.addWaitSpinner()
                                                    }
                                                    appDel.printMessage("fetch receipt started")
                                                    
                                                    SwiftyStoreKit.fetchReceipt(forceRefresh: false) { result in
                                                        appDel.printMessage("fetch receipt ended \(result)")
                                                        switch result {
                                                        case .success(let receiptData):
                                                            appDel.printMessage("fetch receipt success")
                                                            let encryptedReceipt = receiptData.base64EncodedString(options: [])
                                                            appDel.printMessage("calling test")
                                                            self.callTest(receipt: encryptedReceipt, transactionId: purchase.transaction.transactionIdentifier, productId: ProductId, callback:{ data in
                                                                
                                                                appDel.printMessage("received callback from calling test")
                                                                
                                                                DispatchQueue.main.async {
                                                                    if let wait3 = wait3{
                                                                        appDel.printMessage("wait 3 removed")
                                                                        self.removeWaitSpinner(waitView: wait3)
                                                                    }
                                                                }
                                                                
                                                                if let data = data as? [String:Any] {
                                                                    if let update = data["update"] as? Bool, update {
                                                                        DispatchQueue.main.async {
                                                                            appDel.printMessage("calling show count api")
                                                                            self.callShowCountApiAndReload()
                                                                        }
                                                                    }
                                                                    if let err = data["error"] as? Bool, err {
                                                                        if let msg = data["message"] as? String{
                                                                            if msg == "apple receipt is invalid" {
                                                                                
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            })
                                                        case .error(let error):
                                                            appDel.printMessage("fetch receipt error")
                                                            DispatchQueue.main.async {
                                                                if let wait3 = wait3{
                                                                    appDel.printMessage("wait 3 removed")
                                                                    self.removeWaitSpinner(waitView: wait3)
                                                                }
                                                            }
                                                            if let receiptData = SwiftyStoreKit.localReceiptData {
                                                                let encryptedReceipt = receiptData.base64EncodedString(options: [])
                                                                self.callTest(receipt: encryptedReceipt, transactionId: purchase.transaction.transactionIdentifier, productId: ProductId, callback:{ data in
                                                                    
                                                                    appDel.printMessage("received callback from calling test")
                                                                    
                                                                    DispatchQueue.main.async {
                                                                        if let wait3 = wait3{
                                                                            appDel.printMessage("wait 3 removed")
                                                                            self.removeWaitSpinner(waitView: wait3)
                                                                        }
                                                                    }
                                                                    
                                                                    if let data = data as? [String:Any] {
                                                                        if let update = data["update"] as? Bool, update {
                                                                            DispatchQueue.main.async {
                                                                                appDel.printMessage("calling show count api")
                                                                                self.callShowCountApiAndReload()
                                                                            }
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                            print("error", error)
                                                        }
                                                    }
                                                default: // for extra
                                                    //call api to update count
                                                    guard let userId = MyProfile.userId else {
                                                        return;
                                                    }
                                                    self.callUpdateCountApi(userId: userId, type: self.selectedType, count: self.selectedCount)
                                                }
                                            }
                                            if case .error(let error) = result{
                                                
                                                appDel.topViewController?.displayMessage("Purchase failed [\(error.localizedDescription)]")
                                            }
                                        })
                                        
                                    }
                                    else{
                                        if let mssg = msg{
                                            if mssg == "privacy" {
                                                self.openPrivacyPolicy()
                                            }else if mssg == "terms" {
                                                self.openTermsAndConditions()
                                            }
                                        }
                                    }
                                }
                                
                                self.navigationController?.present(vc, animated: true, completion: nil)
                                
                            }
                        
                        default:
                            
                            var wait2:WaitView?
                            
                            DispatchQueue.main.async {
                                wait2 = appDel.topViewController?.addWaitSpinner()
                            }
                            
                            self.paymentType = upgradeType.count
                            
                            SwiftyStoreKit.purchaseProduct(product, completion: { (result:PurchaseResult) in
                                
                                DispatchQueue.main.async {
                                    if let wait2 = wait2 {
                                        appDel.topViewController?.removeWaitSpinner(waitView: wait2)
                                    }
                                }
                                
                                if case .success(let purchase) = result {
                                    
                                    let downloads = purchase.transaction.downloads
                                    if !downloads.isEmpty {
                                        SwiftyStoreKit.start(downloads)
                                    }
                                    // Deliver content from server, then:
                                    if purchase.needsFinishTransaction {
                                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                                    }
                                    
                                    switch ProductId {
                                        
                                    case "AutoRenewableUnlimitedPostsForOneMonth1", "AutoRenewableUnlimitedStoriesForOneMonth1", "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
                                        // Retrive receipt data from local if not found in local then fetch receipt data and send it to server for validation
                                        
                                        var wait3:WaitView?
                                        
                                        DispatchQueue.main.async {
                                            wait3 = self.addWaitSpinner()
                                        }
                                        
                                        SwiftyStoreKit.fetchReceipt(forceRefresh: false) { result in
                                            switch result {
                                            case .success(let receiptData):
                                                let encryptedReceipt = receiptData.base64EncodedString(options: [])
                                                
                                                self.callTest(receipt: encryptedReceipt, transactionId: purchase.transaction.transactionIdentifier, productId: ProductId, callback:{ data in
                                                    
                                                    DispatchQueue.main.async {
                                                        if let wait3 = wait3{
                                                            self.removeWaitSpinner(waitView: wait3)
                                                        }
                                                    }
                                                    
                                                    if let data = data as? [String:Any] {
                                                        if let update = data["update"] as? Bool, update {
                                                            DispatchQueue.main.async {
                                                                self.callShowCountApiAndReload()
                                                            }
                                                        }
                                                    }
                                                })
                                            case .error(let error):
                                                print("error", error)
                                            }
                                        }
                                        
                                    default: // for extra
                                        //call api to update count
                                        guard let userId = MyProfile.userId else {
                                            return;
                                        }
                                        self.callUpdateCountApi(userId: userId, type: self.selectedType, count: self.selectedCount)
                                    }
                                }
                                if case .error(let error) = result{
                                    appDel.topViewController?.displayMessage("Purchase failed [\(error.localizedDescription)]")
                                }
                            })
                        }
                    }
                    
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
            }else{
                appDel.topViewController?.displayMessage("Failed to get product info. Please try again.")
            }
        }
    }
    
    fileprivate func startRestorePurchase(_ ProductId: String, _ indexPath: IndexPath) {
        
        let wait = appDel.topViewController!.addWaitSpinner()
        
        SwiftyStoreKit.retrieveProductsInfo([ProductId]) { result in
            
            appDel.topViewController?.removeWaitSpinner(waitView: wait)
            
            if let product = result.retrievedProducts.first {
                
                let wait = appDel.topViewController!.addWaitSpinner()
                
                self.paymentType = upgradeType.count
                
                SwiftyStoreKit.purchaseProduct(product, completion: { (result:PurchaseResult) in
                    
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    
                    if case .success(let purchase) = result {
                        let downloads = purchase.transaction.downloads
                        if !downloads.isEmpty {
                            SwiftyStoreKit.start(downloads)
                        }
                        // Deliver content from server, then:
                        if purchase.needsFinishTransaction {
                            SwiftyStoreKit.finishTransaction(purchase.transaction)
                        }
                        
                        //call api to update count
                        guard let userId = MyProfile.userId else {
                            return;
                        }
                        
                        self.callUpdateCountApi(userId: userId, type: self.selectedType, count: self.selectedCount)
                    }
                    if case .error(let error) = result{
                        appDel.topViewController?.displayMessage("Purchase failed [\(error.localizedDescription)]")
                    }
                })
            }
            else{
                appDel.topViewController?.displayMessage("Failed to get product info. Please try again.")
            }
        }
    }
    
    //MARK:-
    @IBAction func onRemoveAdsClick(_ sender: Any) {
        
        let alert = getAlertController(title: nil, message: nil)
        
        alert.addAction(UIAlertAction(title: "Purchase", style: .default, handler: { (_) in
            self.removeAdsPurchase()
        }))
        
        alert.addAction(UIAlertAction(title: "Restore Purchase", style: .default, handler: { (_) in
            self.restoreRemoveAdPurchase()
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
        }
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        alert.addAction(cancelAction )
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func removeAdsPurchase() {
        
        let wait = appDel.topViewController!.addWaitSpinner()
        
        SwiftyStoreKit.retrieveProductsInfo([RemoveAdsPurchase]) { result in
            appDel.topViewController?.removeWaitSpinner(waitView: wait)
            
            if let product = result.retrievedProducts.first {
                
                if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PurchaseDescriptionViewController") as? PurchaseDescriptionViewController{
                    
                    vc.localize = false
                    
                    vc.purchaseData = PurchaseData(title: product.localizedTitle,
                                                   amount: product.localizedPrice ?? "KD\(product.price)",
                        image: self.imagesArr.last ?? UIImage(named: "img1")!,
                        descText: product.localizedDescription)
                    vc.callback = {
                        
                        let wait = appDel.topViewController!.addWaitSpinner()
                        
                        self.paymentType = upgradeType.removeAds
                        
                        SwiftyStoreKit.purchaseProduct(product, completion: { (result:PurchaseResult) in
                            
                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            
                            if case .success(let purchase) = result {
                                let downloads = purchase.transaction.downloads
                                if !downloads.isEmpty {
                                    SwiftyStoreKit.start(downloads)
                                }
                                // Deliver content from server, then:
                                if purchase.needsFinishTransaction {
                                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                                }
                                
                                //call api to remove ads
                                guard let userId = MyProfile.userId else {
                                    return;
                                }
                                
                                self.callRemoveAdsApi(userId: userId)
                            }
                            if case .error(let error) = result{
                                appDel.topViewController?.displayMessage("Purchase failed [\(error.localizedDescription)]")
                            }
                        })
                    }
                    
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
            }else{
                
                appDel.topViewController?.displayMessage("Failed to get product info. Please try again.")
                
            }
        }
    }
    
    func restoreRemoveAdPurchase() {
        
        let wait = appDel.topViewController!.addWaitSpinner()
        
        SwiftyStoreKit.retrieveProductsInfo([RemoveAdsPurchase]) { result in
            
            appDel.topViewController?.removeWaitSpinner(waitView: wait)
            
            if let product = result.retrievedProducts.first {
                
                let wait = appDel.topViewController!.addWaitSpinner()
                
                self.paymentType = upgradeType.removeAds
                
                SwiftyStoreKit.purchaseProduct(product, completion: { (result:PurchaseResult) in
                    
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    
                    if case .success(let purchase) = result {
                        let downloads = purchase.transaction.downloads
                        if !downloads.isEmpty {
                            SwiftyStoreKit.start(downloads)
                        }
                        // Deliver content from server, then:
                        if purchase.needsFinishTransaction {
                            SwiftyStoreKit.finishTransaction(purchase.transaction)
                        }
                        
                        //call api to remove ads
                        guard let userId = MyProfile.userId else {
                            return;
                        }
                        
                        self.callRemoveAdsApi(userId: userId)
                    }
                    if case .error(let error) = result{
                        appDel.topViewController?.displayMessage("Purchase failed [\(error.localizedDescription)]")
                    }
                })
            }
        }
    }
    
    //MARK:- API's
    
    func callTest(receipt:String, transactionId: String?, productId: String, callback:@escaping ((AnyObject?)->())) {
        
        let url = URL(string: apiBaseUrl+"/test")!
        var request = URLRequest(url: url)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        
        guard let userId = MyProfile.userId else {
            return;
        }
        
        let parameters: [String: String] = [
            "appleReceipt": receipt,
            "user_id" : userId,
            "transaction_identifier" : transactionId ?? "",
            "product_id": productId,
            "user_type" : "ios"
        ]
        
        print("apple receipt => ", receipt.length)
        
//        print("parameters =>> \(parameters)")
        
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("data is \(data)")
            print("response is \(response)")
            print("error is \(error)")
            
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {
                    print("error", error ?? "Unknown error")
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {
                print("returning from here => \(response.statusCode)")
                return
            }
            
            do {
                let value = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                print("*************")
                print(value)
                DispatchQueue.main.async {
                    callback(value as AnyObject)
                }
            }catch {
                DispatchQueue.main.async {
                    callback(nil)
                }
            }
        }
        task.resume()
    }
    
    func callUpdateCountApi(userId:String, type: String, count: String) {
        
        let params = ["user_id":userId,
                      "type":type,
                      "count":count]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kUpdateAllCount, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    if let des = data["description"] as? [String:Any] {
                        
                        let uesrDetails = UserModel(data: des)
                        
                        MyProfile.AvailablePostCount = uesrDetails.available_post_count
                        MyProfile.AvailableStoryCount = uesrDetails.available_story_count
                        MyProfile.AvailableLiveCount = uesrDetails.available_live_count
                        
                        self.callShowCountApiAndReload()
                        
                        //self.checkForEmpty()
                    }
                }
            }
        }
    }
    
    func callRemoveAdsApi(userId: String) {
        
        let params = ["user_id":userId,
                      "is_ads":"\(false)"]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kNoMoreAds, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    
                    MyProfile.showAds = false
                    
                    if MyProfile.showAds {
                        self.removeAdsView.isHidden = false
                    }else{
                        self.removeAdsView.isHidden = true
                    }
                    
                    appDel.adTimer?.invalidate()
                    
//                    self.checkForEmpty()
                }
            }
        }
    }
    
    func checkForEmpty() {
        if MyProfile.AvailablePostCount?.count == -1 && MyProfile.AvailableStoryCount?.count == -1 && MyProfile.AvailableLiveCount?.count == -1 && MyProfile.showAds == false {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

enum upgradeType {
    case count
    case removeAds
}

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@"
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}


//                                    if let receiptData = SwiftyStoreKit.localReceiptData {
//                                        let encryptedReceipt = receiptData.base64EncodedString(options: [])
//                                        appDel.printMessage("TRANSACTION IDENTIFIER =>> \(purchase.transaction.transactionIdentifier)")
//                                        appDel.printMessage("encryptedReceipt =>> \(encryptedReceipt)")
//                                        self.callTest(receipt: encryptedReceipt, transactionId: purchase.transaction.transactionIdentifier, productId: ProductId, callback: { data in
//
//                                            if let data = data as? [String:Any] {
//                                                if let update = data["update"] as? Bool, update {
//                                                    self.callShowCountApiAndReload()
//                                                }
//                                            }
//                                        })
//                                    }else{
//                                        SwiftyStoreKit.fetchReceipt(forceRefresh: true) { result in
//                                            switch result {
//                                            case .success(let receiptData):
//                                                let encryptedReceipt = receiptData.base64EncodedString(options: [])
//                                                appDel.printMessage("TRANSACTION IDENTIFIER =>> \(purchase.transaction.transactionIdentifier)")
//                                                appDel.printMessage("encryptedReceipt =>> \(encryptedReceipt)")
//                                                self.callTest(receipt: encryptedReceipt, transactionId: purchase.transaction.transactionIdentifier, productId: ProductId, callback:{ data in
//
//                                                    if let data = data as? [String:Any] {
//                                                        if let update = data["update"] as? Bool, update {
//                                                            self.callShowCountApiAndReload()
//                                                        }
//                                                    }
//                                                })
//                                            case .error(let error):
//                                                appDel.printMessage("error", error)
//                                            }
//                                        }
//                                    }

//"• Get Unlimited \(msg) to create.\n• Subscription is auto-renewable which means that once purchased it will be auto-renewed every month until you cancel it 24 hours prior to the end of the current period.\n• Duration of the subscription is 1 Month with a charge of $4.99 every month.\n• iTunes Account will be charged for renewal within 24-hours prior to the end of the current period with the cost of $4.99.\n• Manage your Subscription and Auto-Renewal by going to your Account Settings."
