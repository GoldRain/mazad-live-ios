

import UIKit

class ChatListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var rooms = [Room]()
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Messages", comment: "Messages")
        
        let v = UIView()
        v.backgroundColor = .clear
        self.tableView.tableFooterView = v
        
        self.getRoomList()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onMessageReceived(notification:)), name: .OnMessageRecieved, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onMarkRoomRead(notification:)), name: .RoomUpdateNotification, object: nil)
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getRoomList()
    }
    
    @objc func onMarkRoomRead(notification:Notification) {
        
        if let data = notification.userInfo as? [String:Any]{
            
            if let room = data["room"] as? Room{
                
                //update the chat list and remove unread mark from the list
                
                if let index = self.rooms.firstIndex(where: { (r) -> Bool in
                    return r.mongoId == room.mongoId
                }){
                    if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ChatUserTableViewCell{
                        if room.unreadCount > 0{
                            cell.imgUserProfileImage.superview?.BorderColor =  BLUECOLOR
                            cell.imgUserProfileImage.superview?.BorderWidth = 3.0
                        }
                        else{
                            cell.imgUserProfileImage.superview?.BorderColor =  UIColor.gray
                            cell.imgUserProfileImage.superview?.BorderWidth = 3.0
                        }
                    }
                }
            }
        }
    }
    
    @objc func onMessageReceived(notification:Notification) {
        
        self.getRoomList()
    }
    
    func getRoomList(){
        
        messageCenter?.getRooms(callback: { (rooms) in
            
            self.emptyText = "No Messages!!"
            
            if let rooms = rooms {
                self.rooms = rooms
                mainQueue.async {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count > 0 ? rooms.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.rooms.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChatUserTableViewCell
        
        let room = self.rooms[indexPath.row]
        
        if  let user = room.getAnotherUser(){
            
            cell.lblUserName.text = user.name
            
            if let updatedTime = room.lastUpdateDate {
                
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm"
                let strDate = formatter.string(from: updatedTime)
                let date = formatter.date(from: strDate)
                let newStrDate = formatter.string(from: date!)
                
                cell.lblTime.text = newStrDate
            }
            
            cell.imgUserProfileImage.image = #imageLiteral(resourceName: "user.png")
            
            if let url = user.profileImageUrl {
            
                cell.imgUserProfileImage.sd_setImage(with: URL(string: url), completed: { img, err, cache, url in
                    
                    if img == nil{
                        cell.imgUserProfileImage.image = #imageLiteral(resourceName: "user.png")
                    }
                })
            }
            
            //room.subtitle
            if let subTitle = room.subtitle {
                cell.lblMessage.text = subTitle
            }
        }
        
        if room.unreadCount > 0{
            cell.imgUserProfileImage.superview?.BorderColor =  BLUECOLOR
            cell.imgUserProfileImage.superview?.BorderWidth = 3.0
        }
        else{
            cell.imgUserProfileImage.superview?.BorderColor =  UIColor.gray
            cell.imgUserProfileImage.superview?.BorderWidth = 3.0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController{
            
            let room = self.rooms[indexPath.row]
            vc.room = room
            
            let user = room.getAnotherUser()
            vc.otherId = user?.mongoId
            vc.userName = user?.name
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

class ChatUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgUserProfileImage: UIImageView!
}
