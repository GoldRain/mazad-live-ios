//
//  StreamCollectionView.swift
//  Mazad Live
//
//  Created by Mac-3 on 07/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import Pulsator

var liveStreams = [MazadStream]()

class StreamCollectionView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var HEIGHT:CGFloat = 56
    
    override func awakeFromNib() {
        superview?.awakeFromNib()
        self.delegate = self
        self.dataSource = self
        self.addObserver()
        
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: 60, height: 60)
    }
    
    func addObserver() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateStreamNotification(not:)), name: .UpdateStreamNotification, object: nil)
    }
    
    @objc func onUpdateStreamNotification(not:Notification){
        
        if MyProfile.isSeller {
            return;
        }
        
        messageCenter?.getLiveStream(callback: {  (res) in
            
            liveStreams.removeAll()
            
            if let res = res{
                liveStreams += res
            }
            
            if let userId = MyProfile.userId {
                liveStreams = liveStreams.filter({ (stream) -> Bool in
                    return stream.status == "running" && stream.userId != userId
                })
            }
            
            self.reloadData()
        })
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("Streams count \(liveStreams.count)")

        if liveStreams.count <= 0{
            self.superview?.fd_collapsed = true
        }
        else{
            self.superview?.fd_collapsed = false
        }
        self.superview?.layoutIfNeeded()
        self.superview?.layoutSubviews()
        return liveStreams.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StreamCollectionViewCell
        
        let stream = liveStreams[indexPath.row]
        
        cell.stream = stream
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = CGFloat(40.0)
        let height = CGFloat(50.0)
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let stream = liveStreams[indexPath.row]
        
        //        print("My Country -> \(MyProfile.countryId!)")
        //        
        //        for id in stream.country{
        //            print("Stream Country -> \(id)")
        //        }
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        if let countryId = MyProfile.countryId{
            if stream.country.firstIndex(of: countryId) == nil{
                
                if let wait = wait{
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
                
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                
                vc.alertText = "This Stream isn't Available for your Country. Keep Searching for other Streams!"
                
                appDel.topViewController?.present(vc, animated: true, completion: nil)
                
                return
            }
        }
        
        if let wait = wait{
            appDel.topViewController?.removeWaitSpinner(waitView: wait)
        }
        
        if let top = appDel.topViewController{
            let sb = UIStoryboard(name:"Streaming", bundle:nil)
            if let vc = sb.instantiateViewController(withIdentifier: "LiveStreamViewController") as? LiveStreamViewController{
                vc.displayType = .subscriber
                vc.stream = stream
                vc.stream?.comments.removeAll()
                top.present(vc, animated: true, completion: nil)
            }
        }
    }
}

class StreamCollectionViewCell:UICollectionViewCell{
    
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    
    @IBOutlet weak var mainView:UIView!
    
    var pulsator:Pulsator?
    
    var stream:MazadStream!{
        
        didSet{
            
            if stream.thumbUrl != nil && stream.thumbUrl.trim() != ""{
                imageView.sd_setImage(with: URL(string: stream.thumbUrl!), placeholderImage: #imageLiteral(resourceName: "img3"), options: [], progress: nil, completed: nil)
            }
            else if let user = stream.userDetails.first{
                if user.profilePhoto != nil && user.profilePhoto != ""{
                    imageView.sd_setImage(with: URL(string: user.profilePhoto!), placeholderImage: #imageLiteral(resourceName: "img4"), options: [], progress: nil, completed: nil)
                }
                else{
                    imageView.image = #imageLiteral(resourceName: "img4")
                }
            }
            
            self.addAnimation()
            
            if let name = stream.userDetails.first?.username{
                lblName.text = name
            }
            
            
            
        }
    }
    func addAnimation(){
        
        //add animation to image
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.duration = 0.5
        animation.repeatCount = .infinity
        animation.fromValue = 1.0
        animation.toValue = 0.5
        animation.autoreverses = true
        
        imageView.layer.add(animation, forKey: "transform.scale")
        
        self.pulsator = Pulsator()
        self.pulsator?.backgroundColor = UIColor.red.cgColor
        mainView.subviews.first?.layer.addSublayer(pulsator!)
        self.pulsator?.numPulse = 3
        self.pulsator?.radius = 40.0
        self.pulsator?.start()
        
    }
    override func prepareForReuse() {
        
        self.pulsator?.removeFromSuperlayer()
        self.imageView.layer.removeAllAnimations()
        
    }
    
    override func layoutSubviews() {
        
        self.pulsator?.removeFromSuperlayer()
        self.imageView.layer.removeAllAnimations()
        
        self.addAnimation()
        
    }
    
}
