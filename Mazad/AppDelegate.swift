
import UIKit
import IQKeyboardManagerSwift
import AWSS3
import AWSCore
import Fabric
import Crashlytics
import CoreData
import OneSignal
import GooglePlaces
import GoogleMaps
import GoogleMobileAds
import goSellSDK
import YPImagePicker
import SwifteriOS
import SwiftyStoreKit
import Pages
import Firebase

var appDel:AppDelegate!

//Twitter Keys
let TwitterConsumerKey = "Mm0EvDLgSX3SbBHHH6ou2dUJH"
let TwitterConsumerSecret = "1KY0IblUYDazwe2Kup8TBU1fTVqV6Vi48RqLziynR0Vc9tZvYW"

//TAP Keys
//Test
//let authenticationKey = "sk_test_1TwVWzn4Y6Obl2Gs8UQH3gqk"
//let encryptionKey = "pk_test_zVeYfFwvNxlgJp6SrAdLP8Qn"
//Live
let authenticationKey = "sk_live_xqfRC2K1i9AcmvOFYke68dUn"
let encryptionKey = "pk_live_fA9qwjrTsbZG6ENo82hlW0IC"

//ads interval
let AdInterval:Double = 180.0

//Maximum Amount
let AMOUNT:Double = 1000.0

//SUPPORT INFO
let SupportMail = "Support@mazadlive.com"//"cis.alrabiah@gmail.com"
let SupportPhone = "+965 97582555"

//Google API Maps
let kGMSPlacesClientKey   = "AIzaSyCMiXhx-zoPHuERkIUVPI_E9AFgnEyIP9I"
let kGMSServicesClientKey = "AIzaSyCMiXhx-zoPHuERkIUVPI_E9AFgnEyIP9I"

//UIImage compression
let compression:CGFloat = 0.01

//AWS Details
let accessKey = "AKIAJ7NUFTKSHAX2IQWA"
let secretKey = "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"
let bucket    = "mazadpostimages"

//let AdMobId = "ca-app-pub-1340327703072906~7452699981" //mazad app
let AdMobId = "ca-app-pub-1340327703072906~7452699981"   //mazad app

//let AdUnitId = "ca-app-pub-1340327703072906/6865418765"

//let AdUnitId = "ca-app-pub-3940256099942544/4411468910" //this is test id from ad tutorial
let AdUnitId = "ca-app-pub-1340327703072906/5177970956" //this is test id from ad tutorial

//#210154
let BLUECOLOR = UIColor(red:0.13, green:0.00, blue:0.33, alpha:1.0)

//cyan color // 86BCDA
let CYANCOLOR = UIColor(red:0.53, green:0.74, blue:0.85, alpha:1.0)

//74cf56
let GREENCOLOR = UIColor(red:0.45, green:0.81, blue:0.34, alpha:1.0)

//mazad
let ONESIGNAL = "144eccd5-4016-4a53-b05a-afa5f971ddf3"

//Queue
let mainQueue = DispatchQueue.main
let backgroudQueue = DispatchQueue.global(qos: .background)

//Language Key
let AppleLanguageKey = "AppleLanguages"
let CurrentLanguageKey = "CurrentLanguageKey"

// InApp Purchased Signin Credentials
// yuyanmazad@gmail.com
// Password@12345

var postIdFromDynamicLink = ""
var storyIdFromDynamicLink = ""

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSSubscriptionObserver { //, SKPaymentTransactionObserver {
    
//    SKPaymentTransactionObserver
//    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
//
//        for transaction in transactions{
//
//            switch transaction.transactionState{
//            case .purchased:
//                if let receiptData = SwiftyStoreKit.localReceiptData {
//                    let encryptedReceipt = receiptData.base64EncodedString(options: [])
//                    self.printMessage("jalksjfklasdfjla;ksdfj;laksjal;ksjal;ksjal;ksdj;lkasdf")
//                    self.printMessage("\(transaction)")
//                    self.callTest(receipt: encryptedReceipt, transactionId: "", productId: transaction.transactionIdentifier!, callback: { (_) in
//                    })
//                }
//                break;
//            default:
//                print("do nothing")
//                break;
//            }
//        }
//    }
    
     var interstitial: GADInterstitial?
    
    var adTimer:Timer?
    
    var streamImagesFolder:String?{
        if let id = MyProfile.userId{
            return "postimages/user\(id)/"
        }
        return nil
    }
    
    var postImagesFolder:String?{
        if let id = MyProfile.userId{
            return "postimages/user\(id)/"
        }
        return nil
    }
   
    var userImageFolder:String{
        return "userimages/"
    }
    
    var storyImagesFolder:String?{
        if let id = MyProfile.userId{
            return "storyimages/user\(id)/"
        }
        return nil
    }
    
    var streamImageFolder:String?{
        if let id = MyProfile.userId{
            return "streamimages/user\(id)/"
        }
        return nil
    }
    var documentVerificationFolder:String?{
        if let name = MyProfile.userName{
            return "documentverification/\(name)/"
        }
        return nil
    }
    
    var countryList:[CountryModel]?
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        //Language Set UserDefaults
        if UserDefaults.standard.object(forKey: CurrentLanguageKey) == nil{
            //UserDefaults.standard.set("en", forKey: CurrentLanguageKey)
            UserDefaults.standard.synchronize()
        }
        
        //go sell sdk
        goSellSDK.authenticationKey = authenticationKey // Authentication key
        goSellSDK.encryptionKey = encryptionKey         // Encryption key
        
        //google maps
        GMSPlacesClient.provideAPIKey(kGMSPlacesClientKey)
        GMSServices.provideAPIKey(kGMSServicesClientKey)
        
        //admob
        GADMobileAds.configure(withApplicationID: AdMobId)
        
        
        //fabric
        Fabric.with([Crashlytics.self])
        
        //ONE SIGNAL
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: ONESIGNAL,
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            
            if #available(iOS 10.0, *) {
                
                let center = UNUserNotificationCenter.current()
                center.delegate = self
                
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound, .badge]) {(accepted, error) in
                    if !accepted {
                        print("Notification access denied")
                    }
                }
            }
        })
        
        // Add your AppDelegate as an subscription observer
        OneSignal.add(self as OSSubscriptionObserver)
        
        //END ONESIGNAL
        
        //configure aws US_EAST_2
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USEast2, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        appDel = self
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(StoryImageViewerViewController.self)
        IQKeyboardManager.shared.disabledToolbarClasses.append(StoryImageViewerViewController.self)
        
        
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().tintColor = UIColor(red: 52/255, green: 167/255, blue: 193/255, alpha: 1.0)
        UINavigationBar.appearance().barTintColor = UIColor.purpleColor
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().clipsToBounds = false
        UINavigationBar.appearance().backgroundColor = UIColor.red
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]
        
        if let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView {
            statusBarView.backgroundColor = .clear
        }
        
        let color = UIColor.white
        let font = UIFont(name: "Lato-Medium", size: 16)!//Monthoers 2.0
        
        let attributes: [NSAttributedStringKey: AnyObject] = [
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.foregroundColor: color
        ]
        
        UINavigationBar.appearance().titleTextAttributes = attributes
        
        let backImage = UIImage(named: "back_top_bar")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        if #available(iOS 11, *) {
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-300, 0), for:UIBarMetrics.default)
        } else {
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -200), for:UIBarMetrics.default)
        }
       
        SwiftyStoreKit.completeTransactions { (purchases) in

            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased:
                    
                    if let receiptData = SwiftyStoreKit.localReceiptData {
                        let encryptedReceipt = receiptData.base64EncodedString(options: [])
                        
                        self.printMessage("jalksjfklasdfjla;ksdfj;laksjal;ksjal;ksjal;ksdj;lkasdf")
                        self.printMessage("\(purchase)")
                        
                        switch purchase.productId {
                        case "AutoRenewableUnlimitedPostsForOneMonth1":
                            self.printMessage("self.isPost =>>> \(self.isPost)")
                            if !self.isPost{
                                self.isPost = true
                                self.printMessage("is post comes")
                                self.callTest(receipt: encryptedReceipt, transactionId: purchase.transaction.transactionIdentifier, productId: purchase.productId, callback: { (_) in
                                })
                            }
                        case "AutoRenewableUnlimitedStoriesForOneMonth1":
                            self.printMessage("self.isStory =>>> \(self.isStory)")
                            if !self.isStory{
                                self.isStory = true
                                self.printMessage("is story comes")
                                self.callTest(receipt: encryptedReceipt, transactionId: purchase.transaction.transactionIdentifier, productId: purchase.productId, callback: { (_) in
                                })
                            }
                        case "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
                            self.printMessage("self.isLive =>>> \(self.isLive)")
                            if !self.isLive{
                                self.isLive = true
                                self.printMessage("is live comes")
                                self.callTest(receipt: encryptedReceipt, transactionId: purchase.transaction.transactionIdentifier, productId: purchase.productId, callback: { (_) in
                                })
                            }
                        default:
                            self.printMessage("Do nothing 1")
                            break;
                        }
                    }
                    break;
                default:
                    print("do nothing 2")
                    break;
                }
            }
        }
        
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")

        FirebaseApp.configure()
        
        return true
    }
    
    var isPost = false
    var isStory = false
    var isLive = false
    
    func callTest(receipt:String, transactionId: String?, productId: String, callback:@escaping ((AnyObject?)->())) {
        
        printMessage("call test called")
        
        let url = URL(string: apiBaseUrl+"/test")!
        var request = URLRequest(url: url)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        
        guard let userId = MyProfile.userId else {
            return;
        }
        
        print("Product Id => \(productId)")
        
        switch productId {
            
        case "AutoRenewableUnlimitedPostsForOneMonth1":
            if let postStatus = MyProfile.AvailablePostCount?.status {
                if !postStatus{
                    print("Returning form here 1")
                    return;
                }
            }else{
                print("Returning form here 1 1")
                return;
            }
        case "AutoRenewableUnlimitedStoriesForOneMonth1":
            if let storyStatus = MyProfile.AvailableStoryCount?.status {
                if !storyStatus{
                    print("Returning form here 2")
                    return;
                }
            }else{
                print("Returning form here 2 2")
                return;
            }
        case "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
            if let liveStatus = MyProfile.AvailableLiveCount?.status {
                if !liveStatus{
                    print("Returning form here 3")
                    return;
                }
            }else{
                print("Returning form here 3 3")
                return;
            }
        default:
            print("Do nothing asdf")
            return ;
        }
        
        let parameters: [String: String] = [
            "appleReceipt": receipt,
            "user_id" : userId,
            "transaction_identifier" : transactionId ?? "",
            "product_id": productId,
            "user_type" : "ios"
        ]
        
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {
                    print("error", error ?? "Unknown error")
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {
                return
            }
            
            do {
                let value = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                print("*************")
                print(value)
                DispatchQueue.main.async {
                    callback(value as AnyObject)
                }
            }catch {
                DispatchQueue.main.async {
                    callback(nil)
                }
            }
        }
        task.resume()
    }
    
    // After you add the observer on didFinishLaunching, this method will be called when the notification subscription property changes.
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            
            print("Subscribed for OneSignal push notifications!")
            
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let pID = stateChanges.to.userId {
            
            MyProfile.playerId = pID
            
            print("Current playerId \(pID)")
            
            messageCenter?.updateOnlineStatus(isOnline: true, playerId: pID)
        }
    }
    
    var topViewController:UIViewController? {
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // topController should now be your topmost view controller
            if topController is UINavigationController {
                if let top = topController as? UINavigationController {
                    if let newTop = top.visibleViewController.self {
                        return newTop
                    }
                }
            } else {
                return topController
            }
        }
        return nil
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
       
        
        messageCenter?.updateOnlineStatus(isOnline: false, playerId: MyProfile.playerId ?? "empty")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        // Check last Login
        
        if let userId = MyProfile.userId, let top = self.topViewController {
            
            let wait = top.addWaitSpinner()
            
            if let time = MyProfile.lastLoginTime {
                JSONRequest.makeRequest(kCheckLastLogin, parameters: ["user_id":userId,"time":time]) { (data, error) in
                    
                    top.removeWaitSpinner(waitView: wait)
                    
                    if let data = data as? [String:Any] {
                        if let err = data["error"] as? Bool, err{ // true
                            self.logOutUser()
                            return
                        }
                    }
                    messageCenter?.updateOnlineStatus(isOnline: true, playerId: MyProfile.playerId ?? "empty")
                }
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        messageCenter?.updateOnlineStatus(isOnline: false, playerId: MyProfile.playerId ?? "empty")
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            
            guard error == nil else{
                print("found an error! \(error?.localizedDescription)")
                return
            }
            if let dynamicLink = dynamiclink{
                self.handleIncomingDynamicLink(dynamicLink)
            }
        }
        
        if handled{
            return true
        }else {
            return false
        }
    }
    
    func handleIncomingDynamicLink(_ dynamicLink:DynamicLink){
        guard let url = dynamicLink.url else{
            print("weird , my dynamic link object has no url")
            return
        }
        print("Your incoming url is: \(url)")
        
        if let parameters = url.queryParameters{
            if let type = parameters["type"]{
                if type == "post" {
                    if let postId = parameters["id"]{

                        postIdFromDynamicLink = postId
                        
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController")
                        let nvc = UINavigationController(rootViewController: vc)
                        appDel.topViewController!.present(nvc, animated: true, completion: {
                            print("Myprofile.seller 1 => \(MyProfile.isSeller)")
                            MyProfile.isSeller = MyProfile.isSeller
                            NotificationCenter.default.post(name: NSNotification.Name("changeUser"), object: nil, userInfo: ["user":MyProfile.isSeller])
                        })
                    }
                }else if type == "story"{
                    if let storyId = parameters["id"]{
                        
                        storyIdFromDynamicLink = storyId
                        
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController")
                        let nvc = UINavigationController(rootViewController: vc)
                        appDel.topViewController!.present(nvc, animated: true, completion: {
                            print("Myprofile.seller 2 => \(MyProfile.isSeller)")
                            MyProfile.isSeller = MyProfile.isSeller
                            NotificationCenter.default.post(name: NSNotification.Name("changeUser"), object: nil, userInfo: ["user":MyProfile.isSeller])
                        })
                    }
                }
            }
        }
    }
    
    func printM(_ str:String){
        print("Mazad --> \(str)")
    }
    func printMessage(_ str:String){
        self.printM(str)
    }
    
   //MARK:- call countries api
    func getAllCountries(callback:@escaping (([CountryModel]?, String?)->())) {
        
        if let countryList = self.countryList{
            callback(countryList, nil)
            return
        }
        
        let params = [ "type":"read", "version":VERSION]
        
        var wait:WaitView?
        
        if let top = appDel.topViewController{
            wait = top.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kCountries, parameters: params) { (data, error) in
            
            if let wait = wait, let top = self.topViewController{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let descriptions = data["description"] as? [[String:Any]] {
                        
                        self.countryList = [CountryModel]()
                        
                        for des in descriptions {
                            self.countryList?.append(CountryModel(data: des))
                        }
                        
                        self.countryList?.sort(by: { (country1, country2) -> Bool in
                            autoreleasepool {
                                if let firstName = country1.countryName {
                                    if let secondName = country2.countryName {
                                        return firstName.localizedCaseInsensitiveCompare(secondName) == ComparisonResult.orderedAscending
                                    }
                                }
                                return false
                            }
                        })
                        
                        callback(self.countryList, nil)
                    }
                    else{
                        let msg = NSLocalizedString("Failed to get countries", comment: "Failed to get countries")
                        callback(nil, msg)
                    }
                }else if let msg = data["message"] as? String{
                    let msg = NSLocalizedString(msg, comment: msg)
                    callback(nil, msg)
                }
            }else{
                let msg = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil, msg)
            }
        }
    }
    var applicationDocumentsDirectory: URL? {
        get{
            return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "com.alienbrainz.mazad.Mazad")
        }
    }

    // MARK: - Core Data stack


    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        
        let container = NSPersistentContainer(name: "MazadChat")
        
        if let url = self.applicationDocumentsDirectory {
            
            let urlString = url.path + "/MazadChat.sqlite"
            let description = NSPersistentStoreDescription(url: URL(fileURLWithPath: urlString))
            container.persistentStoreDescriptions = [description]
        }
        
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        
        let context = self.persistentContainer.viewContext
        
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func saveInBackground(){
        mainQueue.async {
            let container: NSPersistentContainer? = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer
            container?.performBackgroundTask{ context in
                if context.hasChanges {
                    do {
                        try context.save()
                    } catch {
                        // Replace this implementation with code to handle the error appropriately.
                        // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                        let nserror = error as NSError
                        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                    }
                }
            }
        }
    }
}

// Extension tell app to be able to get notification when in use and also for extensions
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    //MARK: APNS
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("I am not available in simulator \(error)")
        
    }
    
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("didRecieverremoveNotification")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        self.handleRemoteNotification(userInfo: userInfo)
        
        if UIApplication.shared.applicationState == .background{
            completionHandler(.noData)
        }
        else{
            
            completionHandler(.noData)
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        self.handleRemoteNotification(userInfo: notification.request.content.userInfo)
        if UIApplication.shared.applicationState == .background{
            completionHandler( [.alert, .badge, .sound])
        }
        else{
            completionHandler(.alert)
        }
    }
    
    private func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Tappped app delegate")
        completionHandler()
    }
    func handleRemoteNotification(userInfo:[AnyHashable:Any]){
        print("userInfo:\(userInfo)")
        //        if let push = self.push{
        //            push.application(application, didReceiveRemoteNotification: userInfo)
        //            return
        //        }
        
//        let dict = userInfo as? [String:Any]
//        let aps = dict?["aps"] as? [String:Any]
//        let alert = aps?["alert"] as? [String:Any]
//        let alertMsg = alert?["body"] as? String
        
        let dict1 = userInfo as NSDictionary
        let alert1 = dict1["custom"] as? [String:Any]
        let a = alert1?["a"] as? [String:Any]
        let type = a?["type"] as? String
        
        print("type", type ?? "NO TYPE FOUND")
        
        if type == "message"{
            
            if let message = a?["message"] as? [String:Any]{
                if let typeDetails = message["typeDetails"] as? [String:Any]{
                    if let roomDetails = typeDetails["roomDetails"] as? [String:Any]{
                        if let roomId = roomDetails["_id"] as? String{
                            if let room = messageCenter?.getRoom(roomId: roomId), let otherUser = room.otherId{
                                if let top = appDel.topViewController{
                                    
                                    if !(top is ChatListViewController) && !(top is ChatViewController){
                                        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController{
                                            vc.room = room
                                            vc.otherId = otherUser
                                            top.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }else if type == "logout" {
           
            messageCenter?.updateOnlineStatus(isOnline: false, playerId: "")
            messageCenter?.removeOldDatabase()
            messageCenter?.stop()
            MyProfile.logOutUser()
            appDel.adTimer?.invalidate()
            
            if let top = appDel.topViewController {
                top.dismiss(animated: true, completion: nil)
            }
        }
    }
    // MARK: Utility
    func dispatchlocalNotification(identifier:String, title: String, body: String, userInfo: [AnyHashable: Any]?) {
        
        print("Title --> \(title) Body --> \(body)")
        
        if #available(iOS 10.0, *) {
            
            //iOS 10 or above version
            let content = UNMutableNotificationContent()
            content.title = title
            content.body = body
            content.sound = UNNotificationSound.default()
            if let userInfo = userInfo{
                content.userInfo = userInfo
            }
            
            var dateComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: Date())
            
            dateComponents.second = dateComponents.second! + 1
            
            print(dateComponents)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
            
            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
                if let error = error{
                    print(error)
                }
            })
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            
            // ios 9
            let notification = UILocalNotification()
            notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
            notification.alertBody = body
            notification.alertAction = title
            notification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(notification)
            
        }
        
        print("WILL DISPATCH LOCAL NOTIFICATION AT ", Date())
        
    }
    
    func startAdTimer(){
        
        if self.adTimer != nil{
            return
        }

        self.adTimer = Timer.scheduledTimer(withTimeInterval: AdInterval, repeats: true, block: { (timer) in
           
            // check if top view is following VC then ads should not display
            if let _ = self.topViewController as? PaymentViewController{ return; }
            if let _ = self.topViewController as? LiveStreamViewController{ return; }
            if let _ = self.topViewController as? CreateOrderViewController { return; }
            if let _ = self.topViewController as? AddressesViewController { return; }
            if let _ = self.topViewController as? CreateAddressViewController { return; }
            if let _ = self.topViewController as? UpgradeViewController { return; }
            if let _ = self.topViewController as? YPImagePicker { return; }
            if let _ = self.topViewController as? StartLiveStreamViewController { return; }
            if let _ = self.topViewController as? AuctionItemsViewController { return; }
            if let _ = self.topViewController as? AddAuctionItemViewController { return; }
            if let _ = self.topViewController as? NewPostViewController { return; }
            if let _ = self.topViewController as? StoryViewController { return; }
            if let _ = self.topViewController as? PurchaseDescriptionViewController { return; }
            if let _ = self.topViewController as? UIAlertController { return; }
            if let _ = self.topViewController as? StoryImageViewerViewController { return; }
            if let _ = self.topViewController as? PagesController { return; }
            
            if !MyProfile.showAds { return; }
            
            if self.interstitial == nil {
                self.interstitial = GADInterstitial(adUnitID: AdUnitId)
                self.interstitial?.delegate = self
                let request = GADRequest()
                self.interstitial?.load(request)
            }
            else{
                if let intrestial = self.interstitial{
                    if intrestial.isReady{
                        if let top = self.topViewController{
                            intrestial.present(fromRootViewController: top)
                        }
                    }
                }
            }
        })
    }
}

extension AppDelegate:GADInterstitialDelegate{
    /// Tells the delegate an ad request succeeded.
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        
        print("interstitialDidReceiveAd")
        
        if let _ = MyProfile.userId{
            
            if let top = self.topViewController{
                if !(top is LiveStreamViewController){
                    self.interstitial?.present(fromRootViewController: top)
                }
            }
            
        }
        else{
            self.interstitial = nil
            self.adTimer?.invalidate()
        }
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
        print("interstitialDidDismissScreen")
        
        self.interstitial = nil
        
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
        
        self.interstitial = nil
        
    }
    
    func logOutUser(){
        
        if let liveStreamVC = self.topViewController as? LiveStreamViewController{
            if liveStreamVC.displayType == .publisher {
                liveStreamVC.stopPublishing()
            }
            else{
                liveStreamVC.endSubscription()
            }
            liveStreamVC.stopStreamOnServer()
            liveStreamVC.close()
            
            self.topViewController?.view.window?.rootViewController?.dismiss(animated: true, completion: {
                appDel.topViewController?.navigationController?.popViewController(animated: true)
            })
        }
        else{
            self.topViewController?.view.window?.rootViewController?.dismiss(animated: true, completion: {
                appDel.topViewController?.navigationController?.popViewController(animated: true)
            })
        }
        
        
        messageCenter?.updateOnlineStatus(isOnline: false, playerId: "")
        messageCenter?.removeOldDatabase()
        messageCenter?.stop()
        MyProfile.logOutUser()
        appDel.adTimer?.invalidate()
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        print("I have received a url through a custom scheme! \(url.absoluteString)")
        
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleIncomingDynamicLink(dynamicLink)
            return true
        }else{
            Swifter.handleOpenURL(url)
            return true
        }
    }
}

public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
//    let output = items.map { "\($0)" }.joined(separator: separator)
//    Swift.print(output, terminator: terminator)
}
