//
//  OrderDetailsViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 29/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import AVFoundation
import ImageViewer

class OrderDetailsViewController: UIViewController, GalleryItemsDataSource, GalleryItemsDelegate  {

    @IBOutlet weak var imageMainView: UIView!
    @IBOutlet weak var orderDetailMainView: UIView!
    @IBOutlet weak var orderStatusMainView: UIView!
    @IBOutlet weak var addressMainView: UIView!
    
    @IBOutlet weak var orderImageView: UIImageView!
    @IBOutlet weak var btnPlayVideo: UIButton!
    
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    
    @IBOutlet weak var lblTitleOrderStatus: UILabel!
    @IBOutlet weak var lblTitleTopContraint: NSLayoutConstraint!
    @IBOutlet weak var lblTitleBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblTxnId: UILabel!
    
    @IBOutlet weak var lblAddressUserName: UILabel!
    @IBOutlet weak var lblAd1Ad2City: UILabel!
    @IBOutlet weak var lblStatePinCode: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    var order:OrderListModel!
    
    var galleryController:GalleryViewController?
    
    var dataImage: [GalleryItem] = [GalleryItem]()
    
    var postImageUrls = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //this file was changed
       

        self.title = NSLocalizedString("Order Details", comment: "Order Details")
        
        self.imageMainView.addShadow()
        self.orderDetailMainView.addShadow()
        self.orderStatusMainView.addShadow()
        self.addressMainView.addShadow()
        
        self.setupData()
        
        let postData = order.postData.first
        let storyData = order.storyData.first
        
        if order.type?.lowercased() == "post" {
            if let pImages = postData?.post_images {
                for pImage in pImages {
                    self.postImageUrls.append(pImage.image_url!)
                }
            }
        }else if order.type?.lowercased() == "story" {
            if let sImage = storyData?.url {
                self.postImageUrls.append(sImage)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
         self.navigationController?.isNavigationBarHidden = false
        
        if MyProfile.isSeller {
            self.orderStatusMainView.fd_collapsed = true
            self.lblTitleOrderStatus.fd_collapsed = true
            self.lblTitleTopContraint.constant = 0.0
            self.lblTitleBottomConstraint.constant = 0.0
        }else{
            self.orderStatusMainView.fd_collapsed = false
            self.lblTitleOrderStatus.fd_collapsed = false
            self.lblTitleTopContraint.constant = 10.0
            self.lblTitleBottomConstraint.constant = 10.0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setupData() {
        
        if let displayId = self.order.display_id {
            self.lblOrderId.text = displayId
        }
        
        if let amt = self.order.amount, let doubleAmount = Double(amt) {
            let newAmount = String(format: "%.2f", doubleAmount)
            self.lblTotalAmount.text = newAmount
        }
        
        if let date = self.order.created_at, let doubleDate = Double(date){
            self.lblOrderDate.text = formatDateForDisplayOrder(date: Date(timeIntervalSince1970: doubleDate/1000))
        }
        
        if let orderStatus = self.order.status {
            self.lblOrderStatus.text = orderStatus.capitalized
        }
        
        if let paymentStatus = self.order.user_payment_status {
            self.lblPaymentStatus.text = paymentStatus.capitalized
        }
        
        if let paymentMode = self.order.payment_method?.lowercased() {
            if paymentMode == "card"{
                self.lblPaymentMode.text = paymentMode.capitalized
            }else{
                self.lblPaymentMode.text = paymentMode.uppercased()
            }
        }
        
        if let txnId = self.order.transaction_id, txnId.trim() != "" {
            self.lblTxnId.text = txnId
        }
        else{
            self.lblTxnId.text = NSLocalizedString("NA", comment: "NA")
        }
        
        if let addressId = self.order.address_id {
            
            let addresses = self.order.address
            
            let filterAddress = addresses.filter { (address) -> Bool in
                return address.addressId == addressId
            }
            
            if filterAddress.count == 0 {
                self.lblAddressUserName.text = "Address Not Found!!"
                self.lblAd1Ad2City.isHidden = true
                self.lblStatePinCode.isHidden = true
                self.lblCountry.isHidden = true
                self.lblPhoneNumber.isHidden = true
            }else{
                if let name = filterAddress.first?.name {
                    self.lblAddressUserName.text = name.capitalized
                }
                if let ad1 = filterAddress.first?.addressLineOne, let ad2 = filterAddress.first?.addressLineTwo, let city = filterAddress.first?.city {
                    self.lblAd1Ad2City.text = ad1.capitalized + ", " + ad2.capitalized + ", " + city.capitalized
                }
                if let state = filterAddress.first?.state, let pinCode = filterAddress.first?.pincode {
                    self.lblStatePinCode.text = state.capitalized + ", " + pinCode.capitalized
                }
                if let countryName = filterAddress.first?.countryDetails.first?.countryName {
                    self.lblCountry.text = countryName.capitalized
                }
                if let phone = filterAddress.first?.phone {
                    self.lblPhoneNumber.text = phone
                }
            }
        }
        
        if self.order.type?.lowercased() == "post" {
            if let postData = self.order.postData.first {
                if let imgUrl = postData.post_images.first?.image_url {
                    self.orderImageView.sd_setImage(with: URL(string: imgUrl), placeholderImage:#imageLiteral(resourceName: "ic_placeholder.png") , options: [], completed: nil)
                }
                if let isVideo = postData.post_images.first?.is_video, isVideo {
                    self.imageMainView.gestureRecognizers = nil
                    self.btnPlayVideo.isHidden = false
                }else {
                    self.imageMainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleImageTap)))
                    self.btnPlayVideo.isHidden = true
                }
            }
        }else if self.order.type?.lowercased() == "story" {
            if let storyData = self.order.storyData.first {
                if let imgUrl = storyData.url {
                    self.orderImageView.sd_setImage(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "ic_placeholder.png"), options: [], completed: nil)
                }
                if let isVideo = storyData.is_video, isVideo {
                    self.imageMainView.gestureRecognizers = nil
                    self.btnPlayVideo.isHidden = false
                }else{
                    self.imageMainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleImageTap)))
                    self.btnPlayVideo.isHidden = true
                }
            }
        }
    }
    
    @objc func handleImageTap() {
        
        self.showImagesPreview(index: 0)
    }
    
    //MARK: GalleryItemsDataSource
    func itemCount() -> Int {
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    // MARK: Image preview
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        for url in self.postImageUrls {
            
            let galleryItem = GalleryItem.image { imageCompletion in
                
                SDWebImageDownloader.shared().downloadImage(with: URL(string: url), options: [], progress: nil, completed: { (img, data, err, fin) in
                    
                    if let image = img{
                        imageCompletion(image)
                    }
                })
            }
            self.dataImage.append(galleryItem)
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onPlayClick(_ sender: Any) {
        
        var imageUrl:String?
        
        if self.order.type?.lowercased() == "post" {
            
            guard let postImage = self.order.postData.first?.post_images.first else {
                return
            }
            imageUrl = postImage.image_url
        }
        
        if self.order.type?.lowercased() == "story" {
            
            guard let storyData = self.order.storyData.first else {
                return
            }
            imageUrl = storyData.url
        }
        
        let videoUrl = imageUrl!.replacingOccurrences(of: ".jpg", with: ".mp4")
        
        let player = AVPlayer(url: URL(string: videoUrl)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        
        if let top = appDel.topViewController {
            top.present(playerController, animated: true) {
                player.play()
            }
        }
    }
    
    @IBAction func onNeedHelpClick(_ sender: Any) {
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SupportViewController") as? SupportViewController{
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func onSubmitFeedbackClick(_ sender: Any) {
        
        if let vc = UIStoryboard(name: "Feedback", bundle: nil).instantiateViewController(withIdentifier: "CreateFeedbackViewController") as? CreateFeedbackViewController {
            
            if let _ = self.navigationController {
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                tabBarVC?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
