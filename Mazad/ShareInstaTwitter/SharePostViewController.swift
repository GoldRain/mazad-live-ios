//
//  SharePostViewController.swift
//  Mazad Live
//
//  Created by alienbrainz on 05/02/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit
import Accounts
import Social
import SwifteriOS
import SafariServices

public enum ShareType{
    case instagram
    case twitter
    case all
}

class SharePostViewController: UIViewController, SFSafariViewControllerDelegate {

    var swifter: Swifter?
    
    var hasStoredData = false
    
    var shareImage:UIImage!
    var shareDescription:String!
    
    var shareType:ShareType = .all
    
    @IBOutlet weak var btnInstagram:UIButton!
    @IBOutlet weak var btnTwitter:UIButton!
    
    @IBOutlet weak var btnInstagramSingle:UIButton!
    @IBOutlet weak var btnTwitterSingle:UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        switch self.shareType{
        case ShareType.twitter:
            self.btnTwitterSingle.isHidden = false
        case ShareType.instagram:
            self.btnInstagramSingle.isHidden = false
        default:
            self.btnTwitter.isHidden = false
            self.btnInstagram.isHidden = false
        }
        
        if let key = MyProfile.twitterKey{
            if let secret = MyProfile.twitterSecred{
                self.swifter = Swifter(consumerKey: TwitterConsumerKey, consumerSecret: TwitterConsumerSecret, oauthToken: key, oauthTokenSecret: secret)
                self.hasStoredData = true
            }
            else{
                self.swifter = Swifter(consumerKey: TwitterConsumerKey, consumerSecret: TwitterConsumerSecret)
            }
        }else{
            self.swifter = Swifter(consumerKey: TwitterConsumerKey, consumerSecret: TwitterConsumerSecret)
        }

        
        let button = UIButton()
        button.setTitle("Done", for: .normal)
        button.addTarget(self, action: #selector(self.onDoneClick(_:)), for: .touchUpInside)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @IBAction func onInstagramClick(_ sender:Any){
        
        InstagramHelper.sharedManager.postImageToInstagramWithCaption(imageInstagram: self.shareImage, instagramCaption: self.shareDescription, controller: self)
        
    }

    @IBAction func onTwitterClick(_ sender:Any){
        
        if self.hasStoredData{
            self.createTweet()
        }
        else{
            let failureHandler: (Error) -> Void = { error in
                self.alert(title: "Error", message: error.localizedDescription)
                
            }
            
            let url = URL(string: "swifter://success")!
            
            
            swifter?.authorize(withCallback: url, presentingFrom: self, success: { [unowned self](token, _ ) in

                if let token = token{

                    print("Twitter Logged in" + token.key)
                    print("Twitter Logged in" + token.secret)
                    print("Twitter Logged in" + token.screenName!)
                    print("Twitter Logged in" + token.userID!)

                    MyProfile.twitterKey = token.key
                    MyProfile.twitterSecred = token.secret


                }
                self.createTweet()
            }, failure: failureHandler)
        }
    }
    
    @IBAction func onDoneClick(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    func createTweet(){
        
        let wait = self.addWaitSpinner()
        
        let failureHandler: (Error) -> Void = { error in
            
            self.removeWaitSpinner(waitView: wait)
            self.alert(title: "Error", message: error.localizedDescription)
            
        }
        
        self.swifter?.postTweet(status: self.shareDescription , media: UIImageJPEGRepresentation(self.shareImage, compression)!,success:{ [unowned self] (_)in

            self.removeWaitSpinner(waitView: wait)

            self.displayMessage("Tweet posted!!")

            }, failure: failureHandler)
        
    }
    
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

class InstagramHelper: NSObject, UIDocumentInteractionControllerDelegate {
    
    private let kInstagramURL = "instagram://app"
    private let kUTI = "com.instagram.exclusivegram"
    private let kfileNameExtension = "instagram.igo"
    private let kAlertViewTitle = "Error"
    private let kAlertViewMessage = "Please install the Instagram application"
    
    var documentInteractionController = UIDocumentInteractionController()
    
    // singleton manager
    class var sharedManager: InstagramHelper {
        struct Singleton {
            static let instance = InstagramHelper()
        }
        return Singleton.instance
    }
    
    
    func postImageToInstagramWithCaption(imageInstagram: UIImage, instagramCaption: String, controller: UIViewController) {
        // called to post image with caption to the instagram application
        
        let instagramURL = URL(string: kInstagramURL)
        
        DispatchQueue.main.async {
            
            if UIApplication.shared.canOpenURL(instagramURL!) {
                
                let jpgPath = (NSTemporaryDirectory() as NSString).appendingPathComponent(self.kfileNameExtension)
                if let jpegData = UIImageJPEGRepresentation(imageInstagram, 1.0) {
                    do {
                        try jpegData.write(to: URL(fileURLWithPath: jpgPath), options: .atomicWrite)
                        
                    } catch {
                        appDel.topViewController?.displayMessage("Failed to write")
                    }
                }else {
                    let jpegData = UIImagePNGRepresentation(imageInstagram)
                    do {
                        try jpegData?.write(to: URL(fileURLWithPath: jpgPath), options: .atomicWrite)
                        
                    } catch {
                        appDel.topViewController?.displayMessage("Failed to write")
                    }
                }
                let fileURL = NSURL.fileURL(withPath: jpgPath)
                self.documentInteractionController.url = fileURL
                self.documentInteractionController.delegate = self
                self.documentInteractionController.uti = self.kUTI
                
                // adding caption for the image
                self.documentInteractionController.annotation = ["InstagramCaption": instagramCaption]
                self.documentInteractionController.presentOpenInMenu(from: controller.view.frame, in: controller.view, animated: true)
                
            } else {
                
                // alert displayed when the instagram application is not available in the device
                self.showAlert("", message: self.kAlertViewMessage, controller: controller)
            }
        }
    }
    func showAlert(_ title: String, message: String, controller : UIViewController){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action -> Void in
            //Just dismiss the action sheet
            let selector: Selector = NSSelectorFromString("alertOkButtonHandler")
            if controller.responds(to: selector){
                _ = controller.perform(selector)
            }
        })
        alert.addAction(okAction)
        controller.present(alert, animated: true, completion: nil)
    }}
