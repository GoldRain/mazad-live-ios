

import UIKit
import Contacts


fileprivate enum MenuItem: String {
    case home                   = "Home"
    case invite                 = "Invite"
    case followPeople           = "Follow People"
    case messages               = "Messages"
    case ecommerceSettings      = "Ecommerce Settings"
    case notifications          = "Notifications"
    case privacyAndSecurity     = "Privacy & Security"
    case support                = "Support"
    case settings               = "Settings"
    case switchAccount          = "Switch Account"
    case logout                 = "Logout"
    case changeLanguage         = "Change Language"
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let options: [(MenuItem, UIImage)] = [
        (.home,             #imageLiteral(resourceName: "icon_home_grey2")),
        (.invite,           #imageLiteral(resourceName: "icon_profile_plus")),
        (.messages,         #imageLiteral(resourceName: "icon_text_blue").withRenderingMode(UIImageRenderingMode.alwaysTemplate)),
        (.followPeople,     #imageLiteral(resourceName: "icon_profile_connect")),
        (.ecommerceSettings, #imageLiteral(resourceName: "icon_dollar_screen")),
        (.notifications,    #imageLiteral(resourceName: "icon_bell_grey")),
        (.privacyAndSecurity, #imageLiteral(resourceName: "icon_privacy_grey")),
        (.support,          #imageLiteral(resourceName: "icon_support")),
        (.settings,         #imageLiteral(resourceName: "icon_settings_grey")),
        (.changeLanguage,   #imageLiteral(resourceName: "changeLanguage")),
        (.switchAccount,    #imageLiteral(resourceName: "icon_profileDual_grey")),
        (.logout,           #imageLiteral(resourceName: "icon_shutDown_red"))
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideMenu()

        let v = UIView()
        v.backgroundColor = .clear
        tableView.tableFooterView = v
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showMenu()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let v = touches.first?.view{
            if v == self.view{
                //remove menu
                self.dismissMenu()
                
            }
        }
    }
    
    func dismissMenu(){
        UIView.animate(withDuration: 0.3, animations: {
            self.tableView.alpha = 0.0
//            self.tableView.transform = CGAffineTransform(translationX: -self.tableView.frame.width, y: 0)
        }) { (Success) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func hideMenu(){
        self.tableView.alpha = 0.0
//        self.tableView.transform = CGAffineTransform(translationX: -self.tableView.frame.width, y: 0)
    }
    
    func showMenu(){
        UIView.animate(withDuration: 0.3) {
//            self.tableView.transform = .identity
            self.tableView.alpha = 1.0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell")!
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0)
            if let img = cell.contentView.viewWithTag(1) as? UIImageView{
                if let lblName = cell.contentView.viewWithTag(2) as? UILabel {
                    if let lblEmail = cell.contentView.viewWithTag(3) as? UILabel {
                        if let imgUrl = MyProfile.userProfilePhoto {
                            img.sd_setImage(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "icon_profile_grey2"), options: [], completed: nil)
                        }else{
                            img.image = #imageLiteral(resourceName: "icon_profile_grey2")
                        }
                        if let name = MyProfile.userName {
                            lblName.text = name
                        }
                        if let email = MyProfile.userEmail {
                            lblEmail.text = email
                        }
                    }
                }
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionsCell")!
        if let img = cell.contentView.viewWithTag(1) as? UIImageView{
            if let label = cell.contentView.viewWithTag(2) as? UILabel{
                let data = options[indexPath.row-1]
                img.image = data.1
                if data.0 == .messages{
                    img.tintColor = .lightGray
                }
                
                label.text = NSLocalizedString(data.0.rawValue, comment: data.0.rawValue)
                
                if data.0 == .switchAccount{
                    cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
                }else{
                    cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0)
                }
            }
        }
        self.tableView.separatorStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            let selectedData = self.options[indexPath.row-1]
            self.menuSelectionType(selectedMenu: selectedData.0)
        }
    }
    
    fileprivate func menuSelectionType(selectedMenu: MenuItem){
        UIView.animate(withDuration: 0.3, animations: {
//            self.tableView.transform = CGAffineTransform(translationX: -self.tableView.frame.width, y: 0)
            self.tableView.alpha = 0.0
        }) { (Success) in
            self.dismiss(animated: true, completion: {
                
                if let top = appDel.topViewController{
                    
                    print(type(of: top))
                    
                    switch selectedMenu{
                        
                    case .home:
                        //let index = selectedMenu == .home ? 0 : 3
                        NotificationCenter.default.post(name: NSNotification.Name("switchTabs"), object: nil, userInfo: ["tabIndex": 0])
                        
                    case .notifications:
                        guard let _ = MyProfile.userId else{
                            getLoginDialouge()
                            return;
                        }
                        if let vc = top.storyboard?.instantiateViewController(withIdentifier: "NotificationsSettingViewController") as? NotificationsSettingViewController{
                            top.navigationController?.pushViewController(vc, animated: true)
                        }
                    case .settings:
                        if let vc = top.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController"){
                            top.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    case .switchAccount:
                        guard let _ = MyProfile.userId else{
                            getLoginDialouge()
                            return;
                        }
                        MyProfile.isSeller = !MyProfile.isSeller
                        NotificationCenter.default.post(name: NSNotification.Name("changeUser"), object: nil, userInfo: ["user":MyProfile.isSeller])
                    case .invite:
                        
                        let results = fetchContact()
                        
                        if let vc = top.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as? ContactViewController {
                            vc.contacts = results
                            self.getUsersListFromServer(showWait: true) { (users, msg) in
                                if let usrs = users{
                                    vc.userList = usrs
                                }
                                top.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    case .followPeople:
                        guard let _ = MyProfile.userId else{
                            getLoginDialouge()
                            return;
                        }
                        if let vc = top.storyboard?.instantiateViewController(withIdentifier: "FollowPeopleViewController") as? FollowPeopleViewController{
                            
                            vc.actionType = .FollowPeople
                            vc.userId = MyProfile.userId
                            followVC = vc
                            
                            top.navigationController?.pushViewController(vc, animated: true)
                        }
                    case .ecommerceSettings:
                        guard let _ = MyProfile.userId else{
                            getLoginDialouge()
                            return;
                        }
                        
                        if let vc = top.storyboard?.instantiateViewController(withIdentifier: "ECommerceSettingsViewController"){
                            top.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    case .privacyAndSecurity:
                        guard let _ = MyProfile.userId else{
                            getLoginDialouge()
                            return;
                        }
                        if let vc = top.storyboard?.instantiateViewController(withIdentifier: "PrivacySettingsViewController") as? PrivacySettingsViewController{
                            top.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    case .support:
                        if let vc = top.storyboard?.instantiateViewController(withIdentifier: "SupportViewController") as? SupportViewController{
                            top.navigationController?.pushViewController(vc, animated: true)
                        }
                    case .logout:
                        
                        messageCenter?.updateOnlineStatus(isOnline: false, playerId: "")
                        messageCenter?.removeOldDatabase()
                        messageCenter?.stop()
                        appDel.adTimer?.invalidate()
                        MyProfile.logOutUser()
                        MyProfile.showVerifiedScreen = true
                        
                        top.view.window?.rootViewController?.dismiss(animated: true, completion: {
                            //appDel.topViewController?.navigationController?.popViewController(animated: true)
                        })
                        
                    case .messages:
                        if let vc = top.storyboard?.instantiateViewController(withIdentifier: "ChatListViewController") as? ChatListViewController{
                            top.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    case .changeLanguage:
                        
                        if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "LanguagesViewController") as? LanguagesViewController {
                            
                            top.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            })
        }
    }
    
    func getUsersListFromServer(showWait:Bool, callback:@escaping (([UserModel]?, String?)->())){
        
        guard let top = appDel.topViewController else {
            let str = NSLocalizedString("There is some error", comment: "There is some error")
            callback(nil,str)
            return
        }
        
        var wait:WaitView!
        
        if showWait{
            wait = top.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kSearchUserAll, parameters: ["text": "", "user_id": MyProfile.userId ?? ""]) { (data, error) in
            
            if showWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err{
                    if let array = data["description"] as? [[String:Any]]{
                        autoreleasepool {
                            var users = [UserModel]()
                            
                            for item in array{
                                users.append(UserModel(data: item))
                            }
                            
                            if let _ = MyProfile.userId {
                                users = users.filter({ (user) -> Bool in
                                    return user._id != MyProfile.userId
                                })
                            }
                            callback(users, nil)
                            return;
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
                else{
                    let msg = NSLocalizedString("No users found!!", comment: "No users found!!")
                    callback(nil, msg)
                }
            }
            else{
                let msg = NSLocalizedString("No users found!![1]", comment: "No users found!![1]")
                callback(nil, msg)
            }
        }
    }
}
