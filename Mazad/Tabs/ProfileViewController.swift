
import UIKit
import SDWebImage
import ImageViewer
import DropDown
import AVKit
import AVFoundation
import Pulsator

var profileVC:ProfileViewController?

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ProfileCellDelegate, GalleryItemsDataSource, GalleryItemsDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //For login user Profile
    @IBOutlet weak var btnLive:UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    
    //For Other user Profile
    @IBOutlet weak var btnVerticalDot:UIButton!
    @IBOutlet weak var btnCrossWhite: UIButton!
    
    var galleryController:GalleryViewController?
    var dataImage: [GalleryItem] = [GalleryItem]()
    
    //
    var postData = [BuyerSellerModel]()
    var userData = [UserModel]()
    var userId:String!
    //
    
    var postImages = [PostImageModel]()
    
    var optionType = ["","Block User","Report User"]
    
    //
    @IBOutlet weak var verifiedBadgeView: UIView!
    @IBOutlet weak var btnVerifiedBadge: UIButton!
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.verifiedBadgeView.isHidden = true
        self.verifiedBadgeView.fd_collapsed = true
        
        profileVC = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
        // update UI
        self.checkForUIUpdate()
        
        guard let userId = self.userId else{
            return
        }
        
        tabBarVC?.getUserData(user_id: userId, addWait: true, callback: { (user, msg) in
            if let users = user {
                self.userData = users
            }
            self.getPostData(userId: self.userId) { (posts, msg) in
                if let postArr = posts {
                    self.postData = postArr
                }
                //check for menuOption
                self.checkForMenuOption()
                self.collectionView.reloadData()
            }
            self.collectionView.reloadData()
        })
        // Checking for verified badge
        self.checkForVerifiedBadge()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadingDataAfterUpdate), name: .ReloadProfileViewControllerAfterPostUpload , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleChangeUser(_:)), name: NSNotification.Name("changeUser"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        NotificationCenter.default.removeObserver(self, name: .ReloadProfileViewControllerAfterPostUpload, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("changeUser"), object: nil)
    }
    
    //MARK:- Methods
    func checkForUIUpdate() {
        
        if let myUserId = MyProfile.userId {
            if let otherUserId = self.userId{
                
                if myUserId == otherUserId { // For LoginUser
                    
                    self.btnCrossWhite.isHidden = true
                    self.btnVerticalDot.isHidden = true
                    
                    if MyProfile.isSeller {
                        self.btnLive.isHidden = false
                    }else {
                        self.btnLive.isHidden = true
                    }
                }else{
                    self.btnLive.isHidden = true
                    self.btnMenu.isHidden = true
                }
            }
        }else{ // for skip login
            if let _ = self.userId {
                self.btnLive.isHidden = true
                self.btnMenu.isHidden = true
                self.btnVerticalDot.isHidden = true
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func handleChangeUser(_ sender: NSNotification){
        self.checkForUIUpdate()
        self.checkForVerifiedBadge()
    }
    
    func checkForMenuOption() {
        
        if let myUserId = MyProfile.userId{
            if let otherUserId = self.userId{
                
                if myUserId != otherUserId { // for Other user
                    
                    if let following = userData.first?.following {
                        if following.contains(where: { (following) -> Bool in
                            return following._id == MyProfile.userId
                        }){
                            self.optionType[0] = "Follow Back"
                        }
                    }
                    
                    if let followers = userData.first?.followers {
                        if followers.contains(where: { (follower) -> Bool in
                            return follower._id == MyProfile.userId
                        }) {
                            self.optionType[0] = "Unfollow"
                        }else {
                            self.optionType[0] = "Follow"
                        }
                    }else {
                        self.optionType[0] = "Follow"
                    }
                }
            }
        }
    }
    
    func checkForVerifiedBadge() {
        
        guard let userId = MyProfile.userId else {
//            let str = NSLocalizedString("Failed to get UserId", comment: "Failed to get UserId")
//            displayMessage(str)
            return;
        }
        
        if self.userId == userId {
            if MyProfile.isSeller {
                if !MyProfile.isVerified {
                    
                    JSONRequest.makeRequest(kVerificationDetails, parameters: ["user_id":userId]) { (data, error) in
                        if let data = data as? [String:Any] {
                            if let err = data["error"] as? Bool, !err {
                                if let des = data["description"] as? [String:Any] {
                                    
                                    let verification = VerificationModel(data: des)
                                    
                                    MyProfile.verification = verification
                                    
                                    if MyProfile.verification?.status?.lowercased() == "pending" {
                                        
//                                        self.btnVerifiedBadge.setTitle("Waiting for Document Approval", for: .normal)
//                                        self.verifiedBadgeView.isHidden = false
//                                        self.verifiedBadgeView.fd_collapsed = false
                                        
                                        self.verifiedBadgeView.isHidden = true
                                        self.verifiedBadgeView.fd_collapsed = true
                                        
                                    }else if MyProfile.verification?.status?.lowercased() == "approved" {
                                        
                                        self.verifiedBadgeView.isHidden = true
                                        self.verifiedBadgeView.fd_collapsed = true
                                        
//                                        self.btnVerifiedBadge.setTitle("Document is Approved, Click here to Proceed", for: .normal)
//                                        self.verifiedBadgeView.isHidden = false
//                                        self.verifiedBadgeView.fd_collapsed = false
                                    }
                                    else {
                                        self.showVerificationScreenCheck()
                                    }
                                }
                                else{
                                    self.showVerificationScreenCheck()
                                }
                            }
                        }
                    }
                }else{
                    self.verifiedBadgeView.isHidden = true
                    self.verifiedBadgeView.fd_collapsed = true
                }
            }else{
                self.verifiedBadgeView.isHidden = true
                self.verifiedBadgeView.fd_collapsed = true
            }
        }else{
            self.verifiedBadgeView.isHidden = true
            self.verifiedBadgeView.fd_collapsed = true
        }
        
    }
    
    func showVerificationScreenCheck() {
        if MyProfile.showVerifiedScreen {
            
            self.getVerifiedViewController()
            
            MyProfile.showVerifiedScreen = false
        }else{
            self.verifiedBadgeView.isHidden = false
            self.verifiedBadgeView.fd_collapsed = false
        }
    }
    
    func getVerifiedViewController() {
        
        if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "GetVerifiedViewController") as? GetVerifiedViewController {
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            vc.callback = {  res in
                if res {
                    self.verifiedBadgeView.isHidden = true
                    self.verifiedBadgeView.fd_collapsed = true
//                    self.verifiedBadgeView.isHidden = false
//                    self.verifiedBadgeView.fd_collapsed = false
//
//                    if MyProfile.verification?.status == "pending"{
//                        self.btnVerifiedBadge.setTitle("Waiting for Document Approval", for: .normal)
//                    }
                }else{
                    self.verifiedBadgeView.isHidden = false
                    self.verifiedBadgeView.fd_collapsed = false
                }
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func reloadingDataAfterUpdate() {
        
        self.getPostData(userId: MyProfile.userId!) { (posts, msg) in
            
            self.postData.removeAll()
            
            if let postArr = posts {
                self.postData = postArr
            }
            self.collectionView.reloadData()
        }
    }
    
    func getPostData(userId:String, callback:@escaping (([BuyerSellerModel]?, String?)->())) {
        
        let wait =  appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kGetMyPost, parameters: ["user_id":userId]) { (data, error) in
            
            if let wait = wait{
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    var posts = [BuyerSellerModel]()
                    
                    if let des = data["description"] as? [[String:Any]] {
                        
                        for data in des {
                            posts.append(BuyerSellerModel(data: data))
                        }
                        
                        posts = posts.filter({ (post) -> Bool in
                            return !post.is_deleted
                        })
                        
                        posts.sort(by: { (post1, post2) -> Bool in
                            autoreleasepool {
                                let date1 = Date(timeIntervalSince1970: Double(post1.post_created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(post2.post_created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            }
                        })
                        callback(posts, nil)
                    }
                }
            }
        }
    }
    
    //MARK:- For login User
    @IBAction func onMenuClick(_ sender: UIButton) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController"){
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onCameraClick(_ sender: UIButton) {
        
        appDel.getAllCountries { (countries, msg) in
            
            if let countries = countries{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "StartLiveStreamViewController") as? StartLiveStreamViewController{
                    vc.countries = countries
                    let nvc = UINavigationController(rootViewController: vc)
                    self.present(nvc, animated: true, completion: nil)
                }
            }
            else{
                self.displayMessage(msg ?? "Failed to get countries")
            }
        }
    }
    
    //MARK:- For other User
    var verticalMenuDropDown = DropDown()
    @IBAction func onVerticalDotClick(_ sender: UIButton) {
        
        self.verticalMenuDropDown.dataSource = self.optionType
        self.verticalMenuDropDown.anchorView = self.btnLive
        
        self.verticalMenuDropDown.backgroundColor = .white
        self.verticalMenuDropDown.textColor = BLUECOLOR
        
        self.verticalMenuDropDown.bottomOffset = CGPoint(x: 0, y:(self.verticalMenuDropDown.anchorView?.plainView.bounds.height)!)
        self.verticalMenuDropDown.topOffset = CGPoint(x: 0, y: -(self.verticalMenuDropDown.anchorView?.plainView.bounds.height)!)
        
        self.verticalMenuDropDown.selectionAction = { (index: Int, item: String)  in
            
            if let his_id = self.userData.first?._id {
                
                if item == "Follow" || item == "Follow Back"{
                    
                    let wait = appDel.topViewController?.addWaitSpinner()
                    
                    tabBarVC?.callFollowApi(his_id: his_id, isFollow: 1, callback: { (data) in
                        
                        if let wait = wait {
                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                        }
                        
                        if let _ = data {
                            tabBarVC?.getUserData(user_id: his_id, addWait: true, callback: { (data, msg) in
                                if let uData = data {
                                    self.userData.removeAll()
                                    self.userData = uData
                                    
                                    self.optionType[0] = "Unfollow"
                                    self.collectionView.reloadData()
                                }
                            })
                        }
                        
                    })
                }
                if item == "Unfollow" {
                    
                    let wait = appDel.topViewController?.addWaitSpinner()
                    
                    tabBarVC?.callFollowApi(his_id: his_id, isFollow: 0, callback: { (data) in
                        
                        if let wait = wait {
                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                        }
                        
                        if let _ = data {
                            tabBarVC?.getUserData(user_id: his_id, addWait: true, callback: { (data, msg) in
                                if let uData = data {
                                    self.userData.removeAll()
                                    self.userData = uData
                                    
                                    if let following = self.userData.first?.following {
                                        if following.contains(where: { (following) -> Bool in
                                            return following._id == MyProfile.userId
                                        }){
                                            self.optionType[0] = "Follow Back"
                                        }else {
                                            self.optionType[0] = "Follow"
                                        }
                                    }else {
                                        self.optionType[0] = "Follow"
                                    }
                                    self.collectionView.reloadData()
                                }
                            })
                        }
                    })
                }
                if item == "Block User" {
                    
                    let wait = appDel.topViewController?.addWaitSpinner()
                    
                    tabBarVC?.callBlockApi(user_id: his_id, isBlock: 1, callback: { (msg) in
                        
                        if let wait = wait {
                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                        }
                        
                        if let message = msg {
                            self.dismiss(animated: true, completion: {
                                if let top = appDel.topViewController {
                                    top.displayMessage(message)
                                    categoryVC?.getDataFromApiAndReloadTable()
                                }
                            })
                        }
                    })
                }
                if item == "Report User" {
                    
                    if let rvc = self.storyboard?.instantiateViewController(withIdentifier: "ReportPostViewController") as? ReportPostViewController {
                        
                        rvc.userId = his_id
                        
                        rvc.modalTransitionStyle = .crossDissolve
                        rvc.modalPresentationStyle = .overCurrentContext
                        self.present(rvc, animated: true, completion: nil)
                    }
                }
            }
        }
        self.verticalMenuDropDown.show()
    }
    
    @IBAction func onCrossWhiteClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:-
    @IBAction func onVerifiedBadgeClick(_ sender: Any) {
        
        if MyProfile.verification?.status?.lowercased() == "pending"{
            return;
        }
        
        if MyProfile.verification?.status?.lowercased() == "approved" {
            
            if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PaymentForBadgeViewController") as? PaymentForBadgeViewController {
                
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                
                appDel.topViewController?.present(vc, animated: true, completion: nil)
                
                return;
            }
        }
        self.getVerifiedViewController()
    }
    
    
    //MARK:- CollectionView Method

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.postData.count + 1
    }
 
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            
            let cellIdentifier = "topCell"
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ProfileTopCustomCell
            
            cell.userData = self.userData
            cell.postData = self.postData
            cell.delegate = self
            
            if MyProfile.userId == nil || MyProfile.userId == "" {
                cell.btnEditProfile.isHidden = true
                cell.btnSetting.isHidden = true
            }
            
            if let user = userData.first {
                
                if user._id == MyProfile.userId{ // for login user
                    
                    cell.setUp()
                    cell.btnEditProfile.isHidden = false
                    cell.btnSetting.isHidden = false
                    
                    cell.lblPostCount.text = "\(self.postData.count)"
                    
                    if let followers = self.userData.first?.followers {
                        cell.lblFollowersCount.text = "\(followers.count)"
                    }else {
                        cell.lblFollowersCount.text = "0"
                    }
                    
                    if let following = self.userData.first?.following {
                        cell.lblFollowingCount.text = "\(following.count)"
                    }else {
                        cell.lblFollowingCount.text = "0"
                    }
                    
                }else { // for other user
                    
                    cell.setUpOnClickProfileImage()
                    cell.btnEditProfile.isHidden = true
                    cell.btnSetting.isHidden = true
                }
            }

            return cell
        }
        else{
            
            let cellIdentifier = "bottomCell"
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
            
            let postImages = self.postData[indexPath.row - 1].post_images
            
            if let url = postImages.first?.image_url {
                
                if let imgPost = cell.contentView.viewWithTag(1) as? UIImageView {
                    
                    imgPost.sd_addActivityIndicator()
                    
                    imgPost.sd_setImage(with: URL(string: url)) { (img, err, cache, url) in
                        
                        imgPost.sd_removeActivityIndicator()
                        
                        if img == nil{
                            imgPost.image = UIImage(named: "ic_placeholder")
                        }
                        imgPost.clipsToBounds = true
                    }
                }
            }
            
            if let isVideo = postImages.first?.is_video, isVideo {
                if let btnPlayVideo = cell.contentView.viewWithTag(2) as? UIButton {
                    btnPlayVideo.isHidden =  false
                }
            }else {
                if let btnPlayVideo = cell.contentView.viewWithTag(2) as? UIButton {
                    btnPlayVideo.isHidden =  true
                }
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let viewRect = collectionView.frame
        let widhtHalf = (self.view.frame.width-6)/3
        let width = indexPath.row == 0 ? viewRect.width : widhtHalf
        let height = indexPath.row == 0 ? CGFloat(378.0) : widhtHalf*1.1
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if indexPath.row == 0 {
            return;
        }
        
        let post = postData[indexPath.row - 1]
        
        if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpPostViewController") as? PopUpPostViewController{
            
            vc.post = post
            
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            
            self.present(vc, animated: true, completion: nil)
        }
        
        //let postImages = postData[indexPath.row - 1].post_images
        
        //self.postImages = postImages
        
//        if let isVideo = postImages.first?.is_video, isVideo {
//
//            if let imageUrl = postData[indexPath.row - 1].post_images.first?.image_url {
//                playVideo(imageUrl:imageUrl)
//            }
//        }else {
//            self.showImagesPreview(index: 0)
//        }
    }
    
    func playVideo(imageUrl:String) {
        
        let videoUrl = imageUrl.replacingOccurrences(of: ".jpg", with: ".mp4")
        
        let player = AVPlayer(url: URL(string: videoUrl)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        
        if let top = appDel.topViewController {
            top.present(playerController, animated: true) {
                player.play()
            }
        }
    }
    
    var follower = [UserModel]()
    
    //MARK: ProfileCellDelegate
    func profileSelection(type: ProfileCellSelectionType) {
        
        switch type {
            
        case .edit:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountViewController") as? AccountViewController{
                if let data = self.postData.first {
                    if let userDetails = data.userDetails.first {
                        vc.userDetails = userDetails
                    }
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case .setting:
            self.showSettingController()
            
        case .follower:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowPeopleViewController") as? FollowPeopleViewController {
                vc.actionType = .Follower
                vc.userId = userId
                followVC = vc
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case .following:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowPeopleViewController") as? FollowPeopleViewController {
                vc.actionType = .Following
                vc.userId = userId
                followVC = vc
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func showSettingController(){
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController"){
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: GalleryItemsDataSource
    func itemCount() -> Int {
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    // MARK: Image preview
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        for image in self.postImages {
            
            let galleryItem = GalleryItem.image { imageCompletion in
                
                SDWebImageDownloader.shared().downloadImage(with: URL(string: image.image_url!), options: [], progress: nil, completed: { (img, data, err, fin) in
                    
                    if let image = img{
                        imageCompletion(image)
                    }
                })
            }
            self.dataImage.append(galleryItem)
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
        //self.dismiss(animated: true, completion: nil)
    }
}

enum ProfileCellSelectionType{
    case edit
    case setting
    case follower
    case following
}

protocol ProfileCellDelegate {
    func profileSelection(type: ProfileCellSelectionType)
}

class ProfileTopCustomCell: UICollectionViewCell {
    
    var delegate: ProfileCellDelegate!
    
    var postData = [BuyerSellerModel]()
    var userData = [UserModel]()
    
    @IBOutlet weak var imgVerified:UIImageView!
    @IBOutlet weak var imgLive:UIImageView!
    var pulsator:Pulsator?
    
    @IBOutlet weak var lblFollowingCount: UILabel!
    @IBOutlet weak var lblFollowersCount: UILabel!
    @IBOutlet weak var lblPostCount: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var btnSetting: UIButton!
    
    @IBOutlet weak var pulseView: UIView!
    
    var topArr: (String, UIImage)? {
        didSet{
            if let data = topArr{
                if data.0 == "topCell"{
                    self.profileImage.image = data.1
                }
            }
            self.setUp()
        }
    }
    
    override func prepareForReuse() {
        
        self.imgLive.isHidden = true
        
        self.pulsator?.removeFromSuperlayer()
        
        self.profileImage.layer.removeAllAnimations()
    }
    
    func setUp() {
        
        if let name = MyProfile.userName{
            self.lblName.text = name
        }
        if let countryName = MyProfile.countryName{
            self.lblCountry.text = countryName
        }
        if let value = MyProfile.profileDescription{
            self.lblDescription.text = value
        }
        if let imageUrl = MyProfile.userProfilePhoto {
            self.profileImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "icon_profile_grey2"), options: [], progress: nil, completed: nil)
        }
        if let value = MyProfile.description, value != "" {
            self.lblDescription.text = value
        }else{
            self.lblDescription.text = ""
        }
        
        if MyProfile.isVerified{
            self.imgVerified.image = #imageLiteral(resourceName: "icon_privacy_yellow")
        }else{
            self.imgVerified.image = #imageLiteral(resourceName: "icon_privacy_grey")
        }
    }
    
    func setUpOnClickProfileImage() {
        
        if let name = userData.first?.username{
            self.lblName.text = name
        }
        if let countryName = userData.first?.countryDetails.first?.countryName{
            self.lblCountry.text = countryName
        }
        if let value = userData.first?.description{
            self.lblDescription.text = value
        }
        if let imageUrl = userData.first?.profilePhoto {
            self.profileImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "icon_profile_grey2"), options: [], progress: nil, completed: nil)
        }
        
        self.lblPostCount.text = "\(postData.count)"
        
        if let user = userData.first {
            self.lblFollowersCount.text = "\(user.followers.count)"
            self.lblFollowingCount.text = "\(user.following.count)"
        }
        if let isLive = self.userData.first?.isLive, isLive{
            self.addLiveAnimation()
        }
        if let verified = self.userData.first?.isVerified, verified{
            self.imgVerified.image = #imageLiteral(resourceName: "icon_privacy_yellow")
        }else{
            self.imgVerified.image = #imageLiteral(resourceName: "icon_privacy_grey")
        }
    }
    
    @IBAction func onEditClick(_ sender: UIButton) {
        delegate.profileSelection(type: .edit)
    }
    
    @IBAction func onSettingClick(_ sender: UIButton) {
        delegate.profileSelection(type: .setting)
    }
    
    @IBAction func onFollowersClick(_ sender: Any) {
        delegate.profileSelection(type: .follower)
    }
    
    @IBAction func onFollowingClick(_ sender: Any) {
        delegate.profileSelection(type: .following)
    }
    
    
    func addLiveAnimation(){
        
        self.imgLive.isHidden = false
        
        //add animation to image
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.duration = 1.0
        animation.repeatCount = .infinity
        animation.fromValue = 1.0
        animation.toValue = 0.5
        animation.autoreverses = true
        
        
        self.profileImage.layer.add(animation, forKey: "transform.scale")
        
        self.pulsator = Pulsator()
        self.pulsator?.backgroundColor = UIColor.red.cgColor
        self.pulseView.layer.addSublayer(pulsator!)
        self.pulsator?.numPulse = 3
        self.pulsator?.radius = 70
        self.pulsator?.start()
    }
}
