

import UIKit

class CategoriesSearchViewController:  UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var posts = [BuyerSellerModel]()
    
    var tagList = [TagModel]()
    var filterTag = [TagModel]()
    
    @IBOutlet weak var tableView: UITableView!
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        
        tableView.addSubview(refreshControl)
        //
        
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getData(text: nil, showWait: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        searchVC?.searchTextField.text = ""
        searchVC?.searchTextField.resignFirstResponder()
        
        self.getData(text: nil, showWait: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func getData(text:String?, showWait:Bool) {
        
        self.isSearchMode = false
        
        self.tableView.addWaitView()
        
        searchVC?.getCategoryAndCount(text: text, showWait: false, callback: { (tags, msg) in
            
            self.tableView.removeWaitView()
            
            self.emptyText = NSLocalizedString("No Tag Found!!", comment: "No Tag Found!!")
            
            self.tagList.removeAll()
            
            if let tags = tags {
                self.tagList = tags
            }
            
            self.tableView.reloadData()
        })
    }
    
    var isSearchMode = false
    
    
    func searchTags(text:String,showWait:Bool) {
        
        self.isSearchMode = true
        
        self.filterTag.removeAll()

        self.filterTag = self.tagList.filter({$0.name!.lowercased().contains(text.lowercased())})
        
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchMode {
            return self.filterTag.count
        }else{
            return self.tagList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CategoryTableViewCell
        
        if isSearchMode {
            cell.setupData(tag: self.filterTag[indexPath.row])
        }else{
            cell.setupData(tag: self.tagList[indexPath.row])
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let tagId = self.tagList[indexPath.row]._id
        let name = self.tagList[indexPath.row].name
        
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController {
        
            vc.title = name
            vc.tagId = tagId
            vc.fromVC = .searchCategory
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

class CategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTagFirstLetter: UILabel!
    @IBOutlet weak var lblTagName: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    
    func setupData(tag:TagModel!) {
        
        self.lblTagName.text = tag.name
        self.lblCount.text = "\(tag.number )"
        self.lblTagFirstLetter.text = "\(tag.name!.first!)"
        
        self.lblTagFirstLetter.backgroundColor = getRandomColor()
        self.lblTagFirstLetter.textColor = .white
        
    }
}
