
import UIKit

struct PlaceAndCount {
    
    var name:String!
    var number:Int = 0
    
    init(data:[String:Any]) {
        if let value = data["name"] as? String {
            self.name = value
        }else{
            self.name = "NA"
        }
        if let value = data["number"] as? Int {
            self.number = value
        }
    }
}

class PlacesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var placeList = [PlaceAndCount]()
    
    var filterPlace = [PlaceAndCount]()
    
    @IBOutlet weak var tableView: UITableView!
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        
        tableView.addSubview(refreshControl)
        //
        
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getData(text: nil, showWait: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        searchVC?.searchTextField.text = ""
        searchVC?.searchTextField.resignFirstResponder()
        
        self.getData(text: nil, showWait: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func getData(text:String?, showWait:Bool){
        
        self.isSearchMode = false
        
        if showWait{
            self.tableView.addWaitView()
            
        }
        searchVC?.getPlacesAndCount(text: text, showWait: false, callback: { (places, msg) in
            
            self.tableView.removeWaitView()
            
            self.emptyText = NSLocalizedString("No Places Found!!", comment: "No Places Found!!")
            
            self.placeList.removeAll()
            
            if let places = places {
                self.placeList = places
            }
            
            self.tableView.reloadData()
        })
    }
    
    var isSearchMode = false
    
    func searchPlaces(text:String,showWait:Bool) {
        
        self.isSearchMode = true
        
        self.filterPlace.removeAll()
        
        self.filterPlace = self.placeList.filter({$0.name!.lowercased().contains(text.lowercased())})
        
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchMode {
            return self.filterPlace.count
        }else{
            return self.placeList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PlacesTableViewCell
        
        if isSearchMode {
            cell.setupData(place: self.filterPlace[indexPath.row])
        }else{
            cell.setupData(place: self.placeList[indexPath.row])
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let text = self.placeList[indexPath.row].name
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController {
            vc.title = text
            vc.placeName = text
            vc.fromVC = .placeType
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

class PlacesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTagFirstLetter: UILabel!
    @IBOutlet weak var lblTagName: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    
    func setupData(place:PlaceAndCount!) {
        
        self.lblTagName.text = place.name
        self.lblCount.text = "\(place.number)"
        self.lblTagFirstLetter.text = "\(place.name!.first!)"
        
        self.lblTagFirstLetter.backgroundColor = getRandomColor()
        self.lblTagFirstLetter.textColor = .white
        
    }
}
