

import UIKit
import SDWebImage

class TopViewController:UIViewController {

    var imagesList = [String]()
    
    var posts = [PopularPostsModel]()
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        searchVC?.searchTextField.text = ""
        searchVC?.searchTextField.resignFirstResponder()
        //self.view.endEditing(true)
        
        let window = UIWindow()
        let width = window.frame.width / 2 - 2
        let height = window.frame.height / 3
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: width, height: height)
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.minimumLineSpacing = 1.0
        layout.minimumInteritemSpacing = 1.0
        self.collectionView.setCollectionViewLayout(layout, animated: true)
        
        self.getDataAndReload()
    }
    
    var popularListWorkItem:DispatchWorkItem?
    
    func getDataAndReload() {
        
        if let workItem = self.popularListWorkItem{
            workItem.cancel()
            print("GetDataAndReload 05 Feb Cancelled")
        }
        
        self.popularListWorkItem = DispatchWorkItem(block: {
            
            print("GetDataAndReload 05 Feb Executing")
            
            self.collectionView.addWaitView()
            
            searchVC?.getPopularPostList(showWait: false, callback: {  (orders, str) in
            
                self.collectionView.removeWaitView()
                
                print("GetDataAndReload 05 Feb Completed")
                
                self.posts.removeAll()
                
                if let orders = orders, orders.count > 0 {
                    self.posts = orders
                }
                
                self.collectionView.reloadData()
                
                if postIdFromDynamicLink != ""{
                    let postId = postIdFromDynamicLink
                    postIdFromDynamicLink = ""
                    DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                        if let index = self.posts.firstIndex(where: { (post) -> Bool in
                            if post.type == .sortByOrders{
                                if let p = post.post as? BuyerSellerModel{
                                    return p._id == postId
                                }
                            }
                            return false
                        }){
                            let post = self.posts[index].post as! BuyerSellerModel
                            
                            if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpPostViewController") as? PopUpPostViewController{
                                
                                vc.post = post
                                vc.modalPresentationStyle = .overCurrentContext
                                vc.modalTransitionStyle = .crossDissolve
                                
                                appDel.topViewController?.present(vc, animated: true, completion: nil)
                            }
                        }
                    })
                }
                
                self.popularListWorkItem = nil
            })
            
        })
        
        mainQueue.asyncAfter(deadline: .now()+0.25, execute: self.popularListWorkItem!)
    }
}

extension TopViewController:UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TopCollectionViewCell
        
        let type = self.posts[indexPath.row].type
        let post = self.posts[indexPath.row].post as? BuyerSellerModel
        
        cell.post = post
        cell.index = indexPath
        
        switch type {
        case .sortByOrders:
            cell.btnPopular.isHidden = false
        case .sortByLikes:
            cell.btnPopular.isHidden = true
        }
        return cell
    }
}

extension TopViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let post = self.posts[indexPath.row].post as! BuyerSellerModel
        
        if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpPostViewController") as? PopUpPostViewController{
            
            vc.post = post
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            
            appDel.topViewController?.present(vc, animated: true, completion: nil)
        }
    }
}

class TopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var collectionCellView: UIView!
    @IBOutlet weak var btnPopular: UIButton!
    
    var index:IndexPath!
    
    var post:BuyerSellerModel!{
        didSet{
            var dataImg = [String]()
            
            if let postImages = post?.post_images{
                for post in postImages{
                    autoreleasepool {
                        if let imgUrl = post.image_url{
                            dataImg.append(imgUrl)
                        }
                    }
                }
            }
            if dataImg.count > 0{
                
                if let imageView = Bundle.main.loadNibNamed("PopularImageView", owner: self, options: nil)?.first as? PopularImageView{
                    imageView.post = self.post
                    imageView.indexPath = index
                    switch dataImg.count{
                    case 1:
                        imageView.setCardMode(mode: .one)
                    case 2:
                        imageView.setCardMode(mode: .two)
                    case 3:
                        imageView.setCardMode(mode: .three)
                    case 4:
                        imageView.setCardMode(mode: .four)
                    default:
                        imageView.setCardMode(mode: .four)
                        imageView.setMoreImage(count: dataImg.count-4)
                    }
                    
                    for i in 0..<dataImg.count{
                        autoreleasepool {
                            if let iView = imageView.getImageView(tag: i + 1){
                                iView.image = UIImage(named: "placeholder")
                                
                                iView.sd_addActivityIndicator()
                                
                                iView.sd_setImage(with: URL(string:dataImg[i]), placeholderImage: UIImage(named: "ic_placeholder"), options: [], completed: { (img, data, err, fin) in
                                    iView.sd_removeActivityIndicator()
                                    if let img = img{
                                        iView.image = img
                                    }else{
                                        iView.image = UIImage(named: "ic_placeholder")
                                    }
                                })
                            }
                        }
                    }
                    self.collectionCellView.addSubViewWithConstraints(view: imageView)
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        for v in self.collectionCellView.subviews{
            v.removeFromSuperview()
        }
        self.btnPopular.isHidden = true
    }
}
