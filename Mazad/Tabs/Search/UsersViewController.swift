

import UIKit

class UsersViewController: UIViewController{
    
    var users = [UserModel]()
    var blockedUsers = [UserModel]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        collectionView.addSubview(refreshControl)
        //
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getData(text: nil, showWait: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        searchVC?.searchTextField.text = ""
        searchVC?.searchTextField.resignFirstResponder()
        //self.view.endEditing(true)
        
        let width = (self.view.frame.size.width/3) - 3
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = CGSize(width: width, height: width)
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.minimumLineSpacing = 1.0
        layout.minimumInteritemSpacing = 1.0
        self.collectionView.setCollectionViewLayout(layout, animated: true)
        
        self.getData(text: nil, showWait: true)
    }
    
    //MARK:- getting data
    func getData(text:String?, showWait:Bool) {
        
        if let _ = MyProfile.userId {
            if showWait{
                self.collectionView.addWaitView()
            }
            
            tabBarVC?.callBlockListApi(showWait: false, callback: { (blUsers) in
                
                self.collectionView.removeWaitView()
                
                if let bUsers = blUsers{
                    self.blockedUsers.removeAll()
                    self.blockedUsers = bUsers
                }
                
                self.getUsersList(showWait: showWait, text:text)
            })
        }else{
            self.getUsersList(showWait: true, text: text)
        }
        
    }
    
    func getUsersList(showWait:Bool, text:String?) {
        
        if showWait{
            self.collectionView.addWaitView()
        }
        
        searchVC?.getUsersList(text: text, showWait: false, callback: { (users, msg) in
            
            self.collectionView.removeWaitView()
            
            self.users.removeAll()
            
            if var users = users, users.count > 0 {
                
                for u in self.blockedUsers {
                    autoreleasepool {
                        if let index = users.firstIndex(where: { (us) -> Bool in
                            return us._id == u._id
                        }){
                            users.remove(at: index)
                        }
                    }
                }
                self.users = users
            }
            self.collectionView.reloadData()
        })
    }
}

extension UsersViewController:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UsersCollectionViewCell
        
        cell.user = self.users[indexPath.row]
        
        return cell
    }
}
extension UsersViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let userId = self.users[indexPath.row]._id{
            
            if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
                
                vc.userId = userId
                
                let nav = UINavigationController(rootViewController: vc)
                //Presenting navigation Controller
                nav.modalTransitionStyle = .crossDissolve
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
}

class UsersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    var user:UserModel!{
        
        didSet{
            
            self.lblUserName.text = self.user.username
            
            self.imageUser.image = nil
            
            if let image = self.user.profilePhoto{
                
                self.imageUser.sd_addActivityIndicator()
                
                self.imageUser.sd_setImage(with: URL(string: image)) { (img, err, cache, url) in
                    
                    self.imageUser.sd_removeActivityIndicator()
                    
                    if img == nil{
                        self.imageUser.image = #imageLiteral(resourceName: "icon_profile_grey2")
                    }
                }
            }
        }
    }
}
