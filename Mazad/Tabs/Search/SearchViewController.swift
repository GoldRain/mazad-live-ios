

import UIKit
import CarbonKit


var searchVC:SearchViewController?

class SearchViewController: UIViewController{
    
   let top = NSLocalizedString("Top", comment: "Top")
   let users = NSLocalizedString("Users", comment: "Users")
   let categories = NSLocalizedString("Categories", comment: "Categories")
   let places = NSLocalizedString("Places", comment: "Places")
    
    //instaces of view controller for carbon tab swipe navigation
    var topViewController:TopViewController?
    var usersViewController:UsersViewController?
    var categoriesSearchViewController:CategoriesSearchViewController?
    var placeViewController:PlacesViewController?
    
    //Outlets to UI
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var containerView: UIView!
    
    //carbon tab swipe navigation controller
    public var carbonController: CarbonTabSwipeNavigation!
    
    @IBOutlet weak var searchBarMainView:UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        searchVC = self
        
        searchTextField.placeHolderColor = .white
        searchTextField.delegate = self
        
        self.title = NSLocalizedString("Home", comment: "Home")
        
        //create items for carbon tab swipe navigation
        
        let items = [top,
                     users,
                     categories,
                     places]
        
        carbonController = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        carbonController.setTabBarHeight(46.0)
        
        let normalColor =  UIColor(red: 103/255, green: 65/255, blue: 170/255, alpha: 1.0)
        carbonController.setNormalColor(normalColor, font: UIFont.systemFont(ofSize: 13.0, weight: .medium))
        carbonController.setSelectedColor(.white, font: UIFont.systemFont(ofSize: 13.0, weight: .medium))
        //carbonController.setNormalColor(.lightGray)
        carbonController.setIndicatorColor(.white)
        //carbonController.setSelectedColor(.white)
        carbonController.toolbar.barTintColor = .purpleColor
        carbonController.toolbar.isTranslucent = false
        carbonController.setIndicatorHeight(2.0)
        
        carbonController.insert(intoRootViewController: self, andTargetView: self.containerView)
        
        self.carbonController.setCurrentTabIndex(0, withAnimation: false)
        
        let screenBounds = UIScreen.main.bounds.width
        let widthOfTabIcons = screenBounds/4.0
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 0)
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 1)
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 2)
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 3)
        
        self.title = NSLocalizedString("Search", comment: "Search")
        
        searchBarMainView.bringSubview(toFront: self.searchTextField)
        
        self.searchTextField.delegate = self
    }
    
    
    //MARK: IBActions
    @IBAction func onSearchClick(_ sender: Any) {
        
        guard let text = self.searchTextField.text, text.trim() != "" else {
            return
        }
        
        let index = self.carbonController.currentTabIndex
        
        switch index {
            
        case 1://users
            self.usersViewController?.getData(text: text, showWait: true)
            
        case 2://category
            self.categoriesSearchViewController?.searchTags(text: text, showWait: true)
            
        default://places
            self.placeViewController?.searchPlaces(text: text, showWait: true)
        }
    }
}

//MARK: CarbonTabSwipeNavigationDelegate
extension SearchViewController:CarbonTabSwipeNavigationDelegate{
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        if index == 0 {
            if let vc = self.topViewController{
                return vc
            }
            if let vc = self.storyboard!.instantiateViewController(withIdentifier: "TopViewController") as? TopViewController{
                self.topViewController = vc
                return vc
            }
        }
        else if index == 1 {
            
            if let vc = self.usersViewController{
                return vc
            }
            if let vc = self.storyboard!.instantiateViewController(withIdentifier: "UsersViewController") as? UsersViewController{
                self.usersViewController = vc
                return vc
            }
        }
        else if index == 2 {
            
            if let vc = self.categoriesSearchViewController{
                return vc
            }
            if let vc = self.storyboard!.instantiateViewController(withIdentifier: "CategoriesSearchViewController") as? CategoriesSearchViewController{
                self.categoriesSearchViewController = vc
                return vc
            }
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "CategoriesSearchViewController")
            return vc
        }
        else{
            if let vc = self.placeViewController{
                return vc
            }
            if let vc = self.storyboard!.instantiateViewController(withIdentifier: "PlacesViewController") as? PlacesViewController{
                self.placeViewController = vc
                return vc
            }
        }
        return UIViewController()
    }
    
}

//MARK: TextField Delegate
extension SearchViewController:UITextFieldDelegate{
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if textField.text == nil || textField.text == ""{
            self.onSearchClick(textField)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        let index = self.carbonController.currentTabIndex
        
        switch index {
            
        case 1://users
            if textField.text == ""{
                self.usersViewController?.getData(text: "", showWait: true)
            }else{
                self.usersViewController?.getData(text: textField.text!, showWait: true)
            }
        case 2://category
            if textField.text == "" {
                self.categoriesSearchViewController?.isSearchMode = false
                self.categoriesSearchViewController?.tableView.reloadData()
            }else{
                self.categoriesSearchViewController?.searchTags(text: textField.text!, showWait: true)
            }
        default://places
            if textField.text == "" {
                self.placeViewController?.isSearchMode = false
                self.placeViewController?.tableView.reloadData()
            }else{
                self.placeViewController?.searchPlaces(text: textField.text!, showWait: true)
            }
        }
        return true
    }
    
}


//MARK: API's
extension SearchViewController{
    
    func getUsersList(text:String?, showWait:Bool, callback:@escaping (([UserModel]?, String?)->())){
        
        var searchText:String? = text
        
        if searchText == nil{
            if let txt = self.searchTextField.text?.trim(), txt != ""{
                searchText = txt
            }
        }
        
        guard let top = appDel.topViewController else {
            let str = NSLocalizedString("There is some error", comment: "There is some error")
            callback(nil,str)
            return
        }
        
        var wait:WaitView!
        
        if showWait{
            wait = top.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kSearchUserAll, parameters: ["text": searchText ?? "", "user_id": MyProfile.userId ?? ""]) { (data, error) in
            
            if showWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err{
                    if let array = data["description"] as? [[String:Any]]{
                        autoreleasepool {
                            var users = [UserModel]()
                            
                            for item in array{
                                users.append(UserModel(data: item))
                            }
                            
                            if let _ = MyProfile.userId {
                                users = users.filter({ (user) -> Bool in
                                    return user._id != MyProfile.userId
                                })
                            }
                            callback(users, nil)
                            return;
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
                else{
                    let msg = NSLocalizedString("No users found!!", comment: "No users found!!")
                    callback(nil, msg)
                }
            }
            else{
                let msg = NSLocalizedString("Failed to parse server response!!", comment: "Failed to parse server response!!")
                callback(nil, msg)
            }
        }
    }
    
    func getPlaceList(text:String?, showWait:Bool, callback:@escaping (([BuyerSellerModel]?, String?)->())){
        
        guard let top = appDel.topViewController else {
            callback(nil, "There is some error")
            return
        }
        
        var wait:WaitView!
        
        if showWait{
            wait = top.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kSearchByPlace, parameters: ["placeName": text ?? "" ]) { (data, error) in
            
            if showWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err{
                    if let array = data["description"] as? [Any]{
                        
                        autoreleasepool {
                            var posts = [BuyerSellerModel]()
                            
                            for res in array{
                                if let res = res as? [[String:Any]]{
                                    for item in res{
                                        posts.append(BuyerSellerModel(data: item))
                                    }
                                }
                                if let res = res as? [String:Any] {
                                    posts.append(BuyerSellerModel(data: res))
                                }
                            }
                            
                            posts.sort(by: { (post1, post2) -> Bool in
                                let date1 = Date(timeIntervalSince1970: Double(post1.post_created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(post2.post_created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            })
                            
                            callback(posts, nil)
                            return;
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
                else{
                    let msg = NSLocalizedString("No places found!!", comment: "No places found!!")
                    callback(nil, msg)
                }
            }
            else{
                let msg = NSLocalizedString("Failed to parse server response!!", comment: "Failed to parse server response!!")
                callback(nil, msg)
            }
        }
    }
    
    func getCategoryList(text:String?, showWait:Bool, callback:@escaping (([BuyerSellerModel]?, String?)->())){
        
        guard let top = appDel.topViewController else {
            callback(nil, "There is some error")
            return
        }
        
        var wait:WaitView!
        
        if showWait{
            wait = top.addWaitSpinner()
        }
        let params = ["category": text ?? "" ]
        
        print("Post Count Params \(params)")
        
        JSONRequest.makeRequest(kSearchCategories, parameters: params) { (data, error) in
            
            if showWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err{
                    if let array = data["description"] as? [Any]{
                        
                        autoreleasepool {
                            var posts = [BuyerSellerModel]()
                            
                            for res in array{
                                if let res = res as? [[String:Any]]{
                                    for item in res{
                                        posts.append(BuyerSellerModel(data: item))
                                    }
                                }
                                if let res = res as? [String:Any] {
                                    posts.append(BuyerSellerModel(data: res))
                                }
                            }
                            
                            posts.sort(by: { (post1, post2) -> Bool in
                                let date1 = Date(timeIntervalSince1970: Double(post1.post_created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(post2.post_created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            })
                            
                            callback(posts, nil)
                            return;
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
                else{
                    let msg = NSLocalizedString("No category found!!", comment: "No category found!!")
                    callback(nil, msg)
                }
            }
            else{
                let msg = NSLocalizedString("Failed to parse server response!!", comment: "Failed to parse server response!!")
                callback(nil, msg)
            }
        }
    }
    
    func getPopularPostList(showWait:Bool, callback:@escaping (([PopularPostsModel]?, String?)->())) {
        
        guard let top = appDel.topViewController else {
            callback(nil, "There is some error")
            return
        }
        
        var wait:WaitView!
        
        if showWait{
            wait = top.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kPopularPost, parameters: ["":""]) { (data, error) in
            
            if showWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err{
                    
                    autoreleasepool {
                        var orders = [PopularPostsModel]()
                        
                        if let sortByOrders = data["sortByOrders"] as? [[String:Any]] {
                            for post in sortByOrders {
                                orders.append(PopularPostsModel(type: .sortByOrders, post: BuyerSellerModel(data: post)))
                            }
                        }
                        if let sortByLikes = data["sortByLikes"] as? [[String:Any]] {
                            for post in sortByLikes {
                                //remove duplicacy
                                //if this post doesnot exist in the orders
                                //add it to the orders
                                if orders.contains(where: { (postObj) -> Bool in
                                    autoreleasepool {
                                        if let postObj = postObj.post as? BuyerSellerModel{
                                            if postObj._id == post["_id"] as? String{
                                                return true
                                            }
                                        }
                                        return false
                                    }
                                }) == false{
                                    orders.append(PopularPostsModel(type: .sortByLikes, post: BuyerSellerModel(data: post)))
                                }
                            }
                        }
                        callback(orders, nil)
                    }
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }else{
                    let msg = NSLocalizedString("No Popular Post found!!", comment: "No Popular Post found!!")
                    callback(nil, msg)
                }
            }else{
                let msg = NSLocalizedString("Failed to parse server response!!", comment: "Failed to parse server response!!")
                callback(nil, msg)
            }
        }
    }
    
    func getCategoryAndCount(text:String?, showWait:Bool, callback:@escaping (([TagModel]?, String?)->())) {
        
        var wait:WaitView?
        
        if showWait {
            wait = appDel.topViewController?.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kCategoryAndCount, parameters: [:]) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                if let err = data["error"] as? Bool, !err {
                    if let des = data["description"] as? [[String:Any]] {
                        
                        var tagList = [TagModel]()
                        
                        for tag in des {
                            tagList.append(TagModel(data: tag))
                        }
                        
                        tagList = tagList.filter({ (tag) -> Bool in
                            return tag.number > 0
                        })
                        
                        tagList.sort(by: { (tag1, tag2) -> Bool in
                            return tag1.name!.compare(tag2.name!) == ComparisonResult.orderedAscending
                        })
                        
                        callback(tagList, nil)
                        return;
                        
                    }else if let msg = data["message"] as? String{
                        callback(nil, msg)
                    }
                }else{
                    let msg = NSLocalizedString("No Tag found!!", comment: "No Tag found!!")
                    callback(nil, msg)
                }
            }else{
                let msg = NSLocalizedString("Failed to parse server response!!", comment: "Failed to parse server response!!")
                callback(nil, msg)
            }
        }
    }
    
    func getPlacesAndCount(text:String?, showWait:Bool, callback:@escaping (([PlaceAndCount]?, String?)->())) {
        
        var wait:WaitView?
        
        if showWait {
            wait = appDel.topViewController?.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kPlaceAndCount, parameters: [:]) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                if let err = data["error"] as? Bool, !err {
                    if let des = data["description"] as? [[String:Any]] {
                        
                       var placeList = [PlaceAndCount]()
                        
                        for place in des {
                            placeList.append(PlaceAndCount(data: place))
                        }
                        
                        placeList = placeList.filter({ (place) -> Bool in
                            return place.number > 0
                        })
                        
                        placeList.sort(by: { (place1, place2) -> Bool in
                            return place1.name!.compare(place2.name!) == ComparisonResult.orderedAscending
                        })
                        
                        callback(placeList, nil)
                        return;
                        
                    }else if let msg = data["message"] as? String{
                        callback(nil, msg)
                    }
                }else{
                    let msg = NSLocalizedString("No Places found!!", comment: "No Places found!!")
                    callback(nil, msg)
                }
            }else{
                let msg = NSLocalizedString("Failed to parse server response!!", comment: "Failed to parse server response!!")
                callback(nil, msg)
            }
        }
    }
}

public enum popularType{
    case sortByOrders
    case sortByLikes
}

class PopularPostsModel{
    
    var type = popularType.sortByOrders
    var post:Any!
    
    init(type:popularType, post:Any){
        self.type = type
        self.post = post
    }
}

