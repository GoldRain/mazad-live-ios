//
//  CategoriesBuyerCell.swift
//  Mazad Live
//
//  Created by MAC_2 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import SDWebImage
import ImageViewer
import AVKit
import AVFoundation
import CoreLocation

enum CategoriesBuyerCellOption:String {
    case profileClick = "Profile Click"
    case buyNow = "Buy Now"
    case makeABid = "Make A Bid"
    case menuOption = "Menu Option"
}

protocol CategoryBuyerCellActionDelegate {
    func onBuyerCellAction(indexPath: IndexPath, option:CategoriesBuyerCellOption, post:BuyerSellerModel)
}

class CategoryBuyerCell: UITableViewCell, GalleryItemsDataSource, GalleryItemsDelegate, PayementViewControllerDelegate {
    
    var delegate: CategoryBuyerCellActionDelegate!
    
    var indexPath:IndexPath!
    
    var postDataModel:BuyerSellerModel!
    
    var dataImage: [GalleryItem] = [GalleryItem]()
    var galleryController:GalleryViewController?
    
    var postImages = [PostImageModel]()
    
    var postId = ""
    
    // Only in SoldCel
    @IBOutlet weak var soldDarkView: UIView!
    @IBOutlet weak var lblSold: UILabel!
    //
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    
    @IBOutlet weak var btnHeart: UIButton!
    @IBOutlet weak var btnWhatsapp: UIButton!
    @IBOutlet weak var btnStar: UIButton!
    
    @IBOutlet weak var imageMainView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    
    //buyNow and make a bid is 
    @IBOutlet weak var buyNowView: UIView!
    @IBOutlet weak var btnBuyNow: UIButton!
    
    @IBOutlet weak var makeABidView: UIView!
    @IBOutlet weak var btnMakeABid: UIButton!
    
    @IBOutlet weak var btnUserImage: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserDescription: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var lblPostDetails: UILabel!
    @IBOutlet weak var lblPostDetailsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mainViewOfPostImage: UIView!
    //
    @IBOutlet weak var buySellContainerView: UIView!
    
    @IBOutlet weak var btnThumbsUp: UIButton!
    @IBOutlet weak var btnThumbsDown: UIButton!
    @IBOutlet weak var lblPercentage: UILabel!
    
    @IBOutlet weak var btnMenuOption: UIButton!
    
    @IBOutlet weak var btnPriceForBuyNow: UIButton!
    
    @IBOutlet weak var btnPlayVideo: UIButton!
    //
    @IBOutlet weak var imgVerified: UIImageView!
    @IBOutlet weak var btnPromoteNow: UIButton!
    //
    //Feedback view
    @IBOutlet weak var feedbackMainView: UIView!
    @IBOutlet weak var lblFeedback: UILabel!
    
    
    @objc func onUpdateVoteNotification(not:Notification){
        if let percent = not.userInfo?["percent"] as? String{
            if let postId = not.userInfo?["postId"] as? String{
                if postId == self.postDataModel._id{
                    self.lblPercentage.text = percent
                    self.postDataModel.percent = percent
                }
            }
        }
    }
    
    lazy var geocoder = CLGeocoder()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK:- SetupUI
    func setupUI() {
        
        NotificationCenter.default.removeObserver(self, name: .UpdateVotePercent, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateVoteNotification(not:)), name: .UpdateVotePercent, object: nil)
        
        self.imageMainView.addShadow()
        self.mainContainerView.addShadow()
        self.mainViewOfPostImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePostImageViewer)))
        
        let userDetails = postDataModel.userDetails
        let postImages = postDataModel.post_images
        let postLike = postDataModel.post_like
        let votes = postDataModel.vote
        
        
        self.postImages = postImages
        //
        if self.btnPlayVideo != nil {
            if let isVideo = postImages.first?.is_video, isVideo {
                self.btnPlayVideo.isHidden = false
                self.btnPlayVideo.isUserInteractionEnabled = true
            }else {
                self.btnPlayVideo.isHidden = true
                self.btnPlayVideo.isUserInteractionEnabled = false
            }
        }
        if let userName = userDetails.first?.username {
            self.lblUserName.text = userName
        }
        
        if let latString = postDataModel.latitude, latString != "" {
            if let lngString = postDataModel.longitude, lngString != "" {
                let lat = Double(latString)
                let lng = Double(lngString)
                
                // Create Location
                let location = CLLocation(latitude: lat!, longitude: lng!)
                
                // Geocode Location
                geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                    // Process Response
                    
                    if let error = error {
                        print("Unable to Reverse Geocode Location (\(error))")
                    } else {
                        if let placemarks = placemarks, let placemark = placemarks.first {
                            self.lblUserDescription.text = placemark.compactAddress
                        }
                    }
                }
            }
        }else {
            self.lblUserDescription.text = ""
        }
        
        if let profileImage = userDetails.first?.profilePhoto {
            self.btnUserImage.sd_setImage(with: URL(string: profileImage), for: .normal, placeholderImage: #imageLiteral(resourceName: "icon_profile_grey2"), options: [], completed: nil)
            self.btnUserImage.restorationIdentifier = "\(indexPath.row)"
        }
        
        self.postImage.image = UIImage(named: "ic_placeholder")
        
        if let url = postImages.first?.image_url {
            self.postImage.sd_addActivityIndicator()
            self.postImage.sd_setImage(with: URL(string: url)) { (img, err, cache, url) in
                self.postImage.sd_removeActivityIndicator()
                if img == nil{
                    self.postImage.image = UIImage(named: "ic_placeholder")
                }
            }
        }
        for like in postLike {
            if like._id == MyProfile.userId {
                self.btnHeart.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            }
        }
        if let favPosts = MyProfile.favPostId {
            if favPosts.contains(where: { (favPostId) -> Bool in
                return favPostId == postDataModel._id
            }){
                self.btnStar.setImage(#imageLiteral(resourceName: "ic_star_fill"), for: .normal)
            }
        }
        if votes.count > 0 {
            
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "1"
            }){
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "ic_like_fill"), for: .normal)
            }
            
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "0"
            }){
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "ic_dislike_fill"), for: .normal)
            }
        }
        
        if let amount = postDataModel.amount, let doubleAmount = Double(amount) {
            
            let newAmount = String(format: "%.2f", doubleAmount)
            
            self.btnPriceForBuyNow.setTitle("\(NSLocalizedString("Price", comment: "Price")) KD \(newAmount)", for: .normal)
        }
        
        //check for buynow
        
        if let value = self.postDataModel.buy_now, value {
            if buyNowView != nil && btnBuyNow != nil{
                self.buyNowView.backgroundColor = BLUECOLOR
                 self.btnBuyNow.isUserInteractionEnabled = true
            }
        }else {
            if buyNowView != nil && btnBuyNow != nil {
                self.buyNowView.backgroundColor = UIColor.gray
                self.btnBuyNow.isUserInteractionEnabled = false
            }
        }
        
        //check for Auction
        
        if let value = self.postDataModel.is_auction, value {
            if makeABidView != nil && btnMakeABid != nil {
                self.makeABidView.backgroundColor = BLUECOLOR
                self.btnMakeABid.isUserInteractionEnabled = true
            }
        }else {
            if makeABidView != nil && btnMakeABid != nil {
                self.makeABidView.backgroundColor = UIColor.gray
                self.btnMakeABid.isUserInteractionEnabled = false
            }
        }
        
        if let user = userDetails.first {
            if let userId = MyProfile.userId {
                if user._id == userId {
                    if let isSold = postDataModel.is_sold, !isSold {
                        self.buyNowView.isHidden = true
                        self.makeABidView.isHidden = true
                        self.buyNowView.isUserInteractionEnabled = false
                        self.makeABidView.isUserInteractionEnabled = false
                        self.buySellContainerView.fd_collapsed = true
                    }
                }else {
                    if let isSold = postDataModel.is_sold, !isSold {
                        self.buyNowView.isHidden = false
                        self.makeABidView.isHidden = false
                        self.buySellContainerView.fd_collapsed = false
                        self.buyNowView.isUserInteractionEnabled = true
                        self.makeABidView.isUserInteractionEnabled = true
                    }
                }
            }else{ // it means that user is not login
                if let isSold = postDataModel.is_sold, !isSold {
                    self.buyNowView.isHidden = true
                    self.makeABidView.isHidden = true
                    self.buyNowView.isUserInteractionEnabled = false
                    self.makeABidView.isUserInteractionEnabled = false
                    self.buySellContainerView.fd_collapsed = true
                }
//                self.buyNowView.isHidden = false
//                self.makeABidView.isHidden = false
//                self.buySellContainerView.fd_collapsed = false
//                self.buyNowView.isUserInteractionEnabled = true
//                self.makeABidView.isUserInteractionEnabled = true
            }
        }
        
        if let percent = self.postDataModel.percent, percent != "" {
            //print("percent value \(percent)")
            self.lblPercentage.text = percent
        }else {
            self.lblPercentage.text = "0%"
        }
        
        if let postDetails = self.postDataModel.details, postDetails != "" {
            self.lblPostDetails.text = postDetails
        }else{
            self.lblPostDetails.text = ""
        }
        
//        if let postDetails = self.postDataModel.details, postDetails != "" {
//            self.lblPostDetails.text = postDetails
//            self.lblPostDetailsHeight.constant = 15
//        }else {
//            self.lblPostDetailsHeight.constant = 0
//            self.lblPostDetails.text = ""
//        }
        
        if let isWhatsaappOnly = self.postDataModel.whatsapp_only, isWhatsaappOnly {
            if let userId = MyProfile.userId {
                if userDetails.first?._id == userId {
                    self.btnWhatsapp.isHidden = true
                    self.btnCall.isHidden = true
                }else {
                    self.btnWhatsapp.isHidden = false
                    self.btnCall.isHidden = true
                }
            }
        }
        
        if let isWhatsappAndCall = self.postDataModel.whatsapp_and_call, isWhatsappAndCall {
            if let userId = MyProfile.userId {
                if userDetails.first?._id == userId {
                    self.btnWhatsapp.isHidden = true
                    self.btnCall.isHidden = true
                }else {
                    self.btnWhatsapp.isHidden = false
                    self.btnCall.isHidden = false
                }
            }
        }
        
        if let userId = MyProfile.userId {
            if userDetails.first?._id == userId {
                self.btnMessage.isHidden = true
            }else {
                self.btnMessage.isHidden = false
            }
        }
        
        if let isVerified = userDetails.first?.isVerified, isVerified{
            self.imgVerified.image = #imageLiteral(resourceName: "icon_privacy_yellow")
        }else{
            self.imgVerified.image = #imageLiteral(resourceName: "icon_privacy_grey")
        }
        
        if self.btnPromoteNow != nil {
            
            if let id = postDataModel.post_user_id {
                if id == MyProfile.userId {
                    self.btnPromoteNow.isHidden = false
                    if postDataModel.is_promoted {
                        self.btnPromoteNow.isHidden = false
                        self.btnPromoteNow.setTitle(NSLocalizedString("Promoted", comment: "Promoted"), for: .normal)
                        self.btnPromoteNow.backgroundColor = GREENCOLOR
                        self.btnPromoteNow.isUserInteractionEnabled = false
                    }else{
                        self.btnPromoteNow.isHidden = false
                        self.btnPromoteNow.setTitle(NSLocalizedString("Promote Now", comment: "Promote Now"), for: .normal)
//                        self.btnPromoteNow.backgroundColor = BLUECOLOR
//                        self.btnPromoteNow.isUserInteractionEnabled = true
                        self.btnPromoteNow.backgroundColor = UIColor.gray
                        self.btnPromoteNow.isUserInteractionEnabled = false
                    }
                }else{
                    self.btnPromoteNow.isHidden = true
                    if postDataModel.is_promoted {
                        self.btnPromoteNow.isHidden = false
                        self.btnPromoteNow.setTitle(NSLocalizedString("Sponsored", comment: "Sponsored"), for: .normal)
                        self.btnPromoteNow.backgroundColor = GREENCOLOR
                        self.btnPromoteNow.isUserInteractionEnabled = false
                    }else{
                        self.btnPromoteNow.isHidden = true
                    }
                }
            }
        }
        
        let feedbacks = self.postDataModel.feedback
        
        if feedbacks.count == 0 {
            self.feedbackMainView.fd_collapsed = true
        }else{
            self.feedbackMainView.fd_collapsed = false
            self.lblFeedback.text = NSLocalizedString("Feedback", comment: "Feedback")
        }
    }
    
    var isPromoted:Bool!
    
    //MARK:-
    @objc func handlePostImageViewer() {
        self.showImagesPreview(index: 0)
        //        self.lblDescription.fd_collapsed = !self.lblDescription.fd_collapsed
        //        self.contentView.setNeedsLayout()
        //        self.contentView.layoutSubviews()
        //        self.contentView.layoutIfNeeded()
    }
    
    @IBAction func onPromoteClick(_ sender: Any) {
        
        if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PurchaseDescriptionViewController") as? PurchaseDescriptionViewController{
            
            let desc = NSLocalizedString("Promoted Post will be shown to All Users", comment: "Promoted Post will be shown to All Users")
            let title = NSLocalizedString("Promote Now", comment: "Promote Now")
            
            vc.purchaseData = PurchaseData(title: title,
                                           amount: "KD5",
                image: #imageLiteral(resourceName: "img4.jpg"),
                descText: desc)
            
            vc.callback = {
                
                guard let fullName = MyProfile.userName else {
                    return;
                }
                var firstName = ""
                var lastName = ""
                var components = fullName.components(separatedBy: " ")
                if(components.count > 0)
                {
                    firstName = components.removeFirst()
                    lastName = components.joined(separator: " ")
                }
                guard let email = MyProfile.userEmail else{
                    return;
                }
                guard let number = MyProfile.phone else{
                    return;
                }
                
                _ = PaymentViewController.showPaymentWithData(amount: 5.0,
                                                              currency: "kwd",
                                                              customerFirstName: firstName,
                                                              customerLastName: lastName,
                                                              customerEmail: email,
                                                              customerPhone: number,
                                                              delegate: self)
            }
            
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            
            appDel.topViewController?.present(vc, animated: true, completion: nil)
        }
        
        
    }
    
    //MARK:- Payment ViewController delegate Method
    func paymentWebViewDidCancel(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
        }
    }
    
    func paymentWebViewDidFinish(_ paymentViewController: PaymentViewController, paymentInfo: PaymentData) {
        
        paymentViewController.dismiss(animated: true) {
            print(paymentInfo)
            
            self.callPromotePostApi()
        }
    }
    
    func paymentWebViewDidFail(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
        }
    }
    
    func callPromotePostApi() {
        
        let params = ["post_id":self.postDataModel._id!,
                      "is_promote":"\(true)"]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kUpdatePromote, parameters: params) { (data, error) in
            
            if let wait = wait{
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err {
                    
                    //categoryVC?.getDataFromApiAndReloadTable()
                    
                    self.postDataModel.is_promoted = true
                    categoryVC?.tableView.reloadRows(at: [self.indexPath], with: UITableViewRowAnimation.automatic)
                }else{
                    let str = NSLocalizedString("Failed to Promote Post", comment: "Failed to Promote Post")
                    appDel.topViewController?.displayMessage(str)
                }
            }else{
                let str = NSLocalizedString("Failed to Promote Post", comment: "Failed to Promote Post")
                appDel.topViewController?.displayMessage(str)
            }
        }
    }
    
    //MARK:-
    
    override func prepareForReuse() {
        
        self.btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
        self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
        self.btnHeart.setImage(#imageLiteral(resourceName: "icon_like_red"), for: .normal)
        self.btnStar.setImage(#imageLiteral(resourceName: "icon_star_yellow"), for: .normal)
        
        self.btnPromoteNow.isHidden = true
    }
    
    //MARK:- whatsapp & call & messages Action
    @IBAction func onClickAction(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else {
            getLoginDialouge()
            return;
        }
        
        guard let phone = self.postDataModel.userDetails.first?.phone, let countryCode = self.postDataModel.userDetails.first?.country_code else{
            let str = NSLocalizedString("Phone number Not Found!", comment: "Phone number Not Found!")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        switch sender {
        case btnWhatsapp:
            openWhatsApp(phoneNumber: countryCode + phone, message: "Hello")
        case btnMessage:
            self.startChat()
        case btnCall:
            openCallingApp(phoneNumber:countryCode + phone)
        default:
            break
        }
    }
    
    func startChat() {
        
        guard let _ = MyProfile.userId else {
            getLoginDialouge ()
            return;
        }
        
        //UIApplication.shared.open(URL(string: "sms:12345678901")!, options: [:], completionHandler: nil)
        if let top = appDel.topViewController{
            if let vc = top.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController{
                
                if let otherUserId = self.postDataModel.userDetails.first?._id{
                    
                    messageCenter?.getRoomFromLocal(otherUserId: otherUserId, roomId: nil, callback: {  (room) in
                        
                        vc.room = room
                        vc.otherId = otherUserId
                        vc.userName = self.postDataModel.userDetails.first?.username
                        
                        mainQueue.async {
                            top.navigationController?.pushViewController(vc, animated: true)
                        }
                    })
                }
            }
        }
    }
    
    
    // MARK:- VoteClick
    
    var workItem3:DispatchWorkItem?
    
    var isThumbsUp = 0
    var isThumbsDown = 0
    
    @IBAction func onVoteClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else {
            getLoginDialouge()
            return;
        }
        
        if MyProfile.isSeller {
            let votes = postDataModel.vote
            
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "1"
            }){
                self.isThumbsUp = 1
            }
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "0"
            }){
                self.isThumbsDown = 0
            }
            
        }else {
            let votes = postDataModel.vote
            
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "1"
            }){
                self.isThumbsUp = 1
            }
            if votes.contains(where: { (vote) -> Bool in
                return vote.user_id == MyProfile.userId && vote.response == "0"
            }){
                self.isThumbsDown = 0
            }
        }
        
        workItem3?.cancel()
        
        if sender == self.btnThumbsUp {
            
            if self.btnThumbsUp.image(for: .normal) == #imageLiteral(resourceName: "icon_like_green") {
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "ic_like_fill"), for: .normal)
                if btnThumbsDown.image(for: .normal) == #imageLiteral(resourceName: "ic_dislike_fill") {
                    self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
                }
                self.isThumbsUp = 1
                
            }else {
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
                
                self.isThumbsUp = 1
            }
            
            self.workItem3 = DispatchWorkItem(block: {
                self.callVotePost(isVote: self.isThumbsUp)
            })
        }
        if sender == self.btnThumbsDown {
            
            if self.btnThumbsDown.image(for: .normal) == #imageLiteral(resourceName: "icon_dislike_red") {
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "ic_dislike_fill"), for: .normal)
                if btnThumbsUp.image(for: .normal) == #imageLiteral(resourceName: "ic_like_fill") {
                    btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
                }
                self.isThumbsDown = 0
                
            }else {
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
                
                self.isThumbsDown = 0
            }
            
            self.workItem3 = DispatchWorkItem(block: {
                self.callVotePost(isVote: self.isThumbsDown)
            })
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: self.workItem3!)
    }
    
    func callVotePost(isVote:Int) {
        
        if MyProfile.isSeller {
            guard let postId = postDataModel._id else {
                return
            }
            self.postId = postId
        }else {
            guard let postId = postDataModel._id else {
                return
            }
            self.postId = postId
        }
        
        let params = ["post_id":postId,
                      "user_id":MyProfile.userId!,
                      "is_vote":"\(isVote)"]
        
        JSONRequest.makeRequest(kVote, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [String:Any] {
                        
                        if let votes = des["vote"] as? [[String:Any]] {
                            
                            if MyProfile.isSeller {
                                if self.postDataModel.vote.count > 0 {
                                    self.postDataModel.vote.removeAll()
                                }
                            }else {
                                if self.postDataModel.vote.count > 0 {
                                    self.postDataModel.vote.removeAll()
                                }
                            }
                            
                            for vote in votes {
                                if MyProfile.isSeller {
                                    
                                    self.postDataModel.vote.append(VoteModel(data: vote))
                                }else {
                                    
                                    self.postDataModel.vote.append(VoteModel(data: vote))
                                }
                            }
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                    // add toast
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                // add toast
            }
        }
    }
    
    // MARK:- StarClick
    
    var workItem2:DispatchWorkItem?
    var isFav = false
    
    @IBAction func onStarClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else {
            getLoginDialouge()
            return;
        }
        
        workItem2?.cancel()
        
        if let postIds = MyProfile.favPostId {
            if postIds.contains(where: { (postId) -> Bool in
                if MyProfile.isSeller {
                    if let id = postDataModel._id {
                        return postId == id
                    }
                    return false
                }else {
                    if let id = postDataModel._id {
                        return postId == id
                    }
                    return false
                }
            }) {
                self.isFav = true
            }else {
                self.isFav = false
            }
        }
        
        var action = false
        
        if sender.image(for: .normal) == #imageLiteral(resourceName: "icon_star_yellow"){
            sender.setImage(#imageLiteral(resourceName: "ic_star_fill"), for: .normal)
            action = true
        }else{
            sender.setImage(#imageLiteral(resourceName: "icon_star_yellow"), for: .normal)
            action = false
        }
        
        self.workItem2 = DispatchWorkItem(block: {
            if self.isFav != action{
                self.callFavPost(isFav: action)
            }
            else{
                print("no action required")
            }
        })
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: self.workItem2!)
    }
    
    var favPostId = [String]()
    
    func callFavPost(isFav:Bool) {
        
        if MyProfile.isSeller {
            guard let postId = postDataModel._id else {
                return
            }
            self.postId = postId
        }else {
            guard let postId = postDataModel._id else {
                return
            }
            self.postId = postId
        }
        
        let params = ["post_id":postId,
                      "user_id":MyProfile.userId!,
                      "is_fav":"\(isFav)"]
        
        JSONRequest.makeRequest(kFavPost, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [String:Any] {
                        
                        if let favPosts = des["favPost"] as? [[String:Any]] {
                            
                            self.favPostId.removeAll()
                            
                            for favPost in favPosts {
                                autoreleasepool {
                                    if let fPost = favPost["post_id"] as? String {
                                        self.favPostId.append(fPost)
                                    }
                                }
                            }
                            MyProfile.favPostId?.removeAll()
                            MyProfile.favPostId = self.favPostId
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                    // add toast
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                // add toast
            }
        }
    }
    
    // MARK:- HeartClick
    
    var workItem1:DispatchWorkItem?
    var isLiked = 0
    
    @IBAction func onHeartClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        let postLikes = postDataModel.post_like
        
        for postLike in postLikes {
            
            // if post user id is equal to myprofile user id
            // user already liked the post so dislike it
            // remove user from postLike
            
            if postLike._id == MyProfile.userId {
                self.isLiked = 1
            }
        }
        
        workItem1?.cancel()
        var action = 0
        
        if sender.image(for: .normal) == #imageLiteral(resourceName: "icon_like_red") {
            sender.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            action = 1
            
        }else {
            sender.setImage(#imageLiteral(resourceName: "icon_like_red"), for: .normal)
            action = 0
        }
        
        self.workItem1 = DispatchWorkItem(block: {
            if self.isLiked != action{
                self.callPostLike(isLike: action)
            }
            else{
                print("no action required")
            }
        })
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: self.workItem1!)
    }
    
    func callPostLike(isLike:Int) {
        
        if MyProfile.isSeller {
            guard let postId = postDataModel._id else {
                return
            }
            self.postId = postId
        }else {
            guard let postId = postDataModel._id else {
                return
            }
            self.postId = postId
        }
        
        guard let userId = MyProfile.userId else{
            return;
        }
        
        let params = ["post_id":postId,
                      "user_id":userId,
                      "is_like":"\(isLike)"]
        
        JSONRequest.makeRequest(kPostLike, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [Any] {
                        
                        if let data = des.first as? [String:Any] {
                            
                            if let postLikeArr = data["post_like"] as? [[String:Any]] {
                                
                                for data in postLikeArr {
                                    autoreleasepool{
                                        if let id = data["_id"] as? String {
                                            
                                            if MyProfile.isSeller {
                                                
                                                if id == MyProfile.userId {
                                                    
                                                    self.postDataModel.post_like.append(PostLikeModel(data: data))
                                                }else {
                                                    if let i = self.postDataModel.post_like.firstIndex(where: { (post) -> Bool in
                                                        return post._id == id
                                                    }) {
                                                        self.postDataModel.post_like.remove(at: i)
                                                    }
                                                }
                                            }else {
                                                if id == MyProfile.userId {
                                                    self.postDataModel.post_like.append(PostLikeModel(data: data))
                                                }else {
                                                    if let i = self.postDataModel.post_like.firstIndex(where: { (post) -> Bool in
                                                        return post._id == id
                                                    }) {
                                                        self.postDataModel.post_like.remove(at: i)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                    // add toast
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                // add toast
            }
        }
    }
    
    //MARK:- PlayVideo
    
    @IBAction func onPlayVideoClick(_ sender: Any) {
        
        guard let postImage = self.postDataModel.post_images.first else {
            return
        }
        guard let imageUrl = postImage.image_url else {
            return
        }
        let videoUrl = imageUrl.replacingOccurrences(of: ".jpg", with: ".mp4")
        
        let player = AVPlayer(url: URL(string: videoUrl)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        
        if let top = appDel.topViewController {
            top.present(playerController, animated: true) {
                player.play()
            }
        }
    }
    
    //MARK:- Feedback Click
    
    @IBAction func onFeedbackClick(_ sender: Any) {
        
        if let vc = UIStoryboard(name: "Feedback", bundle: nil).instantiateViewController(withIdentifier: "PostFeedbacksViewController") as? PostFeedbacksViewController {
            
            vc.post = self.postDataModel
            
            appDel.topViewController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- GalleryItemsDataSource
    func itemCount() -> Int {
        
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    //MARK: Image preview
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        for image in self.postImages {
            
            if let img = image.image_url, img != ""{
                
                let galleryItem = GalleryItem.image { imageCompletion in
                    
                    print("Image URl \(img)")
                    
                    SDWebImageDownloader.shared().downloadImage(with: URL(string: img), options: [], progress: nil, completed: { (img, data, err, fin) in
                        
                        if let image = img{
                            imageCompletion(image)
                        }
                    })
                }
                self.dataImage.append(galleryItem)
            }
        }
        
        guard dataImage.count > 0 else{
            if let top = appDel.topViewController{
                top.displayMessage("Image Not Found!!")
            }
            return
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
    }
    
    // MARK:-
    @IBAction func onProfileImageClick(_ sender: Any) {
        delegate.onBuyerCellAction(indexPath: indexPath, option: .profileClick, post: postDataModel)
    }
    
    @IBAction func onMakeABidClick(_ sender: Any) {
        delegate.onBuyerCellAction(indexPath: indexPath, option: .makeABid, post: postDataModel)
        
    }
    
    @IBAction func onBuyNowClick(_ sender: Any) {
        delegate.onBuyerCellAction(indexPath: indexPath, option: .buyNow, post: postDataModel)
    }
    
    @IBAction func onPostMenuClick(_ sender: Any) {
        delegate.onBuyerCellAction(indexPath: indexPath, option: .menuOption, post: postDataModel)
        
    }
}

extension CLPlacemark {
    
    var compactAddress: String? {
        if let name = name {
            var result = name
            
            if let street = thoroughfare {
                result += ", \(street)"
            }
            
            if let city = locality {
                result += ", \(city)"
            }
            
            if let country = country {
                result += ", \(country)"
            }
            
            return result
        }
        return nil
    }
    
}
