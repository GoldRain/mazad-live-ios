

import UIKit
import CarbonKit

var categoryVC:CategoriesViewController?

var homeVC:HomeViewController?

class HomeViewController: UIViewController, CarbonTabSwipeNavigationDelegate  {

    @IBOutlet weak var containerView: UIView!
    public var carbonController: CarbonTabSwipeNavigation!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if let id = MyProfile.userId{
            JSONRequest.makeRequest(kStopMyStream, parameters: ["user_id":id], callback: nil)
            JSONRequest.makeRequest(kAutoConfirmed, parameters: ["user_id":id], callback: nil)
        }
        
        self.title = NSLocalizedString("Home", comment: "Home")
        
        let item1 = NSLocalizedString("Story", comment: "Story")
        let item2 = NSLocalizedString("Categories", comment: "Categories")
        let item3 = NSLocalizedString("Pick", comment: "Pick")
        let item4 = NSLocalizedString("Auctions", comment: "Auctions")
        //let items = [" Story ", " Categories ", " Pick ", " Lyrics ", " Auctions ", " Sample "]
        let items = [item1,item2,item3,item4]
        carbonController = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        carbonController.setTabBarHeight(50.0)
        
        let normalColor = UIColor(red: 103/255, green: 65/255, blue: 170/255, alpha: 1.0)
        carbonController.setNormalColor(normalColor, font: UIFont.systemFont(ofSize: 12.0, weight: .medium))
        carbonController.setSelectedColor(.white, font: UIFont.systemFont(ofSize: 12.0, weight: .medium))
        //carbonController.setNormalColor(.lightGray)
        carbonController.setIndicatorColor(.white)
        //carbonController.setSelectedColor(.white)
        carbonController.toolbar.barTintColor = .purpleColor
        carbonController.toolbar.isTranslucent = false
        carbonController.setIndicatorHeight(2.0)
        
        carbonController.insert(intoRootViewController: self, andTargetView: self.containerView)
        
        if storyIdFromDynamicLink != ""{
            self.carbonController.setCurrentTabIndex(0, withAnimation: false)
        }else{
            self.carbonController.setCurrentTabIndex(1, withAnimation: false)
        }
        
        let screenBounds = UIScreen.main.bounds.width
        let widthOfTabIcons = screenBounds/4
        
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 0)
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 1)
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 2)
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 3)
        
       self.title = NSLocalizedString("Home", comment: "Home")
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            NotificationCenter.default.post(name: .UpdateStreamNotification, object: nil)
        }
        
        homeVC = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "startAnimaton"), object: nil, userInfo: nil)
    }
    
    var selectedCountryId:String?
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        if index == 0 {
            let vc = UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: "StoryViewController")
            return vc
        }else if index == 1 {
            if let categoryVC = categoryVC {
                if let country = selectedCountryId {
                    categoryVC.selectedCountryId = country
                }
                return categoryVC
            }else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
                if let country = selectedCountryId {
                    vc.selectedCountryId = country
                }
                categoryVC = vc
                return vc
            }
        }else if index == 2 {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "PickViewController")
            return vc
        }
//        else if index == 3 {
//            let vc = self.storyboard!.instantiateViewController(withIdentifier: "LyricsViewController")
//            return vc
//        }
//        else if index == 4 {
//            let vc = self.storyboard!.instantiateViewController(withIdentifier: "AuctionsViewController")
//            return vc
//        }
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "AuctionsViewController")
        return vc
    }

}
