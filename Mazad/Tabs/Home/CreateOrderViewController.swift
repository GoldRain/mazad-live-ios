//
//  BuyNowViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 26/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class CreateOrderViewController: UIViewController {
    
    @IBOutlet weak var addressMainView: UIView!
    @IBOutlet weak var topConstraintOfSelectPaymentView: NSLayoutConstraint!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCityAndState: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var btnCreditCardImage: UIButton!
    @IBOutlet weak var btnCODImage: UIButton!
    @IBOutlet weak var btnCreditCardTitle: UIButton!
    @IBOutlet weak var btnCODTitle: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    var amount:String!
    var type_id:String!
    var sellerId:String!
    
    var addressId:String?
    
    var type = ""
    var post:BuyerSellerModel!
    
    var paymentType = [String]()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let title = NSLocalizedString("Create Order", comment: "Create Order")
        self.title = title
        
        if let amount = self.amount, let doubleAmount = Double(amount){
            let newAmount = String(format: "%.2f", doubleAmount)
            self.lblAmount.text = "Amount Payable: KD \(newAmount)"
        }
        
        self.addressMainView.isHidden = true
        self.topConstraintOfSelectPaymentView.constant = -(addressMainView.bounds.height)
        
        self.updateRadioButton(enable: false, imageButton: self.btnCODImage, titleButton: self.btnCODTitle)
        self.updateRadioButton(enable: false, imageButton: self.btnCreditCardImage, titleButton: self.btnCreditCardTitle)
        
        if type == "post" {
            if let paymentTypeArr = self.post.payment_type?.components(separatedBy: ",") {
                self.paymentType = paymentTypeArr
            }
        }
        
        if paymentType.firstIndex(of: "cash") != nil{
            self.updateRadioButton(enable: true, imageButton: self.btnCODImage, titleButton: self.btnCODTitle)
            self.btnNext.setTitle("Confirm", for: .normal)
            self.selectRadioButton(imageButton: self.btnCODImage)
        }
        
        if paymentType.firstIndex(of: "cc") != nil{
            self.updateRadioButton(enable: true, imageButton: self.btnCreditCardImage, titleButton: self.btnCreditCardTitle)
            self.btnNext.setTitle("Next", for: .normal)
            self.selectRadioButton(imageButton: self.btnCreditCardImage)
        }
    }
    
    func updateRadioButton(enable:Bool, imageButton:UIButton, titleButton:UIButton){
        
        if enable{
            imageButton.setImage(#imageLiteral(resourceName: "onRadioButton.png"), for: .normal)
            titleButton.setTitleColor(BLUECOLOR, for: .normal)
        }
        else{
            imageButton.setImage(#imageLiteral(resourceName: "offRadioButton.png"), for: .normal)
            titleButton.setTitleColor(UIColor.lightGray, for: .normal)
        }
        
        titleButton.isUserInteractionEnabled = enable
        imageButton.isUserInteractionEnabled = enable
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func onAddressClick(_ sender: Any) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressesViewController") as? AddressesViewController {
            
            vc.callback = {  address in
                
                if let address = address {
                    if let name = address.name {
                        self.lblName.text = name.capitalized
                    }
                    if let ad1 = address.addressLineOne {
                        self.lblAddress.text = ad1
                        if let ad2 = address.addressLineTwo {
                            self.lblAddress.text = ad1.capitalized + ", " + ad2.capitalized
                        }
                    }
                    if let city = address.city{
                        self.lblCityAndState.text = city.capitalized
                        if let state = address.state {
                            self.lblCityAndState.text = city.capitalized + ", " + state.capitalized
                            if let countryDetails = address.countryDetails.first {
                                if let countryName = countryDetails.countryName {
                                    self.lblCityAndState.text = city.capitalized + ", " + state.capitalized + ", " + countryName.capitalized
                                    if let pincode = address.pincode {
                                        self.lblCityAndState.text = city.capitalized + ", " + state.capitalized + ", " + countryName.capitalized + ", " + pincode
                                    }
                                }
                            }
                        }
                    }
                    if let phone = address.phone {
                        self.lblPhone.text = phone
                    }
                    if let addressId = address.addressId {
                        self.addressId = addressId
                    }
                    self.addressMainView.isHidden = false
                    self.topConstraintOfSelectPaymentView.constant = 20
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func onNextClick(_ sender:UIButton) {
        
        if self.validate() {
            
            if sender.currentTitle == "Next" {
                
                guard let fullName = MyProfile.userName else {
                    return;
                }
                var firstName = ""
                var lastName = ""
                var components = fullName.components(separatedBy: " ")
                if(components.count > 0)
                {
                    firstName = components.removeFirst()
                    lastName = components.joined(separator: " ")
                }
                guard let email = MyProfile.userEmail else{
                    return;
                }
                guard let number = MyProfile.phone else{
                    return;
                }
                _ = PaymentViewController.showPaymentWithData(amount: Decimal(string: self.amount)!,
                                                              currency: "kwd",
                                                              customerFirstName: firstName,
                                                              customerLastName: lastName,
                                                              customerEmail: email,
                                                              customerPhone: number,
                                                              delegate: self)
            }else { // COD
                self.onCashOnDeliveryClick()
            }
        }
    }
  
    //MARK:- calling Create order Api
    fileprivate func createOrder(paymentData:PaymentData) {
        
        guard let userId = MyProfile.userId else{
            self.displayMessage("User Id Not Found!!")
            return
        }
        
        let params = ["user_id":userId,
                      "type_id":self.type_id!,
                      "type":self.type,
                      "address_id":self.addressId!,
                      "payment_method":"card",
                      "transaction_id":paymentData.payid!,
                      "user_payment_status":paymentData.result!,
                      "amount":self.amount!,
                      "seller_id":self.sellerId!,
                      
                      "chargeid":paymentData.chargeid!,
                      "crd":paymentData.crd!,
                      "crdtype":paymentData.crdtype!,
                      "hash":paymentData.hash!,
                      "payid":paymentData.payid!,
                      "ref":paymentData.ref!,
                      "result":paymentData.result!,
                      "trackid":paymentData.trackid!
                    ]
        
        let txnResponse = TransactionResponse(data: params)
        
        if paymentData.result == "SUCCESS" {
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            JSONRequest.makeRequest(kCreateOrder, parameters: params) { (data, error) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data as? [String:Any] {
                    
                    if let err = data["error"] as? Bool, !err {
                        
                        if let _ = data["description"] as? [String:Any] {
                            
                            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionSuccessViewController") as? TransactionSuccessViewController {
                                
                                vc.txnResponse = txnResponse
                                vc.displayType = .Card
                                
                                self.navigationController?.popViewController(completion: {
                                    if let top = appDel.topViewController {
                                        top.present(vc, animated: true, completion: nil)
                                    }
                                }, animation: true)
                            }
                            return;
                        }
                    }
                }
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionFailedViewController") as? TransactionFailedViewController {
                    
                    vc.txnResponse = txnResponse
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }else{
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionFailedViewController") as? TransactionFailedViewController {
                
                vc.txnResponse = txnResponse
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    //MARK:- Cash On Delivery Selected
    func onCashOnDeliveryClick() {
        
        guard let userId = MyProfile.userId else {
            self.displayMessage("User Id Not Found!!")
            return
        }
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kOrderOTP, parameters: ["user_id":userId]) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                
                if let error = data["error"] as? Bool, !error {
                    
                    if let data = data["data"] as? [String:Any] {
                        
                        if let phone = data["phone"] as? String {
                            
                            if let countryCode = data["country_code"] as? String {
                                
                                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOrderOtpViewController") as? VerifyOrderOtpViewController {
                                    
                                    vc.phone = phone
                                    vc.countryCode = countryCode
                                    
                                    vc.type = self.type
                                    vc.typeId = self.type_id
                                    vc.amount = self.amount
                                    vc.addressId = self.addressId
                                    vc.sellerId = self.sellerId
                                    
                                    vc.modalPresentationStyle = .overCurrentContext
                                    vc.modalTransitionStyle = .crossDissolve
                                    
                                    vc.callback = {  res in
                                        
                                        if res {
                                            
                                        }else{
                                            
                                        }
                                    }
                                    self.present(vc, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }else {
                    if let msg = data["message"] as? String {
                        let str = NSLocalizedString(msg, comment: msg)
                        self.displayMessage(str)
                    }
                }
            }else {
                let str = NSLocalizedString("Failed to send OTP", comment: "Failed to send OTP")
                self.displayMessage(str)
            }
        }
    }
    
    func selectRadioButton(imageButton:UIButton){
        
        if imageButton == self.btnCODImage || imageButton == self.btnCODTitle{
            self.btnCODImage.setImage(#imageLiteral(resourceName: "onRadioButton.png"), for: .normal)
            self.btnCreditCardImage.setImage(#imageLiteral(resourceName: "offRadioButton.png"), for: .normal)
            self.btnNext.setTitle("Confirm", for: .normal)
        }
        else{
            self.btnCODImage.setImage(#imageLiteral(resourceName: "offRadioButton.png"), for: .normal)
            self.btnCreditCardImage.setImage(#imageLiteral(resourceName: "onRadioButton.png"), for: .normal)
            self.btnNext.setTitle("Next", for: .normal)
        }
    }
    
    
    @IBAction func onSelectPaymentClick(_ sender: UIButton) {
        
        self.selectRadioButton(imageButton: sender)
    }
    
    //MARK:- Validation
    func validate() -> Bool {
        
        if self.addressMainView.isHidden {
            let msg = NSLocalizedString("Please Select an Address", comment: "Please Select an Address")
            self.displayMessage(msg)
            return false
        }
        return true
    }
}


//MARK: PaymentViewControllerDeletgate
extension CreateOrderViewController:PayementViewControllerDelegate {
    
    func paymentWebViewDidCancel(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
            
        }
    }
    
    func paymentWebViewDidFinish(_ paymentViewController: PaymentViewController, paymentInfo: PaymentData) {
        
        paymentViewController.dismiss(animated: true) {
            print(paymentInfo)
            
            self.createOrder(paymentData: paymentInfo)
        }
    }
    
    func paymentWebViewDidFail(_ paymentViewController: PaymentViewController) {
        
        paymentViewController.dismiss(animated: true) {
            
        }
    }
}
