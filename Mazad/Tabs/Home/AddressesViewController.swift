//
//  AddressesViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 26/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class AddressesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView:UITableView!
    
    var addresses = [AddressModel]()
    
    var callback:(((AddressModel)?)->())?
    
    var refreshControl:UIRefreshControl!
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        let title = NSLocalizedString("Addresses", comment: "Addresses")
        self.title = title
        
        let addButton = UIButton()
        addButton.setImage(UIImage(named: "add"), for: .normal)
        addButton.addTarget(self, action: #selector(onAddAddressClick), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: addButton)
        self.navigationItem.setRightBarButton(barButton, animated: true)
        
        tableView.separatorStyle = .none
        
        //
        
        let str = NSLocalizedString("Refreshing", comment: "Refreshing")
        let attrStr = NSAttributedString(string: str)
        
        refreshControl = getRefreshControl(attrStr: attrStr)
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getAddresses()
    }
    
    func getAddresses() {
        
        guard let userId = MyProfile.userId else {
            return
        }
        
        let params = ["user_id":userId,
                      "is_active":"\(true)",
                      "type":"read"
                    ]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kAddAddress, parameters: params) { (data, error) in
            
            self.emptyText = "No Data Found!!"
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                
                if let error = data["error"] as? Bool, !error {
                    
                    if let description = data["description"] as? [String:Any] {
                        
                        if let addresses = description["addresses"] as? [[String:Any]] {
                            
                            self.addresses.removeAll()
                            
                            for address in addresses {
                                self.addresses.append(AddressModel(data: address))
                            }
                            self.tableView.reloadData()
                        }
                    }
                }
//                else {
//                    if let msg = data["message"] as? String {
//                        let str = NSLocalizedString(msg, comment: msg)
//                        self.displayMessage(str)
//                    }
//                }
            }
//            else {
//                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
//                self.displayMessage(str)
//            }
            
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        
        self.getAddresses()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addresses.count > 0 ? addresses.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.addresses.count <= 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddressCell
        
        let address = self.addresses[indexPath.row]
        
        cell.addShadow()
        
        if let name = address.name {
        	cell.lblUserName.text = name.capitalized
        }
        if let phone = address.phone {  
            cell.lblPhoneNumber.text = phone
        }
        
        var fullAddress = ""
        
        if let addressLine1 = address.addressLineOne {
            fullAddress += addressLine1
        }
        if let addressLine2 = address.addressLineTwo {
            fullAddress += ", "+addressLine2
        }
        if let city = address.city {
            fullAddress += ", "+city
        }
        if let state = address.state {
            fullAddress += ", "+state
        }
        if let countryDetails = address.countryDetails.first {
            if let countryName = countryDetails.countryName {
                fullAddress += ", "+countryName
            }
        }
        if let pincode = address.pincode {
            fullAddress += ", "+pincode
        }
        
        cell.lblFullAddress.text = fullAddress.trim().capitalized
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let address = self.addresses[indexPath.row]
        callback?(address)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onAddAddressClick() {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateAddressViewController") as? CreateAddressViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

class AddressCell:UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblFullAddress: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
}
