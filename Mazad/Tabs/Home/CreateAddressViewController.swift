//
//  AddAddressViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 26/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class CreateAddressViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtUserName: CustomTextField!
    @IBOutlet weak var txtPhoneNumber: CustomTextField!
    @IBOutlet weak var txtAddressLine1: CustomTextField!
    @IBOutlet weak var txtAddressLine2: CustomTextField!
    @IBOutlet weak var txtCity: CustomTextField!
    @IBOutlet weak var txtState: CustomTextField!
    @IBOutlet weak var txtCountryCode: CustomTextField!
    @IBOutlet weak var txtPincode: CustomTextField!
    @IBOutlet weak var txtCountry: CustomTextField!
    
    var Address:AddressModel?
    
    var countryid:String?
    
    //MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let title = NSLocalizedString("Create Address", comment: "Create Address")
        
        self.title = title
        
        self.txtCountryCode.text = "+971"
        self.txtCountry.text = "United Arab Emirates"
        
        appDel.getAllCountries { (countries, msg) in
            
            let country = countries?.filter({ (country) -> Bool in
                return country.countryCode == "+971"
            })
            
            if let countryId = country?.first?._id {
                self.countryid = countryId
            }else{
                self.countryid = "5c6154d9e1372d491ea92714"
            }
        }
        
        self.txtPincode.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
        self.txtCountryCode.isUserInteractionEnabled = false
        self.txtCountry.isUserInteractionEnabled = false
    }
    
    //MARK:-
    @IBAction func onCountryCodeClick(_ sender: Any) {
        
        appDel.getAllCountries { (countries, msg) in
            
            if let countries = countries{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountriesSelectionViewController") as? CountriesSelectionViewController{
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.countriesList = countries
                    vc.callback = {  data in
                        self.txtCountryCode.placeholder = ""
                        self.txtCountryCode.text = data.countryCode
                        self.txtCountry.text = data.countryName
                        self.countryid = data._id
                    }
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if let msg = msg{
                self.displayMessage(msg)
            }
        }
    }
    
    @IBAction func onSaveClick(_ sender: UIButton) {
        
        if self.validate() {
            
            guard let userId = MyProfile.userId else {
                return
            }
            
            let params = ["user_id":userId,
                          "addressLineOne":self.txtAddressLine1.text!,
                          "addressLineTwo":self.txtAddressLine2.text!,
                          "city":self.txtCity.text!,
                          "state":self.txtState.text!,
                          "country_id":self.countryid!,
                          "pincode":self.txtPincode.text!,
                          "is_active":"\(true)",
                          "type": "create",
                          "name":txtUserName.text!,
                          "phone":txtPhoneNumber.text!
                        ]
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            JSONRequest.makeRequest(kAddAddress, parameters: params) { (data, error) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data as? [String:Any] {
                    
                    if let error = data["error"] as? Bool, !error {
                        
                        if let description = data["description"] as? [String:Any] {
                            
                            if let addresses = description["addresses"] as? [[String:Any]] {
                                
                                if let _ = addresses.first {
                                    
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                    }else {
                        if let msg = data["message"] as? String {
                            let str = NSLocalizedString(msg, comment: msg)
                            self.displayMessage(str)
                        }
                    }
                }else {
                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                    self.displayMessage(str)
                }
            }
        }
    }
    
    func validate() -> Bool {
        
        if isEmpty(self.txtUserName.text!){
            let str = NSLocalizedString("Please enter Name", comment: "Please enter Name")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtCountryCode.text!){
            let str = NSLocalizedString("Please select Country Code", comment: "Please select Country Code")
            self.displayMessage(str)
            return false
        }
        if !isValidMobileNumber(self.txtPhoneNumber.text?.trim()) {
            let str = NSLocalizedString("Please enter valid Phone Number", comment: "Please enter valid Phone Number")
            self.displayMessage(str)
            return false
        }else if (self.txtPhoneNumber.text?.trim().count)! > 20 {
            let str = NSLocalizedString("Please enter valid Phone Number", comment: "Please enter valid Phone Number")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtAddressLine1.text!){
            let str = NSLocalizedString("Please enter Address Line 1", comment: "Please enter Address Line 1")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtAddressLine2.text!){
            let str = NSLocalizedString("Please enter Address Line 2", comment: "Please enter Address Line 2")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtCity.text!){
            let str = NSLocalizedString("Please enter City", comment: "Please enter City")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtState.text!){
            let str = NSLocalizedString("Please enter State", comment: "Please enter State")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtPincode.text!){
            let str = NSLocalizedString("Please enter Pincode", comment: "Please enter Pincode")
            self.displayMessage(str)
            return false
        }else if Int(self.txtPincode.text!) == nil {
            let str = NSLocalizedString("Please enter valid Pincode", comment: "Please enter valid Pincode")
            self.displayMessage(str)
            return false
        }else if (self.txtPincode.text?.trim().count)! > 20 {
            let str = NSLocalizedString("Please enter valid Pincode", comment: "Please enter valid Pincode")
            self.displayMessage(str)
            return false
        }
        return true
    }
    
    //MARK:- TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return updatedText.count <= 10
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let textField = textField as? CustomTextField{
            return textField.textFieldShouldBeginEditing(_:textField)
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let textField = textField as? CustomTextField{
            return textField.textFieldShouldEndEditing(_:textField)
        }
        return true
    }
}
