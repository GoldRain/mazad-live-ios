//
//  TransactionFailedViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 28/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class TransactionFailedViewController: UIViewController {

    @IBOutlet weak var lblTxnId:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    
    @IBOutlet weak var mainView:UIView!
    
    
    var txnResponse:TransactionResponse!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.mainView.layer.cornerRadius = 6
        
        if let txnId = txnResponse.txnId {
            self.lblTxnId.text = txnId
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        let myStringDate = formatter.string(from: yourDate!)
        
        self.lblTime.text = myStringDate
    }
    
    @IBAction func onCloseClick(_ sender:UIButton) {
        
        self.dismiss(animated: true) {
            if let top = appDel.topViewController {
                top.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func onContactUsClick(_ sender:UIButton) {
    
        self.displayMessage("This feature will be available soon")
        
    }
}
