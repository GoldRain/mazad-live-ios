//
//  TransactionSuccessViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 28/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

enum DisplayType {
    case Card
    case Cash
}

class TransactionSuccessViewController: UIViewController {
    
    @IBOutlet weak var animationImageView: UIImageView!
    @IBOutlet weak var animationImageViewWidthConstraint: NSLayoutConstraint!

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblTypeId: UILabel!
    @IBOutlet weak var lblTxnId: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblPaymentComplete:UILabel!
    
    var txnResponse:TransactionResponse!
    
    var displayType = DisplayType.Card
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let amount = txnResponse.amount{
            self.lblAmount.text = "KD "+amount
        }
        if let paymentMode = txnResponse.payment_method {
            self.lblPaymentMode.text = paymentMode.capitalized
        }

        if let typeId = txnResponse.type_id {
            self.lblTypeId.text = typeId
        }
        if let txnId = txnResponse.txnId {
            self.lblTxnId.text = txnId
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        let myStringDate = formatter.string(from: yourDate!)
        
        self.lblTime.text = myStringDate
        
        if displayType == .Card {
            let str = NSLocalizedString("Payment Completed", comment: "Payment Completed")
            self.lblPaymentComplete.text = str
            self.lblAmount.isHidden = false
        }else {
            let str = NSLocalizedString("Order is Placed Successfully", comment: "Order is Placed Successfully")
            self.lblPaymentComplete.text = str
            self.lblAmount.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.animationImageViewWidthConstraint.constant = CGFloat(60)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setUpAnimation()
    }
    
    func setUpAnimation(){
        UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.animationImageViewWidthConstraint.constant = CGFloat(90)
            self.animationImageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/1.5)
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }, completion: nil)
        
    }
    
    @IBAction func onDoneClick(_ sender: Any) {
        
        if self.displayType == .Card {
            self.dismiss(animated: true, completion: nil)
        }else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
