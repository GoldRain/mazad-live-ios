//
//  CategoriesBuyerCell.swift
//  Mazad Live
//
//  Created by MAC_2 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import SDWebImage
import ImageViewer

enum CategoriesBuyerCellOption:String{
    case profileClick = "Profile Click"
    case buyNow = "Buy Now"
    case makeABid = "Make A Bid"
    case menuOption = "Menu Option"
}

protocol CategoryCellActionDelegate {
    func onBuyerCellAction(indexPath: IndexPath, option:CategoriesBuyerCellOption, post:BuyerModel)
}

class CategoryBuyerCell: UITableViewCell, GalleryItemsDataSource, GalleryItemsDelegate  {
    
    var delegate: CategoryCellActionDelegate!
    
    var indexPath:IndexPath!
    
    var buyerModel:BuyerModel!
    var sellerModel:SellerModel!
    
    var dataImage: [GalleryItem] = [GalleryItem]()
    var galleryController:GalleryViewController?
    
    var postImages = [PostImageModel]()
    
    var postId = ""
    
    @IBOutlet weak var soldDarkView: UIView!
    @IBOutlet weak var lblSold: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    
    @IBOutlet weak var btnHeart: UIButton!
    @IBOutlet weak var btnWhatsapp: UIButton!
    @IBOutlet weak var btnStar: UIButton!
    
    @IBOutlet weak var imageMainView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    
    @IBOutlet weak var buyNowView: UIView!
    @IBOutlet weak var makeABidView: UIView!
    @IBOutlet weak var btnUserImage: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var mainViewOfPostImage: UIView!
    @IBOutlet weak var subViewOfMainContainerView: UIView!
    
    @IBOutlet weak var bottomVIewContraints: NSLayoutConstraint!
    
    @IBOutlet weak var btnThumbsUp: UIButton!
    @IBOutlet weak var btnThumbsDown: UIButton!
    
    func setupUI() {
        
        self.imageMainView.addShadow()
        self.mainContainerView.addShadow()
        //self.mainContainerView.bringSubview(toFront: self.mainViewOfPostImage)
        self.mainViewOfPostImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePostImageViewer)))
    }
    
    @objc func handlePostImageViewer() {
        self.showImagesPreview(index: 0)
    }
    
    @IBAction func onClickAction(_ sender: UIButton) {
        
        switch sender {
        case btnWhatsapp:
            self.openWhatsApp()
        case btnMessage:
            self.openMessageApp()
        case btnCall:
//            self.openCallingApp()
            self.openWhatsApp()
        default:
            break
        }
    }
    
    override func prepareForReuse() {
        
        self.btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
        self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
        self.btnHeart.setImage(#imageLiteral(resourceName: "icon_like_red"), for: .normal)
        self.btnStar.setImage(#imageLiteral(resourceName: "icon_star_yellow"), for: .normal)
    }
    
    func openCallingApp(){
        if let url = URL(string: "tel://123456789") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    func openMessageApp() {
        //UIApplication.shared.open(URL(string: "sms:12345678901")!, options: [:], completionHandler: nil)
        if let top = appDel.topViewController{
            if let vc = top.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController{
                top.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func openWhatsApp() {
        
        let message = "Message"
        let urlWhats = "whatsapp://send?text=\(message)"
        
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString){
                if UIApplication.shared.canOpenURL(whatsappURL){
                    UIApplication.shared.open(whatsappURL, options: [:]) { (Bool) in
                        
                    }
                }else{
                    //can't open whatsapp
                }
            }
        }
    }
    
    var workItem3:DispatchWorkItem?
    
    var isThumbsUp = 0
    var isThumbsDown = 0
    
    @IBAction func onVoteClick(_ sender: UIButton) {
        
        if MyProfile.isSeller {
            let votes = sellerModel.vote
            for vote in votes {
                if vote.user_id == MyProfile.userId {
                    if vote.response == "1"{
                        self.isThumbsUp = 1
                    }else{
                        self.isThumbsDown = 0
                    }
                }
            }
        }else {
            let votes = buyerModel.vote
            for vote in votes {
                if vote.user_id == MyProfile.userId {
                    if vote.response == "1"{
                        self.isThumbsUp = 1
                    }else{
                        self.isThumbsDown = 0
                    }
                }
            }
        }
        
        var thumbsUpAction = 0
        var thumbsDownAction = 0
        
        workItem3?.cancel()
        
        if sender == self.btnThumbsUp {
            
            if self.btnThumbsUp.currentImage == #imageLiteral(resourceName: "icon_like_green") {
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "ic_like_fill"), for: .normal)
                if btnThumbsDown.currentImage == #imageLiteral(resourceName: "ic_dislike_fill") {
                    self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
                }
                thumbsUpAction = 1
                
            }else {
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
                thumbsUpAction = 1
            }
        }
        if sender == self.btnThumbsDown {
            
            if self.btnThumbsDown.currentImage == #imageLiteral(resourceName: "icon_dislike_red") {
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "ic_dislike_fill"), for: .normal)
                if btnThumbsUp.currentImage == #imageLiteral(resourceName: "ic_like_fill") {
                    btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
                }
                thumbsDownAction = 0
                
            }else {
                self.btnThumbsDown.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
                self.btnThumbsUp.setImage(#imageLiteral(resourceName: "icon_like_green"), for: .normal)
                thumbsDownAction = 0
            }
        }
        
        self.workItem3 = DispatchWorkItem(block: {
            
            if self.isThumbsUp != thumbsUpAction {
                self.callVotePost(isVote: thumbsUpAction)
            }else {
                self.callVotePost(isVote: thumbsUpAction)
            }
            if self.isThumbsDown != thumbsDownAction {
                self.callVotePost(isVote: thumbsDownAction)
            }else {
                self.callVotePost(isVote: thumbsDownAction)
            }
        })
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: self.workItem3!)
    }
    
    func callVotePost(isVote:Int) {
        
        if MyProfile.isSeller {
            guard let postId = sellerModel._id else {
                return
            }
            self.postId = postId
        }else {
            guard let postId = buyerModel._id else {
                return
            }
            self.postId = postId
        }
        
        let params = ["post_id":postId,
                      "user_id":MyProfile.userId!,
                      "is_vote":"\(isVote)"]
        
        JSONRequest.makeRequest(kVote, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [String:Any] {
                        
                        if let votes = des["vote"] as? [Any] {
                            
                            for vote in votes {
                                
                                if let vote = vote as? [String:Any] {
                                    
                                    if let userId = vote["user_id"] as? String{
                                        
                                        if MyProfile.isSeller {
                                            
                                            let votes = self.sellerModel.vote
                                            if votes.count > 0 {
                                                
                                                for _ in votes {
                                                    
                                                    if let index = self.sellerModel.vote.firstIndex(where: { (vote) -> Bool in
                                                        return vote.user_id == userId
                                                    }) {
                                                        self.sellerModel.vote.remove(at: index)
                                                    }else {
                                                        self.sellerModel.vote.append(VoteModel(data: vote))
                                                    }
                                                }
                                            }else {
                                                self.sellerModel.vote.append(VoteModel(data: vote))
                                            }
                                            
                                        }else {
                                            
                                            let votes = self.buyerModel.vote
                                            if votes.count > 0 {
                                                
                                                for _ in votes {
                                                    
                                                    if let index = self.buyerModel.vote.firstIndex(where: { (vote) -> Bool in
                                                        return vote.user_id == userId
                                                    }) {
                                                        self.buyerModel.vote.remove(at: index)
                                                    }else {
                                                        self.buyerModel.vote.append(VoteModel(data: vote))
                                                    }
                                                }
                                            }else {
                                                self.buyerModel.vote.append(VoteModel(data: vote))
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                    // add toast
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                // add toast
            }
        }
    }
    
    @IBAction func onUnlikeClick(_ sender: UIButton) {
        if sender.currentImage == #imageLiteral(resourceName: "ic_dislike_fill"){
            sender.setImage(#imageLiteral(resourceName: "icon_dislike_red"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "ic_dislike_fill") ,for: .normal)
        }
    }
    
    var workItem2:DispatchWorkItem?
    var isFav = false
    
    @IBAction func onStarClick(_ sender: UIButton) {
        
        workItem2?.cancel()
        var action = false
        
        if sender.currentImage == #imageLiteral(resourceName: "icon_star_yellow"){
            isFav = false
            sender.setImage(#imageLiteral(resourceName: "ic_star_fill"), for: .normal)
            action = true
        }else{
            isFav = true
            sender.setImage(#imageLiteral(resourceName: "icon_star_yellow"), for: .normal)
            action = false
        }
        
        self.workItem2 = DispatchWorkItem(block: {
            if self.isFav != action{
                self.callFavPost(isFav: action)
            }
            else{
                print("no action required")
            }
        })
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: self.workItem2!)
    }
    
    var favPostId = [String]()
    
    func callFavPost(isFav:Bool) {
        
        if MyProfile.isSeller {
            guard let postId = sellerModel._id else {
                return
            }
            self.postId = postId
        }else {
            guard let postId = buyerModel._id else {
                return
            }
            self.postId = postId
        }
        
        let params = ["post_id":postId,
                      "user_id":MyProfile.userId!,
                      "is_fav":"\(isFav)"]
        
        JSONRequest.makeRequest(kFavPost, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [String:Any] {
                        
                        if let favPosts = des["favPost"] as? [Any] {
                            
                            self.favPostId.removeAll()
                            
                            for favPost in favPosts {
                                
                                if let fPost = favPost as? [String:String] {
                                    
                                    if let fPostId = fPost["post_id"] {
                                        
                                        self.favPostId.append(fPostId)
                                        
//                                        if (MyProfile.favPostId?.contains(where: { (id) -> Bool in
//                                            print("post post fID \(fPostId) id \(id)")
//                                            return fPostId != id
//                                        }))! {
//                                            MyProfile.favPostId?.append(fPostId)
//                                        } else {
//                                            if let index = MyProfile.favPostId?.firstIndex(of: fPostId) {
//                                                MyProfile.favPostId?.remove(at: index)
//                                            }
//                                        }
                                    }
                                }
                            }
                            MyProfile.favPostId?.removeAll()
                            MyProfile.favPostId = self.favPostId
                            print("favpostCount * \(MyProfile.favPostId?.count)")
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                    // add toast
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                // add toast
            }
        }
    }
    
    var workItem1:DispatchWorkItem?
    var isLiked = 0
    
    @IBAction func onHeartClick(_ sender: UIButton) {
        
        let postLikes = buyerModel.post_like
        
        for postLike in postLikes {
            
            // if post user id is equal to myprofile user id
            // user already liked the post so dislike it
            // remove user from postLike
            if postLike._id == MyProfile.userId {
                self.isLiked = 1
            }
        }
        
        workItem1?.cancel()
        var action = 0
        
        if sender.currentImage == #imageLiteral(resourceName: "icon_like_red") {
            sender.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            action = 1
            
        }else {
            sender.setImage(#imageLiteral(resourceName: "icon_like_red"), for: .normal)
            action = 0
        }
        
        self.workItem1 = DispatchWorkItem(block: {
            if self.isLiked != action{
                self.callPostLike(isLike: action)
            }
            else{
                print("no action required")
            }
        })
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: self.workItem1!)
    }
    
    func callPostLike(isLike:Int) {
        
        if MyProfile.isSeller {
            guard let postId = sellerModel._id else {
                return
            }
            self.postId = postId
        }else {
            guard let postId = buyerModel._id else {
                return
            }
            self.postId = postId
        }
        
        let params = ["post_id":postId,
                      "user_id":MyProfile.userId!,
                      "is_like":"\(isLike)"]
        
        JSONRequest.makeRequest(kPostLike, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [Any] {
                        
                        if let data = des.first as? [String:Any] {
                            
                            if let postLikeArr = data["post_like"] as? [Any] {
                                
                                for data in postLikeArr {
                                    
                                    if let data = data as? [String:Any] {
                                        
                                        if let id = data["_id"] as? String {
                                            
                                            if MyProfile.isSeller {
                                                
                                                if id == MyProfile.userId {
                                                    
                                                    self.sellerModel.post_like.append(PostLikeModel(data: data))
                                                }else {
                                                    if let i = self.sellerModel.post_like.firstIndex(where: { (post) -> Bool in
                                                        return post._id == id
                                                    }) {
                                                        self.sellerModel.post_like.remove(at: i)
                                                    }
                                                }
                                            }else {
                                                if id == MyProfile.userId {
                                                    self.buyerModel.post_like.append(PostLikeModel(data: data))
                                                }else {
                                                    if let i = self.buyerModel.post_like.firstIndex(where: { (post) -> Bool in
                                                        return post._id == id
                                                    }) {
                                                        self.buyerModel.post_like.remove(at: i)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                    // add toast
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                // add toast
            }
        }
    }
    
    //MARK: GalleryItemsDataSource
    func itemCount() -> Int {
        
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    // MARK: Image preview
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        for image in self.postImages {
            let galleryItem = GalleryItem.image { imageCompletion in
                
                SDWebImageDownloader.shared().downloadImage(with: URL(string: image.image_url!), options: [], progress: nil, completed: { (img, data, err, fin) in
                    
                    if let image = img{
                        imageCompletion(image)
                    }
                })
            }
            self.dataImage.append(galleryItem)
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onProfileImageClick(_ sender: Any) {
        
        delegate.onBuyerCellAction(indexPath: indexPath, option: .profileClick, post: buyerModel)
    }
    
    @IBAction func onMakeABidClick(_ sender: Any) {
        
        delegate.onBuyerCellAction(indexPath: indexPath, option: .makeABid, post: buyerModel)
    }
}
