//
//  VerifyOrderOtpViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 27/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class VerifyOrderOtpViewController: UIViewController {
    
    @IBOutlet weak var txtOTP: CustomTextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var mainView:UIView!
    
    var callback:((Bool)->())?
    
    var phone:String!
    var countryCode:String!
    
    var typeId:String!
    var amount:String!
    var addressId:String!
    var sellerId:String!
    var type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.addShadow()

        self.lblTitle.text = "SMS Verification code has been sent to your registred mobile number"
    }
    
    @IBAction func onCloseClick(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
            self.callback!(false)
        })
    }
    
    @IBAction func onVerifyOtpClick(_ sender:UIButton) {
        
        if self.validate() {

            guard let userId = MyProfile.userId else{
                self.displayMessage("User Id Not Found!!")
                return
            }
            let params = ["user_id":userId,
                          "code":self.txtOTP.text!,
                          "country_code":self.countryCode!.replacingOccurrences(of: "+", with: ""),
                          "phone":self.phone!
                        ]

            let wait = appDel.topViewController?.addWaitSpinner()

            JSONRequest.makeRequest(kVerifyOrderOtp, parameters: params) { (data, error) in

                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }

                if let data = data as? [String:Any] {

                    if let err = data["error"] as? Bool, !err {

                        if let msg = data["message"] as? String, msg == "Otp Verified" {
                            
                            self.createOrder()
                            
//                            self.dismiss(animated: true, completion: {
//                                self.callback!(true)
//                            })
                            // call create order Api
                            
                        }
                    }else {
                        if let msg = data["message"] as? String {
                            let str = NSLocalizedString(msg, comment: msg)
                            self.displayMessage(str)
                        }
                    }
                }else {
                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                    self.displayMessage(str)
                }
            }
        }
    }
    
    fileprivate func createOrder() {
        
        guard let userId = MyProfile.userId else {
            self.displayMessage("User Id Not Found!!")
            return
        }
        
        let params = ["user_id":userId,
                      "type_id":self.typeId!,
                      "type":self.type,
                      "address_id":self.addressId!,
                      "payment_method":"cod",
                      "transaction_id":"",
                      "user_payment_status":"",
                      "amount":self.amount!,
                      "seller_id":self.sellerId!
                    ]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kCreateOrder, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err {
                    
                    let txnResponse = TransactionResponse(data: params)
                    
                    let txnId = NSLocalizedString("NA", comment: "NA")
                    txnResponse.txnId = txnId
                    txnResponse.payment_method = "Cash On Delivery"
                    
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionSuccessViewController") as? TransactionSuccessViewController {
                        vc.txnResponse = txnResponse
                        vc.displayType = .Cash
                        
                        self.dismiss(animated: true, completion: {
                            if let top = appDel.topViewController {
                                top.navigationController?.popViewController(completion: {
                                    if let top = appDel.topViewController {
                                        top.present(vc, animated: true, completion: nil)
                                    }
                                }, animation: true)
                            }
                        })
                    }
                }
            }
            let str = NSLocalizedString("Failed to place order", comment: "Failed to place order")
            self.displayMessage(title: nil, msg: str)
        }
    }
    
    //MARK:- Validation
    func validate() -> Bool {
        
        if isEmpty(self.txtOTP.text!){
            let str = NSLocalizedString("Please Enter OTP", comment: "Please Enter OTP")
            self.displayMessage(str)
            return false
        }
        return true
    }
}
