//
//  CardInfoViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 08/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class CardInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var cardNumberText: UITextField!
    @IBOutlet weak var cardCVVText: UITextField!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var imgCardView: UIImageView!
    @IBOutlet weak var cardMonthText: UITextField!
    @IBOutlet weak var cardYearText: UITextField!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var lblPayable: UILabel!
    
    var payableAmt:Int?
    
    typealias type = (_ success : Bool ) ->()
    var callback : type?
    
    typealias typeDict = (_ dict : [String: String] ) ->()
    var callbackDict : typeDict?
    
    enum selectedCardType: String {
        case VISA
        case MasterCard
        case American_Express
        case DinersClub
        case Discover
        case JCB
        case Verve
        case Unknown
    }
    var selectedCard: selectedCardType = .Unknown
    let cardImages = [#imageLiteral(resourceName: "cardVisa"),#imageLiteral(resourceName: "card_master"),#imageLiteral(resourceName: "card_amex"),#imageLiteral(resourceName: "card_dinner"),#imageLiteral(resourceName: "card_orange"),#imageLiteral(resourceName: "cardJcb"),#imageLiteral(resourceName: "cardVerve"),#imageLiteral(resourceName: "cardFront")]
    
    var editMode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Card Info", comment: "Card Info")
        
        self.tableViewHeight.constant = 0
        let str = NSLocalizedString("Total Amount Payable", comment: "Total Amount Payable")
        self.lblPayable.text = "\(str):  KD \(self.payableAmt!)"
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        //test data
        nameText.text = "Demo"
        cardNumberText.text = "8888880000000001"
        cardCVVText.text = "1234"
        cardMonthText.text = "5"
        cardYearText.text = "2021"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let window: UIWindow? = UIApplication.shared.keyWindow
        for layer in (window?.subviews)!{
            if layer.tag == 4{
                layer.removeFromSuperview()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
 
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let view = touches.first?.view{
            if view == self.cardView{
                if self.tableViewHeight.constant == 0.0{
                    UIView.animate(withDuration: 0.3) {
                        self.imgCardView.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
                        self.tableViewHeight.constant = 270.0
                        self.view.layoutIfNeeded()
                    }
                }else{
                    UIView.animate(withDuration: 0.3) {
                        self.imgCardView.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi * 2))
                        self.tableViewHeight.constant = 0.0
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardImages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell", for: indexPath)
        if let imageView = cell.contentView.viewWithTag(1) as? UIImageView{
            imageView.image = self.cardImages[indexPath.row]
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.cardImage.image = self.cardImages[indexPath.row]
        switch indexPath.row {
        case 0: self.selectedCard = .VISA
        case 1: self.selectedCard = .MasterCard
        case 2: self.selectedCard = .American_Express
        case 3: self.selectedCard = .DinersClub
        case 4: self.selectedCard = .Discover
        case 5: self.selectedCard = .JCB
        case 6: self.selectedCard = .Verve
        case 7: self.selectedCard = .Unknown
        default: break
        }
        UIView.animate(withDuration: 0.3) {
            self.imgCardView.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi * 2))
            self.tableViewHeight.constant = 0.0
            self.view.layoutIfNeeded()
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if textField == self.cardNumberText{
            let newString = (cardNumberText.text! as NSString).replacingCharacters(in: range, with: string) as NSString
            if  newString == ""{
                self.cardImage.image = #imageLiteral(resourceName: "cardFront")
            }
            
            if let length = textField.text?.count{
                if length == 1 || length == 3{
                    setupCardType(fromPrefix: newString as String)
                }
            }
            
            if (range.location == 4) || (range.location == 9) || (range.location == 14) {
                if string != "" {
                    let str = "\(textField.text!) "
                    textField.text = str
                }
            }else if (range.location == 5) || (range.location == 10) || (range.location == 15) {
                if string == "" {
                    var str =  "\(textField.text!)"
                    str = String(str.dropLast())
                    textField.text = str
                }
            }
        }
        
        if textField == self.cardMonthText{
            return newLength <= 2
        }
        else if textField == self.cardYearText{
            return newLength <= 4
        }
        else if textField == self.cardCVVText{
            return newLength <= 4
        }
        else{
            return newLength <= 19
        }
        
    }
    
    func setupCardType(fromPrefix:String){
        let new = CardState.init(fromPrefix: fromPrefix)
        if new == .identified(.masterCard){
            self.cardImage.image = #imageLiteral(resourceName: "card_master")
            self.selectedCard = .MasterCard
        }
        else if new == .identified(.amex){
            self.cardImage.image = #imageLiteral(resourceName: "card_amex")
            self.selectedCard = .American_Express
        }
        else if new == .identified(.diners){
            self.cardImage.image = #imageLiteral(resourceName: "card_dinner")
            self.selectedCard = .DinersClub
        }
        else if new == .identified(.visa){
            self.cardImage.image = #imageLiteral(resourceName: "cardVisa")
            self.selectedCard = .VISA
        }
        else if new == .identified(.jcb){
            self.cardImage.image = #imageLiteral(resourceName: "cardJcb")
            self.selectedCard = .JCB
        }
        else if new == .identified(.discover){
            self.cardImage.image = #imageLiteral(resourceName: "card_orange")
            self.selectedCard = .Discover
        }
        if new == .invalid{
            self.cardImage.image = #imageLiteral(resourceName: "cardFront")
            self.selectedCard = .Unknown
        }
        if new == .indeterminate([.amex, .diners,.jcb,.discover,.visa,.masterCard]){
            self.cardImage.image = #imageLiteral(resourceName: "cardFront")
            self.selectedCard = .Unknown
        }
    }
    
    @IBAction func onProceedToPaymentClick(_ sender: Any) {
        
        if validate(){
            
            let waitView = addWaitSpinner()
            var params = [String: String]()
            //        name_on_card
            //        card_number
            //        card_type
            //        cvv
            //        expiry_month
            //        expiry_year
            params["cvv"] = self.cardCVVText.text!
            params["name_on_card"] = nameText.text!
            params["card_number"] = cardNumberText.text!.replacingOccurrences(of: " ", with: "")
            var cardType = self.selectedCard.rawValue
            if cardType == "American_Express"{
                cardType = "American Express"
            }
            params["card_type"] = cardType.lowercased()
            params["expiry_month"] = self.cardMonthText.text!
            params["expiry_year"] = self.cardYearText.text!
            self.removeWaitSpinner(waitView: waitView)
            self.navigationController?.popViewControllerWithHandler {
                if let callBackDict = self.callbackDict{
                    callBackDict(params)
                }
            }
        }
    }
    
    func validate() -> Bool{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let nameOfYear = Int(dateFormatter.string(from: now))!
        
        if isEmpty(nameText.text){
            let msg = NSLocalizedString("Please enter your name.", comment: "Please enter your name.")
            displayMessage(title: nil, msg: msg)
            return false
        }
        
        guard let value = cardNumberText.text?.trim(), value != "" else {
            let msg = NSLocalizedString("Please enter your card no.", comment: "Please enter your card no.")
            displayMessage(title: msg, msg: nil)
            return false
        }
        
        guard let value1 = self.cardCVVText.text?.trim(), value1 != "" else {
            
            let msg = NSLocalizedString("Please enter your cvv no.", comment: "Please enter your cvv no.")
            displayMessage(title: msg, msg: nil)
            return false
        }
        if !isValidMobileNumber(value1){
            let msg = NSLocalizedString("Please enter a valid cvv no.", comment: "Please enter a valid cvv no.")
            displayMessage(title: msg, msg: nil)
            return false
        }
        if value1.count < 3 {
            let msg = NSLocalizedString("Please enter a valid cvv no.", comment: "Please enter a valid cvv no.")
            displayMessage(title: msg, msg: nil)
            return false
        }
        
        guard let value2 = cardMonthText.text?.trim(), value != "" else {
            let msg = NSLocalizedString("Please enter your card expire month", comment: "Please enter your card expire month")
            displayMessage(title: msg, msg: nil)
            return false
        }
        if !isValidMobileNumber(value2){
            let msg = NSLocalizedString("Please enter a valid expire month", comment: "Please enter a valid expire month")
            displayMessage(title: msg, msg: nil)
            return false
        }
        
        guard let value3 = cardYearText.text?.trim(), value != "" else {
            let msg = NSLocalizedString("Please enter your card expire year", comment: "Please enter your card expire year")
            displayMessage(title: msg, msg: nil)
            return false
        }
        if !isValidMobileNumber(value3){
            let msg = NSLocalizedString("Please enter a valid expire year", comment: "Please enter a valid expire year")
            displayMessage(title: msg, msg: nil)
            return false
        }
        if Int(value2)! > 12 || Int(value2)! < 1{
            let msg = NSLocalizedString("Please enter a valid month", comment: "Please enter a valid month")
            displayMessage(title: msg, msg: nil)
            return false
        }
        if Int(value3)! < nameOfYear || Int(value3)! > 2090{
            let msg = NSLocalizedString("Please enter a valid expire year", comment: "Please enter a valid expire year")
            displayMessage(title: msg, msg: nil)
            return false
        }
        return true
    }
    
}
