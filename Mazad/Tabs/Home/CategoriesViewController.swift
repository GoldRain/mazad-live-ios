
import UIKit
import SDWebImage
import ImageViewer
import DropDown
import Firebase

enum FromViewController{
    case searchCategory
    case categoryType
    case placeType
}

class CategoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CategoryBuyerCellActionDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedCountryId:String?
    
    var postData = [BuyerSellerModel]()
    
    //var postData = [BuyerSellerModel]()
    
    var refreshControl:UIRefreshControl!
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    
    var fromVC = FromViewController.categoryType
    
    var activityPostId:String?
    var tagId:String?
    var placeName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification(_:)), name: NSNotification.Name("changeUser"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSoldProductNotification(not:)), name: .OnSoldProduct, object: nil)
        
        let str = NSLocalizedString("Refreshing", comment: "Refreshing")
        let attrStr = NSAttributedString(string: str)
            
        refreshControl = getRefreshControl(attrStr: attrStr)
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        
        if fromVC == .categoryType { //Categories VC
            
            self.getDataFromApiAndReloadTable()
            
            self.navigationController?.isNavigationBarHidden = true
            
            self.tableView.addSubview(refreshControl)
            
        }else if fromVC == .searchCategory { //Search Category
            
            self.navigationController?.isNavigationBarHidden = false
            
            searchVC?.getCategoryList(text: self.tagId!, showWait: true, callback: {  (posts, msg) in
                if let postData = posts {
                    self.postData.removeAll()
                    self.postData = postData
                }
                self.tableView.reloadData()
            })
            
        } else{ // Search Place
            self.navigationController?.isNavigationBarHidden = false
            
            searchVC?.getPlaceList(text: self.placeName!, showWait: true, callback: { (places, msg) in
                if let postData = places {
                    self.postData.removeAll()
                    self.postData = postData
                }
                self.tableView.reloadData()
            })
        }
        
        categoryVC = self
    }
    
    @objc func pullToRefresh() {
        self.refreshControl.endRefreshing()
        self.getDataFromApiAndReloadTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if fromVC == .categoryType { //Categories VC
            
            self.navigationController?.isNavigationBarHidden = true
            
        }else if fromVC == .searchCategory { //Search Category
            
            self.navigationController?.isNavigationBarHidden = false
            
        } else{ // Search Place
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    @objc func onSoldProductNotification(not:Notification){
        
        if let type = not.userInfo?["type"] as? String{
            if type.lowercased() == "post" {
                if let postId = not.userInfo?["typeId"] as? String {
                    
                    if let index = self.postData.firstIndex(where: { (post) -> Bool in
                        return post._id == postId
                    }) {
                        
                        self.postData[index].is_sold = true
                        
                        let indexPath = IndexPath(row: index, section: 0)
                        self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                    }
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleNotification(_ sender: NSNotification){
        
        if let _ = sender.userInfo?["user"] as? Bool{
            if homeVC?.carbonController.currentTabIndex == 1 {
                self.getDataFromApiAndReloadTable()
            }
        }
    }
    
    //MARK:-
    func getDataFromApiAndReloadTable() {
        
        if MyProfile.isSeller {
            
            if let userId = MyProfile.userId {
                
                var showWait = true
                
                if postIdFromDynamicLink != "" || storyIdFromDynamicLink != ""{
                    showWait = false
                }else{
                    showWait = true
                }
                
                self.getMyPosts(userId: userId, showWait: showWait) {  (posts, msg) in
                    
                    self.emptyText = NSLocalizedString("No Posts !!", comment: "No Posts !!")
                    
                    self.postData.removeAll()
                    
                    if let postData = posts {
                        self.postData = postData
                    }
                    self.tableView.reloadData()
                }
            }
        }
        else {
            
            var showWait = true
            
            if postIdFromDynamicLink != "" || storyIdFromDynamicLink != ""{
                showWait = false
            }else{
                showWait = true
            }
            
            self.getAllPosts(countryId: self.selectedCountryId ?? "" , showWait: showWait) {  (posts, msg) in
                
                self.emptyText = NSLocalizedString("No Posts !!", comment: "No Posts !!")
                
                self.postData.removeAll()
                
                if let postData = posts {
                    self.postData = postData
                }
                self.tableView.reloadData()
                
//                if postIdFromDynamicLink != ""{
//                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
//                        if let index = self.postData.firstIndex(where: { (post) -> Bool in
//                            return post._id == postIdFromDynamicLink
//                        }){
//                            let indexPath = IndexPath(row: index, section: 0)
//                            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
//                        }
//                    })
//                }
                
            }
        }
    }
    
    //MARK:- tableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return postData.count > 0 ? postData.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.postData.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        var cell:CategoryBuyerCell!
        
        let myPostData = self.postData[indexPath.row]
        
        if let value = myPostData.is_sold, value{
            cell = tableView.dequeueReusableCell(withIdentifier: "soldCell", for: indexPath) as? CategoryBuyerCell
        }
        else{
            cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CategoryBuyerCell
        }
        
        cell.indexPath = indexPath
        cell.delegate = self
        cell.postDataModel = myPostData
        cell.setupUI()
        
        return cell
    }
    
    // MARK:- call get my post Api
    func getMyPosts(userId:String, showWait:Bool, callback:@escaping (([BuyerSellerModel]?, String?)->())) {
        
        guard let top = appDel.topViewController else {
            return
        }
        var wait:WaitView!
        
        if showWait{
            wait = top.addWaitSpinner()
        }
        
        JSONRequest.makeRequest(kGetMyPost, parameters: ["user_id":userId]) { (data, error) in
            
            if showWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    var posts = [BuyerSellerModel]()
                    
                    if let des = data["description"] as? [[String:Any]] {
                        
                        for data in des {
                            posts.append(BuyerSellerModel(data: data))
                        }
                        
                        posts = posts.filter({ (post) -> Bool in
                            return !post.is_deleted
                        })
                        
                        posts.sort(by: { (post1, post2) -> Bool in
                            autoreleasepool {
                                let date1 = Date(timeIntervalSince1970: Double(post1.post_created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(post2.post_created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            }
                        })
                        
                        posts.sort(by: { (post1, _) -> Bool in
                            return post1.is_promoted == true
                        })
                        
                        callback(posts, nil)
                    }
                    else{
                        let msg = NSLocalizedString("Failed to load post", comment: "Failed to load post")
                        callback(nil, msg)
                    }
                }
                else if let msg = data["message"] as? String{
                    let msg = NSLocalizedString(msg, comment: msg)
                    callback(nil, msg)
                }
            }
            else{
                let msg = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil, msg)
            }
        }
    }
    
    //MARK:- call get all post Api
    
    func getAllPosts(countryId:String, showWait:Bool, callback:@escaping (([BuyerSellerModel]?, String?)->())) {
        
        guard let top = appDel.topViewController else {
            let str = NSLocalizedString("There is some error", comment: "There is some error")
            callback(nil, str)
            return
        }
        
        var wait:WaitView!
        
        if showWait{
            wait = top.addWaitSpinner()
        }
        
        var params = [String:String]()
        
//        guard let userId = MyProfile.userId else {
//            let str = NSLocalizedString("There is some error", comment: "There is some error")
//            callback(nil, str)
//            if let wait = wait {
//                self.removeWaitSpinner(waitView: wait)
//            }
//            return
//        }
        
        params["user_id"] = MyProfile.userId ?? ""
        
        if countryId != "" {
            params["country_id"] = countryId
            params["type"] = "one"
            self.selectedCountryId = nil
        }else {
            params["country_id"] = ""
            params["type"] = "all"
        }
        
        JSONRequest.makeRequest(kGetPost, parameters: params) { (data, error) in
            
            if showWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    var posts = [BuyerSellerModel]()
                    
                    if let des = data["description"] as? [[String:Any]] {
                        
                        for data in des {
                            posts.append(BuyerSellerModel(data: data))
                        }
                        
                        if let userId = MyProfile.userId {
                            posts = posts.filter({ (post) -> Bool in
                                return post.userDetails.first?._id != userId
                            })
                        }
                        
                        posts = posts.filter({ (post) -> Bool in
                            return !post.is_deleted
                        })
                        
                        posts.sort(by: { (post1, post2) -> Bool in
                            autoreleasepool {
                                let date1 = Date(timeIntervalSince1970: Double(post1.post_created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(post2.post_created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            }
                        })
                        
                        posts.sort(by: { (post1, _) -> Bool in
                            return post1.is_promoted == true
                        })
                        callback(posts,nil)
                    }
                    else{
                        let str = NSLocalizedString("Failed to load post", comment: "Failed to load post")
                        callback(nil,str)
                    }
                }
                else if let msg = data["message"] as? String{
                    let str = NSLocalizedString(msg, comment: msg)
                    callback(nil,str)
                }
            }
            else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
    
    // MARK:- Delegate method
    var menuOptionDropDown = DropDown()
    var menuOption = [String]()
    
    func onBuyerCellAction(indexPath: IndexPath, option: CategoriesBuyerCellOption, post: BuyerSellerModel) {
        
        guard let postUserId = post.post_user_id else{
            return;
        }
        
        switch option {
        
        case .profileClick:
            
            if let myUserId = MyProfile.userId {
                if myUserId != postUserId {
                    
                    let userDetails = post.userDetails
                    
                    if let id = userDetails.first?._id {
                        
                        if id == myUserId {
                            return;
                        }
                        
                        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController{
                            vc.userId = id
                            let nav = UINavigationController(rootViewController: vc)
                            nav.modalTransitionStyle = .crossDissolve
                            self.present(nav, animated: true, completion: nil)
                        }
                    }
                }
            }else{
                let userDetails = post.userDetails
                
                if let id = userDetails.first?._id {
                    
                    if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController{
                        vc.userId = id
                        let nav = UINavigationController(rootViewController: vc)
                        nav.modalTransitionStyle = .crossDissolve
                        self.present(nav, animated: true, completion: nil)
                    }
                }
            }
            
        case .buyNow:
            
            guard let _ = MyProfile.userId else{
                getLoginDialouge ()
                return;
            }
            
            // Check if product is available for selected country
            if let value = post.only_user_from_selected_country, value {
                
                if !post.available_country_id.contains(where: { (country) -> Bool in
                    return country._id == MyProfile.countryId
                }){
                    
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
                    
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    
                    vc.alertText = "This Product isn't Available for your Country, Keep Searching for other Products!"
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                    
                    return;
                }
            }
            
            // Check if product is deleted or not
            let params = [
                "type_id": post._id!,
                "type": "post"
            ]
            
            self.tableView.addWaitView()
            
            JSONRequest.makeRequest(kIsDeletedStatus, parameters: params) { (data, error) in
                
                self.tableView.removeWaitView()
                
                if let data = data as? [String:Any] {
                    if let err = data["error"] as? Bool, !err {
                        if let msg = data["message"] as? Bool, !msg { // not deleted
                            
                            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateOrderViewController") as? CreateOrderViewController {
                                
                                if let amount = post.amount, let postId = post._id, let sellerId = post.post_user_id {
                                    vc.amount = amount
                                    vc.type_id = postId
                                    vc.sellerId = sellerId
                                    vc.type = "post"
                                    vc.post = post
                                }
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                        }else{ // deleted
                            let str = NSLocalizedString("This Item is no longer available for Purchase", comment: "This Item is no longer available for Purchase")
                            self.displayMessage(str)
                        }
                    }else{
                        let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                        self.displayMessage(str)
                    }
                }
                else{
                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                    self.displayMessage(str)
                }
            }
            
        case .makeABid:
            
            guard let myUserId = MyProfile.userId else{
                getLoginDialouge()
                return;
            }
            
            if let value = post.only_user_from_selected_country, value {
                
                if !post.available_country_id.contains(where: { (country) -> Bool in
                    return country._id == MyProfile.countryId
                }){
                    
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
                    
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    
                    vc.alertText = NSLocalizedString("This Product isn't Available for your Country, Keep Searching for other Products!", comment: "This Product isn't Available for your Country, Keep Searching for other Products!")
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                    
                    return;
                }
            }
            
            if myUserId != postUserId {
                
                guard let postId = post._id else {
                    return
                }
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertMakeBidViewController") as? CustomAlertMakeBidViewController{
                    
//                    if let amount = post.amount {
//                        vc.minPrice = Double(amount)
//                    }
                    vc.minPrice = 1.0
                    vc.id = postId
                    
                    vc.callback = { amount in
                        
                        guard let userId = MyProfile.userId else {
                            return
                        }
                        
                        var newAmount:String!
                        
                        if let doubleAmount = Double(amount) {
                            newAmount = String(format: "%.2f", doubleAmount)
                        }else{
                            newAmount = amount
                        }
                        
                        let wait = appDel.topViewController?.addWaitSpinner()
                        
                        tabBarVC?.callMakeBidApi(userId: userId, postId: postId, amount: newAmount, callback: { (msg) in
                            
                            if let wait = wait {
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            
                            if let message = msg {
                                self.displayMessage(message)
                            }
                        })
                    }
                    
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        case .menuOption:
            
            guard let myUserId = MyProfile.userId else{
                getLoginDialouge ()
                return;
            }
            
            if let cell = self.tableView.cellForRow(at: indexPath) as? CategoryBuyerCell {
                
                self.menuOption.removeAll()
                
                if myUserId == postUserId {
                    self.menuOption.append(NSLocalizedString("Delete", comment: "Delete"))
                    self.menuOption.append(NSLocalizedString("Repost", comment: "Repost"))
                }else{
                    self.menuOption.append(NSLocalizedString("Report Post", comment: "Report Post"))
                }
                
                self.menuOption.append(NSLocalizedString("Share", comment: "Share"))
                
                self.menuOptionDropDown.dataSource = self.menuOption
                self.menuOptionDropDown.anchorView = cell.btnMenuOption
                
                self.menuOptionDropDown.backgroundColor = .white
                self.menuOptionDropDown.textColor = BLUECOLOR
                
                self.menuOptionDropDown.bottomOffset = CGPoint(x: 0, y:(self.menuOptionDropDown.anchorView?.plainView.bounds.height)!)
                
                self.menuOptionDropDown.topOffset = CGPoint(x: 0, y: -(self.menuOptionDropDown.anchorView?.plainView.bounds.height)!)
                
                self.menuOptionDropDown.selectionAction = { (index: Int, item: String)  in
                    
                    if item == NSLocalizedString("Delete", comment: "Delete") {
                        
                        if let isSold = post.is_sold, isSold {
                            let msg = NSLocalizedString("Sold Post can not be Deleted!!", comment: "Sold Post can not be Deleted!!")
                            self.displayMessage(msg)
                            return;
                        }
                        
                        guard let postId = post._id else {
                            let str = NSLocalizedString("Failed to get Post Id", comment: "Failed to get Post Id")
                            self.displayMessage(str)
                            return
                        }
                        
                        let params = ["post_id":postId,
                                      "is_deleted":"\(true)"]
                        
                        let wait = appDel.topViewController?.addWaitSpinner()
                        
                        JSONRequest.makeRequest(kDeletePost, parameters: params, callback: { (data, error) in
                            
                            if let wait = wait {
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            
                            if let data = data as? [String:Any] {
                                
                                if let err = data["error"] as? Bool, !err {
                                    
                                    self.getDataFromApiAndReloadTable()
                                    
                                }else if let msg = data["message"] as? String{
                                    let str = NSLocalizedString(msg, comment: msg)
                                    self.displayMessage(str)
                                }
                            }else {
                                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                                self.displayMessage(str)
                            }
                        })
                    }
                    
                    if item == NSLocalizedString("Repost", comment: "Repost"){
                        
                        let params = ["user_id":MyProfile.userId!,
                                      "post_id":post._id!]
                        
                        JSONRequest.makeRequest(kRepost, parameters: params, callback: { (data, error) in
                            
                            if let data = data as? [String:Any] {
                                if let err = data["error"] as? Bool, !err {
                                    self.getDataFromApiAndReloadTable()
                                }else if let msg = data["message"] as? String, msg == "You are out of posts." {
                                    self.displayMessage(msg, callback: { () -> (Void) in
                                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpgradeViewController") as? UpgradeViewController {
                                            appDel.topViewController?.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    })
                                }
                            }
                        })
                    }
                    
                    if item == NSLocalizedString("Report Post", comment: "Report Post") {
                        
                        let userDetail = post.userDetails.first
                        
                        if let id = userDetail?._id {
                            if let userId = MyProfile.userId {
                                if id != userId {
                                    
                                    if let postId = post._id {
                                        
                                        if let rvc = self.storyboard?.instantiateViewController(withIdentifier: "ReportPostViewController") as? ReportPostViewController {
                                            
                                            rvc.postId = postId
                                            
                                            rvc.modalTransitionStyle = .crossDissolve
                                            rvc.modalPresentationStyle = .overCurrentContext
                                            self.present(rvc, animated: true, completion: nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if item == NSLocalizedString("Share", comment: "Share"){
                        
                        guard let postId = post._id else {
                            let str = NSLocalizedString("Failed to get Post Id", comment: "Failed to get Post Id")
                            self.displayMessage(str)
                            return
                        }
                        
                        guard let link = URL(string: "https://mazadlive.page.link?type=post&id=\(postId)") else { return }
                        let dynamicLinksDomainURIPrefix = "https://mazadlive.page.link"
                        guard let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix) else{
                            print("wierd!! link builder is not created")
                            return
                        }
                        linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.itwhiz4u.q8mercato.mazadlive")
                        linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: "com.mazadlive")
                        linkBuilder.iOSParameters?.appStoreID = "1450514377"
                        linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
                        if let postDetails = post.details{
                            linkBuilder.socialMetaTagParameters?.title = ""
                            linkBuilder.socialMetaTagParameters?.descriptionText = postDetails
                        }
                        if let imageUrl = post.post_images.first?.image_url, imageUrl != ""{
                            linkBuilder.socialMetaTagParameters?.imageURL = URL(string: imageUrl)
                        }
                        
                        guard let longDynamicLink = linkBuilder.url else { return }
                        print("The long URL is: \(longDynamicLink)")
                        
                        
                        linkBuilder.shorten() { url, warnings, error in
                            
                            guard let url = url, error == nil else { return }
                            
                            print("The short URL is: \(url)")
                            
                            let textToShare = [url]
                            
                            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                            activityViewController.popoverPresentationController?.sourceView = appDel.topViewController!.view // so that iPads won't crash
                            
                            appDel.topViewController!.present(activityViewController, animated: true, completion: {
                                
                            })
                            
                        }
                        
                    }
                    
                }
                self.menuOptionDropDown.show()
            }
        }
    }
}
