
import UIKit
import SDWebImage
import ImageViewer
import DropDown
import Firebase

var auctionVC:AuctionsViewController?

class AuctionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CategoryBuyerCellActionDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedCountryId:String?
    
    var postData = [BuyerSellerModel]()
    
    var refreshControl:UIRefreshControl!
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        auctionVC = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification(_:)), name: NSNotification.Name("changeUser"), object: nil)
        
        self.getDataFromApiAndReloadTable()
        
        let str = NSLocalizedString("Refreshing", comment: "Refreshing")
        let attrStr = NSAttributedString(string: str)
        
        refreshControl = getRefreshControl(attrStr: attrStr)
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getDataFromApiAndReloadTable()
    }
    
    func getDataFromApiAndReloadTable() {
        
        if MyProfile.isSeller {
            
            if let userId = MyProfile.userId {
                
                self.tableView.addWaitView()
                
                categoryVC?.getMyPosts(userId: userId, showWait: false) { (posts, msg) in
                
                    self.tableView.removeWaitView()
                    
                    self.emptyText = NSLocalizedString("No Posts !!", comment: "No Posts !!")
                    
                    if let postData = posts {
                        self.postData.removeAll()
                        self.postData = postData
                        
                        self.postData = self.postData.filter({ (post) -> Bool in
                            return post.is_auction == true
                        })
                        self.postData = self.postData.filter({ (post) -> Bool in
                            return post.is_sold == false
                        })
                        
                        self.postData = self.postData.filter({ (post) -> Bool in
                            return !post.is_deleted
                        })
                        
                        self.tableView.reloadData()
                    }
                }
            }
        }
        else {
            
            self.tableView.addWaitView()
            
            categoryVC?.getAllPosts(countryId: self.selectedCountryId ?? "" , showWait: false) { (posts, msg) in
            
                self.tableView.removeWaitView()
                
                self.emptyText = NSLocalizedString("No Posts !!", comment: "No Posts !!")
                
                if let postData = posts {
                    self.postData.removeAll()
                    self.postData = postData
                    
                    self.postData = self.postData.filter({ (post) -> Bool in
                        return post.is_auction == true
                    })
                    self.postData = self.postData.filter({ (post) -> Bool in
                        return post.is_sold == false
                    })
                    self.postData = self.postData.filter({ (post) -> Bool in
                        return !post.is_deleted
                    })
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSoldProductNotification(not:)), name: .OnSoldProduct, object: nil)
    }
    
    @objc func onSoldProductNotification(not:Notification) {
        
        if let type = not.userInfo?["type"] as? String{
            if type.lowercased() == "post" {
                if let postId = not.userInfo?["typeId"] as? String {
                    
                    if let index = self.postData.firstIndex(where: { (post) -> Bool in
                        return post._id == postId
                    }) {
                        
                        self.postData[index].is_sold = true
                        
                        let indexPath = IndexPath(row: index, section: 0)
                        self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                    }
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleNotification(_ sender: NSNotification){
        
        if let _ = sender.userInfo?["user"] as? Bool{
           
            if homeVC?.carbonController.currentTabIndex != 0 && homeVC?.carbonController.currentTabIndex != 1 && homeVC?.carbonController.currentTabIndex != 2 {
                self.getDataFromApiAndReloadTable()
            }
        }
    }
    
    //MARK:- tableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return postData.count > 0 ? postData.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.postData.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        var cell:CategoryBuyerCell!
        
        let myPostData = self.postData[indexPath.row]
        
        if let value = myPostData.is_sold, value{
            cell = tableView.dequeueReusableCell(withIdentifier: "soldCell", for: indexPath) as? CategoryBuyerCell
        }
        else{
            cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CategoryBuyerCell
        }
        
        cell.indexPath = indexPath
        cell.delegate = self
        cell.postDataModel = myPostData
        cell.setupUI()
        
        return cell
    }
    
    // MARK:- Delegate method
    
    var menuOptionDropDown = DropDown()
    var menuOption = [String]()
    
    func onBuyerCellAction(indexPath: IndexPath, option: CategoriesBuyerCellOption, post: BuyerSellerModel) {
        
        guard let postUserId = post.post_user_id else{
            return;
        }
        
        switch option {
            
        case .profileClick:
            
            if let myUserId = MyProfile.userId {
                if myUserId != postUserId {
                    
                    let userDetails = post.userDetails
                    
                    if let id = userDetails.first?._id {
                        
                        if id == myUserId {
                            return;
                        }
                        
                        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController{
                            vc.userId = id
                            let nav = UINavigationController(rootViewController: vc)
                            nav.modalTransitionStyle = .crossDissolve
                            self.present(nav, animated: true, completion: nil)
                        }
                    }
                }
            }else{
                let userDetails = post.userDetails
                
                if let id = userDetails.first?._id {
                    
                    if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController{
                        vc.userId = id
                        let nav = UINavigationController(rootViewController: vc)
                        nav.modalTransitionStyle = .crossDissolve
                        self.present(nav, animated: true, completion: nil)
                    }
                }
            }
            
        case .buyNow:
            
            guard let _ = MyProfile.userId else{
                getLoginDialouge()
                return;
            }
            
            // Check if product is available for selected country
            if let value = post.only_user_from_selected_country, value {
                
                if !post.available_country_id.contains(where: { (country) -> Bool in
                    return country._id == MyProfile.countryId
                }){
                    
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
                    
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    
                    vc.alertText = NSLocalizedString("This Product isn't Available for your Country, Keep Searching for other Products!", comment: "This Product isn't Available for your Country, Keep Searching for other Products!")
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                    
                    return;
                }
            }
            
            // Check if product is deleted or not
            let params = [
                "type_id": post._id!,
                "type": "post"
            ]
            let wait = appDel.topViewController?.addWaitSpinner()
            JSONRequest.makeRequest(kIsDeletedStatus, parameters: params) { (data, error) in
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data as? [String:Any] {
                    if let err = data["error"] as? Bool, !err {
                        if let msg = data["message"] as? Bool, !msg { // not deleted
                            
                            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateOrderViewController") as? CreateOrderViewController {
                                
                                if let amount = post.amount, let postId = post._id, let sellerId = post.post_user_id {
                                    vc.amount = amount
                                    vc.type_id = postId
                                    vc.sellerId = sellerId
                                    vc.type = "post"
                                    vc.post = post
                                }
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                        }else{ // deleted
                            let str = NSLocalizedString("This Item is no longer available for Purchase", comment: "This Item is no longer available for Purchase")
                            self.displayMessage(str)
                        }
                    }else{
                        let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                        self.displayMessage(str)
                    }
                }
                else{
                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                    self.displayMessage(str)
                }
            }
            
        case .makeABid:
            
            guard let myUserId = MyProfile.userId else{
                getLoginDialouge ()
                return;
            }
            
            if let value = post.only_user_from_selected_country, value {
                
                if !post.available_country_id.contains(where: { (country) -> Bool in
                    return country._id == MyProfile.countryId
                }){
                    
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
                    
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    
                    vc.alertText = NSLocalizedString("This Product isn't Available for your Country, Keep Searching for other Products!", comment: "This Product isn't Available for your Country, Keep Searching for other Products!")
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                    
                    return;
                }
            }
            
            if myUserId != postUserId {

                guard let postId = post._id else {
                    return
                }
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertMakeBidViewController") as? CustomAlertMakeBidViewController{
                    
//                    if let amount = post.amount {
//                        vc.minPrice = Double(amount)
//                    }
                    vc.minPrice = 1.0
                    vc.id = postId
                    
                    vc.callback = { amount in
                        
                        guard let userId = MyProfile.userId else {
                            return
                        }
                        
                        var newAmount:String!
                        
                        if let doubleAmount = Double(amount) {
                            newAmount = String(format: "%.2f", doubleAmount)
                        }else{
                            newAmount = amount
                        }
                        
                        let wait = appDel.topViewController?.addWaitSpinner()
                        
                        tabBarVC?.callMakeBidApi(userId: userId, postId: postId, amount: newAmount, callback: { (msg) in
                            
                            if let wait = wait {
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            
                            if let message = msg {
                                self.displayMessage(message)
                            }
                        })
                    }
                    
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        case .menuOption:
            
            if let cell = self.tableView.cellForRow(at: indexPath) as? CategoryBuyerCell {
                
                let userDetails = post.userDetails.first
                
                guard let userId = MyProfile.userId else{
                    getLoginDialouge()
                    return;
                }
                
                self.menuOption.removeAll()
                
                if MyProfile.isSeller || userDetails?._id == userId {
                    self.menuOption.append(NSLocalizedString("Delete", comment: "Delete"))
                    self.menuOption.append(NSLocalizedString("Repost", comment: "Repost"))
                }else {
                    self.menuOption.append(NSLocalizedString("Report Post", comment: "Report Post"))
                }
                
                self.menuOption.append(NSLocalizedString("Share", comment: "Share"))
                
                self.menuOptionDropDown.dataSource = self.menuOption
                self.menuOptionDropDown.anchorView = cell.btnMenuOption
                
                self.menuOptionDropDown.backgroundColor = .white
                self.menuOptionDropDown.textColor = BLUECOLOR
                
                self.menuOptionDropDown.bottomOffset = CGPoint(x: 0, y:(self.menuOptionDropDown.anchorView?.plainView.bounds.height)!)
                self.menuOptionDropDown.topOffset = CGPoint(x: 0, y: -(self.menuOptionDropDown.anchorView?.plainView.bounds.height)!)
                
                self.menuOptionDropDown.selectionAction = { (index: Int, item: String)  in
                    
                    if item == NSLocalizedString("Delete", comment: "Delete") {
                        
                        if let isSold = post.is_sold, isSold {
                            let msg = NSLocalizedString("Sold Post can not be Deleted!!", comment: "Sold Post can not be Deleted!!")
                            self.displayMessage(msg)
                            return;
                        }
                        
                        guard let postId = post._id else {
                            self.displayMessage("Failed to get Post Id")
                            return
                        }
                        
                        let params = ["post_id":postId,
                                      "is_deleted":"\(true)"]
                        
                        let wait = appDel.topViewController?.addWaitSpinner()
                        
                        JSONRequest.makeRequest(kDeletePost, parameters: params, callback: { (data, error) in
                            
                            if let wait = wait {
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            
                            if let data = data as? [String:Any] {
                                
                                if let err = data["error"] as? Bool, !err {
                                    
                                    self.getDataFromApiAndReloadTable()
                                    
                                }else if let msg = data["message"] as? String{
                                    let str = NSLocalizedString(msg, comment: msg)
                                    self.displayMessage(str)
                                }
                            }else {
                                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                                self.displayMessage(str)
                            }
                        })
                    }
                    
                    if item == NSLocalizedString("Repost", comment: "Repost"){
                        
                        let params = ["user_id":MyProfile.userId!,
                                      "post_id":post._id!]
                        
                        JSONRequest.makeRequest(kRepost, parameters: params, callback: { (data, error) in
                            
                            if let data = data as? [String:Any] {
                                if let err = data["error"] as? Bool, !err {
                                    self.getDataFromApiAndReloadTable()
                                }
                            }
                        })
                    }
                    
                    if item == NSLocalizedString("Report Post", comment: "Report Post") {
                        
                        let userDetail = post.userDetails.first
                        
                        if let id = userDetail?._id {
                            if let userId = MyProfile.userId {
                                if id != userId {
                                    
                                    if let postId = post._id {
                                        
                                        if let rvc = self.storyboard?.instantiateViewController(withIdentifier: "ReportPostViewController") as? ReportPostViewController {
                                            
                                            rvc.postId = postId
                                            
                                            rvc.modalTransitionStyle = .crossDissolve
                                            rvc.modalPresentationStyle = .overCurrentContext
                                            self.present(rvc, animated: true, completion: nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if item == NSLocalizedString("Share", comment: "Share"){
                        
                        guard let postId = post._id else {
                            let str = NSLocalizedString("Failed to get Post Id", comment: "Failed to get Post Id")
                            self.displayMessage(str)
                            return
                        }
                        
                        guard let link = URL(string: "https://mazadlive.page.link?type=post&id=\(postId)") else { return }
                        let dynamicLinksDomainURIPrefix = "https://mazadlive.page.link"
                        guard let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix) else{
                            print("wierd!! link builder is not created")
                            return
                        }
                        linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.itwhiz4u.q8mercato.mazadlive")
                        linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: "com.mazadlive")
                        linkBuilder.iOSParameters?.appStoreID = "1450514377"
                        linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
                        if let postDetails = post.details{
                            linkBuilder.socialMetaTagParameters?.title = ""
                            linkBuilder.socialMetaTagParameters?.descriptionText = postDetails
                        }
                        if let imageUrl = post.post_images.first?.image_url, imageUrl != ""{
                            linkBuilder.socialMetaTagParameters?.imageURL = URL(string: imageUrl)
                        }
                        
                        guard let longDynamicLink = linkBuilder.url else { return }
                        print("The long URL is: \(longDynamicLink)")
                        
                        
                        linkBuilder.shorten() { url, warnings, error in
                            
                            guard let url = url, error == nil else { return }
                            
                            print("The short URL is: \(url)")
                            
                            let textToShare = [url]
                            
                            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                            activityViewController.popoverPresentationController?.sourceView = appDel.topViewController!.view // so that iPads won't crash
                            
                            appDel.topViewController!.present(activityViewController, animated: true, completion: {
                                
                            })
                            
                        }
                        
                    }
                }
                self.menuOptionDropDown.show()
            }
        }
    }
}

