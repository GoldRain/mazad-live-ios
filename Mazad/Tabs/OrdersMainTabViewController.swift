

import UIKit

class OrdersMainTabViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var titleButton: UIButton!
    
    var orderList = [OrderListModel]()
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let title = NSLocalizedString("Orders", comment: "Orders")
        
        self.title = title
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        self.refreshControl.backgroundColor = .clear
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNot), name: NSNotification.Name(rawValue: "orderMainTabVCReloadTable"), object: nil)
    }
    
    @objc func handleNot() {
        self.getDataAndReloadTable()
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getDataAndReloadTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       super.viewWillAppear(true)
        
        self.getDataAndReloadTable()
    }
    
    func getDataAndReloadTable() {
        
        guard let userId = MyProfile.userId else {
            self.emptyText = NSLocalizedString("No Orders !!", comment: "No Orders !!")
            return;
        }
        
        if MyProfile.isSeller{ // seller orders
            
            self.titleButton.setTitle(NSLocalizedString("Customers Orders", comment: "Customers Orders"), for: .normal)
            
            self.tableView.addWaitView()
            
            tabBarVC?.getSellerOrderList(user_id: userId, addWait: false, callback: { (orderList, msg) in
                
                self.tableView.removeWaitView()
                
                self.emptyText = NSLocalizedString("No Orders !!", comment: "No Orders !!")
                
                self.orderList.removeAll()
                
                if let orders = orderList {
                    self.orderList = orders
                    
                }else {
                    if let _ = msg {
                        //self.displayMessage(msg)
                    }
                }
                self.tableView.reloadData()
            })
            
        }
        else{ // buyer orders
            
            let title = NSLocalizedString("Your Orders", comment: "Your Orders")
            
            self.titleButton.setTitle(title, for: .normal)
            
            self.tableView.addWaitView()
            
            tabBarVC?.getOrderList(user_id: userId, addWait: false, callback: {  (orderList, msg) in
            
                self.tableView.removeWaitView()
                
                 self.emptyText = NSLocalizedString("No Orders !!", comment: "No Orders !!")
                
                self.orderList.removeAll()
                
                if let orders = orderList {
                    self.orderList = orders
                    
                }else {
                    if let _ = msg {
                        //self.displayMessage(msg)
                    }
                }
                self.tableView.reloadData()
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderList.count > 0 ? orderList.count + 1 : 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell")!
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0)
            if MyProfile.isSeller {
                if let lbl = cell.contentView.viewWithTag(1) as? UILabel {
                    lbl.text = "Order From"
                }
            }else{
                if let lbl = cell.contentView.viewWithTag(1) as? UILabel {
                    lbl.text = "Order To"
                }
            }
            return cell
        }
        else {
            
            if self.orderList.count == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
                cell.textLabel?.text = self.emptyText
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .gray
                cell.selectionStyle = .none
                return cell
            }
            
            if MyProfile.isSeller { // seller side
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! orderListCell
                
                let order = self.orderList[indexPath.row - 1]
                
                if let displayId = order.display_id {
                    
                    cell.lblOrderId.text = displayId
                }else{
                    cell.lblOrderId.text = NSLocalizedString("NA", comment: "NA")
                }
                
                if let buyerName = order.buyerDetails.first?.username {
                    cell.lblOrderFrom.text = buyerName.capitalized
                }else{
                    cell.lblOrderFrom.text = NSLocalizedString("NA", comment: "NA")
                }
                
                if let amount = order.amount, let doubleAmount = Double(amount) {
                    let newAmount = String(format: "%.2f", doubleAmount)
                    cell.lblAmount.text = newAmount
                }else {
                    cell.lblAmount.text = NSLocalizedString("NA", comment: "NA")
                }
                
                if order.status?.lowercased() == "pending" {
                    cell.lblStatus.text = NSLocalizedString("Pending", comment: "Pending")
                    cell.lblStatus.textColor = .white
                    cell.statusView.backgroundColor = BLUECOLOR
                    cell.btnOrderAccept.isUserInteractionEnabled = true
                }
                else if order.status?.lowercased() == "accepted" {
                    cell.lblStatus.textColor = CYANCOLOR
                    cell.statusView.backgroundColor = .clear
                    cell.lblStatus.text = NSLocalizedString("Waiting for Delivery Approval", comment: "Waiting for Delivery Approval")
                    cell.btnOrderAccept.isUserInteractionEnabled = false
                }
                else if order.status?.lowercased() == "delivery confirmed" {
                    cell.lblStatus.textColor = GREENCOLOR
                    cell.statusView.backgroundColor = .clear
                    cell.lblStatus.text = NSLocalizedString("Delivery Confirmed", comment: "Delivery Confirmed")
                    cell.btnOrderAccept.isUserInteractionEnabled = false
                }
                else if order.status?.lowercased() == "rejected" {
                    cell.lblStatus.text = NSLocalizedString("Rejected", comment: "Rejected")
                    cell.lblStatus.textColor = UIColor.red
                    cell.statusView.backgroundColor = .clear
                    cell.btnOrderAccept.isUserInteractionEnabled = false
                }
                
                cell.selectionStyle = .none
                
                cell.order = order
                
                return cell
            }
            else { // Buyer side
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! orderListCell
                
                let order = self.orderList[indexPath.row - 1]
                
                if let displayId = order.display_id {
                    cell.lblOrderId.text = displayId
                }else{
                    cell.lblOrderId.text = NSLocalizedString("NA", comment: "NA")
                }
                
                if let sellerName = order.seller_data.first?.username{
                    cell.lblOrderFrom.text = sellerName.capitalized
                }else {
                    cell.lblOrderFrom.text = NSLocalizedString("NA", comment: "NA")
                }
                
                if let amount = order.amount, let doubleAmount = Double(amount) {
                    let newAmount = String(format: "%.2f", doubleAmount)
                    cell.lblAmount.text = newAmount
                }else {
                    cell.lblAmount.text = NSLocalizedString("NA", comment: "NA")
                }
                
                if order.status?.lowercased() == "pending" {
                    cell.lblStatus.text = NSLocalizedString("Pending", comment: "Pending")
                    cell.lblStatus.textColor = CYANCOLOR
                    cell.statusView.backgroundColor = .clear
                    cell.btnOrderAccept.isUserInteractionEnabled = false
                }
                else if order.status?.lowercased() == "accepted" {
                    cell.lblStatus.text = NSLocalizedString("Confirm Order Received", comment: "Confirm Order Received")
                    cell.lblStatus.textColor = .white
                    cell.statusView.backgroundColor = BLUECOLOR
                    cell.btnOrderAccept.isUserInteractionEnabled = true
                }
                else if order.status?.lowercased() == "delivery confirmed" || order.status?.lowercased() == "auto confirmed"{
                    cell.lblStatus.text = NSLocalizedString("Completed", comment: "Completed")
                    cell.lblStatus.textColor = GREENCOLOR
                    cell.statusView.backgroundColor = .clear
                    cell.btnOrderAccept.isUserInteractionEnabled = false
                }
                else if order.status?.lowercased() == "rejected" {
                    cell.lblStatus.text = NSLocalizedString("Rejected", comment: "Rejected")
                    cell.lblStatus.textColor = UIColor.red
                    cell.statusView.backgroundColor = .clear
                    cell.btnOrderAccept.isUserInteractionEnabled = false
                }
                
                cell.selectionStyle = .none
                
                cell.order = order
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0 || self.orderList.count <= 0{
            return;
        }
        
        let order = self.orderList[indexPath.row-1]
        
        if MyProfile.isSeller { // seller side
            
            if let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController {
                
                vc.order = order
                
                if let _ = self.navigationController {
                    self.navigationController?.pushViewController(vc, animated: true)
                }else {
                    tabBarVC?.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }else{ // Buyer Side
            if let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController {
                
                vc.order = order
                
                if let _ = self.navigationController {
                    self.navigationController?.pushViewController(vc, animated: true)
                }else {
                    tabBarVC?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
  
}

class orderListCell:UITableViewCell {
    
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderFrom: UILabel!
    
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var lblStatus: UILabel!

    @IBOutlet weak var lblAmount: UILabel!
    
    var order:OrderListModel!
    
    @IBOutlet weak var btnOrderAccept: UIButton!
    
    override func awakeFromNib() {
        
        if let superView = btnOrderAccept.superview {
            superView.bringSubview(toFront: btnOrderAccept)
        }
    }
    
    @IBAction func onOrderAcceptClick(_ sender: Any) {
        
        if MyProfile.isSeller {
            
            if order.status?.lowercased() == "pending"{
                
                if let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "ConfirmOrderViewController") as? ConfirmOrderViewController {
                    
                    vc.callback = {  res in
                        
                        if let res = res {
                            self.confirmOrder(orderId: self.order.orderId, response:res)
                        }
                    }
                    
                    vc.btnTitle = NSLocalizedString("Confirm Order", comment: "Confirm Order")
                    
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
            }
            
        }else {
            if order.status?.lowercased() == "accepted"{
                
                if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfirmationAlertViewController") as? ConfirmationAlertViewController {
                    
                    vc.callback = {  res in
                        
                        if res{
                            self.confirmDelivery(orderId: self.order.orderId)
                        }else{
                            if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SupportViewController") as? SupportViewController{
                                vc.message = "Order is not received."
                                tabBarVC?.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    func confirmDelivery(orderId:String?) {
        
        if let orderId = orderId{
            
            let wait:WaitView? = appDel.topViewController?.addWaitSpinner()
            
            JSONRequest.makeRequest(kDeliveryConfirmed, parameters: ["order_id": orderId]) { (data, error) in
                
                if let wait = wait{
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                if let _ = data as? [String:Any]{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "orderMainTabVCReloadTable"), object: nil, userInfo: nil)
                }
            }
        }
    }
    
    func confirmOrder(orderId:String?, response:Bool) {
        
        if let orderId = orderId{
            
            let wait:WaitView? = appDel.topViewController?.addWaitSpinner()
            
            var params = [String:String]()
            
            params["order_id"] = orderId
            
            if response {
                params["status"] = "accepted"
            }else {
                params["status"] = "rejected"
            }
            
            JSONRequest.makeRequest(kAcceptOrderRequestBySeller, parameters: params) { (data, error) in
                
                if let wait = wait{
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                if let _ = data as? [String:Any]{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "orderMainTabVCReloadTable"), object: nil, userInfo: nil)
                }
            }
        }
    }
}
