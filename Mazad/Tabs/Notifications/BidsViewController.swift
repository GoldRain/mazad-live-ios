//
//  BidsViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 02/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class BidsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnBid: UIButton!
    
    //var posts = [BuyerSellerModel]()
   
    var buyerBids = [BuyerSellerBids]()
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.separatorStyle = .none
        
        if MyProfile.isSeller{
            btnBid.setTitle(NSLocalizedString("Bids for Yours", comment: "Bids for Yours"), for: .normal)
        }else{
            btnBid.setTitle(NSLocalizedString("Your Bids", comment: "Your Bids"), for: .normal)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNot), name: NSNotification.Name(rawValue: "updateBidsViewController"), object: nil)
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getDataAndReloadTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getDataAndReloadTable()
    }
    
    @objc func handleNot() {
        self.getDataAndReloadTable()
    }
    
    func getDataAndReloadTable(){
        
        guard let userId = MyProfile.userId else {
            self.emptyText = NSLocalizedString("No Bids !!", comment: "No Bids !!")
            return
        }
        
        if MyProfile.isSeller { // For seller
            
            self.tableView.addWaitView()
            
            tabBarVC?.getPostBidDetails(user_id: userId, type: "seller", addWait: false, callback: {  (bids, msg) in
                
                self.tableView.removeWaitView()
                
                self.emptyText = NSLocalizedString("No Bids !!", comment: "No Bids !!")
                
                if let bids = bids {
                    self.buyerBids = bids
                }
                
                self.sortBuyerBidArray()
                
                self.tableView.reloadData()
            })
            
        }else { // For buyer
            
            self.tableView.addWaitView()
            
            tabBarVC?.getPostBidDetails(user_id: userId, type: "buyer", addWait: false, callback: {  (bids, msg) in
                
                self.tableView.removeWaitView()
                
                self.emptyText = NSLocalizedString("No Bids !!", comment: "No Bids !!")
                
                if let bids = bids {
                    self.buyerBids = bids
                }
                
                for item in self.buyerBids {
                    
                    if item.type == .post {
                       
                        (item.item as! BuyerSellerModel).bids = (item.item as! BuyerSellerModel).bids.filter({ (bid) -> Bool in
                            return bid.bid_user_id == MyProfile.userId
                        })
                        
                    }else {
                        (item.item as! StoryModel).bids = (item.item as! StoryModel).bids.filter({ (bid) -> Bool in
                            return bid.bid_user_id == MyProfile.userId
                        })
                    }
                    self.sortBuyerBidArray()
                }
                self.tableView.reloadData()
            })
        }
    }
    
    func sortBuyerBidArray() {
        
        self.buyerBids.sort(by: { (firstBid, secondBid) -> Bool in
            
            autoreleasepool {
                if let createdAt1 = firstBid.bidCreatedDate{
                    if let createdAt2 = secondBid.bidCreatedDate{
                        let date1 = Date(timeIntervalSince1970: Double(createdAt1)!/1000)
                        let date2 = Date(timeIntervalSince1970: Double(createdAt2)!/1000)
                        
                        return date1.compare(date2) == .orderedDescending
                    }
                }
                return false
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return buyerBids.count > 0 ? buyerBids.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.buyerBids.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        var cell:BidListCell!
        
        let item = self.buyerBids[indexPath.row]
        
        if item.type == .post { // post
            
            let post = item.item as! BuyerSellerModel
            
            if MyProfile.isSeller { // seller Cell
                cell = tableView.dequeueReusableCell(withIdentifier: "sellerCell", for: indexPath) as? BidListCell
                
                let bidsCount = post.bids.count
                cell.lblBidCount.text = "\(bidsCount)"
                
                if let date = post.post_created_at {
                    cell.lblTime.text = formatDateForDisplayOrder(date: Date(timeIntervalSince1970: Double(date)!/1000))
                }
            }
                
            else{ // buyer Cell
                cell = tableView.dequeueReusableCell(withIdentifier: "buyerCell", for: indexPath) as? BidListCell
                
                let bid = post.bids.filter({ (b) -> Bool in
                    return b.bid_user_id == MyProfile.userId
                })
                
                if let bidAmt = bid.first?.bid_amount {
                    cell.lblBidAmount.text = NSLocalizedString("Amount", comment: "Amount") + ": " + "KD" + bidAmt
                }
                
                if let date = bid.first?.bid_created_at {
                    cell.lblTime.text = formatDateForDisplayOrder(date: Date(timeIntervalSince1970: Double(date)!/1000))
                }
                
                if let status = bid.first?.status?.capitalized {
                    
                    if status == "Pending" {
                        cell.lblStatus.textColor = UIColor(red:0.53, green:0.74, blue:0.85, alpha:1.0)
                        cell.statusMainView.backgroundColor = .clear
                        cell.lblStatus.text = NSLocalizedString("Pending", comment: "Pending")
                    }else{
                        
                        cell.statusMainView.backgroundColor = UIColor(red:0.16, green:0.01, blue:0.33, alpha:1.0)
                        cell.lblStatus.textColor = UIColor.white
                        cell.lblStatus.text = NSLocalizedString("PayNow", comment: "PayNow")
                        
                        if post.is_stream { // stream
                            if let isPaid = bid.first?.is_paid, isPaid{
                                cell.statusMainView.gestureRecognizers = nil
                                cell.statusMainView.backgroundColor = UIColor.lightGray
                            }else{
                                cell.statusMainView.restorationIdentifier = "\(indexPath.row)"
                                cell.statusMainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlebidApproved(_:))))
                                cell.statusMainView.backgroundColor = BLUECOLOR
                            }
                        }else{ // post
                            if let isSold = post.is_sold, !isSold {
                                cell.statusMainView.restorationIdentifier = "\(indexPath.row)"
                                cell.statusMainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlebidApproved(_:))))
                                cell.statusMainView.backgroundColor = BLUECOLOR
                            }else{
                                cell.statusMainView.gestureRecognizers = nil
                                cell.statusMainView.backgroundColor = UIColor.lightGray
                            }
                        }
                    }
                }
            }
            
            if let img = post.post_images.first?.image_url {
                cell.imgPostImage.sd_setImage(with: URL(string: img), completed: nil)
            }
            if let productSerial = post.product_serial, productSerial != "" {
                cell.lblPostDescription.text = productSerial
            }else{
                cell.lblPostDescription.text = NSLocalizedString("NA", comment: "NA")
            }
        }
        else { // story
            
            let story = item.item as! StoryModel
            
            if MyProfile.isSeller{
                cell = tableView.dequeueReusableCell(withIdentifier: "sellerCell", for: indexPath) as? BidListCell
                
                let bidsCount = story.bids.count
                cell.lblBidCount.text = "\(bidsCount)"
            }
                
            else{ // Buyer side
                cell = tableView.dequeueReusableCell(withIdentifier: "buyerCell", for: indexPath) as? BidListCell
                
                if let bidAmt = story.bids.first?.bid_amount {
                    cell.lblBidAmount.text = NSLocalizedString("Amount", comment: "Amount") + ": " + "KD" + bidAmt
                }
                
                if let status = story.bids.first?.status?.capitalized {
                    
                    if status == "Pending" {
                        cell.lblStatus.textColor = UIColor(red:0.53, green:0.74, blue:0.85, alpha:1.0)
                        cell.statusMainView.backgroundColor = .clear
                        cell.lblStatus.text = NSLocalizedString("Pending", comment: "Pending")
                    }else{
                        cell.statusMainView.backgroundColor = UIColor(red:0.16, green:0.01, blue:0.33, alpha:1.0)
                        cell.lblStatus.textColor = UIColor.white
                        cell.lblStatus.text = NSLocalizedString("PayNow", comment: "PayNow")
                        
                        cell.statusMainView.backgroundColor = UIColor.lightGray
                        
                        if let isSold = story.is_sold, !isSold {
                            cell.statusMainView.restorationIdentifier = "\(indexPath.row)"
                            cell.statusMainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlebidApproved(_:))))
                            cell.statusMainView.backgroundColor = BLUECOLOR
                        }
                    }
                }
            }
            
            if let date = story.created_at {
                cell.lblTime.text = formatDateForDisplayOrder(date: Date(timeIntervalSince1970: Double(date)!/1000))
            }
            
            if let img = story.url{
                cell.imgPostImage.sd_setImage(with: URL(string: img), completed: nil)
            }
            
            if let productSerial = story.product_serial, productSerial != "" {
                cell.lblPostDescription.text = productSerial
            }else{
                cell.lblPostDescription.text = NSLocalizedString("NA", comment: "NA")
            }
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    var indexPath:Int!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if buyerBids.count == 0 {
            return;
        }
        
        let bidDetails = self.buyerBids[indexPath.row]
        
        if MyProfile.isSeller { // Seller Side
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "BidListViewController") as? BidListViewController {
                
                vc.bidList = bidDetails
                
                tabBarVC?.navigationController?.pushViewController(vc, animated: true)
            }
        }else{ // Buyer Side
            
            if bidDetails.type == .post {
                
                let post = bidDetails.item as! BuyerSellerModel
                
                if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpPostViewController") as? PopUpPostViewController{
                    vc.post = post
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
                
            }else { // story
                
                let story = bidDetails.item as! StoryModel
                
                if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpStoryViewController") as? PopUpStoryViewController{
                    vc.story = story
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func handlebidApproved(_ tap: UITapGestureRecognizer) {
        
        if let view = tap.view  {
            if let identifier = view.restorationIdentifier{
                if let index = Int(identifier){
                    let bidDetails = self.buyerBids[index]
                    
                    if bidDetails.type == .post {
                        let post = bidDetails.item as! BuyerSellerModel
                        let bid = post.bids.filter { (bid) -> Bool in
                            return bid.bid_user_id == MyProfile.userId
                        }
                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateOrderViewController") as? CreateOrderViewController {
                            
                            if let amount = bid.first?.bid_amount , let postId = post._id, let sellerId = post.post_user_id {
                                vc.amount = amount
                                vc.type_id = postId
                                vc.sellerId = sellerId
                                vc.type = "post"
                                vc.post = post
                                
                                tabBarVC?.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        
                    }else{ // story
                        let story = bidDetails.item as! StoryModel
                        
                        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateOrderViewController") as? CreateOrderViewController {
                            
                            if let amount = story.bids.first?.bid_amount, let storyId = story._id, let sellerId = story.userId, let paymentMode = story.payment_type?.components(separatedBy: ",") {
                                
                                vc.amount = amount
                                vc.type_id = storyId
                                vc.sellerId = sellerId
                                vc.type = "story"
                                vc.paymentType = paymentMode
                                
                                tabBarVC?.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
}

class BidListCell:UITableViewCell {
    
    @IBOutlet weak var imgPostImage:UIImageView!
    @IBOutlet weak var lblPostDescription:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    
    // only in seller cell
    @IBOutlet weak var lblBidCount: UILabel!
    
    // only in buyer cell
    @IBOutlet weak var statusMainView:UIView!
    @IBOutlet weak var lblStatus:UILabel!
    @IBOutlet weak var lblBidAmount:UILabel!
    
    
}

public enum BidType{
    case post
    case story
}

class BuyerSellerBids{
    
    var type = BidType.post
    var item:Any!
    
    init(type:BidType, item:Any){
        self.type = type
        self.item = item
    }
    
    var bidCreatedDate:String?{
        
        if let item = item as? BuyerSellerModel{
            return item.bids.first?.bid_created_at
        }
        if let item = item as? StoryModel{
            return item.bids.first?.bid_created_at
        }
        
        return nil
    }
}
