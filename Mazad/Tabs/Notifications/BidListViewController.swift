

import UIKit

class BidListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    //var bidsList = [BidModel]()
    
    var bidList:BuyerSellerBids!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Bids List", comment: "Bids List")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.navigationController?.isNavigationBarHidden = true
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if bidList.type == .post{ //// bid type is Post
            return (bidList.item as! BuyerSellerModel).bids.count + 1
        }
        else{ // bid type is Story
            return (bidList.item as! StoryModel).bids.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell")!
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0)
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! BidsListDetailCell
            
            var bid:BidModel!
            
            if bidList.type == .post{ // bid type is Post
                bid = (bidList.item as! BuyerSellerModel).bids[indexPath.row - 1]
            }
            else{ // bid type is Story
                bid = (bidList.item as! StoryModel).bids[indexPath.row - 1]
            }
            
            if let buyerName = bid.user_name {
                cell.lblCountry.text = buyerName
            }
            
            if let amt = bid.bid_amount {
                cell.lblAmount.text = amt
            }
            
            if let date = bid.bid_created_at {
                cell.lblDate.text = formatDateForDisplayBids(date: Date(timeIntervalSince1970: Double(date)!/1000))
            }
            
            if let status = bid.status?.capitalized {
                
                if status == "Pending" {
                    cell.statusMainView.backgroundColor = UIColor(red:0.16, green:0.01, blue:0.33, alpha:1.0)
                    cell.lblStatus.textColor = UIColor.white
                }else {
                    cell.statusMainView.backgroundColor = .clear
                    cell.lblStatus.textColor = UIColor(red:0.53, green:0.74, blue:0.85, alpha:1.0)
                }
                cell.lblStatus.text = status.capitalized
            }else{
                cell.lblStatus.text = "Pending"
            }
            
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row == 0 {
            return;
        }
        
        var bid:BidModel!
        
        if bidList.type == .post {
            bid = (bidList.item as! BuyerSellerModel).bids[indexPath.row - 1]
        }else{
            bid = (bidList.item as! StoryModel).bids[indexPath.row - 1]
        }
        
        if bid.status == "approved" {
            return;
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlertBidListViewController") as? AlertBidListViewController {
            
            vc.callback = { res in
                
                self.changeBidStatus(bid: bid, res: res, index:indexPath.row)
            }
            
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func changeBidStatus(bid:BidModel, res: Bool, index:Int) {
    
        var params = [String:String]()
        
        if bidList.type == .post { //post
            params["id"] = bid.post_id
            params["type"] = "post"
        }else{ //story
            params["id"] = bid.story_id
            params["type"] = "story"
        }
        
        params["bid_id"] = bid._id
        
        if res {
            params["status"] = "approved"
        }else {
            params["status"] = "rejected"
        }
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kChangeBidStatus, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let des = data["description"] as? [String:Any] {
                        
                        if let bidList = des["bids"] as? [[String:Any]] {
                            
                            if self.bidList.type == .post {
                                
                                (self.bidList.item as! BuyerSellerModel).bids.removeAll()
                                
                                for bid in bidList {
                                    (self.bidList.item as! BuyerSellerModel).bids.append(BidModel(data: bid))
                                }
                                self.tableView.reloadData()
                            }
                        }
                        if let bidsAndComments = des["bidsAndComments"] as? [[String:Any]] {
                            
                            if self.bidList.type == .story {
                                
                                (self.bidList.item as! StoryModel).bids.removeAll()
                                
                                for bid in bidsAndComments {
                                    (self.bidList.item as! StoryModel).bids.append(BidModel(data: bid))
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                    if !res {
                        if self.bidList.type == .post {
                            (self.bidList.item as! BuyerSellerModel).bids.remove(at: index - 1)
                            self.tableView.reloadData()
                        }else{
                            (self.bidList.item as! StoryModel).bids.remove(at: index - 1)
                            self.tableView.reloadData()
                        }
                    }
                    NotificationCenter.default.post(name: Notification.Name("updateBidsViewController"), object: nil)
                }
                else if let msg = data["message"] as? String{
                    let msg = NSLocalizedString(msg, comment: msg)
                    self.displayMessage(msg)
                }
            }
            else{
                let msg = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                self.displayMessage(msg)
            }
        }
    }
}

class BidsListDetailCell:UITableViewCell {
    
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var statusMainView: UIView!
    
}
