

import UIKit

class FeedbackViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var titleButton:UIButton!
    
    // for buyer
    var feedbackOrders = [PendingFeedbackModel]()
    
    // for seller
//    var posts = [BuyerSellerModel]()
//    var stories = [StoryModel]()
    var sellerFeedback = [SellerFeedback]()
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNot), name: NSNotification.Name(rawValue: "updateFeedbackViewController"), object: nil)
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getDataAndReloadTable()
    }
    
    func getDataAndReloadTable() {
        
        guard let userId = MyProfile.userId else {
            self.emptyText = NSLocalizedString("No Feedbacks !!", comment: "No Feedbacks !!")
            return
        }
        
        if MyProfile.isSeller {
            
            self.tableView.addWaitView()
            
            tabBarVC?.getFeedbackForSeller(user_id: userId, addWait: false, callback: { (arr, msg) in
            
                self.tableView.removeWaitView()
                
                self.emptyText = NSLocalizedString("No Feedbacks !!", comment: "No Feedbacks !!")
                
                if let arr = arr{
                    self.sellerFeedback = arr
                }
                
                self.tableView.reloadData()
                
            })
        }else {
            
            self.tableView.addWaitView()
            
            tabBarVC?.getPendingFeedbackOrder(user_id: userId, addWait: false, callback: { (orders, msg) in
                
                self.tableView.removeWaitView()
                
                self.emptyText = NSLocalizedString("No Feedbacks !!", comment: "No Feedbacks !!")
                
                self.feedbackOrders.removeAll()
                
                if let orders = orders {
                    self.feedbackOrders = orders
                }
                
                self.tableView.reloadData()
            })
        }
    }
    
    @objc func handleNot() {
        
        self.getDataAndReloadTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if MyProfile.isSeller{
            self.titleButton.setTitle(NSLocalizedString("Customer's Feedback", comment: "Customer's Feedback"), for: .normal)
            self.getDataAndReloadTable()
        }
        else{
            self.titleButton.setTitle(NSLocalizedString("Your Products Feedback", comment: "Your Products Feedback"), for: .normal)
            self.getDataAndReloadTable()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if MyProfile.isSeller {
            return sellerFeedback.count > 0 ? sellerFeedback.count : 1
        }else{
            return feedbackOrders.count > 0 ? feedbackOrders.count : 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if MyProfile.isSeller {
            
            if self.sellerFeedback.count == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
                cell.textLabel?.text = self.emptyText
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .gray
                cell.selectionStyle = .none
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "sellerCell", for: indexPath) as! FeedbackCell
            
            let item = self.sellerFeedback[indexPath.row]
            
            if item.type == .post { // Post
                
                let post = item.item as! BuyerSellerModel
                
                if let date = post.post_created_at{
                    let value = formatDateForDisplayBids(date: Date(timeIntervalSince1970: Double(date)!/1000))
                    cell.lblProductDescription.text = value
                }
                else{
                    cell.lblProductDescription.text = NSLocalizedString("NA", comment: "NA")
                }
                
                if let imageUrl = post.post_images.first?.image_url, imageUrl.trim() != ""{
                    cell.imgProductImage.sd_setImage(with: URL(string:imageUrl), completed: nil)
                }
                else{
                    cell.imgProductImage.image = #imageLiteral(resourceName: "ic_placeholder.png")
                }
                
                //cell.lblProductName.text = "Post"
                if let productSerial = post.product_serial, productSerial != "" {
                    cell.lblProductName.text = productSerial
                }else{
                    cell.lblProductName.text = NSLocalizedString("NA", comment: "NA")
                }
                
                let count = post.feedback.count
                
                cell.lblFeedbackCount.text = "\(count)"
                
            }
            else{ // Story
                let story = item.item as! StoryModel
                
                if let date = story.created_at{
                    let value = formatDateForDisplayBids(date: Date(timeIntervalSince1970: Double(date)!/1000))
                    cell.lblProductDescription.text = value
                }
                else{
                    cell.lblProductDescription.text = NSLocalizedString("NA", comment: "NA")
                }
                
                if let imageUrl =  story.url, imageUrl.trim() != ""{
                    cell.imgProductImage.sd_setImage(with: URL(string:imageUrl), completed: nil)
                }
                else{
                    cell.imgProductImage.image = #imageLiteral(resourceName: "ic_placeholder.png")
                }
                
                //cell.lblProductName.text = "Story"
                if let productSerial = story.product_serial, productSerial != "" {
                    cell.lblProductName.text = productSerial
                }else{
                    cell.lblProductName.text = NSLocalizedString("NA", comment: "NA")
                }
                
                let count = story.feedback.count
                
                cell.lblFeedbackCount.text = "\(count)"
            }
            
            cell.selectionStyle = .none
            
            return cell
        }
        else { // Buyer
            
            if self.feedbackOrders.count == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
                cell.textLabel?.text = self.emptyText
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .gray
                cell.selectionStyle = .none
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "buyerCell", for: indexPath) as! FeedbackCell
            
            let data = self.feedbackOrders[indexPath.row]
            
            cell.data = data
            
            if let type = data.type?.lowercased() {
                
                if type == "post" {
                    let postData = data.post_data.first
                    
                    if let imgUrl = postData?.post_images.first?.image_url {
                        cell.imgProductImage.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "ic_placeholder"), options: [], completed: nil)
                    }
                    if let productSerial = postData?.product_serial, productSerial != "" {
                        cell.lblProductName.text = productSerial
                    }else{
                        cell.lblProductName.text = NSLocalizedString("NA", comment: "NA")
                    }
                }
                if type == "story" {
                    let storyData = data.story_data.first
                    
                    if let imgUrl = storyData?.url {
                        cell.imgProductImage.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "ic_placeholder"), options: [], completed: nil)
                    }
                    if let productSerial = storyData?.product_serial, productSerial != "" {
                        cell.lblProductName.text = productSerial
                    }else{
                        cell.lblProductName.text = NSLocalizedString("NA", comment: "NA")
                    }
                }
                
                if let orderDate = data.created_at, let doubleValue = Double(orderDate) {
                    cell.lblProductDescription.text = formatDateForDisplayBids(date: Date(timeIntervalSince1970: doubleValue/1000))
                }else{
                    cell.lblProductDescription.text = NSLocalizedString("NA", comment: "NA")
                }
            }
            
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if MyProfile.isSeller{
            
            if self.sellerFeedback.count == 0 {
                return;
            }
            
            if let vc = UIStoryboard(name: "Feedback", bundle: nil).instantiateViewController(withIdentifier: "RatingAndCommentViewController") as? RatingAndCommentViewController{
                vc.sellerFeedback = self.sellerFeedback[indexPath.row]
                tabBarVC?.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            
            if self.feedbackOrders.count == 0 {
                return;
            }
            
            let data = self.feedbackOrders[indexPath.row]
            let sellerDetails = data.sellerData
            
            if data.type?.lowercased() == "post" {
                
                let post = data.post_data.first
                
                if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpPostViewController") as? PopUpPostViewController{
                    
                    vc.post = post
                    vc.post.userDetails = sellerDetails
                    
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
            }
            if data.type?.lowercased() == "story" {
                
                let story = data.story_data.first
                
                if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpStoryViewController") as? PopUpStoryViewController{
                    
                    vc.story = story
                    vc.story.userDetails = sellerDetails
                    
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
                
            }
        }
    }
}

class FeedbackCell:UITableViewCell {
    
    @IBOutlet weak var imgProductImage: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductDescription: UILabel!
    
    // buyer Cell
    @IBOutlet weak var btnRateNow: UIButton!
    //
    //seller Cell
    @IBOutlet weak var lblFeedbackCount: UILabel!
    //
    var data:PendingFeedbackModel!
    
    @IBAction func onRateNowClick(_ sender: UIButton) {
        
        if let vc = UIStoryboard(name: "Feedback", bundle:nil).instantiateViewController(withIdentifier: "CreateFeedbackViewController") as? CreateFeedbackViewController {
            
            vc.orderData = self.data
            
            tabBarVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

public enum FeedbackType{
    case post
    case story
}

class SellerFeedback {
    var type = FeedbackType.post
    var item:Any!
    
    init(type:FeedbackType, item:Any){
        self.type = type
        self.item = item
    }
}
