//
//  WalletRequestPaymentViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 05/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class WalletRequestPaymentViewController: UIViewController {
    
    @IBOutlet weak var btnRequestPayment:UIButton!
    @IBOutlet weak var btnCancel:UIButton!
    
    var callback:((Bool)->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.btnRequestPayment.addShadow()
        self.btnCancel.addShadow()
    }
    
    @IBAction func onRequestPaymentClick(_ sender:UIButton) {
        
        self.callback(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onCancelClick(_ sender:UIButton) {
        
        self.callback(false)
        self.dismiss(animated: true, completion: nil)
    }
}
