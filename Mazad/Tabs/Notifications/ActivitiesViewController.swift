
import UIKit

var workItem:DispatchWorkItem?

class ActivitiesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var activityList = [ActivityModel]()
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getDataAndReloadTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getDataAndReloadTable()
    }
    
    
    
    //MARK-: Get Data and Reload Data
    func getDataAndReloadTable() {
        
        if let workItem = workItem{
            workItem.cancel()
            print("Workitem cancelled")
        }
        
        workItem = DispatchWorkItem(block: {
        
            print("Workitem executing")
            guard let userId = MyProfile.userId else {
                self.emptyText = NSLocalizedString("No Activities !!", comment: "No Activities !!")
                self.tableView.reloadData()
                return;
            }
            
            self.tableView.addWaitView()
            
            tabBarVC?.getActivityList(user_id: userId, addWait: false, callback: {  (activities, msg) in
                
                self.tableView.removeWaitView()
                
                self.emptyText = NSLocalizedString("No Activities !!", comment: "No Activities !!")
                
                if let ac = activities {
                    self.activityList = ac
                }
                self.tableView.reloadData()
                
            })
            workItem = nil
            print("Workitem completed")
        })
        
        mainQueue.asyncAfter(deadline: .now()+0.25, execute: workItem!)
        
    }
    
    //MARKL:- TableView Function
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityList.count > 0 ? activityList.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.activityList.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ActivitieTableViewCell
        
        let data = self.activityList[indexPath.row]
        
        cell.data = data
        
        if let typeData = data.typeData.first {
            let postImages = typeData.post_images
                cell.typeData = postImages
        }
        
        cell.setUpData()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if activityList.count == 0 {
            return;
        }
        
        let data = self.activityList[indexPath.row]
        
        if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpPostViewController") as? PopUpPostViewController{
            
            vc.post = data.typeData.first
            vc.post.userDetails = data.user_data
            
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            
            appDel.topViewController?.present(vc, animated: true, completion: nil)
        }
    }
}

class ActivitieTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var imgUserImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var data:ActivityModel?
    
    var typeData = [PostImageModel]()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //custom logic goes here
        
        collectionView.dataSource = self
        collectionView.delegate = self
        //disable the user intereaction because when the user will table on
        collectionView.isUserInteractionEnabled = false
    }
    
    func setUpData() {
        
        if let data = data {
            
            if let imgUrl = data.user_data.first?.profilePhoto {
                self.imgUserImage.sd_setImage(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "icon_profile_grey2"), options: [], completed: nil)
            }
            if let name = data.user_data.first?.username {
                self.lblUserName.text = name
            }
            if let msg = data.message {
                self.lblMessage.text = NSLocalizedString(msg, comment: msg)
            }
            if let date = data.created_at {
                self.lblTime.text = formatDateForDisplayOrder(date: Date(timeIntervalSince1970: Double(date)!/1000))
            }
        }
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return typeData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        if let img = cell.contentView.viewWithTag(1) as? UIImageView {
            if let imgUrl = self.typeData[indexPath.row].image_url {
                
                img.sd_setImage(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "ic_placeholder.png"), options: [], completed: { (image, _, _, _) in
                    if image == nil{
                        img.image = #imageLiteral(resourceName: "ic_placeholder.png")
                        print("Failed to load \(imgUrl)")
                    }
                    
                })
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 36.0, height: 36.0)
    }
    
}
