

import UIKit
import CarbonKit


class BuyerNotificationsViewController: UIViewController, CarbonTabSwipeNavigationDelegate  {
    
    @IBOutlet weak var containerView: UIView!
    
    public var carbonController: CarbonTabSwipeNavigation!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = NSLocalizedString("Home", comment: "Home")
        
        let item1 = NSLocalizedString("Activities", comment: "Activities")
        let item2 = NSLocalizedString("Orders", comment: "Orders")
        let item3 = NSLocalizedString("Bids", comment: "Bids")
        let item4 = NSLocalizedString("Feedback", comment: "Feedback")
        let item5 = NSLocalizedString("Payments", comment: "Payments")
        
        let items = [item1,item2,item3,item4,item5]
        
        carbonController = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        carbonController.setTabBarHeight(50.0)
        
        let widthOfTabIcons = self.view.frame.size.width/5
        carbonController.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 0)
        carbonController.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 1)
        carbonController.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 2)
        carbonController.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 3)
        carbonController.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 4)
        
        let normalColor =  UIColor(red: 103/255, green: 65/255, blue: 170/255, alpha: 1.0)
        carbonController.setNormalColor(normalColor, font: UIFont.systemFont(ofSize: 13.0, weight: .medium))
        carbonController.setSelectedColor(.white, font: UIFont.systemFont(ofSize: 13.0, weight: .medium))
        //carbonController.setNormalColor(.lightGray)
        carbonController.setIndicatorColor(.white)
        //carbonController.setSelectedColor(.white)
        carbonController.toolbar.barTintColor = .purpleColor
        carbonController.toolbar.isTranslucent = false
        carbonController.setIndicatorHeight(2.0)
        
        carbonController.insert(intoRootViewController: self, andTargetView: self.containerView)
        
        self.carbonController.setCurrentTabIndex(0, withAnimation: false)
        
        self.title = NSLocalizedString("Notifications", comment: "Notifications")
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        if index == 0 {
            print("Workitem buyer")
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "ActivitiesViewController")
            return vc
        }else if index == 1 {
            let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "OrdersMainTabViewController")
            return vc
        }else if index == 2 {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "BidsViewController")
            return vc
        }else if index == 3 {
            let vc = UIStoryboard(name: "Feedback", bundle: nil).instantiateViewController(withIdentifier: "FeedbackViewController")
            return vc
        }
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "PaymentsViewController")
        vc.willMove(toParentViewController: self)
        self.addChildViewController(vc)
        return vc
    }
    
}
