

import UIKit

class WalletViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var walletData = [WalletPayementModel]()
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getDataAndReloadTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getDataAndReloadTable()
    }
    
    func getDataAndReloadTable() {
        
        guard let userId = MyProfile.userId else {
            self.emptyText = NSLocalizedString("No Data !!", comment: "No Data !!")
            return
        }
        
        self.tableView.addWaitView()
        
        
        tabBarVC?.getWalletList(type: "seller", user_id: userId, addWait: false, callback: {  (walletList, msg) in
            
            self.tableView.removeWaitView()
            
            self.emptyText = NSLocalizedString("No Data !!", comment: "No Data !!")
            
            self.walletData.removeAll()
            
            if let walletList = walletList {
                self.walletData = walletList
            }else{
                //self.displayMessage(msg)
            }
            self.tableView.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return walletData.count > 0 ? walletData.count : 1
    }
    // tag1 = MSglable // tag2 = Statusview // tag3 = Statuslabel // tag4 = Statusbutton
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.walletData.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        let data = self.walletData[indexPath.row]
        
        if let lbl = cell.contentView.viewWithTag(1) as? UILabel {
            
            lbl.text = "\(NSLocalizedString("You have received KD", comment: "You have received KD")) \(data.orderDetails.first?.amount ?? "") \(NSLocalizedString("for order number", comment: "for order number")) \(data.orderDetails.first?.display_id  ?? "")."
        }
        if let walletStatus = data.status?.lowercased() {
            if let orderDeliveryStatus = data.orderDetails.first?.status?.lowercased() {
                if let userPaymentStatus = data.orderDetails.first?.user_payment_status?.lowercased() {
                    
                    if walletStatus == "pending" {
                        if orderDeliveryStatus == "pending" || userPaymentStatus == "pending"{
                            if let lbl = cell.contentView.viewWithTag(3) as? UILabel {
                                lbl.text = NSLocalizedString("Pending", comment: "Pending")
                                lbl.textColor = CYANCOLOR
                            }
                            if let view = cell.contentView.viewWithTag(2) {
                                view.backgroundColor = .clear
                            }
                            if let btn = cell.contentView.viewWithTag(4) as? UIButton {
                                btn.isUserInteractionEnabled = false
                            }
                        }else{ // when delivery is confirmed or autoConfirmed and payment status is completed
                            if let lbl = cell.contentView.viewWithTag(3) as? UILabel {
                                lbl.text = NSLocalizedString("In the wallet Ask for Deposit", comment: "In the wallet Ask for Deposit") //status
                                lbl.textColor = .white
                            }
                            if let view = cell.contentView.viewWithTag(2) {
                                view.backgroundColor = BLUECOLOR
                            }
                            if let btn = cell.contentView.viewWithTag(4) as? UIButton {
                                btn.isUserInteractionEnabled = true
                            }
                        }
                    }else if walletStatus == "requested" {
                        
                        if let lbl = cell.contentView.viewWithTag(3) as? UILabel {
                            lbl.text = NSLocalizedString("Deposit Requested", comment: "Deposit Requested")
                            lbl.textColor = CYANCOLOR
                        }
                        if let view = cell.contentView.viewWithTag(2) {
                            view.backgroundColor = .clear
                        }
                        if let btn = cell.contentView.viewWithTag(4) as? UIButton {
                            btn.isUserInteractionEnabled = false
                        }
                    }else if walletStatus == "completed" {
                        
                        if let lbl = cell.contentView.viewWithTag(3) as? UILabel {
                            lbl.text = "Settled"
                            lbl.textColor = GREENCOLOR
                        }
                        if let view = cell.contentView.viewWithTag(2) {
                            view.backgroundColor = .clear
                        }
                        if let btn = cell.contentView.viewWithTag(4) as? UIButton {
                            btn.isUserInteractionEnabled = false
                        }
                    }
                }
            }
        }
        if let btn = cell.contentView.viewWithTag(4) as? UIButton {
            btn.restorationIdentifier = "\(indexPath.row)"
            btn.addTarget(self, action: #selector(self.handleOnClick(_:)), for: .touchUpInside)
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    @objc func handleOnClick(_ sender:UIButton) {
        
        if let identifier = sender.restorationIdentifier {
            
            if let row = Int(identifier) {
                
                let data = self.walletData[row]
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "WalletRequestPaymentViewController") as? WalletRequestPaymentViewController {
                    
                    vc.callback = { res in
                        
                        if res {
                            if let orderId = data.order_id {
                                self.requestPayment(orderId:orderId)
                            }
                        }
                    }
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    func requestPayment(orderId:String) {
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kRequestPaymentToBuyer, parameters: ["order_id":orderId]) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    
                    self.getDataAndReloadTable()
                    
                }else if let msg = data["message"] as? String {
                    let str = NSLocalizedString(msg, comment: msg)
                    
                    appDel.topViewController?.displayMessage(str)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if walletData.count <= 0 {
            return;
        }
        
        let data = self.walletData[indexPath.row]
        let orderId = data.order_id
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kGetOrderDetails, parameters: ["order_id":orderId!]) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    if let des = data["description"] as? [[String:Any]] {
                        if let orderData = des.first {
                            if let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController {
                                
                                let order = OrderListModel(data: orderData)
                                
                                vc.order = order
                                
                                if let _ = self.navigationController {
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }else {
                                    tabBarVC?.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

