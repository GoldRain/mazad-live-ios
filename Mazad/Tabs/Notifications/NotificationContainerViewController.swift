//
//  NotificationContainerViewController.swift
//  Mazad Live
//
//  Created by Bodacious_mac_aly on 11/2/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class NotificationContainerViewController: UIViewController {

    var sellerVC:SellerNotificationsViewController?
    var buyerVC:BuyerNotificationsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if MyProfile.isSeller{
            if self.sellerVC == nil{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SellerNotificationsViewController") as? SellerNotificationsViewController{
                    self.view.addSubViewWithConstraints(view: vc.view)
                    self.sellerVC = vc
                }
            }
            if let buyerVC = self.buyerVC{
                self.view.sendSubview(toBack: buyerVC.view)
                buyerVC.view.isHidden = true
            }
            self.sellerVC?.view.isHidden = false
        }
        else{
            if self.buyerVC == nil{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "BuyerNotificationsViewController") as? BuyerNotificationsViewController{
                    self.view.addSubViewWithConstraints(view: vc.view)
                    self.buyerVC = vc
                }
            }
            if let sellerVC = self.sellerVC{
                self.view.sendSubview(toBack: sellerVC.view)
                sellerVC.view.isHidden = true
            }
            self.buyerVC?.view.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
