

import UIKit

class RatingAndCommentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var sellerFeedback:SellerFeedback!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let str = NSLocalizedString("Ratings", comment: "Ratings")
        
        self.title = str
        
        self.tableView.tableFooterView = UIView()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sellerFeedback.type == .post{
            return (sellerFeedback.item as! BuyerSellerModel).feedback.count
        }
        else{
            return (sellerFeedback.item as! StoryModel).feedback.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FeedbackDetailCell
        
        if sellerFeedback.type == .post{
            
            cell.post = (sellerFeedback.item as! BuyerSellerModel)
            cell.feedback = (sellerFeedback.item as! BuyerSellerModel).feedback[indexPath.row]
            
        }
        else{
            
            cell.story = (sellerFeedback.item as! StoryModel)
            cell.feedback = (sellerFeedback.item as! StoryModel).feedback[indexPath.row]
            
        }
        cell.selectionStyle = .none
        return cell
    }
}

class FeedbackDetailCell:UITableViewCell{
    
    @IBOutlet weak var imgPhoto:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblRating:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblCountry:UILabel!
    
    var story:StoryModel!{
        didSet{
            if let url = story.url, url.trim() != ""{
                imgPhoto.sd_setImage(with: URL(string:url), completed: nil)
            }
            else{
                imgPhoto.image = #imageLiteral(resourceName: "ic_placeholder.png")
            }
        }
    }
    var post:BuyerSellerModel!{
        didSet{
            if let url = post.post_images.first?.image_url, url.trim() != ""{
                imgPhoto.sd_setImage(with: URL(string:url), completed: nil)
            }
            else{
                imgPhoto.image = #imageLiteral(resourceName: "ic_placeholder.png")
            }
        }
    }
    
    var feedback:FeedbackModel!{
        didSet{
            
            if let user = feedback.user_name{
                self.lblUserName.text = user
            }
            else{
                self.lblUserName.text = "NA"
            }
            
            if let country = feedback.user_country?.countryName{
                self.lblCountry.text = country
            }
            else{
                self.lblCountry.text = "NA"
            }
            if let value = feedback.comment{
                self.lblDescription.text = value
            }
            else{
                self.lblDescription.text = "NA"
            }
            
            self.lblRating.text = ""
            
            if let value = feedback.rating{
                if let intValue = Int(value){
                    for _ in 0..<intValue{
                        if self.lblRating.text == ""{
                            self.lblRating.text = "★"
                        }
                        else{
                            self.lblRating.text = self.lblRating.text! + "★"
                        }
                    }
                }
                else{
                    let intValue = Int(Double(value)!)
                    for _ in 0..<intValue{
                        if self.lblRating.text == ""{
                            self.lblRating.text = "★"
                        }
                        else{
                            self.lblRating.text = self.lblRating.text! + "★"
                        }
                    }
                }
            }
        }
    }
    
}
