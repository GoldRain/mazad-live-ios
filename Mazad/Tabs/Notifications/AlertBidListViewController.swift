//
//  AlertBidListViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 02/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class AlertBidListViewController: UIViewController {

    @IBOutlet weak var btnApproveBid:UIButton!
    @IBOutlet weak var btnRejectBid:UIButton!
    @IBOutlet weak var alertView:UIView!
    
    var callback:((Bool)->())!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let firstView = touches.first?.view{
            if firstView != alertView {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.btnApproveBid.addShadow()
        self.btnRejectBid.addShadow()
    }
    
    @IBAction func onApproveBidClick(_ sender:UIButton) {
        
        self.callback(true)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onRejectBidClick(_ sender:UIButton) {
        
        self.callback(false)
        self.dismiss(animated: true, completion: nil)
    }
}
