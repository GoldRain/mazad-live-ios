

import UIKit

class PaymentsViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleButton:UIButton!
    
    var paymentData = [WalletPayementModel]()
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Refresh Control
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        self.refreshControl = getRefreshControl(attrStr: attrStr)
        
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //
    }
    
    @objc func pullToRefresh() {
        
        self.refreshControl.endRefreshing()
        
        self.getDataAndReloadTable(type: "buyer")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if MyProfile.isSeller{
            self.titleButton.setTitle(NSLocalizedString("Payments In", comment: "Payments In"), for: .normal)
        }
        else{// buyer
            self.titleButton.setTitle(NSLocalizedString("Payments Out", comment: "Payments Out"), for: .normal)
            
            self.getDataAndReloadTable(type: "buyer")
        }
    }
    
    func getDataAndReloadTable(type:String) {
        
        guard let userId = MyProfile.userId else {
            self.emptyText = NSLocalizedString("No Data !!", comment: "No Data !!")
            return;
        }
        
        tabBarVC?.getWalletList(type: type, user_id: userId, addWait: true, callback: {  (walletList, msg) in
            
            self.emptyText = NSLocalizedString("No Data !!", comment: "No Data !!")
            
            self.paymentData.removeAll()
            
            if let walletList = walletList {
                self.paymentData = walletList
            }else{
                //self.displayMessage(msg)
            }
            self.tableView.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentData.count > 0 ? paymentData.count + 1 : 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell")!
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0)
            cell.selectionStyle = .none
            return cell
        }
        else{
            
            if self.paymentData.count == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
                cell.textLabel?.text = self.emptyText
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .gray
                cell.selectionStyle = .none
                return cell
            }
            
            let data = self.paymentData[indexPath.row-1]
            
            var cell:paymentCell!
            
            //if let status = data.status?.capitalized, status == "Pending" {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as? paymentCell
                
            //}else{
              //  cell = tableView.dequeueReusableCell(withIdentifier: "detailCell2", for: indexPath) as? paymentCell
            //}
            
            cell.data = data
            cell.setupData()
            
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if paymentData.count == 0 {
            return;
        }
        
        if indexPath.row == 0 {
            return;
        }
        
        let data = self.paymentData[indexPath.row - 1]
        
        if let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController {
            
            vc.order = data.orderDetails.first
            vc.order.postData = data.typeData
            
            tabBarVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

class paymentCell:UITableViewCell {
    
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    //
    @IBOutlet weak var askForDepositView: UIView!
    @IBOutlet weak var lblAskForDeposit: UILabel!
    //
    @IBOutlet weak var pendingView: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    
    var data:WalletPayementModel!
    
    func setupData() {
        
        if let orderId = data.orderDetails.first?.display_id {
            self.lblOrderId.text = orderId
        }
        if let sellerName = data.seller_name {
            self.lblFrom.text = NSLocalizedString(sellerName, comment: sellerName)
        }
        if let amount = data.orderDetails.first?.amount {
            self.lblAmount.text = amount
        }
        if let method = data.payment_method?.lowercased() {
            if method == "cod" {
                self.lblPaymentMethod.text = NSLocalizedString("COD", comment: "COD")
            }else{
                self.lblPaymentMethod.text = NSLocalizedString("Card", comment: "Card")
            }
        }
        if let status = data.orderDetails.first?.user_payment_status?.lowercased() {
            if status == "pending"{
                self.lblStatus.text = NSLocalizedString(status.capitalized, comment: status.capitalized)
                self.lblStatus.textColor = CYANCOLOR
            }else{// completed
                self.lblStatus.text = NSLocalizedString(status.capitalized, comment: status.capitalized)
                self.lblStatus.textColor = GREENCOLOR
            }
            
        }
    }
}
