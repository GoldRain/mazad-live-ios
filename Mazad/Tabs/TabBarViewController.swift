
import UIKit
import CarbonKit
import YPImagePicker
import AVFoundation
import AVKit
import Photos
import AWSS3
import AWSCore
import SDWebImage

var tabBarVC:TabBarViewController?

class TabBarViewController: UIViewController, CarbonTabSwipeNavigationDelegate {
    
    var countryList:[CountryModel]?
    
    @IBOutlet weak var topSpaceBarView: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var customNavHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSettingNav: UIButton!
    @IBOutlet weak var btnDollarNav: UIButton!
    @IBOutlet weak var btnSwitchProfileNav: UIButton!
    
    public var carbonController: CarbonTabSwipeNavigation!
    public var tabBarView: UIView!
    
    let img1 = UIImageView()
    let img2 = UIImageView()
    let img3 = UIImageView()
    let img4 = UIImageView()
    let img5 = UIImageView()
    
    let lbl1 = UILabel()
    let lbl2 = UILabel()
    let lbl3 = UILabel()
    let lbl4 = UILabel()
    let lbl5 = UILabel()
    
    @IBOutlet weak var containerVIewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Home", comment: "Home")
        
        let item1 = NSLocalizedString("Home", comment: "Home")
        let item2 = NSLocalizedString("Search", comment: "Search")
        let item3 = NSLocalizedString("Orders", comment: "Orders")
        let item4 = NSLocalizedString("Notification", comment: "Notification")
        let item5 = NSLocalizedString("Profile", comment: "Profile")
        
        let items = [item1,item2,item3,item4,item5]
        
        //self.btnCountry.setImage(nil, for: .normal)
//        let countryCode = (NSLocale.current as NSLocale).countryCode
//        if let countryCode = countryCode{
//            let base: UInt32 = 127397
//            let flagStr = countryCode.unicodeScalars.compactMap { String.init(UnicodeScalar(base + $0.value)!) }.joined()
//            self.btnCountry.setTitle(flagStr, for: .normal)
//        }
        
        self.setupBottomView()
        
        carbonController = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        carbonController.setTabBarHeight(0.0)
        carbonController.setIndicatorHeight(0.0)
        
        carbonController.insert(intoRootViewController: self, andTargetView: self.containerView)
        
        self.carbonController.setCurrentTabIndex(0, withAnimation: false)
        self.setTintColor(index: 0)
        
        if postIdFromDynamicLink != "" {
            self.carbonController.setCurrentTabIndex(1, withAnimation: false)
            self.setTintColor(index: 1)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(switchTabsFromMenu(_:)), name: NSNotification.Name("switchTabs"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification(_:)), name: NSNotification.Name("changeUser"), object: nil)
        
        if let userId = MyProfile.userId{
            MessageCenter.start(log: false, userId: userId, notificationToken: MyProfile.playerId ?? "empty")
        }
       
        tabBarVC = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
//        DispatchQueue.main.asyncAfter(deadline: .now()+2.0, execute: {
//            
//            let vc = UIStoryboard(name: "Feedback", bundle: nil).instantiateViewController(withIdentifier: "CreateFeedbackViewController")
//            let nvc = UINavigationController(rootViewController: vc)
//            self.present(nvc, animated: true, completion: nil)
//            
//        })
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func switchTabsFromMenu(_ sender: NSNotification){
        if let index = sender.userInfo?["tabIndex"] as? Int{
            self.carbonController.setCurrentTabIndex(UInt(index), withAnimation: true)
        }
    }
    
    @objc func handleNotification(_ sender: NSNotification){
        
        if let userType = sender.userInfo?["user"] as? Bool{
            if userType{
                // userType = seller
                img3.image = #imageLiteral(resourceName: "icon_camera_grey").withRenderingMode(.alwaysTemplate)
                lbl3.text = NSLocalizedString("Camera", comment: "Camera")
            }else{
                // userType = buyer
                img3.image = #imageLiteral(resourceName: "icon_123")
                lbl3.text = NSLocalizedString("Orders", comment: "Orders")            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
        
    }
    
    var selectedCountryId:String?
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        //ordersTabNavigation
        let index = Int(index)
        
//        print("Index is \(index)")
//        
//        if index == 0{
//            self.hideTabBar()
//        }
//        else{
//            self.showTabBar()
//        }
        
        
        if index == 0 {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            if let country = selectedCountryId {
                vc.selectedCountryId = country
            }
            return vc
        }else if index == 1 {
            let vc = UIStoryboard(name: "Search", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController")
            return vc
        }else if index == 2 {//OrdersMainTabViewController
            let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "OrdersMainTabViewController")
            return vc
        }else if index == 3 {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "NotificationContainerViewController")
            return vc
        }else if index == 4 {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            if let userId = MyProfile.userId {
                vc.userId = userId
            }
            return vc
        }
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController")
        return vc
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        self.setTintColor(index: Int(index))
    
    }
    
    @IBAction func onHomeClick(_ sender: UIButton){
        self.carbonController.setCurrentTabIndex(0, withAnimation: true)
    }
    
    @IBAction func onSearchClick(_ sender: UIButton){
        self.carbonController.setCurrentTabIndex(1, withAnimation: true)
    }
    
    @IBAction func onOrdersClick(_ sender: UIButton){
        
        if MyProfile.isSeller{
            self.onCameraClick()
        }else{
            self.carbonController.setCurrentTabIndex(2, withAnimation: true)
        }
    }
    
    func onCameraClick() {
        
        appDel.getAllCountries { (countries, msg) in
            
            if let countries = countries{
                self.countryList = countries
                
                guard let userId = MyProfile.userId else {
                    return;
                }
                
                let wait = appDel.topViewController?.addWaitSpinner()
                
                JSONRequest.makeRequest(kShowCounts, parameters: ["user_id":userId], callback: { (data, error) in
                    
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    
                    if let data = data as? [String:Any] {
                        if let err = data["error"] as? Bool, !err {
                            if let des = data["description"] as? [String:Any] {
                                if let postDict = des["unlimited_post_count"] as? [String:Any] {
                                    let postObj = CountModel(data: postDict)
                                    if let status = postObj.status {
                                        if status {
                                            self.showPicker()
                                        }else{
                                            if postObj.count == 0 {
                                                let str = NSLocalizedString("You are out of posts. To upload more posts, please upgrade", comment: "You are out of posts. To upload more posts, please upgrade")
                                                self.displayMessage(str, callback: { () -> (Void) in
                                                    if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpgradeViewController") as? UpgradeViewController {
                                                        self.navigationController?.pushViewController(vc, animated: true)
                                                    }
                                                })
                                            }else{
                                                self.showPicker()
                                            }
                                        }
                                    }
                                }
                            }
                        }else{
                            let str = NSLocalizedString("Failed to get response, Please try again later", comment: "Failed to get response, Please try again later")
                            self.displayMessage(str)
                        }
                    }else{
                        let str = NSLocalizedString("Failed to get response, Please try again later", comment: "Failed to get response, Please try again later")
                        self.displayMessage(str)
                    }
                })
            }
            else if let msg = msg{
                self.displayMessage(msg)
            }
        }
    }
    
    @IBAction func onNotificationClick(_ sender: UIButton){
        self.carbonController.setCurrentTabIndex(3, withAnimation: true)
    }
    
    @IBAction func onProfileClick(_ sender: UIButton){
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        self.carbonController.setCurrentTabIndex(4, withAnimation: true)
    }
    
    func setTintColor(index: Int){
        
        print("now index ==> \(index)")
        
        img1.tintColor = index == 0 ? .purpleColor : .imageGrayColor
        img2.tintColor = index == 1 ? .purpleColor : .imageGrayColor
        img3.tintColor = index == 2 ? .purpleColor : .imageGrayColor
        img4.tintColor = index == 3 ? .purpleColor : .imageGrayColor
        img5.tintColor = index == 4 ? .purpleColor : .imageGrayColor
        
        
        lbl1.textColor = index == 0 ? .purpleColor : .imageGrayColor
        lbl2.textColor = index == 1 ? .purpleColor : .imageGrayColor
        lbl3.textColor = index == 2 ? .purpleColor : .imageGrayColor
        lbl4.textColor = index == 3 ? .purpleColor : .imageGrayColor
        lbl5.textColor = index == 4 ? .purpleColor : .imageGrayColor
        
        let navHeight = CGFloat(70.0)
        
        DispatchQueue.main.async {
            
            switch index {
            case 0:
                //self.btnDollarNav.isHidden = false
                
//                if MyProfile.AvailablePostCount == -1 && MyProfile.AvailableStoryCount == -1 && MyProfile.AvailableLiveCount == -1 && MyProfile.showAds == false {
//                    self.btnDollarNav.fd_collapsed = true
//                    self.btnDollarNav.isHidden = true
//                }else{
//                    self.btnDollarNav.fd_collapsed = false
//                    self.btnDollarNav.isHidden = false
//                }
                self.btnDollarNav.fd_collapsed = false
                self.btnDollarNav.isHidden = false
                
                self.btnSettingNav.isHidden = false
                self.btnCountry.isHidden = false
                self.btnSwitchProfileNav.isHidden = false
                self.customNavHeightConstraint.constant = navHeight
                self.lblTitle.text = NSLocalizedString("Home", comment: "Home")
                self.title = NSLocalizedString("Home", comment: "Home")
                //self.view.layoutIfNeeded()
            case 1:
                self.btnDollarNav.isHidden = true
                self.btnSettingNav.isHidden = true
                self.btnSwitchProfileNav.isHidden = true
                self.btnCountry.isHidden = true
                self.customNavHeightConstraint.constant = navHeight
                self.lblTitle.text = NSLocalizedString("Search", comment: "Search")
                self.title = NSLocalizedString("Search", comment: "Search")
                //self.view.layoutIfNeeded()
            case 2:
                self.btnDollarNav.isHidden = true
                self.btnSettingNav.isHidden = true
                self.btnSwitchProfileNav.isHidden = true
                self.btnCountry.isHidden = true
                if MyProfile.isSeller{
                    self.customNavHeightConstraint.constant = navHeight//CGFloat(0.0)
                    self.lblTitle.text = NSLocalizedString("Camera", comment: "Camera")
                    self.title = NSLocalizedString("Camera", comment: "Camera")
                }else{
                    self.customNavHeightConstraint.constant = navHeight
                    self.lblTitle.text = NSLocalizedString("Orders", comment: "Orders")
                    self.title = NSLocalizedString("Orders", comment: "Orders")
                }
                
                //self.view.layoutIfNeeded()
            case 3:
                self.btnDollarNav.isHidden = true
                self.btnSettingNav.isHidden = true
                self.btnSwitchProfileNav.isHidden = true
                self.btnCountry.isHidden = true
                self.customNavHeightConstraint.constant = navHeight
                self.lblTitle.text = NSLocalizedString("Notifications", comment: "Notifications")
                self.title = NSLocalizedString("Notifications", comment: "Notifications")
                //self.view.layoutIfNeeded()
            case 4:
                
                self.customNavHeightConstraint.constant = CGFloat(0.0)
                self.lblTitle.text = NSLocalizedString("Profile", comment: "Profile")
                self.title = NSLocalizedString("Profile", comment: "Profile")
                //self.view.layoutIfNeeded()
                
            default:
                break
            }
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func onCountryClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else {
            getLoginDialouge ()
            return;
        }
        
        appDel.getAllCountries { (countries, msg) in
            
            if let countries = countries{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountriesSelectionViewController") as? CountriesSelectionViewController{
                    vc.type = .tabBarVC
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.countriesList = countries
                    vc.callback = { data in
                        if let flagUrl = data.flagImageUrl {
                            self.btnCountry.sd_setImage(with: URL(string: "\(apiBaseUrl)\(flagUrl)"), for: .normal , completed: nil)
                        }
                        if let name = data.countryName, name == "All" {
                            self.btnCountry.setImage(#imageLiteral(resourceName: "world"), for: .normal)
                            MyProfile.countryName = name
                        }
                        if let id = data._id{
                            self.selectedCountryId = id
                            MyProfile.countryId = id
                        }
                        if let code = data.countryCode{
                            MyProfile.countryCode = code
                        }
                        if let id = self.selectedCountryId {
                            categoryVC?.selectedCountryId = id
                            categoryVC?.getAllPosts(countryId: id, showWait: true, callback: { (_, _) in
                            })
                            self.selectedCountryId = nil
                        }else{
                            categoryVC?.selectedCountryId = nil
                            categoryVC?.getAllPosts(countryId: "", showWait: true, callback: { (_, _) in
                            })
                        }
                    }
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if let msg = msg{
                self.displayMessage(msg)
            }
        }
    }
    
    @IBAction func onSettingClick(_ sender: UIButton) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController"){
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    //UpgradeViewController
    @IBAction func onUpgradeClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpgradeViewController"){
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onSwitchClick(_ sender: UIButton) {
        
        guard let _ = MyProfile.userId else{
            getLoginDialouge()
            return;
        }
        
        MyProfile.isSeller = !MyProfile.isSeller
        
        if sender.currentImage == #imageLiteral(resourceName: "ic_SellerAccount"){
            sender.setImage(#imageLiteral(resourceName: "icon_profileDual2"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "ic_SellerAccount"), for: .normal)
        }
        
        NotificationCenter.default.post(name: NSNotification.Name("changeUser"), object: nil, userInfo: ["user":MyProfile.isSeller])
    }
    
    func showPicker() {
        
        var config = YPImagePickerConfiguration()
        
        /* Set this to true if you want to force the  library output to be a squared image. Defaults to false */
        //         config.library.onlySquare = true
        
        /* Choose what media types are available in the library. Defaults to `.photo` */
        config.library.mediaType = .photoAndVideo
//        config.library.mediaType = .photo
        
        /* Enables selecting the front camera by default, useful for avatars. Defaults to false */
         config.usesFrontCamera = true
        
        /* Adds a Filter step in the photo taking process. Defaults to true */
        // config.showsFilters = false
        
        /* Manage filters by yourself */
        //        config.filters = [YPFilter(name: "Mono", coreImageFilterName: "CIPhotoEffectMono"),
        //                          YPFilter(name: "Normal", coreImageFilterName: "")]
        //        config.filters.remove(at: 1)
        //        config.filters.insert(YPFilter(name: "Blur", coreImageFilterName: "CIBoxBlur"), at: 1)
        
        /* Enables you to opt out from saving new (or old but filtered) images to the
         user's photo library. Defaults to true. */
        config.shouldSaveNewPicturesToAlbum = false
        
        /* Choose the videoCompression. Defaults to AVAssetExportPresetHighestQuality */
        config.video.compression = AVAssetExportPresetMediumQuality
        
        /* Defines the name of the album when saving pictures in the user's photo library.
         In general that would be your App name. Defaults to "DefaultYPImagePickerAlbumName" */
        // config.albumName = "ThisIsMyAlbum"
        
        /* Defines which screen is shown at launch. Video mode will only work if `showsVideo = true`.
         Default value is `.photo` */
        config.startOnScreen = .library
        
        /* Defines which screens are shown at launch, and their order.
         Default value is `[.library, .photo]` */
        config.screens = [.library, .photo, .video]
        
        /* Can forbid the items with very big height with this property */
        //        config.library.minWidthForItem = UIScreen.main.bounds.width * 0.8
        
        /* Defines the time limit for recording videos.
         Default is 30 seconds. */
         config.video.recordingTimeLimit = 120.0
        
        /* Defines the time limit for videos from the library.
         Defaults to 60 seconds. */
        config.video.libraryTimeLimit = 120.0
        
        /* Adds a Crop step in the photo taking process, after filters. Defaults to .none */
        config.showsCrop = .rectangle(ratio: (16/9))
        
        /* Defines the overlay view for the camera. Defaults to UIView(). */
        // let overlayView = UIView()
        // overlayView.backgroundColor = .red
        // overlayView.alpha = 0.3
        // config.overlayView = overlayView
        
        /* Customize wordings */
        config.wordings.libraryTitle = NSLocalizedString("Select Media", comment: "Select Media")
        config.wordings.save = NSLocalizedString("Next", comment: "Next")
        
        /* Defines if the status bar should be hidden when showing the picker. Default is true */
        config.hidesStatusBar = false
        
        /* Defines if the bottom bar should be hidden when showing the picker. Default is false */
        //config.hidesBottomBar = true
        
        config.library.maxNumberOfItems = 5
        
        /* Disable scroll to change between mode */
        // config.isScrollToChangeModesEnabled = false
        
        config.library.minNumberOfItems = 1
        
        /* Skip selection gallery after multiple selections */
         config.library.skipSelectionsGallery = true
        
        /* Here we use a per picker configuration. Configuration is always shared.
         That means than when you create one picker with configuration, than you can create other picker with just
         let picker = YPImagePicker() and the configuration will be the same as the first picker. */
        
        
        /* Only show library pictures from the last 3 days */
        //let threDaysTimeInterval: TimeInterval = 3 * 60 * 60 * 24
        //let fromDate = Date().addingTimeInterval(-threDaysTimeInterval)
        //let toDate = Date()
        //let options = PHFetchOptions()
        //options.predicate = NSPredicate(format: "creationDate > %@ && creationDate < %@", fromDate as CVarArg, toDate as CVarArg)
        //
        ////Just a way to set order
        //let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: true)
        //options.sortDescriptors = [sortDescriptor]
        //
        //config.library.options = options
        
        config.showsFilters = false
        
        config.isScrollToChangeModesEnabled = false
        config.showsCrop = YPCropType.none
        
        let picker = YPImagePicker(configuration: config)
        
        /* Change configuration directly */
        // YPImagePickerConfiguration.shared.wordings.libraryTitle = "Gallery2"
        
        
        /* Multiple media implementation */
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            _ = items.map { print("🧀 \($0)") }
            
            let selectedItems = items
//            var selectedImages = [YPMediaPhoto]()
            var selectedImages = [UIImage]()
            var selectedVideoUrl = [URL]()
            
            for photos in selectedItems {
                
                switch photos {
                    
                case .photo(let photo):
                    
                    //selectedImages.append(photo)
                    selectedImages.append(photo.image)
                    
                case .video(let video):
                    
                    selectedImages.append(video.thumbnail)
                    selectedVideoUrl.append(video.url)
                    
//                    let assetURL = video.url
//                    let playerVC = AVPlayerViewController()
//                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
//                    playerVC.player = player
//                    
//                    picker.dismiss(animated: true, completion: { [weak self] in
//                        self?.present(playerVC, animated: true, completion: nil)
//                    })
                }
            }
            
            appDel.getAllCountries { (countries, msg) in
                
                if let countries = countries{
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewPostViewController") as? NewPostViewController {
                        
                        vc.selectedPhotos = selectedImages
                        vc.selectedVideosUrl = selectedVideoUrl
                        vc.countryList = countries
                        
                        picker.dismiss(animated: true, completion: {
                            if let top = appDel.topViewController{
                                
                                top.navigationController?.pushViewController(vc, animated: true)
                            }
                        })
                    }
                }
                else if let msg = msg{
                    self.displayMessage(msg)
                }
            }
        }
        
        present(picker, animated: true, completion: nil)
        
//            if let firstItem = items.first {
//                switch firstItem {
//                case .photo(let photo):
//                    //                    self.selectedImageV.image = photo.image
//                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewPostViewController") as? NewPostViewController{
//
//                        vc.selectedPhoto =
//
//
//                        //                self.present(vc, animated: true, completion: nil)
//
//                        //picker.navigationController?.pushViewController(vc, animated: true)
//                        picker.dismiss(animated: true, completion: {
//                            if let top = appDel.topViewController{
//
//                                top.navigationController?.pushViewController(vc, animated: true)
//
//                            }
//                        })
//                    }
//
//                case .video(let video):
//
////                    self.selectedImageV.image = video.thumbnail
//
//                    let assetURL = video.url
//                    let playerVC = AVPlayerViewController()
//                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
//                    playerVC.player = player
//
//                    picker.dismiss(animated: true, completion: { [weak self] in
//                        self?.present(playerVC, animated: true, completion: nil)
//                    })
//                }
//            }
//        }
        
    }
    
    // MARK:- call user Data api
    func getUserData(user_id:String, addWait:Bool, callback:@escaping (([UserModel]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["user_id":user_id]
        
        JSONRequest.makeRequest(kUserData, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let data = data["description"] as? [Any] {
                        
                        var userData = [UserModel]()
                        
                        if let des = data.first as? [String:Any] {
                            userData.append(UserModel(data: des))
                        }
                        callback(userData, nil)
                    }
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
 
    // MARK:- Follow Api
    func callFollowApi(his_id:String, isFollow:Int, callback:@escaping ((UserModel?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        let params = ["his_id":his_id,
                      "self_id":MyProfile.userId!,
                      "is_follow":"\(isFollow)"]
        
        JSONRequest.makeRequest(kFollow, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let data = data["description"] as? [Any] {
                        
                        if let des = data.first as? [String:Any] {
                            
                            callback(UserModel(data: des))
                        }
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "DismissFollowAlert"), object: nil)
                    }
                }else if let msg = data["message"] as? String{
                    let str = NSLocalizedString(msg, comment: msg)
                    self.displayMessage(str)
                    callback(nil)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                self.displayMessage(str)
                callback(nil)
            }
        }
    }
    
    // MARK:- call Block user api
    func callBlockApi(user_id:String, isBlock:Int, callback:@escaping ((String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        let params = ["his_id":user_id,
                      "self_id":MyProfile.userId!,
                      "is_block":"\(isBlock)"]
        
        JSONRequest.makeRequest(kBlock, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let msg = data["message"] as? String{
                        callback(msg)
                    }
                }else {
                    callback(nil)
                }
            }else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil)
            }
        }
    }
    
    // MARK:- call report User api
    func callReportUserApi(user_id:String, reason:String, message:String, callback:@escaping ((String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        let params = ["his_id":user_id,
                      "self_id":MyProfile.userId!,
                      "reason":reason,
                      "message":message]
        
        JSONRequest.makeRequest(kReportUser, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let msg = data["message"] as? String{
                        callback(msg)
                    }
                }else {
                    callback(nil)
                }
            }else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil)
            }
        }
    }
    
    // MARK:- call report Post api
    func callReportPostApi(postId:String, reason:String, message:String, callback:@escaping ((String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        let params = ["post_id":postId,
                      "self_id":MyProfile.userId!,
                      "reason":reason,
                      "message":message]
        
        JSONRequest.makeRequest(kReportPost, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let msg = data["message"] as? String{
                        callback(msg)
                    }
                }else {
                    callback(nil)
                }
            }else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil)
            }
        }
    }
    
    // MARK:- call makeBid api
    func callMakeBidApi(userId:String, postId:String, amount:String, callback:@escaping ((String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        let params = ["post_id":postId,
                      "user_id":userId,
                      "comment":"",
                      "bid_amount":amount,
                      "is_pinned":"\(false)"]
        
        JSONRequest.makeRequest(kRaiseMyBid, parameters: params ) { (data, error) in
            
            if let data = data as? [String:Any]{
                
                if let err = data["error"] as? Bool, !err{
                    
                    if let _ = data["description"] as? [Any] {
                        
                        let str = NSLocalizedString("Bid added successfully", comment: "Bid added successfully")
                        callback(str)
                    }
                    else{
                        let str = NSLocalizedString("Failed to add Bid. Please try again", comment: "Failed to add Bid. Please try again")
                        callback(str)
                    }
                }
                else if let msg = data["message"] as? String{
                    let str = NSLocalizedString(msg, comment: msg)
                    callback(str)
                }
            }
            else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(str)
            }
        }
    }
    
    //MARK:- call orderList api
    
    func getOrderList(user_id:String, addWait:Bool, callback:@escaping (([OrderListModel]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["user_id":user_id]
        
        JSONRequest.makeRequest(kOrderList, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let data = data["description"] as? [[String:Any]] {
                        
                        var orderList = [OrderListModel]()
                        
                        for order in data {
                            orderList.append(OrderListModel(data: order))
                        }
                        
                        orderList.sort(by: { (order1, order2) -> Bool in
                            autoreleasepool {
                                let date1 = Date(timeIntervalSince1970: Double(order1.created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(order2.created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            }
                        })
                        
                        callback(orderList, nil)
                    }
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
    
    //MARK:- call activities api
    
    func getActivityList(user_id:String, addWait:Bool, callback:@escaping (([ActivityModel]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["user_id":user_id]
        
        JSONRequest.makeRequest(kActivities, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let data = data["description"] as? [[String:Any]] {
                        
                        var activityList = [ActivityModel]()
                        
                        for order in data {
                            activityList.append(ActivityModel(data: order))
                        }
                        
                        //filtering activity list
                        
                        activityList = activityList.filter({ (activity) -> Bool in
                            return activity.typeData.count != 0
                        })
                        
                        // sorting of activityList
                        
                        activityList.sort(by: { (activity1, activity2) -> Bool in
                            autoreleasepool {
                                let date1 = Date(timeIntervalSince1970: Double(activity1.created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(activity2.created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            }
                        })
                        
                        callback(activityList, nil)
                    }
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
    
    //MARK:- call post bid details api for buyer bid
    
    func getPostBidDetails(user_id:String, type:String, addWait:Bool, callback:@escaping (([BuyerSellerBids]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["user_id":user_id,
                      "type":type]
        
        JSONRequest.makeRequest(kPostBidDetails, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    var buyerBidDetails = [BuyerSellerBids]()
                    
                    if let posts = data["post"] as? [[String:Any]] {
                        for post in posts {
                            buyerBidDetails.append(BuyerSellerBids(type: .post, item: BuyerSellerModel(data: post)))
                        }
                    }
                    
                    if let stories = data["story"] as? [[String:Any]] {
                        for story in stories {
                            buyerBidDetails.append(BuyerSellerBids(type: .story, item: StoryModel(data: story)))
                        }
                    }
                    callback(buyerBidDetails, nil)
                    
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
    
    //MARK:- call pendingFeedbackOrder api
    
    func getPendingFeedbackOrder(user_id:String, addWait:Bool, callback:@escaping (([PendingFeedbackModel]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["user_id":user_id]
        
        JSONRequest.makeRequest(kPendingFeedbackOrder, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let data = data["description"] as? [[String:Any]] {
                        
                        var pendingFeedback = [PendingFeedbackModel]()
                        
                        for order in data {
                            pendingFeedback.append(PendingFeedbackModel(data: order))
                        }
                        
                        pendingFeedback.sort(by: { (Feedback1, Feedback2) -> Bool in
                            autoreleasepool {
                                let date1 = Date(timeIntervalSince1970: Double(Feedback1.created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(Feedback2.created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            }
                        })
                        callback(pendingFeedback, nil)
                    }
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
    
    //MARK:- call walletList api
    
    func getWalletList(type:String, user_id:String, addWait:Bool, callback:@escaping (([WalletPayementModel]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["type_id":user_id,
                      "type":type]
        
        JSONRequest.makeRequest(kWalletList, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let list = data["description"] as? [[String:Any]] {
                        
                        var walletList = [WalletPayementModel]()
                        
                        for ls in list {
                            walletList.append(WalletPayementModel(data: ls))
                        }
                        
                        walletList.sort(by: { (wallet1, wallet2) -> Bool in
                            autoreleasepool {
                                let date1 = Date(timeIntervalSince1970: Double(wallet1.created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(wallet2.created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            }
                        })
                        
                        callback(walletList, nil)
                    }
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
    
    //MARK:- call Feedback Received api
    
    func getFeedbackForSeller(user_id:String, addWait:Bool, callback:@escaping (([SellerFeedback]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["user_id":user_id]
                      
        JSONRequest.makeRequest(kFeedbackReceived, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                   var feedbackArray = [SellerFeedback]()
                    
                    if let posts = data["posts"] as? [[String:Any]] {
                        for post in posts {
                            feedbackArray.append(SellerFeedback(type: .post, item: BuyerSellerModel(data: post)))
                        }
                    }
                    if let stories = data["stories"] as? [[String:Any]] {
                        for story in stories {
                            feedbackArray.append(SellerFeedback(type: .story, item: StoryModel(data: story)))
                        }
                    }
                    
                    for item in feedbackArray {
                        autoreleasepool {
                            if item.type == .post {
                                
                                (item.item as! BuyerSellerModel).feedback.sort(by: { (feedback1, feedback2) -> Bool in
                                    
                                    let date1 = Date(timeIntervalSince1970: Double(feedback1.created_at!)!/1000)
                                    let date2 = Date(timeIntervalSince1970: Double(feedback2.created_at!)!/1000)
                                    
                                    return date1.compare(date2) == .orderedDescending
                                })
                                
                            }else {
                                (item.item as! StoryModel).feedback.sort(by: { (feedback1, feedback2) -> Bool in
                                    
                                    let date1 = Date(timeIntervalSince1970: Double(feedback1.created_at!)!/1000)
                                    let date2 = Date(timeIntervalSince1970: Double(feedback2.created_at!)!/1000)
                                    
                                    return date1.compare(date2) == .orderedDescending
                                })
                            }
                        }
                    }
                    
                    callback(feedbackArray, nil)
                    
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
    
    //MARK:- call sellerOrder api
    
    func getSellerOrderList(user_id:String, addWait:Bool, callback:@escaping (([OrderListModel]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["user_id":user_id]
        
        JSONRequest.makeRequest(kSellerOrder, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let data = data["description"] as? [[String:Any]] {
                        
                        var orderList = [OrderListModel]()
                        
                        for order in data {
                            orderList.append(OrderListModel(data: order))
                        }
                        
                        orderList.sort(by: { (order1, order2) -> Bool in
                            autoreleasepool {
                                let date1 = Date(timeIntervalSince1970: Double(order1.created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(order2.created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            }
                        })
                        
                        callback(orderList, nil)
                    }
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
    
    //MARk: Tag list
    func getTagList(callback:@escaping (([TagModel])->())) {
        
        let params = ["type":"read",
                      "version":VERSION]
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kTags, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let description = data["description"] as? [[String:Any]] {
                        
                        var tags = [TagModel]()
                        
                        for des in description {
                            tags.append(TagModel(data: des))
                        }
                        
                        tags = tags.filter({!$0.isDeleted})
                        
                        callback(tags)
                    }
                }else if let msg = data["message"] as? String{
                    let str = NSLocalizedString(msg, comment: msg)
                    self.displayMessage(str)
                }
            }else {
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                self.displayMessage(str)
            }
        }
    }
    
    //MARK:- Call blockList api
    func callBlockListApi(showWait:Bool, callback:@escaping (([UserModel]?)->())) {
        
        var wait:WaitView?
        
        if showWait {
            wait = appDel.topViewController?.addWaitSpinner()
        }
        
        if let userId = MyProfile.userId {
            
            let params = ["user_id":userId]
            
            JSONRequest.makeRequest(kBlockList, parameters: params) { (data, error) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data as? [String:Any] {
                    
                    if let err = data["error"] as? Bool, !err {
                        
                        if let description = data["description"] as? [[String:Any]] {
                            
                            var userData = [UserModel]()
                            
                            for des in description {
                                userData.append(UserModel(data: des))
                            }
                            callback(userData)
                        }
                    }
                }
            }
        }
    }
    
    // MARK:- call report story api
    func callReportStoryApi(storyId:String, reason:String, message:String, callback:@escaping ((String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        let params = ["story_id":storyId,
                      "self_id":MyProfile.userId!,
                      "reason":reason,
                      "message":message]
        
        JSONRequest.makeRequest(kReportStory, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let msg = data["message"] as? String{
                        callback(msg)
                    }
                }else {
                    callback(nil)
                }
            }else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil)
            }
        }
    }
    
    // MARK:- call FollowerList api
    func getFollowerList(user_id:String, addWait:Bool, callback:@escaping (([UserModel]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            callback(nil, nil)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["user_id":user_id]
        
        JSONRequest.makeRequest(kFollowerList, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let data = data["description"] as? [[String:Any]] {
                        
                        var userData = [UserModel]()
                        
                        for des in data {
                            userData.append(UserModel(data: des))
                        }
                        callback(userData, nil)
                    }
                    else{
                        callback(nil, nil)
                    }
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
    
    // MARK:- call FollowerList api
    func getFollowingList(user_id:String, addWait:Bool, callback:@escaping (([UserModel]?, String?)->())) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            callback(nil, nil)
            return;
        }
        
        guard let top = appDel.topViewController else {
            callback(nil, nil)
            return
        }
        
        var wait:WaitView!
        
        if addWait{
            wait = top.addWaitSpinner()
        }
        
        let params = ["user_id":user_id]
        
        JSONRequest.makeRequest(kFollowingList, parameters: params) { (data, error) in
            
            if addWait{
                top.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]  {
                
                if let err = data["error"] as? Bool, !err {
                    
                    if let data = data["description"] as? [[String:Any]] {
                        
                        var userData = [UserModel]()
                        
                        for des in data {
                            userData.append(UserModel(data: des))
                        }
                        callback(userData, nil)
                    }
                    else{
                        callback(nil, nil)
                    }
                }else if let msg = data["message"] as? String{
                    callback(nil, msg)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                callback(nil,str)
            }
        }
    }
}

extension TabBarViewController{
    
    func hideTabBar(){
        
        self.view.sendSubview(toBack: self.tabBarView)
        
        UIView.animate(withDuration: 0.25) {
            self.containerVIewBottomConstraint.constant = 0
        }
        
    }
    func showTabBar(){
        
        self.view.bringSubview(toFront: self.tabBarView)
        
        UIView.animate(withDuration: 0.25) {
            self.containerVIewBottomConstraint.constant = 56.0
        }
    }
    
    //MARK: add bottom view
    func setupBottomView(){
        
        let bottomMainView = UIView()
        bottomMainView.clipsToBounds = true
        bottomMainView.backgroundColor = .darkGray
        bottomMainView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(bottomMainView)
        bottomMainView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0).isActive = true
        bottomMainView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0).isActive = true
        //bottomMainView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0).isActive = true
        if #available(iOS 11, *) {
            let guide = view.safeAreaLayoutGuide
            bottomMainView.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: 0.0).isActive = true
        }else{
            bottomMainView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0).isActive = true
        }
        bottomMainView.heightAnchor.constraint(equalToConstant: 56.0).isActive = true
        
        let view1 = UIView()
        view1.backgroundColor = .white
        view1.translatesAutoresizingMaskIntoConstraints = false
        
        let view2 = UIView()
        view2.backgroundColor = .white
        view2.translatesAutoresizingMaskIntoConstraints = false
        
        let view3 = UIView()
        view3.backgroundColor = .white
        view3.translatesAutoresizingMaskIntoConstraints = false
        
        let view4 = UIView()
        view4.backgroundColor = .white
        view4.translatesAutoresizingMaskIntoConstraints = false
        
        let view5 = UIView()
        view5.backgroundColor = .white
        view5.translatesAutoresizingMaskIntoConstraints = false
        
        bottomMainView.addSubview(view1)
        bottomMainView.addSubview(view2)
        bottomMainView.addSubview(view3)
        bottomMainView.addSubview(view4)
        bottomMainView.addSubview(view5)
        
        view1.leadingAnchor.constraint(equalTo: bottomMainView.leadingAnchor, constant: 0.0).isActive = true
        view1.topAnchor.constraint(equalTo: bottomMainView.topAnchor, constant: 0.0).isActive = true
        view1.bottomAnchor.constraint(equalTo: bottomMainView.bottomAnchor, constant: 0.0).isActive = true
        view1.widthAnchor.constraint(equalToConstant: view.frame.width*0.20).isActive = true
        
        view2.leadingAnchor.constraint(equalTo: view1.trailingAnchor, constant: 0.0).isActive = true
        view2.topAnchor.constraint(equalTo: bottomMainView.topAnchor, constant: 0.0).isActive = true
        view2.bottomAnchor.constraint(equalTo: bottomMainView.bottomAnchor, constant: 0.0).isActive = true
        view2.widthAnchor.constraint(equalToConstant: view.frame.width*0.20).isActive = true
        
        view3.leadingAnchor.constraint(equalTo: view2.trailingAnchor, constant: 0.0).isActive = true
        view3.topAnchor.constraint(equalTo: bottomMainView.topAnchor, constant: 0.0).isActive = true
        view3.bottomAnchor.constraint(equalTo: bottomMainView.bottomAnchor, constant: 0.0).isActive = true
        view3.widthAnchor.constraint(equalToConstant: view.frame.width*0.20).isActive = true
        
        view4.leadingAnchor.constraint(equalTo: view3.trailingAnchor, constant: 0.0).isActive = true
        view4.topAnchor.constraint(equalTo: bottomMainView.topAnchor, constant: 0.0).isActive = true
        view4.bottomAnchor.constraint(equalTo: bottomMainView.bottomAnchor, constant: 0.0).isActive = true
        view4.widthAnchor.constraint(equalToConstant: view.frame.width*0.20).isActive = true
        
        view5.leadingAnchor.constraint(equalTo: view4.trailingAnchor, constant: 0.0).isActive = true
        view5.topAnchor.constraint(equalTo: bottomMainView.topAnchor, constant: 0.0).isActive = true
        view5.bottomAnchor.constraint(equalTo: bottomMainView.bottomAnchor, constant: 0.0).isActive = true
        view5.widthAnchor.constraint(equalToConstant: view.frame.width*0.20).isActive = true
        
        
        let imageSize = self.view.frame.width*0.036
        let lblCenterY = CGFloat(1.4)
        let fontSize = UIFont.systemFont(ofSize: 10.0)
        let centerYMultiplier = CGFloat(0.75)
        
        let btn1 = UIButton()
        btn1.addTarget(self, action: #selector(onHomeClick(_:)), for: .touchUpInside)
        btn1.translatesAutoresizingMaskIntoConstraints = false
        
        let btn2 = UIButton()
        btn2.addTarget(self, action: #selector(onSearchClick(_:)), for: .touchUpInside)
        btn2.translatesAutoresizingMaskIntoConstraints = false
        
        let btn3 = UIButton()
        btn3.addTarget(self, action: #selector(onOrdersClick(_:)), for: .touchUpInside)
        btn3.translatesAutoresizingMaskIntoConstraints = false
        
        let btn4 = UIButton()
        btn4.addTarget(self, action: #selector(onNotificationClick(_:)), for: .touchUpInside)
        btn4.translatesAutoresizingMaskIntoConstraints = false
        
        let btn5 = UIButton()
        btn5.addTarget(self, action: #selector(onProfileClick(_:)), for: .touchUpInside)
        btn5.translatesAutoresizingMaskIntoConstraints = false
        
        
        img1.image = #imageLiteral(resourceName: "icon_home")
        img1.tintColor = UIColor.purpleColor
        img1.translatesAutoresizingMaskIntoConstraints = false
        
        
        img2.image = #imageLiteral(resourceName: "icon_magnifier")
        img2.tintColor = UIColor.darkGray
        img2.translatesAutoresizingMaskIntoConstraints = false
        
        
        img3.image = #imageLiteral(resourceName: "icon_123")
        img3.tintColor = UIColor.darkGray
        img3.translatesAutoresizingMaskIntoConstraints = false
        
        
        img4.image = #imageLiteral(resourceName: "icon_bell")
        img4.tintColor = UIColor.darkGray
        img4.translatesAutoresizingMaskIntoConstraints = false
        
        
        img5.image = #imageLiteral(resourceName: "icon_profile_grey")
        img5.tintColor = UIColor.darkGray
        img5.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl1.text = NSLocalizedString("Home", comment: "Home")
        lbl1.font = fontSize
        lbl1.textColor = .purpleColor
        lbl1.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl2.text = NSLocalizedString("Search", comment: "Search")
        lbl2.font = fontSize
        lbl2.textColor = .imageGrayColor
        lbl2.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl3.text = NSLocalizedString("Orders", comment: "Orders")
        lbl3.font = fontSize
        lbl3.textColor = .imageGrayColor
        lbl3.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl4.text = NSLocalizedString("Notifications", comment: "Notifications")
        lbl4.font = fontSize
        lbl4.textColor = .imageGrayColor
        lbl4.translatesAutoresizingMaskIntoConstraints = false
        
        
        lbl5.text = NSLocalizedString("Profile", comment: "Profile")
        lbl5.font = fontSize
        lbl5.textColor = .imageGrayColor
        lbl5.translatesAutoresizingMaskIntoConstraints = false
        
        
        view1.addSubview(btn1)
        view2.addSubview(btn2)
        view3.addSubview(btn3)
        view4.addSubview(btn4)
        view5.addSubview(btn5)
        
        view1.addSubview(img1)
        view2.addSubview(img2)
        view3.addSubview(img3)
        view4.addSubview(img4)
        view5.addSubview(img5)
        
        view1.addSubview(lbl1)
        view2.addSubview(lbl2)
        view3.addSubview(lbl3)
        view4.addSubview(lbl4)
        view5.addSubview(lbl5)
        
        
        btn1.leadingAnchor.constraint(equalTo: view1.leadingAnchor, constant: 0.0).isActive = true
        btn1.trailingAnchor.constraint(equalTo: view1.trailingAnchor, constant: 0.0).isActive = true
        btn1.topAnchor.constraint(equalTo: view1.topAnchor, constant: 0.0).isActive = true
        btn1.bottomAnchor.constraint(equalTo: view1.bottomAnchor, constant: 0.0).isActive = true
        
        btn2.leadingAnchor.constraint(equalTo: view2.leadingAnchor, constant: 0.0).isActive = true
        btn2.trailingAnchor.constraint(equalTo: view2.trailingAnchor, constant: 0.0).isActive = true
        btn2.topAnchor.constraint(equalTo: view2.topAnchor, constant: 0.0).isActive = true
        btn2.bottomAnchor.constraint(equalTo: view2.bottomAnchor, constant: 0.0).isActive = true
        
        btn3.leadingAnchor.constraint(equalTo: view3.leadingAnchor, constant: 0.0).isActive = true
        btn3.trailingAnchor.constraint(equalTo: view3.trailingAnchor, constant: 0.0).isActive = true
        btn3.topAnchor.constraint(equalTo: view3.topAnchor, constant: 0.0).isActive = true
        btn3.bottomAnchor.constraint(equalTo: view3.bottomAnchor, constant: 0.0).isActive = true
        
        btn4.leadingAnchor.constraint(equalTo: view4.leadingAnchor, constant: 0.0).isActive = true
        btn4.trailingAnchor.constraint(equalTo: view4.trailingAnchor, constant: 0.0).isActive = true
        btn4.topAnchor.constraint(equalTo: view4.topAnchor, constant: 0.0).isActive = true
        btn4.bottomAnchor.constraint(equalTo: view4.bottomAnchor, constant: 0.0).isActive = true
        
        btn5.leadingAnchor.constraint(equalTo: view5.leadingAnchor, constant: 0.0).isActive = true
        btn5.trailingAnchor.constraint(equalTo: view5.trailingAnchor, constant: 0.0).isActive = true
        btn5.topAnchor.constraint(equalTo: view5.topAnchor, constant: 0.0).isActive = true
        btn5.bottomAnchor.constraint(equalTo: view5.bottomAnchor, constant: 0.0).isActive = true
        
        let centerX1 = NSLayoutConstraint(item: img1, attribute: .centerX, relatedBy: .equal, toItem: view1, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let centerY1 = NSLayoutConstraint(item: img1, attribute: .centerY, relatedBy: .equal, toItem: view1, attribute: .centerY, multiplier: centerYMultiplier, constant: 0.0)
        img1.widthAnchor.constraint(equalToConstant: imageSize).isActive = true
        img1.heightAnchor.constraint(equalToConstant: imageSize).isActive = true
        
        let centerX2 = NSLayoutConstraint(item: img2, attribute: .centerX, relatedBy: .equal, toItem: view2, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let centerY2 = NSLayoutConstraint(item: img2, attribute: .centerY, relatedBy: .equal, toItem: view2, attribute: .centerY, multiplier: centerYMultiplier, constant: 0.0)
        img2.widthAnchor.constraint(equalToConstant: imageSize).isActive = true
        img2.heightAnchor.constraint(equalToConstant: imageSize).isActive = true
        
        let centerX3 = NSLayoutConstraint(item: img3, attribute: .centerX, relatedBy: .equal, toItem: view3, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let centerY3 = NSLayoutConstraint(item: img3, attribute: .centerY, relatedBy: .equal, toItem: view3, attribute: .centerY, multiplier: centerYMultiplier, constant: 0.0)
        img3.widthAnchor.constraint(equalToConstant: imageSize*1.35).isActive = true
        img3.heightAnchor.constraint(equalToConstant: imageSize*1.35).isActive = true
        
        let centerX4 = NSLayoutConstraint(item: img4, attribute: .centerX, relatedBy: .equal, toItem: view4, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let centerY4 = NSLayoutConstraint(item: img4, attribute: .centerY, relatedBy: .equal, toItem: view4, attribute: .centerY, multiplier: centerYMultiplier, constant: 0.0)
        img4.widthAnchor.constraint(equalToConstant: imageSize).isActive = true
        img4.heightAnchor.constraint(equalToConstant: imageSize).isActive = true
        
        let centerX5 = NSLayoutConstraint(item: img5, attribute: .centerX, relatedBy: .equal, toItem: view5, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let centerY5 = NSLayoutConstraint(item: img5, attribute: .centerY, relatedBy: .equal, toItem: view5, attribute: .centerY, multiplier: centerYMultiplier, constant: 0.0)
        img5.widthAnchor.constraint(equalToConstant: imageSize).isActive = true
        img5.heightAnchor.constraint(equalToConstant: imageSize).isActive = true
        
        let lblCenterY1 = NSLayoutConstraint(item: lbl1, attribute: .centerY, relatedBy: .equal, toItem: view1, attribute: .centerY, multiplier: lblCenterY, constant: 0.0)
        lbl1.centerXAnchor.constraint(equalTo: img1.centerXAnchor, constant: 0.0).isActive = true
        
        let lblCenterY2 = NSLayoutConstraint(item: lbl2, attribute: .centerY, relatedBy: .equal, toItem: view2, attribute: .centerY, multiplier: lblCenterY, constant: 0.0)
        lbl2.centerXAnchor.constraint(equalTo: img2.centerXAnchor, constant: 0.0).isActive = true
        
        let lblCenterY3 = NSLayoutConstraint(item: lbl3, attribute: .centerY, relatedBy: .equal, toItem: view3, attribute: .centerY, multiplier: lblCenterY, constant: 0.0)
        lbl3.centerXAnchor.constraint(equalTo: img3.centerXAnchor, constant: 0.0).isActive = true
        
        let lblCenterY4 = NSLayoutConstraint(item: lbl4, attribute: .centerY, relatedBy: .equal, toItem: view4, attribute: .centerY, multiplier: lblCenterY, constant: 0.0)
        lbl4.centerXAnchor.constraint(equalTo: img4.centerXAnchor, constant: 0.0).isActive = true
        
        let lblCenterY5 = NSLayoutConstraint(item: lbl5, attribute: .centerY, relatedBy: .equal, toItem: view5, attribute: .centerY, multiplier: lblCenterY, constant: 0.0)
        lbl5.centerXAnchor.constraint(equalTo: img5.centerXAnchor, constant: 0.0).isActive = true
        
        
        NSLayoutConstraint.activate([centerX1,centerY1, centerX2,centerY2, centerX3,centerY3, centerX4,centerY4, centerX5,centerY5,
                                     lblCenterY1, lblCenterY2, lblCenterY3, lblCenterY4, lblCenterY5])
        
        bottomMainView.addShadow(shadowColor: .gray, size: CGSize(width: 0, height: -10))
        
        self.tabBarView = bottomMainView
        
    }
}
