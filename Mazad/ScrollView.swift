//
//  ScrollView.swift
//  Mazad Live
//
//  Created by Mac-3 on 13/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//


import UIKit

open class ScrollView: UIScrollView, UIScrollViewDelegate {
    
    @IBOutlet weak var touchDelegate: UIViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.delegate = self
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        touchDelegate?.touchesBegan(touches, with: event)
    }
    
    open func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool){
        touchDelegate?.view.endEditing(true)
    }
}
