//
//  TagSelectViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 03/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class TagSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var tagList = [TagModel]()
    
    var previousController:NewPostViewController!
    var selectedTags = [TagModel]()
    
    var ROWHEIGHT:CGFloat = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.btnContinue.addShadow()
        self.btnCancel.addShadow()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        let height = self.tableView.frame.size.height
//
//        self.ROWHEIGHT = CGFloat (Int(height) / self.tagList.count)
//
//        if self.ROWHEIGHT < 50 {
//            self.ROWHEIGHT = 50.0
//        }
        
//        var newHeight = CGFloat(self.tagList.count) * ROWHEIGHT
//        
//        if newHeight >= height{
//            newHeight = height - ROWHEIGHT
//            tableView.isScrollEnabled = true
//        }
//        else{
//            tableView.isScrollEnabled = false
//        }
        
//        UIView.animate(withDuration: 0.25) {
//            self.tableViewHeight.constant = newHeight
//        }
        
        return tagList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROWHEIGHT
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TagSelectCell
   
        let tag = self.tagList[indexPath.row]

        cell.lblName.text = tag.name
        cell.selectionStyle = .none

        if let _ = self.tempTags.firstIndex(where: { (t) -> Bool in
            return t._id == tag._id
        }){
            cell.btnSelect.setImage(#imageLiteral(resourceName: "onRadioButton"), for: .normal)
        }
        else{
            cell.btnSelect.setImage(#imageLiteral(resourceName: "offRadioButton"), for: .normal)
        }
        
        return cell
    }
    
    var tempTags = [TagModel]()
    
    func addTag(tag:TagModel) {
        
        if let index = self.tempTags.firstIndex(where: { (t) -> Bool in
            return t._id == tag._id
        }){
            self.tempTags.remove(at: index)
        }
        else {
            self.tempTags.append(tag)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! TagSelectCell
    
        if cell.btnSelect.currentImage == UIImage(named: "onRadioButton") {
            cell.btnSelect.setImage(#imageLiteral(resourceName: "offRadioButton"), for: .normal)
            //self.previousController.addTag(tag: self.tagList[indexPath.row])
            self.addTag(tag: self.tagList[indexPath.row])
        }else {
            cell.btnSelect.setImage(#imageLiteral(resourceName: "onRadioButton"), for: .normal)
            self.addTag(tag: self.tagList[indexPath.row])
            //self.previousController.addTag(tag: self.tagList[indexPath.row])
        }
    }
    
    @IBAction func onContinueClick(_ sender: Any) {
        
        self.previousController.selectedTags = self.tempTags
        
        self.dismiss(animated: true) {
            
            if self.previousController.selectedTags.count > 0 {
                self.previousController.lblTags.font = UIFont.boldSystemFont(ofSize: 13)
            }else {
                self.previousController.lblTags.font = UIFont.systemFont(ofSize: 13, weight: .thin)
                self.previousController.lblTags.textColor = UIColor(red:0.47, green:0.47, blue:0.47, alpha:1.0)
            }
        }
    }
    
    @IBAction func onCancelClick(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
            if self.previousController.selectedTags.count > 0 {
                self.previousController.lblTags.font = UIFont.boldSystemFont(ofSize: 13)
            }else {
                self.previousController.lblTags.font = UIFont.systemFont(ofSize: 13, weight: .thin)
                self.previousController.lblTags.textColor = UIColor(red:0.47, green:0.47, blue:0.47, alpha:1.0)
            }
        }
    }
}

class TagSelectCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    
    override func prepareForReuse() {
        self.btnSelect.setImage(#imageLiteral(resourceName: "offRadioButton.png"), for: .normal)
    }
}
