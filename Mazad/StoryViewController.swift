

import UIKit
import Pages
import YPImagePicker
import AVFoundation
import AVKit
import Photos
import Pages

class StoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnCreateStory: UIButton!
    @IBOutlet weak var createStoryView: UIView!
    
    @IBOutlet weak var btnMenuOption: UIButton!
    
    var myStories = [StoryModel]()
    
    var emptyText = NSLocalizedString("loading..", comment: "loading..")
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let str = NSLocalizedString("Pull to refresh", comment: "Pull to refresh")
        let attrStr = NSAttributedString(string: str)
        
        refreshControl = getRefreshControl(attrStr: attrStr)
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.separatorStyle = .none
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification(_:)), name: NSNotification.Name("changeUser"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleReloadNot), name: NSNotification.Name("ReloadStoryViewController"), object: nil)
    }
    
    
    @objc func handleNotification(_ sender: NSNotification){
        
        if let _ = sender.userInfo?["user"] as? Bool{
            
            if homeVC?.carbonController.currentTabIndex == 0 {
                self.getMyStories()
            }
        }
        
        if MyProfile.isSeller {
            self.createStoryView.isHidden = false
        }else {
            self.createStoryView.isHidden = true
        }
    }
    
    @objc func handleReloadNot() {
        self.getMyStories()
    }
    
    @objc func pullToRefresh(){
        
        self.refreshControl.endRefreshing()
        
        self.getMyStories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getMyStories()
        
        if MyProfile.isSeller {
            self.createStoryView.isHidden = false
        }else {
            self.createStoryView.isHidden = true
        }
        
        if let image = MyProfile.userProfilePhoto{
            self.btnCreateStory.sd_setImage(with: URL(string: image), for: .normal, completed: nil)
        }
    }
    
    @IBAction func onAddStoryClick(_ sender: Any) {
        
        guard let userId = MyProfile.userId else {
            return;
        }
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kShowCounts, parameters: ["user_id":userId], callback: {  (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    if let des = data["description"] as? [String:Any] {
                        if let storyDict = des["unlimited_story_count"] as? [String:Any] {
                            let storyObj = CountModel(data: storyDict)
                            if let status = storyObj.status {
                                if status {
                                    self.showPicker()
                                }else{
                                    if storyObj.count == 0 {
                                        let str = NSLocalizedString("You are out of stories. To upload more stories, please upgrade", comment: "You are out of stories. To upload more stories, please upgrade")
                                        self.displayMessage(str, callback: {  () -> (Void) in
                                            if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpgradeViewController") as? UpgradeViewController {
                                                self.navigationController?.pushViewController(vc, animated: true)
                                            }
                                        })
                                    }else{
                                        self.showPicker()
                                    }
                                }
                            }
                        }
                    }
                }else{
                    let str = NSLocalizedString("Failed to get response, Please try again later", comment: "Failed to get response, Please try again later")
                    self.displayMessage(str)
                }
            }else{
                let str = NSLocalizedString("Failed to get response, Please try again later", comment: "Failed to get response, Please try again later")
                self.displayMessage(str)
            }
        })
    }
    
    //MARK:-
    func getMyStories() {
        
//        guard let userId = MyProfile.userId else {
//            return
//        }
        
        var params = [String:String]()
        
        params["user_id"] = MyProfile.userId ?? ""
        
        if MyProfile.isSeller{
            params["type"] = "seller"
        }else {
            params["type"] = "buyer"
        }
        
        self.tableView.addWaitView()
        
        JSONRequest.makeRequest(kGetMyStories, parameters: params) {  (data, error) in
            
            self.emptyText = NSLocalizedString("No Stories !!", comment: "No Stories !!")
            
            self.tableView.removeWaitView()
            
            if let data = data as? [String:Any] {
                
                if let err = data["error"] as? Bool, !err{
                    
                    self.myStories.removeAll()
                    
                    if let des = data["description"] as? [[String:Any]] {
                        
                        for data in des {
                            self.myStories.append(StoryModel(data: data))
                        }

                        self.myStories = self.myStories.filter({ (story) -> Bool in
                            autoreleasepool {
                                if let status = story.status?.lowercased(){
                                    return status == "running"
                                }
                                return false
                            }
                        })
                        
                        self.myStories.sort(by: { (story1, story2) -> Bool in
                            autoreleasepool {
                                let date1 = Date(timeIntervalSince1970: Double(story1.created_at!)!/1000)
                                let date2 = Date(timeIntervalSince1970: Double(story2.created_at!)!/1000)
                                return date1.compare(date2) == .orderedDescending
                            }
                        })
                    }
                    self.tableView.reloadData()
                    
                    if storyIdFromDynamicLink != ""{
                        let storyId = storyIdFromDynamicLink
                        storyIdFromDynamicLink = ""
                        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                            if let index = self.myStories.firstIndex(where: { (story) -> Bool in
                                return story._id == storyId
                            }){
                                self.tableView(self.tableView, didSelectRowAt: IndexPath(row: index, section: 0))
                            }
                        })
                    }
                }
                else if let msg = data["message"] as? String{
                    let _ = NSLocalizedString(msg, comment: msg)
                }
            }
            else{
                let _ = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myStories.count > 0 ? myStories.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.myStories.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = self.emptyText
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StoryCell
        
        cell.index = indexPath
        cell.story = self.myStories[indexPath.row]
        
        cell.setupUI()
        cell.mainContainerView.addShadow()
        
        return cell
    }
    
    var pages:PagesController?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
//        DispatchQueue.global(qos: .background).async {
        
            var temp = [StoryModel]()
            
            for story in self.myStories{
                temp.append(story)
            }
            
            for _ in 0..<indexPath.row {
                temp.remove(at: 0)
            }
            
            var arr = [StoryImageViewerViewController]()
            
            for i in 0..<temp.count {
                autoreleasepool{
                    if let vc = UIStoryboard(name: "Story", bundle:nil).instantiateViewController(withIdentifier: "StoryImageViewerViewController") as? StoryImageViewerViewController{
                        vc.story = temp[i]
                        arr.append(vc)
                    }
                }
            }
            
            self.pages = PagesController(arr)
            
            for item in arr{
                self.pages?.addChildViewController(item)
                item.didMove(toParentViewController: self.pages!)
            }
            
            let count = arr.count-1
            var index = 0
            
            for item in arr {
                autoreleasepool{
                    item.pages = self.pages
                    item.storiesVC = self
                    if index < count{
                        item.hasNext = true
                    }else {
                        item.hasNext = false
                    }
                    index = index + 1
                }
            }
            
            self.pages?.showPageControl = false
            self.pages?.showBottomLine = false
            
            self.pages?.modalPresentationStyle = .overCurrentContext
            self.pages?.modalTransitionStyle = .crossDissolve
            self.pages?.pagesDelegate = self
            
            mainQueue.async {
                if let wait = wait{
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                self.present(self.pages!, animated: true, completion: {
                })
            }
//        }
    }
}
extension StoryViewController:PagesControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, setViewController viewController: UIViewController, atPage page: Int) {
        print("Page Number \(page)")
    }

    func showPicker() {
        
        var config = YPImagePickerConfiguration()
        config.library.mediaType = .photoAndVideo
        config.shouldSaveNewPicturesToAlbum = false
        config.video.compression = AVAssetExportPresetMediumQuality
        config.startOnScreen = .library
        config.screens = [.library, .photo, .video]
        config.video.recordingTimeLimit = 120.0
        config.video.libraryTimeLimit = 120.0
        config.video.trimmerMaxDuration = 120.0
        config.showsCrop = .rectangle(ratio: (16/9))
        config.wordings.libraryTitle = "Select Media"
        config.wordings.save = "Next"
        config.hidesStatusBar = false
        config.library.maxNumberOfItems = 1
        config.library.minNumberOfItems = 1
        config.library.skipSelectionsGallery = true
        config.showsFilters = false
        config.isScrollToChangeModesEnabled = false
        config.showsCrop = YPCropType.none
        
        let picker = YPImagePicker(configuration: config)
        
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            _ = items.map { print("🧀 \($0)") }
            
            let selectedItems = items
            var selectedImage:UIImage?
            var selectedVideoUrl:URL?
            
            for photos in selectedItems {
                
                switch photos {
                    
                case .photo(let photo):
                    
                    selectedImage = photo.image
                    
                case .video(let video):
                    
                    selectedImage = video.thumbnail
                    selectedVideoUrl = video.url
                    
//                    let assetURL = video.url
//                    let playerVC = AVPlayerViewController()
//                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
//                    playerVC.player = player
//
//                    picker.dismiss(animated: true, completion: { [weak self] in
//                        self?.present(playerVC, animated: true, completion: nil)
//                    })
                }
            }
            
            if let vc = UIStoryboard(name:"Story", bundle:nil).instantiateViewController(withIdentifier: "CreateStoryViewController") as? CreateStoryViewController{
                
                vc.selectedPhoto = selectedImage
                if let url = selectedVideoUrl {
                    vc.selectedVideoUrl = url
                }
                picker.dismiss(animated: true, completion: {
                    if let top = appDel.topViewController{
                        top.navigationController?.pushViewController(vc, animated: true)
                    }
                })
            }
        }
        present(picker, animated: true, completion: nil)
    }

}
