//
//  FeebackViewController.swift
//  Mazad Live
//
//  Created by alienbrainz on 29/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import AVFoundation
import ImageViewer

class CreateFeedbackViewController: UIViewController, UITextViewDelegate, GalleryItemsDataSource, GalleryItemsDelegate {
    
    @IBOutlet weak var ratingButton: RatingButtons!
    @IBOutlet weak var txtComment: UITextView!
    
    @IBOutlet weak var mainImageView: UIView!
    @IBOutlet weak var imgPostImage: UIImageView!
    @IBOutlet weak var lblOrderDetails: UILabel!
    @IBOutlet weak var btnPlayVideo: UIButton!
    
    @IBOutlet weak var lblTextViewCount:UILabel!
    
    var orderData:PendingFeedbackModel!
    
    var galleryController:GalleryViewController?
    
    var dataImage: [GalleryItem] = [GalleryItem]()
    var postImageUrls = [String]()
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        self.txtComment.delegate = self
        
        self.title = NSLocalizedString("Feedback", comment: "Feedback")
        
        //self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.handlePop))
        
        self.mainImageView.addShadow()
        self.txtComment.addShadow()
        
        //check media is video or not
        if let typeData = self.orderData.post_data.first {
            if let postImages = typeData.post_images.first {
                if let isVideo = postImages.is_video, isVideo {
                    self.imgPostImage.gestureRecognizers = nil
                    self.btnPlayVideo.isHidden = false
                }else {
                    self.mainImageView.bringSubview(toFront: self.imgPostImage)
                    self.imgPostImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnImage)))
                    self.btnPlayVideo.isHidden = true
                }
            }
        }
        //
        self.ratingButton.backgroundColor = .white
        self.txtComment.backgroundColor = .white
        
        self.addRatingView(superview: self.ratingButton)
        
        // setup data
        if let imgUrl = orderData.post_data.first?.post_images.first?.image_url {
            self.imgPostImage.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "ic_placeholder"), options: [], completed: nil)
        }
        if let des = orderData.post_data.first?.details, des != "" {
            self.lblOrderDetails.text = des
        }else {
            self.lblOrderDetails.text = ""
        }
        
        let postData = orderData.post_data.first
        
        if let pImages = postData?.post_images {
            for pImage in pImages {
                self.postImageUrls.append(pImage.image_url!)
            }
        }
    }
    
    @objc func handlePop(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func addRatingView(superview:RatingButtons){
        
        let rb = superview.loadNib()
        superview.addSubViewWithConstraints(view: rb)
        superview.backgroundColor = .clear
        rb.backgroundColor = .clear
    }
    
    @IBAction func onSubmitClick(_ sender: Any) {
        
        if let ratingsView = self.ratingButton.subviews.first as? RatingButtons{
            if ratingsView.rating <= 0 {
                appDel.topViewController?.displayMessage("Please provide ratings!!")
                return
            }
        }
        
        let alert = getAlertController(title: "Submit", message: nil)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
            self.submitFeedback()
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        
        guard let stringRange = Range(range, in: currentText) else {
            return false
        }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return updatedText.count <= 100
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.lblTextViewCount.text = "\(textView.text.count)" + "/" + "100"
        
        if textView.text.count < 100 {
            self.lblTextViewCount.textColor = .black
        }else{
            self.lblTextViewCount.textColor = .red
        }
    }
    
    func submitFeedback() {
        
        var ratings = 0
        
        if let ratingsView = self.ratingButton.subviews.first as? RatingButtons{
            ratings = ratingsView.rating
        }
        
        guard let userId = MyProfile.userId else {
            return
        }
        guard let typeId = self.orderData.type_id else{
            return
        }
        guard let orderId = self.orderData.order_id else {
            return
        }
        
        let params = ["type_id":typeId,
                      "user_id":userId,
                      "rating":"\(ratings)",
                      "comment":self.txtComment.text ?? "",
                      "order_id":orderId]
        
        var apiName = ""
        
        let type = orderData.type?.capitalized
        
        if type == "Post" {
            apiName = kAddFeedbackOnPost
        }else if type == "Story" {
            apiName = kAddFeedbackOnStory
        }
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(apiName, parameters: params) { (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    if let _ = data["description"] as? [String:Any] {
                        // pop up the VC and relaod feedback VC
                        
                        NotificationCenter.default.post(name: Notification.Name("updateFeedbackViewController"), object: nil)
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                }else if let msg = data["message"] as? String{
                    let str = NSLocalizedString(msg, comment: msg)
                    self.displayMessage(str)
                }
            }else{
                let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                self.displayMessage(str)
            }
        }
    }
    
    @objc func handleTapOnImage() {
        self.showImagesPreview(index: 0)
    }
    
    //MARK: GalleryItemsDataSource
    func itemCount() -> Int {
        return dataImage.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return dataImage[index]
    }
    
    // MARK: Image preview
    func showImagesPreview(index:Int){
        
        self.dataImage.removeAll()
        
        for url in self.postImageUrls {
            
            let galleryItem = GalleryItem.image { imageCompletion in
                
                SDWebImageDownloader.shared().downloadImage(with: URL(string: url), options: [], progress: nil, completed: { (img, data, err, fin) in
                    
                    if let image = img{
                        imageCompletion(image)
                    }
                })
            }
            self.dataImage.append(galleryItem)
        }
        
        let closeButton = GalleryConfigurationItem.closeButtonMode(.builtIn)
        let deleteButton = GalleryConfigurationItem.deleteButtonMode(.none)
        let seeAllButton = GalleryConfigurationItem.thumbnailsButtonMode(.none)
        
        self.galleryController = GalleryViewController(startIndex: index, itemsDataSource: self, itemsDelegate: self, displacedViewsDataSource: nil, configuration: [closeButton,deleteButton,seeAllButton])
        
        if let top = appDel.topViewController{
            top.presentImageGallery(galleryController!, completion: {
                
            })
        }
    }
    
    func removeGalleryItem(at index: Int) {
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onPlayVideoClick(_ sender: Any) {
        
        guard let postImage = self.orderData.post_data.first?.post_images.first else {
            return
        }
        guard let imageUrl = postImage.image_url else {
            return
        }
        let videoUrl = imageUrl.replacingOccurrences(of: ".jpg", with: ".mp4")
        
        let player = AVPlayer(url: URL(string: videoUrl)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        
        if let top = appDel.topViewController {
            top.present(playerController, animated: true) {
                player.play()
            }
        }
    }
}
