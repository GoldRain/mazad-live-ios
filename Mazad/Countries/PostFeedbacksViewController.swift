//
//  PostFeedbacksViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 29/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class PostFeedbacksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var post:BuyerSellerModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.title = NSLocalizedString("Feedbacks", comment: "Feedbacks")
        
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.post.feedback.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! postFeedbackCell
        
        cell.post = self.post
        cell.feedback = self.post.feedback[indexPath.row]
        
        cell.selectionStyle = .none
        return cell
    }
}

class postFeedbackCell:UITableViewCell {
    
    @IBOutlet weak var postImage: UIImageView!
    
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblRating:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblCountry:UILabel!
    
    var post:BuyerSellerModel!{
        didSet{
            if let url = post.post_images.first?.image_url, url.trim() != ""{
                postImage.sd_setImage(with: URL(string:url), completed: nil)
            }
            else{
                postImage.image = #imageLiteral(resourceName: "ic_placeholder.png")
            }
        }
    }
    
    var feedback:FeedbackModel!{
        didSet{
            
            if let user = feedback.user_name{
                self.lblUserName.text = user
            }
            else{
                self.lblUserName.text = "NA"
            }
            
            if let country = feedback.user_country?.countryName{
                self.lblCountry.text = country
            }
            else{
                self.lblCountry.text = "NA"
            }
            if let value = feedback.comment{
                self.lblDescription.text = value
            }
            else{
                self.lblDescription.text = "NA"
            }
            
            self.lblRating.text = ""
            
            if let value = feedback.rating{
                if let intValue = Int(value){
                    for _ in 0..<intValue{
                        if self.lblRating.text == ""{
                            self.lblRating.text = "★"
                        }
                        else{
                            self.lblRating.text = self.lblRating.text! + "★"
                        }
                    }
                }
                else{
                    let intValue = Int(Double(value)!)
                    for _ in 0..<intValue{
                        if self.lblRating.text == ""{
                            self.lblRating.text = "★"
                        }
                        else{
                            self.lblRating.text = self.lblRating.text! + "★"
                        }
                    }
                }
            }
        }
    }
}
