//
//  country.swift
//  P2P Trend
//
//  Created by Bodacious on 25/11/17.
//  Copyright © 2017 webblue tech. All rights reserved.
//

import UIKit

/*
 "e164_cc": "93",
 "iso2_cc": "AF",
 "e164_sc": 0,
 "geographic": true,
 "level": 1,
 "name": "Afghanistan",
 "example": "701234567",
 "display_name": "Afghanistan (AF) [+93]",
 "full_example_with_plus_sign": "+93701234567",
 "display_name_no_e164_cc": "Afghanistan (AF)",
 "e164_key": "93-AF-0"*/

class Country {
    
    var e164_cc:String?
    var iso2_cc:String?
    var e164_sc:Int?
    var geographic:Bool?
    var level:Int?
    var name:String?
    var example:String?
    var display_name:String?
    var full_example_with_plus_sign:String?
    var display_name_no_e164_cc:String?
    var e164_key:String?
    
    required init(){
        
    }
    
    init(data:[String:Any]) {
        
        if let value = data["e164_cc"] as? String{
            self.e164_cc = value
        }
        if let value = data["iso2_cc"] as? String{
            self.iso2_cc = value
        }
        if let value = data["e164_sc"] as? Int{
            self.e164_sc = value
        }
        if let value = data["geographic"] as? Bool{
            self.geographic = value
        }
        if let value = data["level"] as? Int{
            self.level = value
        }
        if let value = data["name"] as? String{
            self.name = value
        }
        if let value = data["example"] as? String{
            self.example = value
        }
        if let value = data["display_name"] as? String{
            self.display_name = value
        }
        if let value = data["full_example_with_plus_sign"] as? String{
            self.full_example_with_plus_sign = value
        }
        if let value = data["display_name_no_e164_cc"] as? String{
            self.display_name_no_e164_cc = value
        }
        if let value = data["e164_key"] as? String{
            self.e164_key = value
        }
    }

}
