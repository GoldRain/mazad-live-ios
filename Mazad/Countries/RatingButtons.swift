//
//  RatingButton.swift
//  GlobalDriver
//
//  Created by Bodacious_mac_aly on 7/30/18.
//  Copyright © 2018 AlienBrainz Softwares Pvt. Ltd. All rights reserved.
//

import UIKit

class RatingButtons: UIView {

    var contentView:UIView!
    var rating = 0
    
    @IBOutlet var buttons: [UIButton]!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
        print("now vinit 1")
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("now vinit 2")
    }
    
    func resetAllStars(){
        self.rating = 0
        
        self.reset()
        
        self.setRating()
    }
    
    func setRatings(value:Int){
        
        self.rating = value
        
        self.reset()
        
        self.setRating()
        
        self.disable()
        
    }
    
    @IBAction func onStarClick(_ sender:UIButton){
    
        self.rating = sender.tag
        
        self.reset()
        
        self.setRating()
    }
    func reset(){
        for b in self.buttons{
            b.setImage(#imageLiteral(resourceName: "icon_star"), for: .normal)
        }
    }
    func disable(){
        for b in self.buttons{
            b.isEnabled = false
        }
    }
    func enable(){
        for b in self.buttons{
            b.isEnabled = true
        }
    }
    func setRating(){
        if self.rating != 0{
            for i in 1...self.rating{
                if let v = self.viewWithTag(i) as? UIButton{
                    v.setImage(#imageLiteral(resourceName: "icon_star_fill"), for: .normal)
                }
            }
        }
    }
    

}
