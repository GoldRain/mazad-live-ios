//
//  AuctionItemModel.swift
//  Mazad Live
//
//  Created by MAC_2 on 11/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import Foundation

class AuctionItemModel {
    
    var productSerial : String?
    var startingPrice : String?
    var description : String?
    var paymentMethod: String?
    var image : UIImage?
    var imageUrl:String?
    
    init(productSerial:String,startingPrice:String,description:String,paymentMethod:String,image:UIImage) {
        
        self.productSerial = productSerial
        self.startingPrice = startingPrice
        self.description = description
        self.paymentMethod = paymentMethod
        self.image = image
    }
    
    /*
     "current_product" =     {
     description = shhshss;
     "image_url" = "https://s3.us-east-2.amazonaws.com/mazadpostimages/postimages/user5c0a5ac207be2938de836e24/Vjk0kUDJ3qsSSZopNFD7.jpg";
     "is_sold" = 0;
     price = 12345;
     "product_serial" = asd;
     "stream_id" = 5c4079a053e08c4932a06e01;
     }
     */
    

    init(data:[String:Any]){
        
        self.productSerial = data["product_serial"] as? String
        self.startingPrice = data["price"] as? String
        self.description = data["description"] as? String
        self.paymentMethod = data["payment_type"] as? String
        self.imageUrl = data["image_url"] as? String
        
    }
    
}
