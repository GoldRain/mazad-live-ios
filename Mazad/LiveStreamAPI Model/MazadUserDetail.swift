//
//  MazadUserDetail.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 12, 2018

import Foundation


class MazadUserDetail : NSObject, NSCoding{

    var v : Int!
    var id : String!
    var countryCode : String!
    var descriptionField : String!
    var email : String!
    var favPost : [MazadFavPost]!
    var followers : [MazadFollower]!
    var following : [MazadFollowing]!
    var isLive : String!
    var isOnline : Bool!
    var password : String!
    var phone : String!
    var profilePhoto : String!
    var socketId : String!
    var type : String!
    var username : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        v = dictionary["__v"] as? Int
        id = dictionary["_id"] as? String
        countryCode = dictionary["country_code"] as? String
        descriptionField = dictionary["description"] as? String
        email = dictionary["email"] as? String
        isLive = dictionary["is_live"] as? String
        isOnline = dictionary["is_online"] as? Bool
        password = dictionary["password"] as? String
        phone = dictionary["phone"] as? String
        profilePhoto = dictionary["profilePhoto"] as? String
        socketId = dictionary["socket_id"] as? String
        type = dictionary["type"] as? String
        username = dictionary["username"] as? String
        favPost = [MazadFavPost]()
        if let favPostArray = dictionary["favPost"] as? [[String:Any]]{
            for dic in favPostArray{
                let value = MazadFavPost(fromDictionary: dic)
                favPost.append(value)
            }
        }
        followers = [MazadFollower]()
        if let followersArray = dictionary["followers"] as? [[String:Any]]{
            for dic in followersArray{
                let value = MazadFollower(fromDictionary: dic)
                followers.append(value)
            }
        }
        following = [MazadFollowing]()
        if let followingArray = dictionary["following"] as? [[String:Any]]{
            for dic in followingArray{
                let value = MazadFollowing(fromDictionary: dic)
                following.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if v != nil{
            dictionary["__v"] = v
        }
        if id != nil{
            dictionary["_id"] = id
        }
        if countryCode != nil{
            dictionary["country_code"] = countryCode
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if email != nil{
            dictionary["email"] = email
        }
        if isLive != nil{
            dictionary["is_live"] = isLive
        }
        if isOnline != nil{
            dictionary["is_online"] = isOnline
        }
        if password != nil{
            dictionary["password"] = password
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        if profilePhoto != nil{
            dictionary["profilePhoto"] = profilePhoto
        }
        if socketId != nil{
            dictionary["socket_id"] = socketId
        }
        if type != nil{
            dictionary["type"] = type
        }
        if username != nil{
            dictionary["username"] = username
        }
        if favPost != nil{
            var dictionaryElements = [[String:Any]]()
            for favPostElement in favPost {
                dictionaryElements.append(favPostElement.toDictionary())
            }
            dictionary["favPost"] = dictionaryElements
        }
        if followers != nil{
            var dictionaryElements = [[String:Any]]()
            for followersElement in followers {
                dictionaryElements.append(followersElement.toDictionary())
            }
            dictionary["followers"] = dictionaryElements
        }
        if following != nil{
            var dictionaryElements = [[String:Any]]()
            for followingElement in following {
                dictionaryElements.append(followingElement.toDictionary())
            }
            dictionary["following"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        v = aDecoder.decodeObject(forKey: "__v") as? Int
        id = aDecoder.decodeObject(forKey: "_id") as? String
        countryCode = aDecoder.decodeObject(forKey: "country_code") as? String
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        favPost = aDecoder.decodeObject(forKey: "favPost") as? [MazadFavPost]
        followers = aDecoder.decodeObject(forKey: "followers") as? [MazadFollower]
        following = aDecoder.decodeObject(forKey: "following") as? [MazadFollowing]
        isLive = aDecoder.decodeObject(forKey: "is_live") as? String
        isOnline = aDecoder.decodeObject(forKey: "is_online") as? Bool
        password = aDecoder.decodeObject(forKey: "password") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        profilePhoto = aDecoder.decodeObject(forKey: "profilePhoto") as? String
        socketId = aDecoder.decodeObject(forKey: "socket_id") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        username = aDecoder.decodeObject(forKey: "username") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if v != nil{
            aCoder.encode(v, forKey: "__v")
        }
        if id != nil{
            aCoder.encode(id, forKey: "_id")
        }
        if countryCode != nil{
            aCoder.encode(countryCode, forKey: "country_code")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if favPost != nil{
            aCoder.encode(favPost, forKey: "favPost")
        }
        if followers != nil{
            aCoder.encode(followers, forKey: "followers")
        }
        if following != nil{
            aCoder.encode(following, forKey: "following")
        }
        if isLive != nil{
            aCoder.encode(isLive, forKey: "is_live")
        }
        if isOnline != nil{
            aCoder.encode(isOnline, forKey: "is_online")
        }
        if password != nil{
            aCoder.encode(password, forKey: "password")
        }
        if phone != nil{
            aCoder.encode(phone, forKey: "phone")
        }
        if profilePhoto != nil{
            aCoder.encode(profilePhoto, forKey: "profilePhoto")
        }
        if socketId != nil{
            aCoder.encode(socketId, forKey: "socket_id")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if username != nil{
            aCoder.encode(username, forKey: "username")
        }
    }
}