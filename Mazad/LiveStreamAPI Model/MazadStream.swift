//
//  MazadStream.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 12, 2018

import Foundation

class MazadStream : NSObject, NSCoding {

    var v : Int!
    var id : String!
    var country : [String]!
    var status : String!
    var streamName : String!
    var thumbUrl : String!
    var userId : String!
    var userDetails : [MazadUserDetail]!
    var viewerCount : Int!
    var comments : [MazadComment]!
    var currentItem:AuctionItemModel?

    var bookmarkedBids:[MazadComment] {
        if self.comments.count > 0{
            return self.comments.filter({ (comment) -> Bool in
                return comment.bookmarked
            })
        }
        return [MazadComment]()
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        v = dictionary["__v"] as? Int
        id = dictionary["_id"] as? String
        status = dictionary["status"] as? String
        streamName = dictionary["stream_name"] as? String
        thumbUrl = dictionary["thumbUrl"] as? String
        userId = dictionary["user_id"] as? String
        
        viewerCount = dictionary["viewerCount"] as? Int
        
        if let value = viewerCount{
            if value < 0{
                viewerCount = 0
            }
        }
        
        userDetails = [MazadUserDetail]()
        if let userDetailsArray = dictionary["userDetails"] as? [[String:Any]]{
            for dic in userDetailsArray{
                let value = MazadUserDetail(fromDictionary: dic)
                userDetails.append(value)
            }
        }
        
        country = [String]()
        if let contries = dictionary["country"] as? [[String:Any]]{
            for value in contries{
                if let id = value["country_id"] as? String{
                    country.append(id)
                }
            }
        }
        
        comments = [MazadComment]()
        if let commentsArray = dictionary["comment"] as? [[String:Any]]{
            for dic in commentsArray{
                let value = MazadComment(fromDictionary: dic)
                comments.append(value)
            }
        }
        
//        if let item = dictionary["current_product"] as? NSMutableDictionary{
//            self.currentItem = AuctionItemModel(data: item)
//        }
        
        
//        print("All Keys")
//        print(dictionary.keys)
//        if let value = dictionary["current_product"]{
//            print(type(of: value))
//        }
        
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if v != nil{
            dictionary["__v"] = v
        }
        if id != nil{
            dictionary["_id"] = id
        }
        if status != nil{
            dictionary["status"] = status
        }
        if streamName != nil{
            dictionary["stream_name"] = streamName
        }
        if thumbUrl != nil{
            dictionary["thumbUrl"] = thumbUrl
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        if viewerCount != nil{
            dictionary["viewerCount"] = viewerCount
        }
        if userDetails != nil{
            var dictionaryElements = [[String:Any]]()
            for userDetailsElement in userDetails {
                dictionaryElements.append(userDetailsElement.toDictionary())
            }
            dictionary["userDetails"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        v = aDecoder.decodeObject(forKey: "__v") as? Int
        id = aDecoder.decodeObject(forKey: "_id") as? String
        comments = aDecoder.decodeObject(forKey: "comment") as? [MazadComment]
        country = aDecoder.decodeObject(forKey: "country") as? [String]
        status = aDecoder.decodeObject(forKey: "status") as? String
        streamName = aDecoder.decodeObject(forKey: "stream_name") as? String
        thumbUrl = aDecoder.decodeObject(forKey: "thumbUrl") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? String
        userDetails = aDecoder.decodeObject(forKey: "userDetails") as? [MazadUserDetail]
        viewerCount = aDecoder.decodeObject(forKey: "viewerCount") as? Int
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if v != nil{
            aCoder.encode(v, forKey: "__v")
        }
        if id != nil{
            aCoder.encode(id, forKey: "_id")
        }
        if comments != nil{
            aCoder.encode(comments, forKey: "comment")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if streamName != nil{
            aCoder.encode(streamName, forKey: "stream_name")
        }
        if thumbUrl != nil{
            aCoder.encode(thumbUrl, forKey: "thumbUrl")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        if userDetails != nil{
            aCoder.encode(userDetails, forKey: "userDetails")
        }
        if viewerCount != nil{
            aCoder.encode(viewerCount, forKey: "viewerCount")
        }
    }
    
    var highestBookmarkedBid:MazadComment?{
        if self.bookmarkedBids.count > 0{
            let sorted = self.bookmarkedBids.sorted { (first, second) -> Bool in
                if let firstA = Double(first.amount){
                    if let secondA = Double(second.amount){
                        return firstA > secondA
                    }
                }
                return false
            }
            return sorted.first
        }
        return nil
    }
    
    var highestBid:MazadComment?{
        
        let filtered = self.comments.filter({$0.amount != nil && $0.amount != ""})
        
        let sorted = filtered.sorted { (first, second) -> Bool in
            if let firstA = Double(first.amount){
                if let secondA = Double(second.amount){
                    return firstA > secondA
                }
            }
            return false
        }
        return sorted.first
        
        
    }
}


