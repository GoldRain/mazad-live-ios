//
//  MazadFavPost.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 12, 2018

import Foundation


class MazadFavPost : NSObject, NSCoding{

    var id : String!
    var postId : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["_id"] as? String
        postId = dictionary["post_id"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["_id"] = id
        }
        if postId != nil{
            dictionary["post_id"] = postId
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "_id") as? String
        postId = aDecoder.decodeObject(forKey: "post_id") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "_id")
        }
        if postId != nil{
            aCoder.encode(postId, forKey: "post_id")
        }
    }
}