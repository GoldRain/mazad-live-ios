//
//  MazadComment.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 12, 2018

import Foundation


class MazadComment : NSObject, NSCoding{

    
    var id : String!
    var comment : String!
    var amount : String!
    var user : [MazadUserDetail]!
    var bookmarked:Bool! = false
    var status:String? = "pending"
    var createdAt:Date?
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["_id"] as? String
        comment = dictionary["comment"] as? String
        amount = dictionary["amount"] as? String
        bookmarked = dictionary["bookmarked"] as? Bool ?? false
        
        status = dictionary["status"] as? String
        
        user = [MazadUserDetail]()
        if let userIdArray = dictionary["user_id"] as? [[String:Any]]{
            for dic in userIdArray{
                let value = MazadUserDetail(fromDictionary: dic)
                user.append(value)
            }
        }
        
        if let value = dictionary["created_at"] as? String{
            if let doubleValue = Double(value){
                self.createdAt = Date(timeIntervalSince1970: doubleValue/1000)
            }
        }
        
        if let value = dictionary["created_at"] as? Double{
            self.createdAt = Date(timeIntervalSince1970: value/1000)
        }
    }
    
    var isApproved:Bool!{
        if let status = self.status{
            if status == "approved"{
                return true
            }
        }
        return false
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["_id"] = id
        }
        if comment != nil{
            dictionary["comment"] = comment
        }
        dictionary["bookmarked"] = bookmarked
        if user != nil{
            var dictionaryElements = [[String:Any]]()
            for userIdElement in user {
                dictionaryElements.append(userIdElement.toDictionary())
            }
            dictionary["user"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "_id") as? String
        comment = aDecoder.decodeObject(forKey: "comment") as? String
        bookmarked = aDecoder.decodeObject(forKey: "bookmarked") as? Bool
        user = aDecoder.decodeObject(forKey: "user") as? [MazadUserDetail]
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "_id")
        }
        if comment != nil{
            aCoder.encode(comment, forKey: "comment")
        }
        
        aCoder.encode(bookmarked, forKey: "bookmarked")
        
        if user != nil{
            aCoder.encode(user, forKey: "user")
        }
    }
    
}
