//
//  LaunchViewController.swift
//  Mazad Live
//
//  Created by alienbrainz on 30/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if MyProfile.userId ==  nil{
//            UserDefaults.standard.set(nil, forKey: CurrentLanguageKey)
//            UserDefaults.standard.synchronize()
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let _ = MyProfile.userId {
            
            // Check last Login
            if let userId = MyProfile.userId, let time = MyProfile.lastLoginTime {
                JSONRequest.makeRequest(kCheckLastLogin, parameters: ["user_id":userId,"time":time]) { (data, error) in
                    
                    if let data = data as? [String:Any] {
                        if let err = data["error"] as? Bool, !err{ // true
                            
                            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController"){
                                
                                let nvc = UINavigationController(rootViewController: vc)
                                
                                if MyProfile.showAds{
                                    appDel.startAdTimer()
                                }
                                
                                self.present(nvc, animated: true) {
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name("changeUser"), object: nil, userInfo: ["user":MyProfile.isSeller])
                                }
                                return
                            }
                        }
                    }
                    MyProfile.logOutUser()
                    self.checkForLanguage()
                }
            }
        }
        else{
            self.checkForLanguage()
        }
    }
    
    func checkForLanguage(){
        
        if UserDefaults.standard.object(forKey: CurrentLanguageKey) == nil {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LanguageSelectViewController"){
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else{
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationMainViewController"){
                self.navigationController?.setViewControllers([vc], animated: true)
            }
        }
    }
}
