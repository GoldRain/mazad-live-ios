

import UIKit

class ConfirmationAlertViewController: UIViewController {

    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var alertView: UIView!
    
    var callback:((Bool)->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnAgree.addShadow()
        btnCancel.addShadow()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let firstView = touches.first?.view{
            if firstView != alertView {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onAgreeClick(_ sender: UIButton) {
        callback(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onCancelClick(_ sender: UIButton) {
        callback(false)
        self.dismiss(animated: false, completion: nil)
    }

}
