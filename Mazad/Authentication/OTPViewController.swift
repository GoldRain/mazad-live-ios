
import UIKit
import PinCodeTextField

enum ChangePasswordType {
    case email
    case phone
}

enum viewControllerType1 {
    case signUp
    case forgotPassword
}

class OTPViewController: UIViewController {
    
    var countryCode: String?
    var phoneNumber: String?
    
    var email:String?
    
    var pinCode:String?
    
    var registerParams = [String:String]()
    
    // Enum variable
    var changePasswordType = ChangePasswordType.email
    var VCType = viewControllerType1.signUp
    
    @IBOutlet weak var txtPinCode: PinCodeTextField!
    @IBOutlet weak var lblNumber: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if changePasswordType == .email && VCType == .forgotPassword {
            if let email = self.email {
                self.lblNumber.text = email
            }
        }else if changePasswordType == .phone || VCType == .signUp {
            if let code = self.countryCode{
                if let number = self.phoneNumber{
                    self.lblNumber.text = "\(code) \(number)"
                }
            }
        }
        
        txtPinCode.keyboardType = .numberPad
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onBackClick(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    @IBAction func onContinueClick(_ sender: UIButton) {
        
        if let code = self.txtPinCode.text{
            
            if code.count < 6 {
                let str = NSLocalizedString("Please enter pin code", comment: "Please enter pin code")
                self.displayMessage(str)
                return
            }
        }else{
            let str = NSLocalizedString("Please enter pin code", comment: "Please enter pin code")
            self.displayMessage(str)
            return
        }
        
        // when user click on signUp
        
        self.countryCode = countryCode?.replacingOccurrences(of: "+", with: "")
        
        if VCType == .signUp {
            
            let params = ["version":VERSION,
                          "type":"phone",
                          "country_code": countryCode! ,
                          "phone":phoneNumber!,
                          "code": self.txtPinCode.text!
                        ]
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            JSONRequest.makeRequest(kVerifyOtp, parameters: params) { (data, error) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data {
                    
                    if let error = data["error"] as? Bool, !error {
                        
                        if let message = data["message"] as? String, message == "Otp Verified" {
                            
                            JSONRequest.makeRequest(kRegister, parameters: self.registerParams) { (data, error) in
                                
                                if let data = data as? [String:Any]{
                                    
                                    if let err = data["error"] as? Bool, !err{
                                        
                                        if data["data"] as? [String:Any] != nil {
                                            
                                            if let controllers = self.navigationController?.viewControllers {
                                                let str = NSLocalizedString("Register Successfully", comment: "Register Successfully")
                                                
                                                self.displayMessage(str) { () -> (Void) in
                                                    
                                                    self.navigationController?.popToViewController(controllers[0], animated: true)
                                                }
                                            }
                                        }
                                        else{
                                            let str = NSLocalizedString("Failed to register. Please try again", comment: "Failed to register. Please try again")
                                            self.displayMessage(str)
                                        }
                                    }
                                    else if let msg = data["message"] as? String{
                                        let str = NSLocalizedString(msg, comment: msg)
                                        self.displayMessage(str)
                                    }
                                }
                                else{
                                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                                    self.displayMessage(str)
                                }
                            }
                        }else {
                            if let wait = wait {
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            if let msg = data["message"] as? String {
                                let str = NSLocalizedString(msg, comment: msg)
                                self.displayMessage(str)
                            }
                        }
                    }else {
                        if let wait = wait {
                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                        }
                        let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                        self.displayMessage(str)
                    }
                }
            }
        }
        else if VCType == .forgotPassword {
            
            var params = [String:String]()
            var type:String?
            
            if changePasswordType == .email {
                type = "email"
                
                params["version"] = VERSION
                params["type"] = "email"
                params["email"] = email
                params["code"] = pinCode
                
            } else if changePasswordType == .phone {
                type = "phone"
                
                params["version"] = VERSION
                params["type"] = "phone"
                params["phone"] = phoneNumber
                params["country_code"] = countryCode
                params["code"] = pinCode
            }
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            JSONRequest.makeRequest(kVerifyOtp, parameters: params) { (data, error) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data as? [String:Any] {
                    
                    if let error = data["error"] as? Bool, !error {
                        
                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as? ChangePasswordViewController {
                            
                            vc.type = type
                            vc.email = self.email
                            vc.phone = self.phoneNumber
                            vc.countryCode = self.countryCode
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    }else {
                        if let msg = data["message"] as? String{
                            let str = NSLocalizedString(msg, comment: msg)
                            self.displayMessage(str)
                        }
                    }
                }else {
                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                    self.displayMessage(str)
                }
            }
        }
    }
}
