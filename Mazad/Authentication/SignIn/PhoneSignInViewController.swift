

import UIKit


class PhoneSignInViewController: UIViewController {
    
    @IBOutlet weak var btnSeller: UIButton!
    @IBOutlet weak var btnBuyer: UIButton!
    
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCountryCode: CustomTextField!
    
    var isSeller = true
    
    //var countriesList = [Countries]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtCountryCode.text = "+973"
        
        if UserDefaults.standard.string(forKey: CurrentLanguageKey) == "ar" {
            self.txtPhone.textAlignment = .right
        }
        //self.getCountryCode()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func onForgotPasswordClick(_ sender: UIButton) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController"){
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onSignInClick(_ sender: UIButton) {
        
        guard WebAPI.isReachable() else {
            let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
            appDel.topViewController?.displayMessage(str)
            return;
        }
        
        if self.validate() {
            
            let countryCode = self.txtCountryCode.text!.trim().replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+", with: "")
            
            let params = ["type":"phone",
                          "country_code": countryCode,
                          "phone": self.txtPhone.text!.trim(),
                          "password": self.txtPassword.text!.trim()
            ]
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            JSONRequest.makeRequest(kLoginApi, parameters: params) { (data, error) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data as? [String:Any]{
                    
                    if let err = data["error"] as? Bool, !err{
                        
                        MyProfile.isSeller = self.isSeller
                        
                        if let userDataArr = data["data"] as? [Any]  {
                            
                            if let userData = userDataArr.first as? [String:Any] {
                                
                                MyProfile.set(rawData: userData)
                                
                                // Check if user is subscribed or not
//                                if let availablePost = MyProfile.AvailablePostCount{
//                                    if availablePost.status{
//                                        MyProfile.isSubscribedUser = true
//                                    }
//                                }
//                                if let availableStory = MyProfile.AvailableStoryCount{
//                                    if availableStory.status{
//                                        MyProfile.isSubscribedUser = true
//                                    }
//                                }
//                                if let availableLive = MyProfile.AvailableLiveCount{
//                                    if availableLive.status{
//                                        MyProfile.isSubscribedUser = true
//                                    }
//                                }
                                //
                                
                                if let verificationData = userData["verification"] as? [String:Any] {
                                    let verification = VerificationModel(data: verificationData)
                                    
                                    MyProfile.verification = verification
                                }
                                
                                if !MyProfile.isVerified {
                                    MyProfile.showVerifiedScreen = true
                                }else{
                                    MyProfile.showVerifiedScreen = false
                                }
                                
                                if MyProfile.showAds {
                                    appDel.startAdTimer()
                                }
                                
                                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController"){
                                    
                                    let nvc = UINavigationController(rootViewController: vc)
                                    
                                    self.navigationController?.popToRootViewController(animated: true)
                                    
                                    self.present(nvc, animated: true, completion: {
                                        
                                        NotificationCenter.default.post(name: NSNotification.Name("changeUser"), object: nil, userInfo: ["user":MyProfile.isSeller])
                                        
                                        self.txtPhone.text = nil
                                        self.txtPassword.text = nil
                                    })
                                }
                            }else{
                                let str = NSLocalizedString("Failed to login", comment: "Failed to login")
                                self.displayMessage(str)
                            }
                        }
                    }
                    else if let msg = data["message"] as? String{
                        let str = NSLocalizedString(msg, comment: msg)
                        self.displayMessage(str)
                    }
                }
                else{
                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                    self.displayMessage(str)
                }
            }
        }
    }
    
    @IBAction func onSignUpClick(_ sender: UIButton) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController"){
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onSwitchBtnClick(_ sender: UIButton) {
        
        let btn = sender == self.btnBuyer ? self.btnSeller : self.btnBuyer
        
        if sender.currentImage == #imageLiteral(resourceName: "icon_switch_on"){
            sender.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            btn?.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            btn?.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
        }
        
        if btnBuyer.currentImage == #imageLiteral(resourceName: "icon_switch_on"){
            self.isSeller = false
        }
        else{
            self.isSeller = true
        }
    }
    
    @IBAction func onCountryCodeClick(_ sender: Any) {
        
        appDel.getAllCountries { (countries, msg) in
            
            if let countries = countries{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountriesSelectionViewController") as? CountriesSelectionViewController{
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.countriesList = countries
                    vc.callback = {  data in
                        self.txtCountryCode.placeholder = ""
                        //self.txtCountryCode.text = data.e164_cc
                        self.txtCountryCode.text = data.countryCode
                    }
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if let msg = msg{
                self.displayMessage(msg)
            }
        }
    }
    
    
    func validate() -> Bool{
        
        if isEmpty(self.txtPhone.text!){
            let str = NSLocalizedString("Please enter Phone", comment: "Please enter Phone")
            self.displayMessage(str)
            return false
        }
        if isEmpty(txtPassword.text!){
            let str = NSLocalizedString("Please enter password", comment: "Please enter password")
            self.displayMessage(str)
            return false
        }
//        if txtPassword.text!.count < 8{
//            let str = NSLocalizedString("Password should be 8 characters long", comment: "Password should be 8 characters long")
//            self.displayMessage(str)
//            return false
//        }
        
        return true
    }
    
}
