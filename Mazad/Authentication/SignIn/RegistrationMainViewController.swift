

import UIKit
import CarbonKit


class RegistrationMainViewController: UIViewController, CarbonTabSwipeNavigationDelegate {
    
    @IBOutlet weak var containerView: UIView!
    public var carbonController: CarbonTabSwipeNavigation!
    let screenBounds = UIScreen.main.bounds.width
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(handleSkipLogin))
        self.navigationItem.rightBarButtonItem?.tintColor = .white

        self.title = NSLocalizedString("Sign In", comment: "Sign In")
        
        let items = [NSLocalizedString("Email Sign In", comment: "Email Sign In"), NSLocalizedString("Phone Sign In", comment: "Phone Sign In")]
        
        carbonController = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        carbonController.setTabBarHeight(50.0)
        
        //carbonController.setNormalColor(.lightGray)
        carbonController.setIndicatorColor(.purpleColor)
        //carbonController.setSelectedColor(UIColor.black)
        carbonController.setIndicatorHeight(2.0)
        carbonController.setNormalColor(.lightGray, font: UIFont.systemFont(ofSize: 12.0, weight: .medium))
        carbonController.setSelectedColor(.darkGray, font: UIFont.systemFont(ofSize: 12.0, weight: .medium))
        carbonController.insert(intoRootViewController: self, andTargetView: self.containerView)
        
        self.carbonController.setCurrentTabIndex(0, withAnimation: false)
        let widthOfTabIcons = screenBounds/2
        
        
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 0)
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 1)
    }
    
    @objc func handleSkipLogin() {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController"){
            
            let nvc = UINavigationController(rootViewController: vc)
            
            self.navigationController?.popToRootViewController(animated: true)
            
            self.present(nvc, animated: true, completion: {
                MyProfile.isSeller = false
                NotificationCenter.default.post(name: NSNotification.Name("changeUser"), object: nil, userInfo: ["user":MyProfile.isSeller])
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        if index == 0 {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "EmailSignInViewController")
            return vc
        }
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "PhoneSignInViewController")
        return vc
    }

}
