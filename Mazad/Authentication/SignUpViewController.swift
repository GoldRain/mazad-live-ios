import UIKit

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var imageViewMain: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var txtCountryCode: CustomTextField!
    @IBOutlet weak var txtPhone: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let str = NSLocalizedString("Sign Up", comment: "Sign Up")
        self.title = str
        
        self.txtCountryCode.placeholder = "+973"
        self.txtCountryCode.text = "+973"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let view = touches.first?.view{
            if view == self.imageViewMain{
                self.showImagePicker()
            }
        }
    }

    @IBAction func onSignUpClick(_ sender: UIButton) {
        
        if validate() {
            
            let alert = getAlertController(title: "", message: "By clicking Sign Up, you agree with our Data Privacy policy")
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                self.signingUp()
            }))
            alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: { (_) in
                return;
            }))
            alert.addAction(UIAlertAction(title: "Show", style: .default, handler: { (_) in
                
                if let vc = UIStoryboard(name:"Main2", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionViewController") as? TermsAndConditionViewController{
                    
                    vc.title = NSLocalizedString("Data Privacy", comment: "Data Privacy")
                    
                    if let currentValue = UserDefaults.standard.value(forKey: CurrentLanguageKey) as? String{
                        
                        if currentValue == "en"{
                            vc.url = URL(string: kDataPrivacyEn)
                        }else{
                            vc.url = URL(string: kDataPrivacyAr)
                        }
                    }else{
                        vc.url = URL(string: kDataPrivacyEn)
                    }
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }))
            
            self.navigationController?.present(alert, animated: true, completion: nil)
            
        }
    }
    
    fileprivate func signingUp() {
        let wait = appDel.topViewController?.addWaitSpinner()
        let randomName = randomString(length: 20)
        
        let countryCode = self.txtCountryCode.text!.trim().replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+", with: "")
        
        uploadImagesToAws([self.profileImageView.image!], folderName: appDel.userImageFolder, randomName: randomName) { (url) in
            
            DispatchQueue.main.async {
                
                if let url = url {
                    
                    let registerParams = ["country_code" : countryCode,
                                          "phone" : self.txtPhone.text!.trim(),
                                          "email" : self.txtEmail.text!.trim(),
                                          "username" : self.txtName.text!.trim(),
                                          "password" : self.txtPassword.text!.trim(),
                                          "userType": "buyer",
                                          "profilePic": url.first! //UIImageJPEGRepresentation(self.profileImageView.image!, 0.001)!.base64EncodedString()
                    ]
                    
                    let sendOtpParams = ["country_code":countryCode,
                                         "phone":self.txtPhone.text!.trim()]
                    
                    let checkEmailParams = ["email":self.txtEmail.text!.trim()]
                    
                    JSONRequest.makeRequest(kCheckEmail, parameters: checkEmailParams, callback: {  (data, error) in
                        
                        if let data = data as? [String:Any] {
                            
                            if let err = data["error"] as? Bool, !err {
                                
                                JSONRequest.makeRequest(kSendOtp, parameters: sendOtpParams , callback: {  (data, error) in
                                    
                                    if let wait = wait {
                                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                                    }
                                    
                                    if let data = data as? [String:Any] {
                                        
                                        if let err = data["error"] as? Bool, !err {
                                            
                                            if let data = data["data"] as? [String:Any] {
                                                
                                                if let otp = data["otp"] as? String {
                                                    
                                                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as? OTPViewController {
                                                        
                                                        vc.countryCode = self.txtCountryCode.text!
                                                        vc.phoneNumber = self.txtPhone.text!
                                                        vc.VCType = .signUp
                                                        vc.registerParams = registerParams
                                                        vc.pinCode = otp
                                                        
                                                        self.navigationController?.pushViewController(vc, animated: true)
                                                    }
                                                }else{
                                                    let str = NSLocalizedString("Failed to send OTP. Please try again", comment: "Failed to send OTP. Please try again")
                                                    self.displayMessage(str)
                                                }
                                            }
                                        }else if let msg = data["message"] as? String{
                                            let str = NSLocalizedString(msg, comment: msg)
                                            self.displayMessage(str)
                                        }
                                    }else{
                                        let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                                        self.displayMessage(str)
                                    }
                                })
                                
                            }else if let message = data["message"] as? String{
                                
                                if let wait = wait {
                                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                                }
                                let str = NSLocalizedString(message, comment: message)
                                self.displayMessage(str)
                            }
                        }else{
                            if let wait = wait {
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                            self.displayMessage(str)
                        }
                    })
                }
            }
        }
    }
    
    @IBAction func onTermsClick(_ sender: Any) {
        
        if let vc = UIStoryboard(name:"Main2", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionViewController") as? TermsAndConditionViewController{
            vc.title = NSLocalizedString("Terms and Conditions", comment: "Terms and Conditions")
            
            if let currentValue = UserDefaults.standard.value(forKey: CurrentLanguageKey) as? String{
                print("current value = \(currentValue)")
                
                if currentValue == "en"{
                    vc.url = URL(string: kTermsAndConditionEn)
                }else{
                    vc.url = URL(string: kTermsAndConditionAr)
                }
            }else{
               vc.url = URL(string: kTermsAndConditionEn)
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func onSignInClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCheckClick(_ sender: UIButton) {
        if sender.currentImage == #imageLiteral(resourceName: "tick"){
            sender.setImage(nil, for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
        }
    }
    
    @IBAction func onCountryCodeClick(_ sender: Any) {
        
        appDel.getAllCountries { (countries, msg) in
            
            if let countries = countries{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountriesSelectionViewController") as? CountriesSelectionViewController{
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.countriesList = countries
                    vc.callback = { data in
                        self.txtCountryCode.placeholder = ""
                        //self.txtCountryCode.text = data.e164_cc
                        self.txtCountryCode.text = data.countryCode
                    }
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if let msg = msg{
                self.displayMessage(msg)
            }
        }
    }
    
    func showImagePicker(){
        
        let alertController = getAlertController(title: "Select Source", message: nil)
        
        alertController.view.tintColor = .black
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.imagePickerSourceType(source: .camera)
        }))
        
        alertController.addAction(UIAlertAction(title: "Gallary", style: .default, handler: { (_) in
            self.imagePickerSourceType(source: .photoLibrary)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerSourceType(source: UIImagePickerControllerSourceType){
        let imagePicker = UIImagePickerController()
        imagePicker.navigationBar.tintColor = .white
        imagePicker.allowsEditing = true
        imagePicker.sourceType = source
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImage: UIImage!
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            selectedImage = editedImage
        }
        else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            selectedImage = originalImage
        }
        self.profileImageView.image = selectedImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func validate() -> Bool{
        if self.profileImageView.image == nil{
            let str = NSLocalizedString("Please select image", comment: "Please select image")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtName.text!){
            let str = NSLocalizedString("Please enter user name", comment: "Please enter user name")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtCountryCode.text!){
            let str = NSLocalizedString("Please enter country code", comment: "Please enter country code")
            self.displayMessage(str)
            return false
        }
        if !isValidMobileNumber(self.txtPhone.text?.trim()) {
            let str = NSLocalizedString("Please enter valid Phone Number", comment: "Please enter valid Phone Number")
            self.displayMessage(str)
            return false
        }else if (self.txtPhone.text?.trim().count)! > 20 {
            let str = NSLocalizedString("Please enter valid Phone Number", comment: "Please enter valid Phone Number")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtEmail.text!){
            let str = NSLocalizedString("Please enter email id", comment: "Please enter email id")
            self.displayMessage(str)
            return false
        }
        if !isValidEmailId(self.txtEmail.text!){
            let str = NSLocalizedString("Please enter valid email id", comment: "Please enter valid email id")
            self.displayMessage(str)
            return false
        }
        if isEmpty(txtPassword.text!){
            let str = NSLocalizedString("Please enter password", comment: "Please enter password")
            self.displayMessage(str)
            return false
        }
        if txtPassword.text!.count < 8{
            let str = NSLocalizedString("Password should be 8 characters long", comment: "Password should be 8 characters long")
            self.displayMessage(str)
            return false
        }
        if isEmpty(txtConfirmPassword.text!){
            let str = NSLocalizedString("Please confirm password", comment: "Please confirm password")
            self.displayMessage(str)
            return false
        }
        if txtPassword.text! != txtConfirmPassword.text!{
            let str = NSLocalizedString("Password mismatch", comment: "Password mismatch")
            self.displayMessage(str)
            return false
        }
        if btnTerms.currentImage != #imageLiteral(resourceName: "tick"){
            let str = NSLocalizedString("Please agree to our terms", comment: "Please agree to our terms")
            self.displayMessage(str)
            return false
        }
        return true
    }
}
