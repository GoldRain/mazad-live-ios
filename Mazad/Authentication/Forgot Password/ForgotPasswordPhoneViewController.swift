

import UIKit

class ForgotPasswordPhoneViewController: UIViewController {

     @IBOutlet weak var txtCountryCode:UITextField!
    @IBOutlet weak var txtPhone:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.txtCountryCode.placeholder = "971"
    }

    @IBAction func onCountryCodeClick(_ sender: Any) {
        
        appDel.getAllCountries { (countries, msg) in
            
            if let countries = countries{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountriesSelectionViewController") as? CountriesSelectionViewController{
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.countriesList = countries
                    vc.callback = {  data in
                        self.txtCountryCode.placeholder = ""
                        //self.txtCountryCode.text = data.e164_cc
                        self.txtCountryCode.text = data.countryCode
                    }
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if let msg = msg{
                self.displayMessage(msg)
            }
        }
    }
    
}
