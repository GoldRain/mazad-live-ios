

import UIKit
import CarbonKit

class ForgotPasswordViewController: UIViewController, CarbonTabSwipeNavigationDelegate {
    
    @IBOutlet weak var containerView: UIView!
    public var carbonController: CarbonTabSwipeNavigation!
    let screenBounds = UIScreen.main.bounds.width
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let items = ["Email", "Phone Number"]
        
        carbonController = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        carbonController.setTabBarHeight(45.0)
        
        //carbonController.setNormalColor(.lightGray)
        carbonController.setIndicatorColor(.purpleColor)
        //carbonController.setSelectedColor(UIColor.black)
        carbonController.setNormalColor(.lightGray, font: UIFont.systemFont(ofSize: 12.0, weight: .medium))
        carbonController.setSelectedColor(.darkGray, font: UIFont.systemFont(ofSize: 12.0, weight: .medium))
        carbonController.setIndicatorHeight(2.0)
        carbonController.insert(intoRootViewController: self, andTargetView: self.containerView)
        
        self.carbonController.setCurrentTabIndex(0, withAnimation: false)
        let widthOfTabIcons = screenBounds/2
        
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 0)
        self.carbonController.carbonSegmentedControl?.setWidth(widthOfTabIcons, forSegmentAt: 1)
        
        let str = NSLocalizedString("Forgot Password ", comment: "Forgot Password")
        self.title = str
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    var emailVC:ForgotPasswordEmailViewController?
    var phoneVC:ForgotPasswordPhoneViewController?
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        if index == 0 {
            if let vc = self.emailVC{
                return vc
            }
            self.emailVC = self.storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordEmailViewController") as? ForgotPasswordEmailViewController
            return self.emailVC!
        }
        if let vc = self.phoneVC{
            return vc
        }
        self.phoneVC = self.storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordPhoneViewController") as? ForgotPasswordPhoneViewController
        return self.phoneVC!
    }

    @IBAction func onLoginClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onResetPasswordClick(_ sender: UIButton) {
        
        if self.carbonController.currentTabIndex == 0{
            
            if let vc = self.emailVC{
                //call api
                
                guard let email = vc.txtEmail.text?.trim(), email != "" else{
                    let str = NSLocalizedString("Enter email", comment: "Enter email")
                    self.displayMessage(str)
                    return
                }

                let params = ["version": VERSION,
                              "type": "email",
                              "email" : email,
                              "admin":"\(false)"
                              ]
                
                let wait = appDel.topViewController?.addWaitSpinner()
                
                JSONRequest.makeRequest(kForgotPassword, parameters: params ) { (data, error) in
                    
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    
                    if let data = data as? [String:Any] {
                        
                        if let err = data["error"] as? Bool, !err{
                            
                            if let data = data["data"] as? [String:Any] {
                                
                                if let pinCode = data["otp"] as? String {
                                    
                                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as? OTPViewController{
                                        
                                        vc.email = email
                                        vc.changePasswordType = .email
                                        vc.pinCode = pinCode
                                        vc.VCType = .forgotPassword
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }else {
                                let str = NSLocalizedString("Failed to Reset Password", comment: "Failed to Reset Password")
                                self.displayMessage(str)
                            }
                        }else if let msg = data["message"] as? String{
                            let str = NSLocalizedString(msg, comment: msg)
                            self.displayMessage(str)
                        }
                    }else{
                        let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                        self.displayMessage(str)
                    }
                }
            }
        }
        else{
            if let vc = self.phoneVC{
                //call api
                
                guard let phone = vc.txtPhone.text?.trim(), phone != "" else{
                    let str = NSLocalizedString("Enter Phone Number", comment: "Enter Phone Number")
                    self.displayMessage(str)
                    return
                }
                guard let countryCode = vc.txtCountryCode.text?.trim(), countryCode != "" else {
                    let str = NSLocalizedString("Select Country Code", comment: "Select Country Code")
                    self.displayMessage(str)
                    return
                }
               
                let params = ["phone" : phone,
                              "version": VERSION,
                              "type" : "phone",
                              "country_code": countryCode.replacingOccurrences(of: "+", with: "").replacingOccurrences(of: " ", with: ""),
                              "admin":"\(false)"
                            ]
                
                let wait = appDel.topViewController?.addWaitSpinner()
                
                JSONRequest.makeRequest(kForgotPassword, parameters: params ) { (data, error) in
                    
                    if let wait = wait {
                        appDel.topViewController?.removeWaitSpinner(waitView: wait)
                    }
                    
                    if let data = data as? [String:Any] {
                        
                        if let err = data["error"] as? Bool, !err{
                            
                            if let data = data["data"] as? [String:Any] {
                                
                                if let pinCode = data["otp"] as? String{
                                    
                                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as? OTPViewController{
                                        
                                        vc.countryCode = countryCode
                                        vc.phoneNumber = phone
                                        vc.pinCode = pinCode
                                        vc.changePasswordType = .phone
                                        vc.VCType = .forgotPassword
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }else {
                                let str = NSLocalizedString("Failed to Reset Password", comment: "Failed to Reset Password")
                                self.displayMessage(str)
                            }
                        }else if let msg = data["message"] as? String{
                            let str = NSLocalizedString(msg, comment: msg)
                            self.displayMessage(str)
                        }
                    }else{
                        let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                        self.displayMessage(str)
                    }
                }
            }
        }
    }
}

