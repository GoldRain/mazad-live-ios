//
//  ChangePasswordViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 26/11/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    
    var type:String?
    var email:String?
    var phone:String?
    var countryCode:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let str = NSLocalizedString("Change Password", comment: "Change Password")
        self.title = str
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func onChangePasswordClick(_ sender: Any) {
        
        if validate() {
            
            var params = [String:String]()
            
            if type == "email"{
                    params = ["version":VERSION,
                              "password": txtPassword.text!.trim(),
                              "type":"email",
                              "email":email!
                            ]
            }else if type == "phone" {
                params = ["version":VERSION,
                          "password": txtPassword.text!.trim(),
                          "type":"phone",
                          "phone":phone!,
                          "country_code":countryCode!
                        ]
            }
            
            let wait = appDel?.topViewController?.addWaitSpinner()
            
            JSONRequest.makeRequest(kChangePassword, parameters: params) { (data, error) in
                
                if let wait = wait {
                    appDel?.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let data = data {
                    
                    if let error = data["error"] as? Bool, !error {
                        
                        let str = NSLocalizedString("Password Changed Successfully", comment: "Password Changed Successfully")
                        self.displayMessage(str, callback: {  () -> (Void) in
                            if let viewController = self.navigationController?.viewControllers {
                                self.navigationController?.popToViewController(viewController[0], animated: true)
                            }
                        })
                        
                    }else {
                        
                        if let msg = data["message"] as? String {
                            let str = NSLocalizedString(msg, comment: msg)
                            self.displayMessage(str)
                        }
                    }
                }else {
                    let str = NSLocalizedString("Failed to parse server response", comment: "Failed to parse server response")
                    self.displayMessage(str)
                }
            }
        }
    }
    
    func validate() -> Bool {
        
        if isEmpty(txtPassword.text!){
            let str = NSLocalizedString("Please enter password", comment: "Please enter password")
            self.displayMessage(str)
            return false
        }
        if txtPassword.text!.count < 8{
            let str = NSLocalizedString("Password should be 8 characters long", comment: "Password should be 8 characters long")
            self.displayMessage(str)
            return false
        }
        if isEmpty(txtConfirmPassword.text!){
            let str = NSLocalizedString("Please confirm password", comment: "Please confirm password")
            self.displayMessage(str)
            return false
        }
        if txtPassword.text! != txtConfirmPassword.text!{
            let str = NSLocalizedString("Password mismatch", comment: "Password mismatch")
            self.displayMessage(str)
            return false
        }
        
        return true
    }
}
