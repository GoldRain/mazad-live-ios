//
//  Helper.swift
//  Mazad Live
//
//  Created by Mac-3 on 07/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import SDWebImage
import AWSS3
import AWSCore
import Alamofire
import SystemConfiguration
import Contacts

let imageBgcolor = [#colorLiteral(red: 0.9411764706, green: 0.2470588235, blue: 0.9019607843, alpha: 1), #colorLiteral(red: 0.4823529412, green: 0.2156862745, blue: 0.2078431373, alpha: 1), #colorLiteral(red: 0.2705882353, green: 0.862745098, blue: 1, alpha: 1), #colorLiteral(red: 0.2156862745, green: 0.9960784314, blue: 0.4392156863, alpha: 1), #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.2196078431, alpha: 1), #colorLiteral(red: 0.9411764706, green: 0.2470588235, blue: 0.9019607843, alpha: 1), #colorLiteral(red: 0.4823529412, green: 0.2156862745, blue: 0.2078431373, alpha: 1), #colorLiteral(red: 0.2705882353, green: 0.862745098, blue: 1, alpha: 1), #colorLiteral(red: 0.2156862745, green: 0.9960784314, blue: 0.4392156863, alpha: 1), #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.2196078431, alpha: 1)]

func getRandomColor()->UIColor{
    let randomNumber = Int(arc4random_uniform(UInt32(4)))
    return imageBgcolor[randomNumber]
}

func randomString(length: Int) -> String {
    
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
}

func writeData(_ data:Data, fileName:String) -> String?{
    
    let url = URL(fileURLWithPath: fileName.replacingOccurrences(of: "file:///", with: ""))
    do{
        try data.write(to: url, options: .atomic)
        return url.absoluteString
    }
    catch{
        return nil
    }
}

func uploadVideosToAws(_ videos: [Data], folderName:String, randomName:String , callback:@escaping (([String]?)->())) {
    
    var uploadUrls = [String]()
    
    var count = videos.count
    for video in videos{
        
        if let documentsPathURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            //This gives you the URL of the path
            let randomFileName = randomName + ".mp4"
            let imageFileName = folderName + randomFileName
            
            do{
                //crate upload request
                let uploadRequest = AWSS3TransferManagerUploadRequest()!
                uploadRequest.bucket = bucket
                uploadRequest.acl = .publicRead
                //                    uploadRequest.uploadProgress = {(bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                //
                //                        print("Uploading \(bytesSent)")
                //
                //                    }
                
                let localUrl = documentsPathURL.appendingPathComponent(randomFileName)
                //copy files to crate url
                try video.write(to: localUrl)
                uploadRequest.contentType = "video/mp4"
                
                //set upload body and filename
                uploadRequest.body = localUrl
                uploadRequest.key = imageFileName
                
                let transferManager = AWSS3TransferManager.default()
                transferManager.upload(uploadRequest).continueWith { (task:AWSTask) -> Any? in
                    
                    if let error = task.error {
                        print("Upload failed with error: (\(error.localizedDescription))")
                        print("Upload failed with exception (\(task.description))")
                        count = -1
                        DispatchQueue.main.async {
                            callback(nil)
                        }
                    }
                    
                    if task.result != nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                        print("Uploaded to:\(publicURL!)")
                        
                        if let publicURL = publicURL{
                            
                            uploadUrls.append(publicURL.absoluteString)
                            try! FileManager.default.removeItem(at: localUrl)
                        }
                        count = count - 1
                        if count >= 0{
                            if count == 0{
                                DispatchQueue.main.async {
                                    callback(uploadUrls)
                                }
                            }
                        }
                    }
                    else{
                        print("Upload Error uploading file to aws")
                        DispatchQueue.main.async {
                            callback(nil)
                        }
                    }
                    return nil
                }
            }
            catch{
                print("Error writing file")
                count = -1
                DispatchQueue.main.async {
                callback(nil)
                }
            }
        }
    }
}

func uploadImagesToAws(_ images: [UIImage], folderName:String, randomName:String, callback:@escaping (([String]?)->())) {
    
    var uploadUrls = [String]()
    
    var count = images.count
    for img in images{
        
        
        //This gives you the URL of the path
        var randomFileName = ""
        if images.count <= 1 {
            randomFileName = randomName + ".jpg"
        }else{
            randomFileName = randomString(length: 20) + ".jpg"
        }
        
        let imageFileName = folderName + randomFileName
        
        do{
            
            let transferUtility = AWSS3TransferUtility.default()
            let uploadExpression = AWSS3TransferUtilityUploadExpression()
            uploadExpression.setValue("AES256", forRequestHeader: "x-amz-server-side-encryption")
            
            uploadExpression.progressBlock = {(task, progress) in
                print("Upload progress: ", progress.fractionCompleted)
            }
            
            let uploadCompletionHandler = { (task: AWSS3TransferUtilityUploadTask, error: Error?) -> Void in
                if let error = error {
                    //Error completing transfer. Handle Error
                    print("Upload failed with error: (\(error.localizedDescription))")
                    print("Upload failed with exception (\(task.description))")
                    count = -1
                    DispatchQueue.main.async {
                        callback(nil)
                    }
                }
                else {
                    //Successfully uploaded.
                    
                }
            }
            
            transferUtility.uploadData(
                UIImageJPEGRepresentation(img, compression)!,
                bucket: bucket,
                key: imageFileName,
                contentType: "image/jpg",
                expression: uploadExpression,
                completionHandler: uploadCompletionHandler
                ).continueWith (block: { (task) -> Any? in
                    if let error = task.error {
                        print(error.localizedDescription)
                        //Error initiating transfer. Handle error
                        count = -1
                        DispatchQueue.main.async {
                            callback(nil)
                        }
                        return nil
                    }
                    if task.isCompleted{
                        
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(bucket).appendingPathComponent(imageFileName)
                        print("Uploaded to:\(publicURL!)")
                        
                        if let publicURL = publicURL{
                            
                            uploadUrls.append(publicURL.absoluteString)
                            SDWebImageManager.shared().saveImage(toCache: img, for: publicURL)
                        }
                        count = count - 1
                        if count >= 0{
                            if count == 0{
                                DispatchQueue.main.async {
                                    callback(uploadUrls)
                                }
                            }
                        }
                    }
                    
                    return nil
                })
            
        }
        
    }
}

func deleteAwsFile(folderName:String, fullPath:String){
    
    if let fileName = URL(string: fullPath)?.lastPathComponent{
        let s3 = AWSS3.default()
        if let deleteObjectRequest = AWSS3DeleteObjectRequest(){
            deleteObjectRequest.bucket = bucket
            deleteObjectRequest.key = folderName+fileName
            s3.deleteObject(deleteObjectRequest).continueWith { (task) -> Any? in
                if let error = task.error {
                    print("Error occurred: \(error)")
                }
                else{
                    print("Deleted successfully.")
                }
                return nil
            }
        }
    }
}

func getAlertController(title:String?, message:String?) -> UIAlertController{
    var alertController:UIAlertController!
    
    if UIDevice.current.userInterfaceIdiom == .pad{
        alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    }
    else{
        alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
    }
    return alertController
}

func formatDateForDisplayOrder(date:Date) -> String{
    
    let formatter = DateFormatter()
    
    formatter.dateFormat = "EEE dd-MMM-yyyy hh:mm a"
    
    return formatter.string(from: date)
    
}

func formatDateForDisplayBids(date:Date) -> String{
    
    let formatter = DateFormatter()
    
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    let myString = formatter.string(from: date)
    let yourDate = formatter.date(from: myString)
    
    formatter.dateFormat = "dd-MMM-yyyy"
    
    return formatter.string(from: yourDate!)
    
}

func getRefreshControl(attrStr:NSAttributedString) -> UIRefreshControl {
    
    let refreshControl = UIRefreshControl()
    
    refreshControl.backgroundColor = .clear
    refreshControl.tintColor = BLUECOLOR
    refreshControl.attributedTitle = attrStr
    
    return refreshControl
}

func openCallingApp(phoneNumber:String){
    if let url = URL(string: "tel://\(phoneNumber)") {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url as URL)
        }
    }
}

func openWhatsApp(phoneNumber:String, message:String?) {
    
    var  urlWhats = ""
    
    if let message = message{
        urlWhats = "whatsapp://send?phone=\(phoneNumber)&text=\(message)"
    }
    else{
        urlWhats = "whatsapp://send?phone=\(phoneNumber)"
    }
    
    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
        if let whatsappURL = URL(string: urlString){
            if UIApplication.shared.canOpenURL(whatsappURL){
                UIApplication.shared.open(whatsappURL, options: [:]) { (Bool) in
                    
                }
            }else{
                print("Whatsapp not installed")
            }
        }
    }
}

func fetchContact() -> [CNContact] {
    
    let contactStore = CNContactStore()
    let keysToFetch = [ CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey,
                        CNContactImageDataAvailableKey, CNContactThumbnailImageDataKey, CNContactImageDataKey] as! [CNKeyDescriptor]
    
    // Get all the containers
    var allContainers: [CNContainer] = []
    do {
        allContainers = try contactStore.containers(matching: nil)
    } catch {
        print("Error fetching containers")
    }
    
    var results: [CNContact] = []
    
    // Iterate all containers and append their contacts to our results array
    
    for container in allContainers {
        
        let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
        
        do {
            let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch )
            
            results.append(contentsOf: containerResults)
        } catch {
            print("Error fetching results for container")
        }
    }
    return results
}

class WebAPI{
    
    public static func isReachable() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
    }
}

func getLoginDialouge(){
    
    let alert = getAlertController(title: "Login Required", message: "This feature is not available without login!")
    
    alert.addAction(UIAlertAction(title: "Login", style: .default, handler: { (_) in
        appDel.topViewController?.view.window?.rootViewController?.dismiss(animated: true, completion: {
          
        })
    }))
    
    alert.addAction(UIAlertAction(title: "Later", style: .destructive, handler: { (_) in
    }))
    
    appDel.topViewController?.present(alert, animated: true, completion: nil)
}
