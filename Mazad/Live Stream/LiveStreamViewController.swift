//Stream: Status 7 msg -> NetStream.Play.UnpublishNotify

import UIKit
import AVKit
import R5Streaming

enum StreamType{
    case subscriber
    case publisher
}

class LiveStreamViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    let BidTimeInterval = 180
    
    var threeMinuteTimer:Timer?
    
    var startTime:Date!
    
    var productWonVC:ProductWonViewController?
    
    var auctionItems:[AuctionItemModel]!
    
    var currentItemIndex = -1
    
    var customAlertMakeBidViewController:CustomAlertMakeBidViewController?
    
    var streamName:String!
    
    var selectedCountry = [CountryModel]()
    
    var timer:Timer?
    
    var showBookmarkedBids = false
    
    var displayType = StreamType.publisher
    
    var publisher:PublishViewController!
    
    @IBOutlet weak var controlsView: UIView!
    
    var subscriber: SubcribeViewController!
    
    var stream:MazadStream?
    
    @IBOutlet weak var btnTimer: UIButton!
    @IBOutlet weak var btnNextItem: UIButton!
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnLive: UIButton!
    @IBOutlet weak var lblViewCount: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var btnViewBids: UIButton!
    
    @IBOutlet weak var btnInfo: UIButton!
    
    @IBOutlet weak var btnCountView:UIView!
    @IBOutlet weak var btnClose:UIButton!
    @IBOutlet weak var btnSwitchCamera:UIButton!
    @IBOutlet weak var btnLight:UIButton!
    @IBOutlet weak var btnMicrophone:UIButton!
    
    //
    @IBOutlet weak var btnNextItemLeadingConstraint: NSLayoutConstraint!
    //
    
    var secondsAuctionTimer = 0
    var seconds = 1
    
    func updateUI(hide:Bool){
//        self.btnCountView.isHidden = hide
//        self.btnLive.isHidden = hide
//        self.btnSwitchCamera.isHidden = hide
//        self.btnMicrophone.isHidden = hide
//        self.btnLight.isHidden = hide
////        self.btnClose.isHidden = hide
//        
//        self.commentView.isHidden = hide
        
        
        if !hide{
            self.startTimer()
        }
        
    }
    
    func startTimer(){
        
        if self.timer != nil{
            return
        }
        
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (_) in
            
            self.seconds = self.seconds + 1
            
            self.lblTime.text = " " + self.printSecondsToHoursMinutesSeconds(seconds: self.seconds) + " "
            
            if self.displayType == .publisher{
                
                if (self.seconds % 50)  == 0{
                    
                    self.updateThumbImageOnServer()
                    
                }
            }
        })
    }
    
    func updateThumbImageOnServer(){
        //update thumb image for stream
        if self.publisher.stream != nil{
            
            let randomName = randomString(length: 20)
            
            if let image = self.publisher.stream.getImage(){
                if let folder = appDel.streamImageFolder{
                    uploadImagesToAws([image], folderName: folder, randomName: randomName, callback: {  (urls) in
                        if let url = urls?.first{
                            if let streamId = self.stream?.id{
                                JSONRequest.makeRequest(kUpdateThumb, parameters: ["image_url": url, "stream_id": streamId ], callback: nil)
                            }
                        }
                    })
                }
            }
        }
    }
    
    var audioSession:AVAudioSession!
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        
        // Prepare Audio Session
        self.audioSession = AVAudioSession.sharedInstance()
        do{
            
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord, mode: AVAudioSessionModeVideoChat, options: .defaultToSpeaker)
            
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        }
        catch let error{
            
            self.displayMessage("Failed to activate Audio Session. \(error.localizedDescription)") { () -> (Void) in
                self.dismiss(animated: true, completion: nil)
            }
            
            return
        }
        self.startTime = Date()
        
        self.secondsAuctionTimer = BidTimeInterval
        
        //create stream name
        if let userName = MyProfile.userName?.trim().replacingOccurrences(of: " ", with: ""){
           self.streamName = userName + "_\(Int(Date().timeIntervalSince1970))"
        }
        
        self.updateUI(hide: true)
        
        self.controlsView.backgroundColor = .clear

        txtField.placeHolderColor = .white
        
        if self.displayType == .publisher{
           
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "publishView") as? PublishViewController{
                
                self.publisher = vc
                
                publisher.streamName = self.streamName
                
                self.view.addSubViewWithConstraints(view: self.publisher.view)
                
                self.view.sendSubview(toBack: self.publisher.view)
                
                self.publisher.callback = {  status, msg in
                    
                    guard let status = status else {
                        
                        self.stopPublishing()
                        
                        self.displayMessage("Status nil", callback: {  () -> (Void) in
                        
                            self.close()
                            
                        })
                        
                        return
                    }
                    switch(status){
                    case .ERROR, .TIMEOUT, .CLOSE, .LICENSE_ERROR:
                        print("Error is => \(msg)")
                        self.stopPublishing()
                        
                        if status == .LICENSE_ERROR{
                            
                        }
                        else{
                            self.displayMessage("Stream Error [CLB_LS]", callback: {  () -> (Void) in
                                
                                self.close()
                                
                            })
                        }
                        
//                    case .DISCONNECTED:
//                        
//                        self.stopPublishing()
//                        
//                        self.displayMessage("Stream Error [CLB_DISCONNECTED]", callback: { () -> (Void) in
//                            
//                            self.close()
//                            
//                        })
                        
                    case .START_STREAMING:
                        
                        self.createStreamOnServer()
                        
                    default: break
                    }
                }
                
                
            }
            
            self.commentView.fd_collapsed = true
            
        }
        else{
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "subscribeView") as? SubcribeViewController{

                self.subscriber = vc

                subscriber.streamDetails = self.stream

                view.addSubViewWithConstraints(view: self.subscriber.view)

                view.sendSubview(toBack: subscriber.view)
                
                self.subscriber.callback = {  status, msg in
                    
                    guard let status = status else {
                        print("Stream: Status NA msg -> \(msg)")
                        return
                    }
                    
                    print("Stream: Status \(status.rawValue) msg -> \(msg)")
                    
                    self.lblTime.text = msg
                    
                    switch(status){
                        
                    case .VIDEO_RENDER_START:
                        
                        if let streamId = self.stream?.id, let ms = messageCenter, ms.connected{
                            
                            messageCenter?.joinStream(streamId: streamId, isJoin: true)
                            
                            self.btnInfo.isHidden = false
                            
                            ms.addComment(comment: "joined..", type: .text, streamId: streamId)
                            
                            self.view.viewWithTag(152)?.removeFromSuperview()
                            
                            NotificationCenter.default.addObserver(self, selector: #selector(self.onUpldateStreamNotification(not:)), name: .UpdateStreamNotification, object: nil)
                            
                            NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateStreamProductNotification(not:)), name: .UpdateStreamProductNotification, object: nil)
                            
                            self.updateUI(hide: false)
                            
                            
                        }
                        else{
                            
                            self.endSubscription()
                            
                            self.displayMessage("Failed to join stream [SIDNIL]", callback: {  () -> (Void) in
                                
                                self.close()
                                
                            })
                        }
                    case .ERROR:
                        self.endSubscription()
                        
                        self.displayMessage("Stream Error \(msg)", callback: {  () -> (Void) in
                            
                            self.close()
                            
                        })
                    case .TIMEOUT:
                        self.endSubscription()
                        
                        self.displayMessage("Stream Error \(msg)", callback: {  () -> (Void) in
                            
                            self.close()
                            
                        })
                    case .LICENSE_ERROR:
                        self.endSubscription()
                        
                        self.displayMessage("Stream Error \(msg)", callback: {  () -> (Void) in
                            
                            self.close()
                            
                        })
                    case .DISCONNECTED:
                        
                        self.endSubscription()
                        
                        self.displayMessage("Stream Error \(msg)", callback: {  () -> (Void) in
                            
                            self.close()
                            
                        })
                    case .NET_STATUS:
                        
                        if msg == "NetStream.Play.UnpublishNotify" {
                            
                            if let _ = appDel.topViewController as? LiveStreamViewController{
                                if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "StreamEndViewController") as? StreamEndViewController{
                                    
                                    vc.modalPresentationStyle = .overCurrentContext
                                    vc.modalTransitionStyle = .crossDissolve
                                    
                                    vc.callback = {
                                        self.close()
                                    }
                                    
                                    appDel.topViewController?.present(vc, animated: true, completion: {
                                        
                                        self.timer?.invalidate()
                                        
                                        if self.displayType == .subscriber{
                                            self.endSubscription()
                                        }
                                        else{
                                            self.stopPublishing()
                                        }
                                    })
                                }
                            }
                            
                        }else{
                            self.endSubscription()
                            
                            self.displayMessage("Failed to connect Live Stream, Try again later!!", callback: { () -> (Void) in
                                self.close()
                            })
                        }
                    
                    default:break
                    }
                }
                
                self.createSubscription()
            }
            
            self.controlsView.isHidden = true
            
        }
        
        self.tableView.backgroundColor = UIColor.gray.withAlphaComponent(0.2)
        self.tableView.CornerRadious = 2.0
        self.updateStreamDetailsOnUI()
        
    }
    
    deinit {
        
        self.timer?.invalidate()
        self.threeMinuteTimer?.invalidate()
        
        if self.displayType == .subscriber{
            self.subscriber.stop()
        }
        else{
            self.publisher.stop()
        }
        
    }
    
    func updateStreamDetailsOnUI(){
        
        guard let stream = self.stream else{
            
            self.lblViewCount.text = "0"
            
            if let name = MyProfile.userName{
                self.lblUserName.text = name.capitalized
            }
            else{
                self.lblUserName.text = NSLocalizedString("loading..", comment: "loading..")
            }
            
            if let profilePhoto = MyProfile.userProfilePhoto, profilePhoto != ""{
                print("user profile photo ==> \(profilePhoto)")
                self.imgUser.sd_setImage(with: URL(string: profilePhoto), placeholderImage: #imageLiteral(resourceName: "icon_profile_grey2"), options: [], progress: nil, completed: nil)
            }
            
            self.btnViewBids.fd_collapsed = true
            
            return
        }
        
        if let user = stream.userDetails.first{
            self.lblUserName.text = user.username
            if user.profilePhoto != nil && user.profilePhoto != ""{
                print("user profile photo ==> \(user.profilePhoto!)")
                self.imgUser.sd_setImage(with: URL(string: user.profilePhoto!), placeholderImage: #imageLiteral(resourceName: "icon_profile_grey2"), options: [], progress: nil, completed: nil)
            }
        }
        
        self.lblViewCount.text = "\(stream.viewerCount!)"
        
        if stream.bookmarkedBids.count > 0{
            self.btnViewBids.fd_collapsed = false
        }
        else{
            self.btnViewBids.fd_collapsed = true
        }
        
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func printSecondsToHoursMinutesSeconds (seconds:Int) -> String {
        
        let (h, m, s) = secondsToHoursMinutesSeconds (seconds: seconds)
        
        let hr = h >= 10 ? "\(h)" : "0\(h)"
        let mn = m >= 10 ? "\(m)" : "0\(m)"
        let sec = s >= 10 ? "\(s)" : "0\(s)"
        
        return "\(hr):\(mn):\(sec)"
    }
    
    func printSecondsTosMinutesSeconds (seconds:Int) -> String {
        
        let (_, m, s) = secondsToHoursMinutesSeconds (seconds: seconds)
        
        let mn = m >= 10 ? "\(m)" : "0\(m)"
        let sec = s >= 10 ? "\(s)" : "0\(s)"
        
        return "\(mn):\(sec)"
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
       
    }
    
     @objc func onUpdateStreamProductNotification(not:Notification){
        
        guard let stream = self.stream else{
            return
        }
        
        if let streamObj = not.userInfo?["stream"] as? MazadStream{
            
            if stream.id == streamObj.id{
                if let _ = streamObj.currentItem{
                    
                    if let obj = stream.comments.last {
                        stream.comments.removeAll()
                        stream.comments.append(obj)
                    }
                    
                    self.tableView.reloadData()
                    
                    self.productWonVC?.removeFromParentViewController()
                    self.productWonVC?.view.removeFromSuperview()
                    self.productWonVC = nil
                    
                    
                    self.stream?.currentItem = streamObj.currentItem
                    self.btnInfo.isHidden = false
                    
                    if self.displayType == .publisher{
                        
                        self.btnTimer.isHidden = false
                        self.btnNextItem.isHidden = false
                        
                        if self.currentItemIndex < self.auctionItems.count - 1 {
                            self.btnNextItem.fd_collapsed = false
                            self.btnNextItemLeadingConstraint.constant = 20.0
                        }else{
                            self.btnNextItem.fd_collapsed = true
                            self.btnNextItemLeadingConstraint.constant = 0.0
                        }
                    }
                }
            }
        }
    }
    //this is to stop multiple dialogs
    var awardedComment = [MazadComment]()
    
    @objc func onUpldateStreamNotification(not:Notification){
        
        guard let stream = self.stream else{
            return
        }
        if let streamObj = not.userInfo?["stream"] as? MazadStream{
            
            if stream.id == streamObj.id{
            
                //copy required values
                self.stream?.viewerCount = streamObj.viewerCount
                self.stream?.userDetails.first?.username = streamObj.userDetails.first?.username
                self.stream?.userDetails.first?.profilePhoto = streamObj.userDetails.first?.profilePhoto
                
                self.customAlertMakeBidViewController?.dismiss(animated: false, completion: nil)
                
                if self.displayType == .subscriber{
                    if let comment = streamObj.comments.first(where: { (c) -> Bool in
                        return c.isApproved && c.amount != nil
                    }){
                        
                        //this is to stop multiple award pop up dialog
                        if nil == self.awardedComment.firstIndex(where: { (c) -> Bool in
                            return c.id == comment.id
                        }){
                            self.awardedComment.append(comment)
                            if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "ProductWonViewController") as? ProductWonViewController{
                                
                                self.productWonVC = vc
                                
                                vc.comment = comment
                                
                                self.addChildViewController(vc)
                                vc.willMove(toParentViewController: self)
                                
                                self.view.addSubViewWithConstraints(view: vc.view)
                                
                                vc.lblSold.text = "This item is Sold for KD \(comment.amount!)"
                                
                                vc.closeCallback = {
                                    
                                    self.stream?.comments.removeAll()
                                    
                                    self.tableView.reloadData()
                                    
                                    self.productWonVC = nil
                                    
                                }
                            }
                        }
                    }
                }
                
                if streamObj.status == "stopped"{
                    
                   self.customAlertMakeBidViewController?.dismiss(animated: false, completion: nil)
                    
//                    if let comment = streamObj.comments.first(where: { (c) -> Bool in
//                        return c.isApproved && c.amount != nil
//                    }){
//                        self.lblSold.alpha = 0.0
//                        self.lblSold.isHidden = false
////                        if let name = comment.user.first?.username{
//                            self.lblSold.textColor = .green
////                            self.lblSold.text = "This item is Sold to \(name) for KD \(comment.amount!)"
//                        self.lblSold.text = "This item is Sold for KD \(comment.amount!)"
//                            UIView.animate(withDuration: 0.25) {
//                                self.lblSold.alpha = 1.0
//                            }
////                        }
//                    }
//
                    
                    if let _ = appDel.topViewController as? LiveStreamViewController{
                        if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "StreamEndViewController") as? StreamEndViewController{
                            
                            vc.modalPresentationStyle = .overCurrentContext
                            vc.modalTransitionStyle = .crossDissolve
                            
                            vc.callback = {
                                self.close()
                            }
                            
                            appDel.topViewController?.present(vc, animated: true, completion: {
                                
                                self.timer?.invalidate()
                                
                                if self.displayType == .subscriber{
                                    self.endSubscription()
                                }
                                else{
                                    self.stopPublishing()
                                }
                            })
                        }
                    }
                    
                }
                else{
                    if streamObj.comments.count > 0{
                        for comment in streamObj.comments{
                            if let _ = stream.comments.firstIndex(where: { (cmt) -> Bool in
                                return comment.id == cmt.id
                            }){
                                
                            }
                            else{
//                                //if the comment is before the start date
//                                if let createAt = comment.createdAt, createAt > self.startTime{
//                                    
//                                    print("qqqq createAt \(createAt)")
//                                    print("qqqq startTime \(self.startTime)")
//                                    
//                                    return;
//                                }
                                
                                self.stream?.comments.append(comment)
                                
                                if !self.showBookmarkedBids{
                                    if self.stream?.comments.count == 1{
                                        self.tableView.reloadData()
                                    }
                                    else{
                                        let indexPath = IndexPath(row: self.stream!.comments.count-1, section: 0)
                                        self.tableView.insertRows(at: [indexPath], with: .fade)
                                        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                                    }
                                }
                            }
                        }
                    }
                    //update the UI
                    self.updateStreamDetailsOnUI()
                }
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self)
        
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
    }
    
     @IBAction func onInfoClick(_ sender: UIButton) {
        
        guard let stream = self.stream else{
            return
        }
        
        if let item = self.stream?.currentItem{
            if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "ProductInfoViewController") as? ProductInfoViewController{
                
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                
                vc.item = item
                
                if self.auctionItems != nil{
                    vc.currentItem = self.currentItemIndex + 1
                    vc.itemCount = self.auctionItems.count
                }
                
                appDel.topViewController?.present(vc, animated: true, completion: nil)
            }
        }
        else{
           
            JSONRequest.makeRequest(kGetCurrentStreamProduct, parameters: ["stream_id": stream.id!], callback: {  (data, error) in
                
                if let data = data as? [String:Any]{
                    if let desc = data["description"] as? [String:Any]{
                        if let product = desc["current_product"] as? [String:Any]{
                            
                            self.stream?.currentItem = AuctionItemModel(data: product)
                            
                            if let item = self.stream?.currentItem{
                                if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "ProductInfoViewController") as? ProductInfoViewController{
                                    
                                    vc.modalPresentationStyle = .overCurrentContext
                                    vc.modalTransitionStyle = .crossDissolve
                                    
                                    vc.item = item
                                    
                                    if self.auctionItems != nil{
                                        
                                        vc.currentItem = self.currentItemIndex + 1
                                        vc.itemCount = self.auctionItems.count
                                    }
                                    
                                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }
                
            })
        }
        
    }
    
    @IBAction func onCloseClick(_ sender: UIButton) {
       
        if self.displayType == .publisher{
            self.stopPublishing()
        }
        else{
            self.endSubscription()
        }
        
        self.stopStreamOnServer()
        
        self.close()
        
    }
    func close(){
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func createStreamOnServer(){
        
        
        var selectedCountryIdString = ""
        
        for country in self.selectedCountry{
            
            if selectedCountryIdString == ""{
                if let id = country._id{
                    selectedCountryIdString = selectedCountryIdString + id
                }
            }
            else if let id = country._id{
                selectedCountryIdString = selectedCountryIdString + "," + id
            }
        }
        
        if selectedCountryIdString == ""{
            self.displayMessage("Please select country!!", callback: {
            
                self.stopPublishing()
                
                self.close()
                
            })
            
            return
        }
        
        //call the api to create stream
        let wait = appDel.topViewController?.addWaitSpinner()
        
        //call api to create live stream
        let params = ["stream_name": self.streamName!,
            "status": "running",
            "user_id": "\(MyProfile.userId!)",
            "country_id": selectedCountryIdString,
            "start_price": self.auctionItems[0].startingPrice!]
        
        JSONRequest.makeRequest(kCreateLiveStream, parameters: params, callback: { (data, error) in
            
            if let wait = wait{
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any]{
                
                NotificationCenter.default.addObserver(self, selector: #selector(self.onUpldateStreamNotification(not:)), name: .UpdateStreamNotification, object: nil)
                
                NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateStreamProductNotification(not:)), name: .UpdateStreamProductNotification, object: nil)
                
                if let err = data["error"] as? Bool, !err{
                    if let dict = data["stream"] as? [String:Any]{
                        if let userDict = data["userDetails"] as? [String:Any]{
                            
                            self.stream = MazadStream(fromDictionary: dict)
                            
                            self.stream?.userDetails = [MazadUserDetail(fromDictionary: userDict)]
                            
                            self.publisher.streamDetails = self.stream
                            
                            self.updateUI(hide: false)
                            
                            self.updateProductForStream()
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
                                
                                //update thumb image for stream
                                if self.publisher.stream != nil{
                                    let randomName = randomString(length: 20)
                                    if let image = self.publisher.stream.getImage(){
                                        if let folder = appDel.streamImageFolder{
                                            uploadImagesToAws([image], folderName: folder, randomName: randomName, callback: { (urls) in
                                                if let url = urls?.first{
                                                    if let streamId = self.stream?.id{
                                                        JSONRequest.makeRequest(kUpdateThumb, parameters: ["image_url": url, "stream_id": streamId ], callback: nil)
                                                    }
                                                }
                                            })
                                        }
                                    }
                                }
                            })
                            return
                        }
                    }
                }
                
                if let msg = data["message"] as? String{
                    self.displayMessage(msg, callback: { () -> (Void) in
                        self.stopPublishing()
                        self.close()
                    })
                }
                else{
                    self.displayMessage("Failed to create Live Stream!! Please try again.", callback: { () -> (Void) in
                        self.stopPublishing()
                        self.close()
                    })
                }
            }
            else{
                self.displayMessage("Failed to create Live Stream!! Please try again.", callback: {() -> (Void) in
                    self.stopPublishing()
                    self.close()
                })
            }
        })
       
    }
    
    func updateProductForStream(){
        
        //will recieve 'updateproduct' on socket
        
        self.currentItemIndex = self.currentItemIndex + 1
        //stream_id: ObjectId(stream_id), image_url: req.body.image_url, product_serial: req.body.product_serial, price: req.body., paymet_type: req.body.paymet_type, is_sold: req.body.is_sold, description: req.body.description
        
        let wait = appDel.topViewController?.addWaitSpinner()
        let randomName = randomString(length: 20)
        if let streamId = self.stream?.id{
            let currentItemToSet = self.auctionItems[self.currentItemIndex]
            if let image = currentItemToSet.image{
                uploadImagesToAws([image], folderName: appDel.postImagesFolder!, randomName: randomName) { (urls) in
                    if let url = urls?.first{
                        
                        let params = ["stream_id": "\(streamId)",
                            "image_url": url,
                            "product_serial": currentItemToSet.productSerial!,
                            "start_price": currentItemToSet.startingPrice!,
                            "payment_type": currentItemToSet.paymentMethod!,
                            "is_sold": "false",
                            "description": currentItemToSet.description!]
                        
                        JSONRequest.makeRequest(kUpdateStreamProduct, parameters: params, callback: { (data, error) in
                            
                            if let wait = wait{
                                appDel.topViewController?.removeWaitSpinner(waitView: wait)
                            }
                            
                            
                            if let data = data as? [String:Any]{
                                
                                if let res = data["success"] as? Bool, res == false{
                                    
                                    //rollback
                                    self.currentItemIndex = self.currentItemIndex - 1
                                    deleteAwsFile(folderName: url, fullPath: appDel.postImagesFolder!)
                                    
                                    self.showErrorUpdateAlert()
                                    
                                }
                                else{
                                    
                                    //remove all current comments as the product is updated on server
                                    self.stream?.comments.removeAll()
                                    
                                    self.tableView.reloadData()
                                    
                                    if self.currentItemIndex != 0 {
                                        if let streamId = self.stream?.id{
                                            let text = NSLocalizedString("Auction Item has been Changed!", comment: "Auction Item has been Changed!")
                                            
                                            messageCenter?.addComment(comment: text, type: .text, streamId: streamId)
                                        }
                                    }
                                    
                                }
                            }
                            else{
                                
                                //rollback
                                self.currentItemIndex = self.currentItemIndex - 1
                                deleteAwsFile(folderName: url, fullPath: appDel.postImagesFolder!)
                                
                                self.showErrorUpdateAlert()
                            }
                            
                        })
                        
                    }
                    else{
                        if let wait = wait{
                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                        }
                        
                        //rollback
                        self.currentItemIndex = self.currentItemIndex - 1
                        
                        self.showErrorUpdateAlert()
                    }
                }
            }
        }
        
    }
    
    func showErrorUpdateAlert(){
        
        let alert = getAlertController(title: "", message: "Failed to update product info. Retry?")
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
            
            self.updateProductForStream()
            
        }))
        alert.addAction(UIAlertAction(title: "No, Close Stream", style: .destructive, handler: { (_) in
            
            self.close()
            
        }))
        
        appDel.topViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    func startPublishing(){
        
        self.publisher.start()
        
    }
    func stopPublishing(){
        
        if let _ = self.streamName{
            self.stopStreamOnServer()
        }
        
        self.publisher.stop()
        
    }
    func createSubscription(){
        
        self.subscriber.start()
        
    }
    func endSubscription(){
        
        self.subscriber.stop()
        
    }
    func stopStreamOnServer(){
        
        if self.displayType == .publisher{
            
            if let stream = self.stream{
                messageCenter?.stopStream(streamId: stream.id)
            }
            JSONRequest.makeRequest(kStopMyStream, parameters: ["user_id":MyProfile.userId!], callback: nil)
        }
        else{
            if let stream = self.stream{
                messageCenter?.joinStream(streamId: stream.id, isJoin: false)
            }
        }
   }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let stream = self.stream{
            if self.showBookmarkedBids{
                return stream.bookmarkedBids.count
            }
            else{
                return stream.comments.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let stream = self.stream else{
            let cell = UITableViewCell(style: .default, reuseIdentifier:  "cell")
            cell.textLabel?.text = "Waiting for Bids.."
            cell.textLabel?.font = UIFont.systemFont(ofSize: 13.0)
            cell.textLabel?.textColor = getRandomColor()
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            return cell
        }
        
        //return empty cell
        if (self.showBookmarkedBids && stream.bookmarkedBids.count <= 0) || stream.comments.count <= 0{
            
            let cell = UITableViewCell(style: .default, reuseIdentifier:  "cell")
            if self.showBookmarkedBids{
                cell.textLabel?.text = "No Bookmarked bids!!"
            }
            else{
                cell.textLabel?.text = "Waiting for Bids.."
            }
            cell.textLabel?.font = UIFont.systemFont(ofSize: 13.0)
            cell.textLabel?.textColor = getRandomColor()
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            return cell
            
        }
        
        
        
        var comment:MazadComment!
        if self.showBookmarkedBids{
            comment = stream.bookmarkedBids[indexPath.row]
        }
        else{
            comment = stream.comments[indexPath.row]
        }
        if comment.comment != nil{
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as! BidTableViewCell
            cell.comment = comment
            cell.indexPath = indexPath
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            cell.delegate = nil
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "bidCell") as! BidTableViewCell
            cell.comment = comment
            cell.indexPath = indexPath
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            cell.delegate = self
            if self.displayType == .subscriber{
                cell.btnBid.isHidden = true
                cell.btnPin.isHidden = true
            }
            else{
                cell.btnBid.isHidden = false
                cell.btnPin.isHidden = false
            }
            return cell
        }
    }
    
    @IBAction func onNextItemClick(_ sender: UIButton) {
        
        guard self.currentItemIndex != -1 else{
            return
        }
        
        if self.currentItemIndex < self.auctionItems.count - 1{
            self.updateProductForStream()
        }
    }
    
    @IBAction func onThreeMinTimerClick(_ sender: UIButton) {
        
        if let timer = self.threeMinuteTimer{
            
            timer.invalidate()
            self.secondsAuctionTimer = BidTimeInterval
            self.threeMinuteTimer = nil
            sender.setTitle(NSLocalizedString("Start 3 min. timer", comment: "Start 3 min. timer"), for: .normal)
            
            if let streamId = self.stream?.id{
                let text = NSLocalizedString("The seller has disabled the Timer!", comment: "The seller has disabled the Timer!")
                
                messageCenter?.addComment(comment: text, type: .text, streamId: streamId)
            }
        }
        else{
            self.startThreeMinutesTimer()
        }
        
    }
    
    
    func startThreeMinutesTimer(){
        
        self.threeMinuteTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (_) in
            
            //this is for first time to send a message in live stream
            if self.secondsAuctionTimer == self.BidTimeInterval{
                
                self.view.endEditing(true)
                
                if let streamId = self.stream?.id{
                
                    let text = NSLocalizedString("Hurry Up!! You have 3 minutes to win the product", comment: "Hurry!! You have 3 minutes to win the product")
                    
                    messageCenter?.addComment(comment: text, type: .text, streamId: streamId)
                }
                
            }
            else if self.secondsAuctionTimer == 120{
                
                self.view.endEditing(true)
                
                if let streamId = self.stream?.id{
                    
                    let text = NSLocalizedString("Hurry Up!! You have 2 minutes to win the product", comment: "Hurry!! You have 2 minutes to win the product")
                    
                    messageCenter?.addComment(comment: text, type: .text, streamId: streamId)
                }
            }
            else if self.secondsAuctionTimer <= 60{
                
                if self.secondsAuctionTimer == 60{
                    
                    self.view.endEditing(true)
                    
                    if let streamId = self.stream?.id{
                        
                        let text = NSLocalizedString("Hurry Up!! You have 1 minute to win the product", comment: "Hurry!! You have 1 minute to win the product")
                        
                        messageCenter?.addComment(comment: text, type: .text, streamId: streamId)
                    }
                }
                
                if self.secondsAuctionTimer == 30{
                    
                    self.view.endEditing(true)
                    
                    if let streamId = self.stream?.id{
                        
                        let text = NSLocalizedString("Hurry Up!! You have 30 seconds to win the product", comment: "Hurry!! You have 30 seconds to win the product")
                        
                        messageCenter?.addComment(comment: text, type: .text, streamId: streamId)
                    }
                }
                else if self.secondsAuctionTimer == 10{
                    
                    self.view.endEditing(true)
                    
                    if let streamId = self.stream?.id{
                        
                        let text = NSLocalizedString("Hurry Up!! You have 10 seconds to win the product", comment: "Hurry!! You have 10 seconds to win the product")
                        
                        messageCenter?.addComment(comment: text, type: .text, streamId: streamId)
                    }
                }
            }
            
            self.secondsAuctionTimer = self.secondsAuctionTimer - 1
            
            self.btnTimer.setTitle("Ending \(self.printSecondsTosMinutesSeconds(seconds: self.secondsAuctionTimer))", for: .normal)
            
            if self.secondsAuctionTimer == 0{
                
                if let streamId = self.stream?.id{
                    
                    let text = NSLocalizedString("Bids for this item has been closed", comment: "Bids for this item has been closed")
                    
                    messageCenter?.addComment(comment: text, type: .text, streamId: streamId)
                }
                
                //stop the timer and set the button title
                self.threeMinuteTimer?.invalidate()
                self.secondsAuctionTimer = self.BidTimeInterval
                self.threeMinuteTimer = nil
                self.btnTimer.setTitle(NSLocalizedString("Start 3 min. timer", comment: "Start 3 min. timer"), for: .normal)
                
                //grab the heighest bookmark bid
                if let bid = self.stream?.highestBookmarkedBid{
                    self.awardBid(bid)
                }//grab the heightest bids
                else if let bid = self.stream?.highestBid{
                    self.bookmarkBid(bid, streamId: self.stream!.id!, stream: self.stream!, callback: {
                        self.awardBid(bid)
                    })
                }
                else{
                    
                    if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "ProductSoldViewController") as? ProductSoldViewController{
                        
                        self.addChildViewController(vc)
                        
                        vc.willMove(toParentViewController: self)
                        
                        vc.callback = {  selectedOption in
                            
                            if selectedOption == .moveToNext{
                                vc.view.removeFromSuperview()
                                vc.removeFromParentViewController()
                                self.updateProductForStream()
                            }
                            else if selectedOption == .stopStream{
                                
                                vc.view.removeFromSuperview()
                                vc.removeFromParentViewController()
                                self.publisher.callback = nil
                                
                                //present the stream ended view
                                self.customAlertMakeBidViewController?.dismiss(animated: false, completion: nil)
                                
                                if let _ = appDel.topViewController as? LiveStreamViewController{
                                    if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "StreamEndViewController") as? StreamEndViewController{
                                        
                                        vc.modalPresentationStyle = .overCurrentContext
                                        vc.modalTransitionStyle = .crossDissolve
                                        
                                        vc.callback = {
                                            self.close()
                                        }
                                        
                                        appDel.topViewController?.present(vc, animated: true, completion: {
                                            if self.displayType == .publisher{
                                                self.stopPublishing()
                                            }
                                        })
                                    }
                                }
                            }
                            else{
                                vc.view.removeFromSuperview()
                                vc.removeFromParentViewController()
                            }
                        }
                        
                        self.view.addSubViewWithConstraints(view: vc.view)
                        vc.didMove(toParentViewController: self)
                        
                        if self.currentItemIndex >= (self.auctionItems.count-1){
                            vc.disableNextDisable()
                        }
                        
                        vc.lblMessage.text = NSLocalizedString("No Bids!!", comment: "No Bids!!")
                        vc.animationSubImageView.image = UIImage(named: "ic_info")
                        vc.btnContinueWithSameItem.isHidden = false
                    }
                }
            }
        })
    }
    
    @IBAction func onSwitchCameraClick(_ sender: UIButton) {
        
        self.publisher.switchCamera()
        
    }
    
    @IBAction func onFlashLightClick(_ sender: UIButton) {
        
        if let device = AVCaptureDevice.default(for: AVMediaType.video){
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        device.torchMode = AVCaptureDevice.TorchMode.off
                        sender.setImage(#imageLiteral(resourceName: "ic_light_on.png"), for: .normal)
                    } else {
                        try device.setTorchModeOn(level: 1.0)
                        sender.setImage(#imageLiteral(resourceName: "ic_light_off.png"), for: .normal)
                    }
                    device.unlockForConfiguration()
                } catch {
                    print(error)
                }
            }
        }
    }
    
    @IBAction func onMicClick(_ sender: UIButton) {
        
        if sender.image(for: .normal) == #imageLiteral(resourceName: "ic_mic_off"){
            self.publisher.stream.pauseAudio = true
            sender.setImage(#imageLiteral(resourceName: "ic_mic_on.png"), for: .normal)
        }
        else{
            self.publisher.stream.pauseAudio = false
            sender.setImage(#imageLiteral(resourceName: "ic_mic_off.png"), for: .normal)
        }
    }
    
    @IBAction func onSendClick(_ sender: Any) {
        
        self.view.endEditing(true)
        
        guard let streamId = self.stream?.id else{
            return
        }
        
        if let text = self.txtField.text, text.trim() != ""{
            messageCenter?.addComment(comment: text, type: .text, streamId: streamId)
            self.txtField.text = nil
        }
        else{
            let msg = NSLocalizedString("Enter Comment", comment: "Enter Comment")
            self.displayMessage(msg)
        }
    }
    
    @IBAction func onBidClick(_ sender: Any) {
        
        self.view.endEditing(true)
        
        guard let streamId = self.stream?.id else{
            return
        }
        
        if let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "CustomAlertMakeBidViewController") as? CustomAlertMakeBidViewController{
            
            self.customAlertMakeBidViewController = vc
            
            if let bid = self.stream?.highestBid, let amount = Double(bid.amount){
                vc.minPrice = amount
            }
            else {
                if self.displayType == .publisher{
                    if currentItemIndex != -1{
                        if let price = self.auctionItems[self.currentItemIndex].startingPrice, let doublePrice = Double(price){
                            vc.minPrice = doublePrice
                        }
                        else{
                            vc.minPrice = 1.0
                        }
                    }
                    else{
                        vc.minPrice = 1.0
                    }
                }
                else{
                    if let price = self.stream?.currentItem?.startingPrice, let doublePrice = Double(price){
                        vc.minPrice = doublePrice
                    }
                    else{
                        vc.minPrice = 1.0
                    }
                }
            }
            
            
            vc.type = .stream
            vc.id = streamId
            
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.callback = {  amount in
                
                var newAmount:String!
                
                if let doubleAmount = Double(amount) {
                    newAmount = String(format: "%.2f", doubleAmount)
                }else{
                    newAmount = amount
                }
                
                messageCenter?.addComment(comment: newAmount, type: .amount, streamId: streamId)
                self.customAlertMakeBidViewController = nil
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onShowBookMarkBidClick(_ sender: UIButton) {

        self.showBookmarkedBids = !self.showBookmarkedBids
        
        if self.showBookmarkedBids{
            sender.setTitle("View Live Comments", for: .normal)
        }
        else{
            sender.setTitle("View Bookmark Bids", for: .normal)
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self.tableView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.size.height)
        }) { (_) in
            self.tableView.reloadData()
            UIView.animate(withDuration: 0.25, animations: {
                self.tableView.transform = .identity
            })
        }
    }
    
    @IBAction func onMuteClick(_ sender: Any) {
        self.publisher.stream.pauseAudio = !self.publisher.stream.pauseAudio
    }
    
}
extension LiveStreamViewController: BidTableViewCellDelegate{
    
    func bookmarkBid(_ comment: MazadComment, streamId: String, stream: MazadStream, callback:(()->())?) {
        comment.bookmarked = !comment.bookmarked
        
        let params = ["stream_id":"\(streamId)",
            "comment_id": "\(comment.id!)",
            "is_bookMarked": comment.bookmarked ? "true" : "false"]
        
        JSONRequest.makeRequest(kBookmarkBid, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any]{
                print(data)
            }
        }
        
        if stream.bookmarkedBids.count > 0{
            self.btnViewBids.fd_collapsed = false
        }
        else{
            self.btnViewBids.fd_collapsed = true
            self.showBookmarkedBids = false
        }
        
        self.tableView.reloadData()
        
        callback?()
        
    }
    
    fileprivate func awardBid(_ comment:MazadComment) {
        
        if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "AwardBidViewController") as? AwardBidViewController{
            
            vc.stream = self.stream
            
            if let stream = self.stream{
                
                vc.comments = stream.bookmarkedBids
                
                if let index = vc.comments.firstIndex(where: {$0.id == comment.id}){
                    vc.comments.remove(at: index)
                }
                //sort bids by amount
                vc.comments = vc.comments.sorted(by: { (firstComment, secondComment) -> Bool in
                    
                    if let firstAmount = Double(firstComment.amount){
                        if let secondAmount = Double(secondComment.amount){
                            return firstAmount < secondAmount
                        }
                    }
                    return false
                })
                vc.comments.insert(comment, at: 0)
                vc.selectedIndex = [IndexPath(row: 0, section: 0)]
            }
            
            vc.item = self.auctionItems[self.currentItemIndex]
            
            vc.currentItem = self.currentItemIndex + 1
            
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            
            vc.callback = {
                
                
                if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "ProductSoldViewController") as? ProductSoldViewController{
                    
                    self.addChildViewController(vc)
                    
                    vc.willMove(toParentViewController: self)
                    
                    vc.callback = {  selectedOption in
                        
                        if selectedOption == .moveToNext{
                            
                            vc.view.removeFromSuperview()
                            
                            vc.removeFromParentViewController()
                            
                            self.updateProductForStream()
                            
                        }
                        else if selectedOption == .stopStream{
                            
                            vc.view.removeFromSuperview()
                            
                            vc.removeFromParentViewController()
                            
                            self.publisher.callback = nil
                            
                            //present the stream ended view
                            self.customAlertMakeBidViewController?.dismiss(animated: false, completion: nil)
                            
                            if let _ = appDel.topViewController as? LiveStreamViewController{
                                if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "StreamEndViewController") as? StreamEndViewController{
                                    
                                    vc.modalPresentationStyle = .overCurrentContext
                                    vc.modalTransitionStyle = .crossDissolve
                                    
                                    vc.callback = {
                                        self.close()
                                    }
                                    
                                    appDel.topViewController?.present(vc, animated: true, completion: {
                                        if self.displayType == .publisher{
                                            self.stopPublishing()
                                        }
                                    })
                                }
                            }
                        }
                        
                    }
                    
                    self.view.addSubViewWithConstraints(view: vc.view)
                    vc.didMove(toParentViewController: self)
                    
                    if self.currentItemIndex >= (self.auctionItems.count-1){
                        vc.disableNextDisable()
                    }
                    
                    //                            appDel.topViewController?.present(vc, animated: true, completion: nil)
                    
                }
            }
            
            appDel.topViewController?.present(vc, animated: true, completion: nil)
            
            
        }
    }
    
    func onBidCellAction(for action: BidTableCellAction, indexPath: IndexPath, comment: MazadComment) {
        
        guard let stream = self.stream else{
            return
        }
        
        guard let streamId = self.stream?.id else{
            return
        }
        switch action{
            
        case .bookmarkBid, .removeBookmarkBid:
            
            bookmarkBid(comment, streamId:streamId, stream:stream, callback:nil)
            
            //                //if bookmark bids
            //                if showBookmarkedBids{
            //                    if self.stream.bookmarkedBids.count > 0{
            //                        self.tableView.beginUpdates()
            //                        self.tableView.reloadRows(at: [indexPath], with: .none)
            //                        self.tableView.endUpdates()
            //                    }
            //                    else{
            //                        self.tableView.reloadData()
            //                    }
            //                }
            //                else{
            //                    self.tableView.beginUpdates()
            //                    self.tableView.reloadRows(at: [indexPath], with: .none)
            //                    self.tableView.endUpdates()
            //                }
            
        case .awardBid:
            
            if let _ = stream.bookmarkedBids.firstIndex(where: {$0.id == comment.id}){
                self.awardBid(comment)
            }
            else{
                self.bookmarkBid(comment, streamId:streamId, stream:stream, callback:{
                    self.awardBid(comment)
                })
            }
        }
    }
}
