//
//  ProductWonViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 11/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class ProductWonViewController: UIViewController {
    
    var closeCallback:(()->())!
    
    var comment:MazadComment!
    
    @IBOutlet weak var lblSold:UILabel!
    
    @IBOutlet weak var animationImageView: UIImageView!
    @IBOutlet weak var animationImageViewWidthConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let amount = self.comment.amount{
            if let user = self.comment.user.first?.username{
                self.lblSold.text = "This product is sold for KD \(amount) to \(user)"
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.animationImageViewWidthConstraint.constant = CGFloat(60)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setUpAnimation()
    }
    
    func setUpAnimation(){
        UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.animationImageViewWidthConstraint.constant = CGFloat(90)
            self.animationImageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/1.5)
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }, completion: nil)
    }
    
    
    @IBAction func onCloseClick(_ sender: Any) {
        
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
        
        self.closeCallback()
        
    }
}
