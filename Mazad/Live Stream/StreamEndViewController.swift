//
//  StreamEndViewController.swift
//  Mazad Live
//
//  Created by alienbrainz on 05/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class StreamEndViewController: UIViewController {
    
    var callback:(()->())!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    
    @IBAction func onDoneClick(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.callback()
        })
    }
    
}
