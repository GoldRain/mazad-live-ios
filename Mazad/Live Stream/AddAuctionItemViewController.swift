
//
//  AddAuctionItemViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 11/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class AddAuctionItemViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imgProductImage: UIImageView!
    @IBOutlet weak var txtProductSerial: UITextField!
    @IBOutlet weak var txtStartingPrice: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnCreditCard:UIButton!
    @IBOutlet weak var btnCash:UIButton!
    
    @IBOutlet weak var creditCardView: UIView!
    @IBOutlet weak var cashView: UIView!
    
    @IBOutlet weak var imgMainView: UIView!
    @IBOutlet weak var productSerialMainView: UIView!
    @IBOutlet weak var startingPriceMainView: UIView!
    @IBOutlet weak var descriptionMainView: UIView!
    
   
    
    var callback:((AuctionItemModel)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        
        self.title = NSLocalizedString("Add Product", comment: "Add Product")
        
        self.txtProductSerial.delegate = self
        self.txtDescription.delegate = self
        
        imgMainView.addShadow()
        productSerialMainView.addShadow()
        startingPriceMainView.addShadow()
        descriptionMainView.addShadow()
        
        creditCardView.addShadow()
        cashView.addShadow()

//        imgProductImage.image = #imageLiteral(resourceName: "bg_iPhone.png")
//        txtProductSerial.text = "Ser12233"
//        txtStartingPrice.text = "1"
//        txtDescription.text = "Sample description"
    }
    
    @IBAction func onImageClick(_ sender: UIButton) {
        
        let alert = getAlertController(title: "Choose Image", message: nil)

        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .destructive, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
             let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
         let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            imgProductImage.image = editedImage
            
        }else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgProductImage.image = originalImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onAddAuctionClick(_ sender: Any) {
        
        if validate() {
            
            let item = AuctionItemModel(productSerial: self.txtProductSerial.text!, startingPrice: self.txtStartingPrice.text!, description: self.txtDescription.text!, paymentMethod: self.paymentType.joined(separator: ","), image: self.imgProductImage.image!)
            callback!(item)
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    var paymentType = ["cc"]
    
    @IBAction func onSwitchClick(_ sender: UIButton) {
        
        if sender.currentImage == #imageLiteral(resourceName: "icon_switch_off"){
            sender.setImage(#imageLiteral(resourceName: "icon_switch_on"), for: .normal)
            if sender == self.btnCreditCard {
                self.paymentType.append("cc")
            }else if sender == self.btnCash {
                self.paymentType.append("cash")
            }
        }else{
            sender.setImage(#imageLiteral(resourceName: "icon_switch_off"), for: .normal)
            if sender == self.btnCreditCard {
                if let index = self.paymentType.firstIndex(of: "cc") {
                    self.paymentType.remove(at: index)
                }
            }else if sender == self.btnCash {
                if let index = self.paymentType.firstIndex(of: "cash") {
                    self.paymentType.remove(at: index)
                }
            }
        }
    }
    
    
    func validate() -> Bool {
        
        if self.imgProductImage.image == nil{
            let str = NSLocalizedString("Please Select Image", comment: "Please Select Image")
            self.displayMessage(str)
            return false
        }
        if isEmpty(self.txtProductSerial.text!){
            let str = NSLocalizedString("Please enter Product Serial", comment: "Please enter Product Serial")
            self.displayMessage(str)
            return false
        }
        if let price = self.txtStartingPrice.text, !price.isEmpty, let doublePrice = price.doubleValue, doublePrice > 0.0{
            if doublePrice >= AMOUNT {
                let str = NSLocalizedString("Maximum Price can not be more than 1000", comment: "Maximum Price can not be more than 1000")
                self.displayMessage(str)
                return false
            }
        }else{
            let str = NSLocalizedString("Please enter valid Price", comment: "Please enter valid rice")
            self.displayMessage(str)
            return false
        }
        if self.txtDescription.text == "Description" || self.txtDescription.text == "" {
            let str = NSLocalizedString("Please enter Details about the Product", comment: "Please enter Details about the Product")
            self.displayMessage(str)
            return false
        }
        if self.btnCreditCard.currentImage == UIImage(named: "icon_switch_off") && self.btnCash.currentImage == UIImage(named: "icon_switch_off") {
            let str = NSLocalizedString("Please Select Payment Method", comment: "Please Select Payment Method")
            self.displayMessage(str)
            return false
        }
        return true
    }
    
    //MARK:- TextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        
        guard let stringRange = Range(range, in: currentText) else {
            return false
        }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return updatedText.count <= 100
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description" {
            textView.text = ""
            textView.textColor = .black
        }
    }
    // MARK:- TextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 30
    }
    
}
