//
//  AuctionItemsViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 11/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class AuctionItemsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AuctionItemsCellDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    
    var auctionItems = [AuctionItemModel]()
    
    var selectedCountry:[CountryModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Auction Items", comment: "Auction Items")
        
        //right bar button
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        button.setImage(UIImage(named: "add"), for: .normal)
        button.addTarget(self, action: #selector(handleAddAuctionItem), for: .touchUpInside)
        
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: button), animated: true)
        
        //left bar button
        let button1 = UIButton(type: .custom)
        button1.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        button1.setImage(UIImage(named: "back_top_bar"), for: .normal)
        button1.addTarget(self, action: #selector(self.handleBackAction), for: .touchUpInside)
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: button1), animated: true)
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        
        tableView.separatorStyle = .none
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func handleBackAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleAddAuctionItem() {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAuctionItemViewController") as? AddAuctionItemViewController {
            
            vc.callback = {  item in
                
                self.auctionItems.append(item)
                self.tableView.reloadData()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.auctionItems.count > 0{
            tableView.separatorStyle = .singleLine
        }
        else{
            tableView.separatorStyle = .none
        }
        
        return auctionItems.count > 0 ? auctionItems.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if auctionItems.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "emptyCell")
            cell.textLabel?.text = "Click + button to add products"
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .gray
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AuctionItemCell
        
        let item = self.auctionItems[indexPath.row]
        
        cell.item = item
        cell.indexPath = indexPath.row
        cell.delegate = self
        cell.selectionStyle = .none
        
        return cell
    }
    
    //MARK: Go Live Click
    @IBAction func onGoLiveClick(_ sender: Any) {
        
        if self.validate(){
            
            if !Reachability.isConnectedToNetwork(){
                
                let str = NSLocalizedString("Please check your internet connection", comment: "Please check your internet connection")
                
                self.displayMessage(str)
                
                return
            }
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            // Start live strem
            let sb = UIStoryboard(name: "Streaming", bundle: nil)
            
            if let vc = sb.instantiateViewController(withIdentifier: "LiveStreamViewController") as? LiveStreamViewController{
                
                vc.auctionItems = self.auctionItems
                vc.displayType = .publisher
                vc.selectedCountry = self.selectedCountry
            
                if let wait = wait{
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                self.dismiss(animated: true) {
                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    //Deleagte Method
    func onDeleteClick(indexPath: Int, item: AuctionItemModel) {
        
        let alert = getAlertController(title: "", message: "Are you sure want to delete this item")
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
        }
        let delete = UIAlertAction(title: "Delete", style: .destructive) { (_) in
            self.auctionItems.remove(at: indexPath)
            self.tableView.reloadData()
        }
        
        alert.addAction(cancel)
        alert.addAction(delete)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func validate() -> Bool {
        
        if auctionItems.count == 0 {
            let str = NSLocalizedString("Add items for auction", comment: "Add items for auction")
            self.displayMessage(str)
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.auctionItems.isEmpty{
            return;
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductInfoViewController") as? ProductInfoViewController {
            
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            
            vc.item = self.auctionItems[indexPath.row]
            
            vc.currentItem = indexPath.row + 1
            vc.itemCount = self.auctionItems.count
            
            self.present(vc, animated: true, completion: nil)
        }
    }
}

class AuctionItemCell:UITableViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var lblProductSerial: UILabel!
    @IBOutlet weak var lblStartingPrice: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
//    @IBOutlet weak var lblPaymentMethod: UILabel!
    
    var delegate: AuctionItemsCellDelegate!
    
    var indexPath:Int!
    
    var item:AuctionItemModel! {
        
        didSet {
            if let img = item.image{
                self.productImage.image = img
            }else{
                self.productImage.image = #imageLiteral(resourceName: "ic_placeholder.png")
            }
            if let value = item.productSerial{
                self.lblProductSerial.text = value
            }else{
                self.lblProductSerial.text = "NA"
            }
            if let value = item.startingPrice{
                self.lblStartingPrice.text = "KD \(value)"
            }else{
                self.lblStartingPrice.text = "NA"
            }
            if let value = item.description{
                self.lblDescription.text = value
            }else{
                self.lblDescription.text = "NA"
            }
//            if let value = item.paymentMethod{
//                self.lblPaymentMethod.text = value
//            }else{
//                self.lblPaymentMethod.text = "NA"
//            }
        }
    }
    
    @IBAction func onDeleteClick(_ sender: Any) {
        delegate.onDeleteClick(indexPath: indexPath, item: item)
    }
}
protocol AuctionItemsCellDelegate {
    func onDeleteClick(indexPath: Int,item:AuctionItemModel)
}
