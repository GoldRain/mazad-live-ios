//
//  PublishViewController.swift
//  Mazad Live
//
//  Created by Mac-3 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//



import UIKit
import R5Streaming

public enum R5StreamStatus:Int{
    case CONNECTED          //0
    case DISCONNECTED       //1
    case ERROR              //2
    case TIMEOUT            //3
    case CLOSE              //4
    case START_STREAMING    //5
    case STOP_STREAMING     //6
    case NET_STATUS         //7
    case AUDIO_MUTE         //8
    case AUDIO_UNMUTE       //9
    case VIDEO_MUTE         //10
    case VIDEO_UNMUTE       //11
    case LICENSE_ERROR      //12
    case LICENSE_VALID      //13
    case BUFFER_FLUSH_START //14
    case BUFFER_FLUSH_EMPTY //15
    case VIDEO_RENDER_START //16
}

class PublishViewController: R5VideoViewController, R5StreamDelegate{
    
    var callback:((R5StreamStatus?, String)->())?
    
    var config:R5Configuration!
    var stream:R5Stream!
    
    var cameraPosition:AVCaptureDevice.Position = AVCaptureDevice.Position.front
    var streamDetails:MazadStream!
    var streamName:String!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        config = R5Configuration()
        config.host = ServerIPLiveStream
        config.port = ServerPort
        config.contextName = ContextName
        config.licenseKey = R5LiscenceKey
        
    }
    
    func start(){
        
        self.showPreview(false)
        
        print(self.streamName)
        
//        self.stream.publish(self.streamDetails.streamName!, type: R5RecordTypeRecord)
        
        self.stream.publish(self.streamName, type: R5RecordTypeRecord)
        
        //capture stream image
        
        //        DispatchQueue.main.asyncAfter(deadline: .now()+0.10) {
        //
        //            let image = self.stream.getImage()
        //
        //            let imageView = UIImageView(image: image)
        //            imageView.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        //            self.view.addSubview(imageView)
        //            imageView.center = self.view.center
        //
        //        }
    }
    
    func stop(){
        
        self.stream.stop()
        self.stream.delegate = nil
        self.preview(preview:false)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.preview(preview:true)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.stop()
    }
    
    var camera:R5Camera?
    
    func preview(preview:Bool) {
        
//        let cameraDevice: AVCaptureDevice? = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: cameraPosition)
//
//        let camera = R5Camera(device: cameraDevice, andBitRate: 512)
        
        let cameras = AVCaptureDevice.devices(for: .video)
        let cameraDevice: AVCaptureDevice? = cameras.last
        camera = R5Camera(device: cameraDevice, andBitRate: 512)
        
        let audioDevice = AVCaptureDevice.default(for: .audio)
        
        var microphone: R5Microphone? = nil
        if let aDevice = audioDevice {
            microphone = R5Microphone(device: aDevice)
        }
        
        let connection = R5Connection(config: config)
        
        self.stream = R5Stream(connection: connection)
        self.stream.attachVideo(camera)
        self.stream.attachAudio(microphone)
        
        if let orientation = camera?.orientation{
            camera?.orientation = ((orientation +  90 ) % 360)
        }
        
        self.stream.delegate = self
        self.attach(stream)
        
        self.showPreview(preview)
        
        if preview{
            self.start()
        }
    }
    
    
    func switchCamera(){
        
        if self.cameraPosition == .front{
            
            self.cameraPosition = .back
            
            let cameraDevice: AVCaptureDevice? = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: self.cameraPosition)
            
            
            
//            let camera = R5Camera(device: cameraDevice, andBitRate: 512)
            
            self.camera?.device = cameraDevice
            
//            self.stream.attachVideo(camera)
            
        }
        else{
            self.cameraPosition = .front
            
            let cameraDevice: AVCaptureDevice? = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: self.cameraPosition)
            
//            let camera = R5Camera(device: cameraDevice, andBitRate: 512)
            
//            self.stream.attachVideo(camera)
            
            self.camera?.device = cameraDevice
        }
    }
    
    //MARK: R5StreamDelegate
    func onR5StreamStatus(_ stream: R5Stream!, withStatus statusCode: Int32, withMessage msg: String!) {
        print("Stream Status \(statusCode) --> \(r5_string_for_status(statusCode)) - \(msg)");
        
        self.callback?(R5StreamStatus(rawValue: Int(statusCode)), msg)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let top = appDel.topViewController{
            top.view.endEditing(true)
        }
    }
    
}
