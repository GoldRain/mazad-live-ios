//
//  MessageCenter.swift
//  Mazad Live
//
//  Created by Mac-3 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import SocketIO
import Alamofire
import CoreData

enum StreamCommentType:String{
    case text = "comment"
    case amount = "amount"
    
}
//MessageTypes
let MessageTypeImage                = "image"
let MessageTypeVideo                = "video"
let MessageTypeText                 = "text"
let MessageTypeAlert                = "alert"

var messageCenter:MessageCenter?



typealias MessageSentCallback = ((ChatMessage?)->())
typealias RoomUpdateCallback = ((Room?)->())


let CreateStream = "createStream" // when user after start live streaming call this
//param : stream_name: "", status:"",user_id,""

let JoinLeaveStream = "joinLeaveStream" // when stop stream
//param ; user_id:"", stream_name:""

let StopStream = "stopStream" // when publisher stop stream

let ListenOnMessageReceived     = "messageReceived" //when a new message is received

let UpdateOnlineStatus = "updateOnlineStatus" // call for update user status
//param  user_id:"",  is_online : true/false as boolean


let UpdateStream      = "updateStream"                 //call when any user is live return details of stream

let AddComment = "addComment" //to send comment on a post

let UpdateVote = "updateVote" //when a vote percent is updated

let LogOut     = "logout" //

let UpdateProduct = "updateProduct" //will recieve this emit when a product for stream updates

let Verification = "idVerification" // will receive this emit when a user id is verified

let Sold = "sold"   // will receive this emit when a product is sold

class MessageCenter{
    
    let handleQueue = DispatchQueue.main //DispatchQueue(label: "messageCenter", attributes: .concurrent)
    
    var done = false
    
    //UUID's for listener
    var listenerUUID = [UUID?]()
    
    
    var manager:SocketManager?
    var socket:SocketIOClient?
    var log:Bool = false
    
    var backgroundContext:NSManagedObjectContext!
    
    var userId:String!
    
    init() {
        self.backgroundContext = appDel.persistentContainer.newBackgroundContext()
        backgroundContext.automaticallyMergesChangesFromParent = true
        backgroundContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
        backgroundContext.undoManager = nil
    }
    
    ///Start the Message Center, this should be called only once in the application
    /// - Parameters:
    ///     - log: boolean parameter which represents that the message center log should be printed
    ///     - userId: logged user id
    class func start(log:Bool, userId:String, notificationToken:String){
        
        messageCenter?.userId = userId
        
        if messageCenter == nil{
            
            messageCenter?.display("Starting server socket")
            
            messageCenter = MessageCenter()
            messageCenter?.log = log
        }
        
        messageCenter?.userId = userId
        
        messageCenter?.handleQueue.async {
            
            if messageCenter?.manager?.status != SocketIOStatus.connected{
                
                messageCenter?.manager = SocketManager(socketURL: URL(string: apiBaseUrl)!, config: [.log(log), .compress])
                
                if let manager = messageCenter?.manager{
                    
                    messageCenter?.display("Server Socket Started")
                    
                    messageCenter?.socket = manager.defaultSocket
                    
                    messageCenter?.onSocketConnect()
                    
                    messageCenter?.socket?.on("disconnect", callback: { (data, ack) in
                        
                        if let messageCenter = messageCenter{
                            for udid in messageCenter.listenerUUID{
                                if let udid = udid{
                                    messageCenter.socket?.off(id: udid)
                                }
                            }
                            
                            messageCenter.listenerUUID.removeAll()
                        }
                        
                    })
                    
                    messageCenter?.socket?.connect()
                    
                }
            }
        }
    }
    func onSocketConnect(){
        
        let uuid = messageCenter?.socket?.on(clientEvent: .connect) {data, ack in
            
            messageCenter?.display("Socket connected")
            
            self.onLogoutEvent()
            
            self.login(userId: self.userId!, notificationToken: "")
            
            self.onStreamEvent()
            
            self.onVoteUpdateEvent()
            
            self.getRoomsFromServer(roomId: nil, callback: nil)
            
            self.onMessageReceived()
            
            self.onIdVerification()
            
            self.onSoldProduct()
        }
        self.listenerUUID.append(uuid)
    }
    
    func onSocketDisconnect(){
        
        let uuid = messageCenter?.socket?.on(clientEvent: .disconnect) {data, ack in

            messageCenter?.display("Socket disconnected")
            
        }
        self.listenerUUID.append(uuid)
    }
    
    func onStreamEvent(){
        
//        if MyProfile.isSeller {
//            return;
//        }
        
        let uuid = messageCenter?.socket?.on(UpdateStream, callback: { (data, ack) in

            print("Stream name", UpdateStream, "EVENT")
            
            if let data = data as? [[String:Any]]{
                if let parsedData = data.first{
                    
                    print(parsedData)
                    
                    if let streamDict = parsedData["stream"] as? [String:Any]{
                        let stream = MazadStream(fromDictionary: streamDict)
                        if let userDict = parsedData["userDetails"] as? [String:Any]{
                            stream.userDetails = [MazadUserDetail(fromDictionary: userDict)]
                        }
                        if let userDict = parsedData["userDetails"] as? [[String:Any]]{
                            for user in userDict{
                                 stream.userDetails.append(MazadUserDetail(fromDictionary: user))
                            }
                        }
                        
                        if let comments = parsedData["comments"] as? [[String:Any]]{
                            for dict in comments{
                                stream.comments.append(MazadComment(fromDictionary: dict))
                            }
                        }
                        NotificationCenter.default.post(name: .UpdateStreamNotification, object: nil, userInfo: ["stream":stream])
                    }
                }
            }
        })
        self.listenerUUID.append(uuid)
        
        let uuid1 = messageCenter?.socket?.on(UpdateProduct, callback: { (data, ack) in
            
            print("Stream name", UpdateProduct, "EVENT")
            
            if let data = data as? [[String:Any]]{
                if let parsedData = data.first{
                    
                    print(parsedData)
                    
                    if let streamDict = parsedData["stream"] as? [String:Any]{
                        let stream = MazadStream(fromDictionary: streamDict)
                        if let userDict = parsedData["userDetails"] as? [String:Any]{
                            stream.userDetails = [MazadUserDetail(fromDictionary: userDict)]
                        }
                        if let userDict = parsedData["userDetails"] as? [[String:Any]]{
                            for user in userDict{
                                stream.userDetails.append(MazadUserDetail(fromDictionary: user))
                            }
                        }
                        
                        if let comments = parsedData["comments"] as? [[String:Any]]{
                            for dict in comments{
                                stream.comments.append(MazadComment(fromDictionary: dict))
                            }
                        }
                        
                        JSONRequest.makeRequest(kGetCurrentStreamProduct, parameters: ["stream_id": stream.id!], callback: {  (data, error) in
                            
                            if let data = data as? [String:Any]{
                                if let desc = data["description"] as? [String:Any]{
                                    if let product = desc["current_product"] as? [String:Any]{
                                        stream.currentItem = AuctionItemModel(data: product)
                                        
                                        NotificationCenter.default.post(name: .UpdateStreamProductNotification, object: nil, userInfo: ["stream":stream])
                                    }
                                }
                            }
                            
                        })
                        
                        NotificationCenter.default.post(name: .UpdateStreamProductNotification, object: nil, userInfo: ["stream":stream])
                        
                    }
                }
            }
        })
        
        self.listenerUUID.append(uuid1)
    }
    func onVoteUpdateEvent(){
        
        let uuid = messageCenter?.socket?.on(UpdateVote, callback: { (data, ack) in
            
            print("Update name\(UpdateVote) EVENT")
            
            ////{"error":false,"message":"Vote updated","post_id":"5c1d178777c5534a7d404517","voteStatus":1,"percent":"100%"}
            
            if let data = data as? [[String:Any]]{
                if let parsedData = data.first{
                    if let postId = parsedData["post_id"] as? String{
                        if let percent = parsedData["percent"] as? String{
                            let value = Int(percent.replacingOccurrences(of: "%", with: ""))
                            NotificationCenter.default.post(name: .UpdateVotePercent, object: nil, userInfo: ["postId":postId, "percent":"\(value!)%"])
                        }
                    }
                }
            }
        })
        self.listenerUUID.append(uuid)
    }
    
    func updateOnlineStatus(isOnline:Bool, playerId:String) {
        
        if let lastLoginTime = MyProfile.lastLoginTime{
            
            let params:[String:Any] = ["user_id":self.userId!,
                                       "user_type" : "ios",
                                       "is_online":isOnline,
                                       "playerId":playerId,
                                       "last_login":lastLoginTime
            ]
            
            self.socket?.emit(UpdateOnlineStatus, params)
            
            print("Update Online ==> \(params)")
            
            if isOnline{
                NotificationCenter.default.post(name: .UpdateStreamNotification, object: nil, userInfo: nil)
            }
        }
    }
    
    var connected:Bool{
        return self.manager?.status == SocketIOStatus.connected
    }
    
    func login(userId:String, notificationToken:String){
        
        self.updateOnlineStatus(isOnline: true, playerId: notificationToken)
        
    }
    
    func onLogoutEvent() {
        
        let uuid = messageCenter?.socket?.on(LogOut, callback: { (data, ack) in
            
            print("Logout Event")
            
            appDel.logOutUser()
            
        })
        self.listenerUUID.append(uuid)
    }
    
    func stopStream(streamId:String){
        
        let param = [["user_id" : userId,
                      "stream_id" : streamId]]
        
        self.socket?.emit(StopStream, with: param)
        
    }
    
    func joinStream(streamId:String, isJoin:Bool) {
        
        let param = [["user_id" : userId,
                      "stream_id" : streamId,
                      "is_join" : isJoin ? true : false ]]
        
        self.socket?.emit(JoinLeaveStream, with: param)
        
    }
    
    func addComment(comment:String, type:StreamCommentType, streamId:String){
        
        let param = [["user_id" : userId,
                      "stream_id" : streamId,
                      "text" : comment,
                      "type":type.rawValue]]
        
        self.socket?.emit(AddComment, with: param)
        
    }
    
    func stop(){
        
        handleQueue.async {
            
            self.updateOnlineStatus(isOnline: false, playerId: "")

            for udid in self.listenerUUID{
                if let udid = udid{
                    self.socket?.off(id: udid)
                }
            }

            self.listenerUUID.removeAll()

            self.manager?.disconnect()
            messageCenter = nil
            self.done = false
        }
        
    }
    func disconnect(){
        
        //        handleQueue.async {
        //            self.socket?.emit(EmitDisconnect, ["user_id":self.userId])
        //
        //            for udid in self.listenerUUID{
        //                if let udid = udid{
        //                    self.socket?.off(id: udid)
        //                }
        //            }
        //
        //            self.listenerUUID.removeAll()
        //
        //            self.manager?.disconnect()
        //        }
    }
    
    func display(_ printMessage:Any){
        
        handleQueue.async {
            if self.log{
                
                print("\n\n************* Start MessageCenter Log *************\n\n\(printMessage)\n\n************* End MessageCenter Log *************\n\n")
            }
        }
    }
    
    func getLiveStream(callback:@escaping (([MazadStream]?)->())){
        
        JSONRequest.makeRequest(kStreamDetails, parameters: ["user_id":self.userId!]) { (data, error) in
            
            if let data = data as? [String:Any]{
                if let err = data["error"] as? Bool, !err{
                    if let streamData = data["description"] as? [[String:Any]]{
                        var streams = [MazadStream]()
                        for streamDict in streamData{
                            
                            let stream = MazadStream(fromDictionary: streamDict)
                            streams.append(stream)
                        }
                        callback(streams)
                        return;
                    }
                }
            }
            callback(nil)
        }
    }
    
    //TODO: Listener for user verification approval
    func onIdVerification() {
        
        let uuid = messageCenter?.socket?.on(Verification, callback: { (data, ack) in
            
            if let data = data as? [[String:Any]]{
                if let parsedData = data.first{
                    if let des = parsedData["description"] as? [String:Any] {
                        if let status = des["status"] as? String {
                            if status.lowercased() == "approved" {
                                
                                // present card Vc for payment for verified badge
                                
                                if let _ = appDel.topViewController as? PaymentForBadgeViewController{
                                    return
                                }
                                
                                if let vc = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PaymentForBadgeViewController") as? PaymentForBadgeViewController {
                                    
                                    vc.modalTransitionStyle = .crossDissolve
                                    vc.modalPresentationStyle = .overCurrentContext
                                    
                                    appDel.topViewController?.present(vc, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }
            }
        })
        self.listenerUUID.append(uuid)
    }
    
    func onSoldProduct() {
        
        let uuid = messageCenter?.socket?.on(Sold, callback: { (data, ack) in
            
            if let data = data as? [[String:Any]] {
                if let des = data.first{
                    if let type = des["type"] as? String {
                        if let typeId = des["type_id"] as? String {
                            NotificationCenter.default.post(name: .OnSoldProduct, object: nil, userInfo: ["type":type,
                                                                                                          "typeId":typeId])
                        }
                    }
                }
            }
        })
        self.listenerUUID.append(uuid)
    }
    
}
//MARK: Chat Messages
extension MessageCenter{
    
    func createRoom(name:String, otherUserId:String, callback:@escaping ((Room?)->())) {
        
        let params: [String:String] = [
            "user_id1": self.userId,
            "user_id2": otherUserId
        ]
        
        JSONRequest.makeRequest(kCreateRoom, parameters: params, callback: {  (data, error) in
            
            if let data = data as? [String:Any]{
                if let error = data["error"] as? Bool, !error{
                    if let roomData = data["description"] as? [String:Any]{
                        if let mongoId = roomData["_id"] as? String{
                            self.getRoomsFromServer(roomId: mongoId, callback: { (rooms) in
                                callback(rooms?.first)
                            })
                        }
                    }
                }
                else{
                    if let msg = data["message"] as? String{
                        
                        print("Error create group\n\(msg)")
                    }
                    callback(nil)
                }
            }
            else{
                callback(nil)
            }
        })
    }
    
    func getRoomFromLocal(otherUserId:String?, roomId:String?, callback:@escaping ((Room?) -> ())) {
        
        self.backgroundContext.perform{
            
            let fetchRequest: NSFetchRequest<Room> = Room.fetchRequest()
            
            if let otherUserId = otherUserId{
                fetchRequest.predicate = NSPredicate(format: "otherId = %@", otherUserId)
            }
            else if let roomId = roomId{
                fetchRequest.predicate = NSPredicate(format: "mongoId = %@", roomId)
            }
            
            do {
                let list = try self.backgroundContext.fetch(fetchRequest)
                
                if list.count > 0 {
                    callback(list.first)
                }else {
                    callback(nil)
                }
                
            }catch{
                print("error fetching in get Room => \(error)")
                callback(nil)
            }
        }
    }
    func getRoomFromLocal(otherUserId:String?, roomId:String?) -> Room? {
        
            let fetchRequest: NSFetchRequest<Room> = Room.fetchRequest()
            
            if let otherUserId = otherUserId{
                fetchRequest.predicate = NSPredicate(format: "otherId = %@", otherUserId)
            }
            else if let roomId = roomId{
                fetchRequest.predicate = NSPredicate(format: "mongoId = %@", roomId)
            }
            
            do {
                let list = try self.backgroundContext.fetch(fetchRequest)
                
                return list.first
                
            }catch{
                print("error fetching in get Room => \(error)")
                
            }
        return nil
    }
    
    func getChatUser(callback:@escaping (([ChatUser]?) -> ())){
        
        self.backgroundContext.perform{
            
            let fetchRequest: NSFetchRequest<ChatUser> = ChatUser.fetchRequest()
            
            do {
                let list = try self.backgroundContext.fetch(fetchRequest)
                
                if list.count > 0 {
                    callback(list)
                }else {
                    callback(nil)
                }
                
            }catch{
                print("error fetching in get ChatUser => \(error)")
                callback(nil)
            }
        }
    }
    
    func getRooms(callback:@escaping (([Room]?) -> ())){
        
        self.backgroundContext.perform{
            
            let fetchRequest: NSFetchRequest<Room> = Room.fetchRequest()
           
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "lastUpdateDate", ascending: false)]
            
            do {
                let list = try self.backgroundContext.fetch(fetchRequest)
                
                if list.count > 0 {
                    callback(list)
                }else {
                    callback(nil)
                }
                
            }catch{
                
                print("error fetching in get Rooms => \(error)")
                
                callback(nil)
                
            }
        }
    }
    

    // MARK: Text Message
    func sendTextMessage(room:Room, tempId:String, sentStatus:String, text:String, contentType:String,  callback:@escaping MessageSentCallback) -> ChatMessage? {
        
        if let message = NSEntityDescription.insertNewObject(forEntityName: "ChatMessage", into: backgroundContext) as? ChatMessage{
            
            message.mongoId = tempId
            message.contentType = contentType
            message.sentStatus = sentStatus
            message.text = text
            message.room = room
            message.createdAt = Date()
            message.senderId = self.userId
           
            if contentType == MessageTypeText{
                room.subtitle = text
            }
            else if contentType == MessageTypeImage{
                room.subtitle = "Photo"
            }
            else if contentType == MessageTypeVideo{
                room.subtitle = "Video"
            }
            
            room.lastUpdateDate = Date()
            
            handleQueue.async {
                
                let params = ["sender_id" : self.userId!,
                              "room_id": room.mongoId!,
                              "message":text,
                              "messageType":contentType]
                
                JSONRequest.makeRequest(kSendMessage, parameters: params, callback: {  (data, error) in
                    
                    if let data = data as? [String:Any]{
                        
                        if let error = data["error"] as? Bool, !error {

                            if let messageDetails = data["messageDetails"] as? [String:Any] {
                                if let id = messageDetails["_id"] as? String{
                                    if let _ = messageDetails["sender_id"] as? String {
                                        if let _ = messageDetails["room_id"] as? String {
                                            if let text = messageDetails["message"] as? String {
                                                if let messageType = messageDetails["message_type"] as? String {
                                                    if let createdAt = messageDetails["created_at"] as? String {
                                                        
                                                        //update message id in db
                                                        message.mongoId = id
                                                        message.sentStatus = "sent"
                                                        message.contentType = messageType
                                                        message.text = text
                                                        message.createdAt = Date(timeIntervalSince1970: Double(createdAt)!/1000)
                                                        
                                                        do {
                                                            try self.backgroundContext.save()
                                                        }catch {
                                                            print("error while saving \(error)")
                                                        }
                                                        
                                                        DispatchQueue.main.async {
                                                            //callback to update local array
                                                            callback(message)
                                                        }
                                                        return
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }else {
                            if let msg = data["message"] as? String, msg == "Room not found."{
                                
                                self.deleteSingleRoom(room:room)
                                
                                if let top = appDel.topViewController {
                                    let str = NSLocalizedString(msg, comment: msg)
                                    top.navigationController?.popViewController(animated: true)
                                    top.displayMessage(str)
                                }
                            }
                        }
                    }
                    
                    message.sentStatus = "error"
                    do {
                        try self.backgroundContext.save()
                    }catch {
                        print("error while saving \(error)")
                    }
                    
                    DispatchQueue.main.async {
                        //callback to update local array
                        callback(message)
                    }
                })
            }
            return message
        }
        return nil
    }
    
    func getRoomsFromServer(roomId:String?, callback:(([Room]?)->())?) {
        
        guard let userId = self.userId else {
            return
        }
        
        let params = ["room_id":roomId ?? "",
                      "user_id":userId]
        
        JSONRequest.makeRequest(kGetRooms, parameters: params) { (data, error) in
            
            if let data = data as? [String:Any] {
                
                if let error = data["error"] as? Bool, !error {
                    
                    if let rooms = data["description"] as? [[String:Any]] {
                        
                        var roomsArray = [Room]()
                        
                        var count = rooms.count
                        
                        if count == 0{
                            callback?(nil)
                            return
                        }
                        
                        for room in rooms {
                            
                            if let roomMongoId = room["_id"] as? String {
                                if let createdAt = room["created_at"] as? String {
                                    if let userArr = room["usersDetails"] as? [[String:Any]] {
                                        
                                        var userData = [UserModel]()
                                        
                                        for user in userArr {
                                            userData.append(UserModel(data: user))
                                        }
                                        
                                        userData = userData.filter({ (user) -> Bool in
                                            return user._id != self.userId
                                        })
                                        
                                        if let otherId = userData.first?._id {
                                            
                                            self.saveRoom(otherId:otherId, roomMongoId:roomMongoId, createdAt:createdAt, userName: userData.first?.username ?? "", profileImageUrl:userData.first?.profilePhoto ?? "", callback: { room in
                                                if let room = room{
                                                    roomsArray.append(room)
                                                }
                                                count = count - 1
                                                if count == 0{
                                                    if roomsArray.count > 0{
                                                        callback?(roomsArray)
                                                    }
                                                    else{
                                                        callback?(nil)
                                                    }
                                                }
                                            })
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getUser(userId:String) -> ChatUser?{
        
        let fetchRequest:NSFetchRequest = ChatUser.fetchRequest()
        
        fetchRequest.predicate = NSPredicate(format: "mongoId = %@", userId)
        
        do {
            let list = try self.backgroundContext.fetch(fetchRequest)
            
            return list.first
        }
        catch let error{
            print("Error in Getting user \(error)")
        }
        
        return nil
    }
    
    func saveUser(userId:String, name:String, profileImage:String) -> ChatUser?{
        
        var user:ChatUser?
        
        if let u = self.getUser(userId: userId){
            user = u
        }
        else{
            user = ChatUser(context: self.backgroundContext)
        }
        
        if let user = user{
            
            user.mongoId = userId
            user.name = name
            user.profileImageUrl = profileImage
            
            do{
                try self.backgroundContext.save()
                return user
            }
            catch let error{
                print("Error saving user \(error)")
            }
        }
        
        return nil
    }
    
    func getRoom(roomId:String) -> Room?{
        
        let fetchRequest:NSFetchRequest = Room.fetchRequest()
        
        fetchRequest.predicate = NSPredicate(format: "mongoId = %@", roomId)
        
        do {
            let list = try self.backgroundContext.fetch(fetchRequest)
            
            return list.first
        }
        catch let error{
            print("Error in Getting Room \(error)")
        }
        
        return nil
    }
    
    func saveRoom(otherId:String, roomMongoId:String, createdAt:String, userName:String, profileImageUrl:String, callback:@escaping ((Room?)->())) {
        
        self.backgroundContext.perform {
            
            if let user = self.saveUser(userId: otherId, name: userName, profileImage: profileImageUrl){
                
                var room:Room?
                
                if let r = self.getRoom(roomId: roomMongoId){
                    room = r
                    print("Room Action Found for Id \(roomMongoId)")
                }
                else{
                    room = NSEntityDescription.insertNewObject(forEntityName: "Room", into: self.backgroundContext) as? Room
                    print("Room Action Creating for Id \(roomMongoId)")
                }
                
                if let room = room {
                    
                    room.otherId = otherId
                    room.mongoId = roomMongoId
                    room.users = room.users?.adding(user) as! NSSet
                    user.room = user.room?.adding(room) as! NSSet
                    
                    if let createdAt = Double(createdAt){
                        room.createdAt = Date(timeIntervalSince1970: createdAt/1000)
                    }
                    if room.lastUpdateDate == nil{
                        room.lastUpdateDate = room.createdAt
                    }
                    
                    do {
                        try self.backgroundContext.save()
                        callback(room)
                        return
                    }catch{
                        print("error while saving rooms \(error)")
                    }
                }
                callback(nil)
            }
        }
    }
    
    func onMessageReceived() {
        
        handleQueue.async {
            
            guard self.socket != nil else{
                return
            }
            
            let uuid = self.socket?.on(ListenOnMessageReceived, callback: { (response, socketAck) in
                
                self.display("New Message Received")
                
                print(response)
                
                if let response = response.first as? [String:Any]{
                    
                    if let messageDetails = response["messageDetails"] as? [String:Any] {
                        if let id = messageDetails["_id"] as? String{
                            if let senderId = messageDetails["sender_id"] as? String {
                                if let roomId = messageDetails["room_id"] as? String {
                                    if let text = messageDetails["message"] as? String {
                                        if let messageType = messageDetails["message_type"] as? String {
                                            if let createdAt = messageDetails["created_at"] as? String {
                                                
                                                let message = ChatMessage(context: self.backgroundContext)
                                                
                                                if let room = self.getRoomFromLocal(otherUserId: nil, roomId: roomId){
                                                    
                                                    message.room = room
                                                    message.mongoId = id
                                                    message.senderId = senderId
                                                    message.text = text
                                                    message.contentType = messageType
                                                    message.createdAt = Date(timeIntervalSince1970: Double(createdAt)!/1000)
                                                    room.lastUpdateDate = message.createdAt
                                                    room.subtitle = text
                                                    room.unreadCount = room.unreadCount + 1
                                                    
                                                    do {
                                                        try self.backgroundContext.save()
                                                        
                                                        mainQueue.async {
                                                            
                                                            NotificationCenter.default.post(name: .OnMessageRecieved, object: nil, userInfo: ["message":message])
                                                            
                                                            self.dispatchLocalNotificationFor(message: message)
                                                            
                                                        }
                                                    }catch {
                                                        print("error while saving on received msg \(error)")
                                                    }
                                                }
                                                else{
                                                    self.getRoomsFromServer(roomId: roomId, callback: {  (rooms) in
                                                        
                                                        if let room = rooms?.first{
                                                            
                                                            message.room = room
                                                            message.senderId = senderId
                                                            message.text = text
                                                            message.contentType = messageType
                                                            message.createdAt = Date(timeIntervalSince1970: Double(createdAt)!/1000)
                                                            room.lastUpdateDate = message.createdAt
                                                            room.unreadCount = room.unreadCount + 1
                                                            room.subtitle = text
                                                            
                                                            do {
                                                                try self.backgroundContext.save()
                                                                mainQueue.async {
                                                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "onReceivedMessage"), object: nil, userInfo: nil)
                                                                }
                                                            }catch {
                                                                print("error while saving on received msg \(error)")
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
            self.listenerUUID.append(uuid)
        }
    }
    
    func dispatchLocalNotificationFor(message:ChatMessage){
        
        var senderName:String?
        
        if let user = self.getUser(userId: message.senderId ?? "NA"){
            senderName = user.name ?? ""
        }
        
        if let top = appDel.topViewController{
            if let vc = top.navigationController?.viewControllers.last{
                if let chatVC = vc as? ChatViewController{
                    if let room = chatVC.room{
                        if room.mongoId != message.room?.mongoId{
                           appDel.dispatchlocalNotification(identifier: "message", title: senderName ?? "New Message", body: message.text ?? "", userInfo: ["messageId": message.mongoId!])
                        }
                    }
                }
                else if !(vc is ChatListViewController){
                   appDel.dispatchlocalNotification(identifier: "message", title: senderName ?? "New Message", body: message.text ?? "", userInfo: ["messageId": message.mongoId!])
                }
            }
        }
    }
    
    func removeOldDatabase(){
        
        deleteEntities(fetchRequest: ChatMessage.fetchRequest())
        deleteEntities(fetchRequest: Room.fetchRequest())
    }
    
    func deleteEntities<T: NSManagedObject>(fetchRequest: NSFetchRequest<T>){
        
        do{
            let list: [T] = try(appDel.persistentContainer.viewContext.fetch(fetchRequest) as [T])
            for item in list{
                appDel.persistentContainer.viewContext.delete(item)
            }
            appDel.saveContext()
        }catch{
            print(error)
        }
    }
    
    func deleteSingleRoom(room:Room) {
        
        let fetchRequest:NSFetchRequest = Room.fetchRequest()
        
        if let mongoId = room.mongoId {
            fetchRequest.predicate = NSPredicate(format: "mongoId = %@", mongoId)
            
            do{
                let list = try appDel.persistentContainer.viewContext.fetch(fetchRequest)
                for item in list{
                    appDel.persistentContainer.viewContext.delete(item)
                }
                appDel.saveContext()
            }catch{
                print(error)
            }
        }
        
    }
}

extension ChatUser{
    
    func getRoom(roomId:String?) -> Room?{
        
        if let rooms = self.room?.allObjects as? [Room]{
            if let room = rooms.first(where: { (r) -> Bool in
                return r.mongoId == roomId
            }){
                return room
            }
        }
        return nil
    }
    
    func getRoom(room:Room) -> Room?{
        return self.getRoom(roomId: room.mongoId)
    }
}

extension Room{
    
    func getAnotherUser() -> ChatUser?{
        
        if let users = self.users?.allObjects as? [ChatUser]{
            if let user = users.first(where: { (u) -> Bool in
                return u.mongoId != MyProfile.userId
            }){
                return user
            }
        }
        return nil
    }
    
    func markRead(){
        
        self.unreadCount = 0
        
        if let messages = self.messages?.allObjects{
            for message in messages{
                if let message = message as? ChatMessage{
                    message.isRead = true
                }
            }
        }
        
        NotificationCenter.default.post(name: .RoomUpdateNotification, object: nil, userInfo: ["room":self])
    }
    
}
