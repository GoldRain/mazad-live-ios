//
//  ProductSoldViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 11/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class ProductSoldViewController: UIViewController {

    @IBOutlet weak var lblMessage:UILabel!
    
    @IBOutlet weak var btnNext:UIButton!
    
    @IBOutlet weak var btnContinueWithSameItem:UIButton!
    
    @IBOutlet weak var animationImageView: UIImageView!
    
    @IBOutlet weak var animationSubImageView: UIImageView!
    
    @IBOutlet weak var animationImageViewWidthConstraint: NSLayoutConstraint!
    
    var callback:((ProductSoldCallbackOption)->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func disableNextDisable(){
        
        self.btnNext.backgroundColor = UIColor.gray
        self.btnNext.isUserInteractionEnabled = false
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.animationImageViewWidthConstraint.constant = CGFloat(60)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.setUpAnimation()
        
    }
    
    func setUpAnimation(){
        
        UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.animationImageViewWidthConstraint.constant = CGFloat(90)
            self.animationImageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/1.5)
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }, completion: nil)
        
    }
    
    @IBAction func onNextClick(_ sender: Any) {
        
        self.callback(.moveToNext)
        
    }
    
    @IBAction func onCloseClick(_ sender: Any) {
        
        self.callback(.stopStream)
        
    }
    @IBAction func onKeepCurrentItemClick(_ sender: Any) {
        
        self.callback(.keepTheCurrentItem)
        
    }
}

public enum ProductSoldCallbackOption{
    case moveToNext
    case stopStream
    case keepTheCurrentItem
}
