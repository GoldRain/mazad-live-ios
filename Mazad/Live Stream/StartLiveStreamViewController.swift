

import UIKit

class StartLiveStreamViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var selectedCountry = [CountryModel]()
    
    @IBOutlet weak var listMainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let btn = UIButton()
    var countries = [CountryModel]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //add the current user country to selected countried
        for data in self.countries{
            if data._id == MyProfile.countryId {
                self.selectedCountry.append(data)
            }
        }
        
//        self.navigationController?.isNavigationBarHidden = false
//        self.navigationController?.navigationBar.barTintColor = .clear
//        btn.setImage(#imageLiteral(resourceName: "icon_settings").withRenderingMode(.alwaysOriginal), for: .normal)
//        btn.imageEdgeInsets = UIEdgeInsetsMake(12, 42, 12, 0)
//        btn.bounds = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
//        let rightBarButton = UIBarButtonItem(customView: btn)
//        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
        //var sortedArray = swiftArray.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        
        //self.setSliderBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onBackClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StartLiveStreamSelectCountryCell
        let country = self.countries[indexPath.row]
        
        cell.selectionStyle = .none
        
        let imageUrl = country.flagImageUrl!
        var completeUrl =  "\(apiBaseUrl)\(imageUrl)"
        completeUrl = completeUrl.replacingOccurrences(of: " ", with: "%20")
        
        
        if let imgCountry = cell.contentView.viewWithTag(1) as? UIImageView{
            if let lblName = cell.contentView.viewWithTag(2) as? UILabel{
                imgCountry.image = nil
                imgCountry.sd_setImage(with: URL(string: completeUrl), completed: nil)
                lblName.text = country.countryName
            }
        }
        
        if self.selectedCountry.contains(where: { (c) -> Bool in
            return c._id == country._id
        }){
            cell.btnSelectCountry.setImage(#imageLiteral(resourceName: "onRadioButton"), for: .normal)
        }
        else{
            cell.btnSelectCountry.setImage(#imageLiteral(resourceName: "offRadioButton"), for: .normal)
        }
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! StartLiveStreamSelectCountryCell
        
        if cell.btnSelectCountry.image(for: .normal) == UIImage(named: "onRadioButton") {
            
            cell.btnSelectCountry.setImage(#imageLiteral(resourceName: "offRadioButton"), for: .normal)
        }else {
            cell.btnSelectCountry.setImage(#imageLiteral(resourceName: "onRadioButton"), for: .normal)
            
        }
        
        self.addCountry(country: countries[indexPath.row])
    }
    
    func addCountry(country: CountryModel) {
        
        if let index = self.selectedCountry.firstIndex(where: { (t) -> Bool in
            return t.countryName == country.countryName
        }){
            self.selectedCountry.remove(at: index)
        }
        else {
            self.selectedCountry.append(country)
        }
    }
    
    @IBAction func onStartLiveVideoClick(_ sender: UIButton) {
        
        if self.selectedCountry.count <= 0{
            self.displayMessage("Please select country!!")
            return;
        }
        guard let userId = MyProfile.userId else {
            return;
        }
        
        let wait = appDel.topViewController?.addWaitSpinner()
        
        JSONRequest.makeRequest(kShowCounts, parameters: ["user_id":userId], callback: {  (data, error) in
            
            if let wait = wait {
                appDel.topViewController?.removeWaitSpinner(waitView: wait)
            }
            
            if let data = data as? [String:Any] {
                if let err = data["error"] as? Bool, !err {
                    if let des = data["description"] as? [String:Any] {
                        if let liveStreamDict = des["unlimited_live_count"] as? [String:Any] {
                            let liveStreamObj = CountModel(data: liveStreamDict)
                            if let status = liveStreamObj.status {
                                if status {
                                    if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "AuctionItemsViewController") as? AuctionItemsViewController {
                                        vc.selectedCountry = self.selectedCountry
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }else{
                                    if liveStreamObj.count == 0 {
                                        let str = NSLocalizedString("You are out of live streams. To start live streams, please upgrade", comment: "You are out of live streams. To start live streams, please upgrade")
                                        self.displayMessage(str, callback: { () -> (Void) in
                                            if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpgradeViewController") as? UpgradeViewController {
                                                self.dismiss(animated: true, completion: {
                                                    appDel.topViewController?.navigationController?.pushViewController(vc, animated: true)
                                                })
                                            }
                                        })
                                    }else{
                                        if let vc = UIStoryboard(name: "Streaming", bundle: nil).instantiateViewController(withIdentifier: "AuctionItemsViewController") as? AuctionItemsViewController {
                                            vc.selectedCountry = self.selectedCountry
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    let str = NSLocalizedString("Failed to get response, Please try again later", comment: "Failed to get response, Please try again later")
                    self.displayMessage(str)
                }
            }else{
                let str = NSLocalizedString("Failed to get response, Please try again later", comment: "Failed to get response, Please try again later")
                self.displayMessage(str)
            }
        })
        
//        self.dismiss(animated: true, completion: {
//
//            if let top = appDel.topViewController{
        
//                let sb = UIStoryboard(name: "Streaming", bundle: nil)
//                if let vc = sb.instantiateViewController(withIdentifier: "LiveStreamViewController") as? LiveStreamViewController{
//                    vc.displayType = .publisher
//                    vc.selectedCountry = self.selectedCountry
//                    top.present(vc, animated: true, completion: nil)
//                }
//            }
//        })
    }
    
    
    @IBAction func onSliderValueChange(_ sender: UISlider){
//        let mainTableViewOffSet = self.tableView.contentSize.height - self.tableView.frame.height + self.tableView.contentInset.bottom
//
//        let valueY = mainTableViewOffSet*CGFloat(sender.value/100)
//        self.tableView.scrollRectToVisible(CGRect(origin: CGPoint(x: 0, y: valueY), size: self.tableView.bounds.size), animated: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
//        let mainTableViewOffSet = self.tableView.contentSize.height - self.tableView.frame.height + self.tableView.contentInset.bottom
//
//        let value = self.tableView.contentOffset.y/(mainTableViewOffSet/100)
//        self.sliderBar.value = Float(value)
    }
    
    lazy var sliderBar: CustomSlider = {
        let slider = CustomSlider()
        slider.addTarget(self, action: #selector(onSliderValueChange(_:)), for: .valueChanged)
        return slider
    }()
    
    private func setSliderBar(){
        
        
        self.listMainView.addSubview(sliderBar)
        self.listMainView.bringSubview(toFront: sliderBar)
        sliderBar.trailingAnchor.constraint(equalTo: listMainView.trailingAnchor, constant: self.listMainView.frame.width*0.14).isActive = true
        sliderBar.centerYAnchor.constraint(equalTo: listMainView.centerYAnchor, constant: 0.0).isActive = true
        let height = NSLayoutConstraint(item: sliderBar, attribute: .width, relatedBy: .equal, toItem: listMainView, attribute: .height, multiplier: 0.8, constant: 0.0)
        NSLayoutConstraint.activate([height])
        sliderBar.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        
    }
    

}

class StartLiveStreamSelectCountryCell:UITableViewCell {
    
    @IBOutlet weak var imgCountryFalg: UIImageView!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var btnSelectCountry: UIButton!
    
    override func prepareForReuse() {
        self.btnSelectCountry.setImage(#imageLiteral(resourceName: "offRadioButton.png"), for: .normal)
    }
}

extension UIImage {
    
    func scaleToSize(newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext();
        return newImage
    }
}

class CustomSlider: UISlider {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setCustomization()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setCustomization(){
        translatesAutoresizingMaskIntoConstraints = false
        tintColor = UIColor(red: 23/255, green: 177/255, blue: 237/255, alpha: 1.0)
        maximumTrackTintColor = .white
        maximumValue = 100
        minimumValue = 0
        value = 50
        let img = #imageLiteral(resourceName: "imgSliderThumb").scaleToSize(newSize: CGSize(width: 12.0, height: 13.0))
        setThumbImage(img, for: .normal)
    }
}

func loadCountryJson() -> [Country?]?{
    
    var countries = [Country]()
    
    if let url = Bundle.main.url(forResource: "country-codes", withExtension: "json") {
        
        do{
            let jsonString = try String(contentsOf: url)
            
            if let dataFromString = jsonString.data(using: .utf8, allowLossyConversion: false) {
                let json = try JSONSerialization.jsonObject(with: dataFromString, options: .mutableContainers)
                if let json = json as? [Any]{
                    for item in json{
                        if let item = item as? [String:Any]{
                            let c = Country(data: item)
                            countries.append(c)
                        }
                    }
                    return countries
                }
            }
        }
        catch{
            return nil
        }
    }
    return nil
}

func emojiFlag(countryCode:String) -> String {
    
    let base : UInt32 = 127397
    var s = ""
    for v in countryCode.unicodeScalars {
        s.append(String(describing:UnicodeScalar(base + v.value)!))
    }
    return s
}

