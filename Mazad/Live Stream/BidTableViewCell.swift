//
//  BidTableViewCell.swift
//  Mazad Live
//
//  Created by Mac-3 on 12/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

enum BidTableCellAction{
    case bookmarkBid
    case removeBookmarkBid
    case awardBid
}

protocol BidTableViewCellDelegate {
    func onBidCellAction(for action:BidTableCellAction, indexPath:IndexPath, comment:MazadComment)
}

class BidTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCommentText: UILabel!
    
    @IBOutlet weak var btnPin: UIButton!
    @IBOutlet weak var btnBid: UIButton!
    
    var delegate:BidTableViewCellDelegate?
    
    var indexPath:IndexPath!
    var comment:MazadComment! {
        didSet{
            if let user = comment.user.first{
                if user.profilePhoto != nil && user.profilePhoto != ""{
                    self.imgUser.sd_setImage(with: URL(string: user.profilePhoto!), completed: nil)
                }
                
                //set random color
                let color = getRandomColor()
                self.lblUserName.textColor = color
                self.lblCommentText.textColor = color
                
                self.lblUserName.text = user.username
                if comment.comment != nil{
                    self.lblCommentText.text = comment.comment.trim()
                }
                else if comment.amount != nil{
                    self.lblCommentText.text = "KD"+comment.amount.trim()
                    if comment.bookmarked{
                        self.btnPin.backgroundColor = BLUECOLOR
                    }
                }
            }
        }
    }
    
    override func prepareForReuse() {
        
        self.imgUser.image = #imageLiteral(resourceName: "mazad_ic_home")
        if self.btnPin != nil{
            self.btnPin.setImage(UIImage(named: "icon_pin_circle"), for: .normal)
            self.btnPin.backgroundColor = .clear
        }
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBidButtonClick(_ sender:UIButton){
        
        if sender.image(for: .normal) == UIImage(named: "icon_pin_circle"){
            self.delegate?.onBidCellAction(for: .bookmarkBid, indexPath: self.indexPath, comment: self.comment)
            btnPin.backgroundColor = BLUECOLOR
        }
        else{
            self.delegate?.onBidCellAction(for: .removeBookmarkBid, indexPath: self.indexPath, comment: self.comment)
             btnPin.backgroundColor = BLUECOLOR
        }
    }
    
    @IBAction func onAwardBidClick(_ sender:UIButton){
        
        self.delegate?.onBidCellAction(for: .awardBid, indexPath: self.indexPath, comment: self.comment)
        
    }

}
