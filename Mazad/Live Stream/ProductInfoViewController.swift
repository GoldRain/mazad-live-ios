//
//  ProductInfoViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 12/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit

class ProductInfoViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var topImageView: UIView!
    
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var btnProductCount: UIButton!
    
    @IBOutlet weak var lblProductSerial: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    
    var item:AuctionItemModel!
    var itemCount:Int?
    var currentItem:Int?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        mainView.addShadow()
        
        topImageView.addShadow()
        
        setupData()
    }
    
    func setupData() {
        
        if let value = item.image {
            self.productImage.image = value
        }
        else if let url = item.imageUrl{
            
            self.productImage.image = #imageLiteral(resourceName: "ic_placeholder.png")
            
            self.productImage.sd_setImage(with: URL(string: url)) { (img, error, type, url) in
                
                if img == nil{
                    self.productImage.image = #imageLiteral(resourceName: "ic_placeholder.png")
                }
            }
        }
        if let value = item.productSerial {
            self.lblProductSerial.text = value
        }
        if let value = item.startingPrice {
            self.lblProductPrice.text = value
        }
        if let value = item.description {
            self.lblProductDescription.text = value
        }
        if let value = item.paymentMethod?.replacingOccurrences(of: "cc", with: "Card").replacingOccurrences(of: "cash", with: "Cash") {
            self.lblPaymentMode.text = value
        }
        
        if let currentItem = self.currentItem, let itemCount = self.itemCount{
            self.btnProductCount.setTitle("Auction Item \(currentItem)/\(itemCount)", for: .normal)
        }
        else{
           self.btnProductCount.isHidden = true
        }
        
    }
    
    
    @IBAction func onCloseClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
