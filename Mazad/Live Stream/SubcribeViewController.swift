//
//  SubcribeViewController.swift
//  Mazad Live
//
//  Created by Mac-3 on 06/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit
import R5Streaming

class SubcribeViewController: R5VideoViewController, R5StreamDelegate {
    
    var callback:((R5StreamStatus?, String)->())!
    
    var config:R5Configuration!
    
    var stream:R5Stream?
    
    var streamDetails:MazadStream!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        config = R5Configuration()
        config.host = ServerIPLiveStream
        config.port = ServerPort
        config.contextName = ContextName
        config.licenseKey = R5LiscenceKey

        
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func start() {
        
        self.stream?.stop()
        
        print("Stream: Status Start Subscribe")
        
        let connection = R5Connection(config: config)
        
        stream = R5Stream(connection: connection)
        
        self.stream?.delegate = self
        
        self.attach(self.stream!)
        
        self.stream?.play(self.streamDetails.streamName)
        
        print("Stream: Status Playing Stream \(self.streamDetails.streamName!)")
        
        
        
    }
    
    func stop() {
        
        stream?.stop()
        self.stream?.delegate = nil
        
    }
    
    //MARK: R5StreamDelegate
    func onR5StreamStatus(_ stream: R5Stream!, withStatus statusCode: Int32, withMessage msg: String!) {
       
        self.callback(R5StreamStatus(rawValue: Int(statusCode)), msg)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let top = appDel.topViewController{
            top.view.endEditing(true)
        }
    }
}
