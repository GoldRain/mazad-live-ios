//
//  AwardBidViewController.swift
//  Mazad Live
//
//  Created by alienbrainz on 05/01/19.
//  Copyright © 2019 AlienBrainz. All rights reserved.
//

import UIKit
//"Award Item no 1 with srial 123456 for $123" //vc.currentItem = self.currentItemIndex + 1
class AwardBidViewController: UIViewController {
    
    var stream:MazadStream!
    var comments:[MazadComment]!
    var item:AuctionItemModel!
    
    var selectedIndex = [IndexPath]()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    var callback:(()->())!
    
    var currentItem:Int!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.lblTitle.text = "Award Item no. \(currentItem!) with serial \(self.item.productSerial!)"
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    @IBAction func onConfirmClick(_ sender: Any) {
        self.awardBid()
    }
    
    @IBAction func onCancelClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //function to award bid
    func awardBid(){
        
        guard self.selectedIndex.count > 0 else{
            return
        }
        var awardBidIds = [String]()
        for item in self.selectedIndex{
            
            let comment = self.comments[item.row]
            awardBidIds.append(comment.id!)
        }
        
        let comment = self.comments[self.selectedIndex[0].row]
        
        let wait = appDel.topViewController?.addWaitSpinner()

        let randomName = randomString(length: 20)

        if let image = self.item.image{
            uploadImagesToAws([image], folderName: appDel.postImagesFolder!, randomName: randomName) { (urls) in
                if let url = urls?.first{
                    if let desc = self.item.description{
                        if let amount = self.item.startingPrice{
                            if let productSerial = self.item.productSerial{
                                if let paymentMethod = self.item.paymentMethod{

                                    /*
                                     user_id, image_url, description, amount, product_serial, payment_type )*/

                                    let parmas = ["user_id": comment.user.first!.id!,
                                                  "image_url": url,
                                                  "description": desc,
                                                  "amount": amount,
                                                  "product_serial": productSerial,
                                                  "payment_type": paymentMethod,
                                                  "stream_id":self.stream.id!,
                                                  "bid_id": awardBidIds.joined(separator: ","),
                                                  "is_sold": "false"]

                                    JSONRequest.makeRequest(kAwardBid, parameters: parmas) { (data, error) in

                                        if let wait = wait{
                                            appDel.topViewController?.removeWaitSpinner(waitView: wait)
                                        }

                                        if let data = data as? [String:Any]{
                                            if let err = data["error"] as? Bool, !err{
                                                self.dismiss(animated: true, completion: {
                                                    self.callback()
                                                })
                                            }
                                            else{
                                                if let msg = data["message"] as? String{
                                                    appDel.topViewController?.displayMessage(msg)
                                                }else{
                                                    appDel.topViewController?.displayMessage(NSLocalizedString("Failed to award!!. Please try again", comment: "Failed to award!!. Please try again"))
                                                }
                                            }
                                        }
                                        else{
                                            appDel.topViewController?.displayMessage(NSLocalizedString("Failed to parse server response!!. Please try again", comment: "Failed to parse server response!!. Please try again"))
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
extension AwardBidViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        let item = self.comments[indexPath.row]
        
        
        
        if let imageView = cell.contentView.viewWithTag(1) as? UIImageView{
            imageView.image = #imageLiteral(resourceName: "user.png")
            if let url = item.user.first?.profilePhoto{
                imageView.sd_setImage(with: URL(string: url)) { (img, _, _, _) in
                    if img == nil{
                        imageView.image = #imageLiteral(resourceName: "user.png")
                    }
                }
            }
        }
        
        if let lblName = cell.contentView.viewWithTag(2) as? UILabel{
            if let name = item.user.first?.username{
                lblName.text = name
            }
        }
        if let lblAmount = cell.contentView.viewWithTag(3) as? UILabel{
            
            lblAmount.text = "KD"+item.amount!
            
        }
        if let imgCheck = cell.contentView.viewWithTag(4) as? UIImageView{
            if self.selectedIndex.contains(indexPath){
                imgCheck.image = #imageLiteral(resourceName: "icon_check_green")
            }
            else{
                imgCheck.image = nil
            }
        }
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)!
        if let imgCheck = cell.contentView.viewWithTag(4) as? UIImageView{
            if self.selectedIndex.contains(indexPath){
                imgCheck.image = nil
                if let index = self.selectedIndex.firstIndex(of: indexPath){
                    self.selectedIndex.remove(at: index)
                }
            }
            else{
                imgCheck.image = #imageLiteral(resourceName: "icon_check_green")
                self.selectedIndex.append(indexPath)
            }
        }
    }
}
