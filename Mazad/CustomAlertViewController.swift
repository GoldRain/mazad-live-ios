

import UIKit

class CustomAlertViewController: UIViewController {
    
    var callback:(()->())?

    @IBOutlet weak var lblAlert: UILabel!
    
    @IBOutlet weak var lblAlertText: UILabel!
    
    var alertText = ""
    
    var titleText:String!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let view = touches.first?.view{
            if view == self.view{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblAlertText.text = self.alertText
        
        if let msg = titleText, msg != "" {
            self.lblAlert.text = msg
        }
    }

    @IBAction func onContinueClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.callback?()
        })
    }
}
