//
//  ReportPostViewController.swift
//  Mazad Live
//
//  Created by MAC_2 on 17/12/18.
//  Copyright © 2018 AlienBrainz. All rights reserved.
//

import UIKit

class ReportPostViewController: UIViewController {
    
    let values = ["Nudity", "Violence", "Harassment", "Sucicide or Self-Injury",
                  "False News", "Spam", "Unauthorized Sales",
                  "Hate Speech", "Terrorism", "Incorrect Voting Info", "Other"]
    
    var buttonIndex = 0
    
    var userId:String?
    var postId:String?
    var storyId:String?
    
    let normalHeight = 320
    let expandHeight = 370
    
    @IBOutlet weak var btnReport: UIButton!
    
    @IBOutlet var buttons: [UIButton]!
    
    @IBOutlet weak var txtComment: UITextField!
    
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if let sender = self.view.viewWithTag(7) as? UIButton{
            sender.backgroundColor = self.btnReport.backgroundColor
            sender.setTitleColor(.white, for: .normal)
        }
        
        if let _ = self.postId {
            self.lblTitle.text = "Give Feedback for this Post"
        }
        if let _ = self.userId{
            self.lblTitle.text = "Give Feedback for this User"
        }
        if let _ = self.storyId {
            self.lblTitle.text = "Give Feedback on this Story"
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let v = touches.first?.view{
            if v == self.view{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onReasonClick(_ sender: UIButton) {
        
        for button in self.buttons{
            button.backgroundColor = .white
            button.setTitleColor(.black, for: .normal)
        }
        
        sender.backgroundColor = self.btnReport.backgroundColor
        sender.setTitleColor(.white, for: .normal)
        
        self.buttonIndex = sender.tag - 1
        
        if sender.tag == 11{
            UIView.animate(withDuration: 0.30) {
                self.heightConstant.constant = CGFloat(self.expandHeight)
            }
        }
        else{
            UIView.animate(withDuration: 0.30) {
                self.heightConstant.constant = CGFloat(self.normalHeight)
            }
        }
    }
    
    @IBAction func onReportClick(_ sender: Any) {
        
        var textt:String?
        
        if self.buttonIndex == 10{
            if let text = self.txtComment.text{
                if text.isEmpty{
                    self.displayMessage("Enter Description")
                    return
                }
                else{
                    textt = text
                }
            }
        }
        
        guard let tVC = tabBarVC else{
            return
        }
        
        if let postId = self.postId{
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            tVC.callReportPostApi(postId: postId, reason: self.values[self.buttonIndex], message: textt ?? "", callback: {  (msg) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let message = msg{
                    
                    self.dismiss(animated: true, completion: {
                        if let cVC = categoryVC {
                            cVC.getDataFromApiAndReloadTable()
                        }
                        if let top = appDel.topViewController {
                            top.displayMessage(message)
                        }
                    })
                }
            })
        }
            
        else if let userId = self.userId{
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            tVC.callReportUserApi(user_id: userId, reason: self.values[self.buttonIndex], message: textt ?? "") { (msg) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let message = msg {
                    
                    self.dismiss(animated: true, completion: {
                        if let cVC = categoryVC {
                            cVC.getDataFromApiAndReloadTable()
                        }
                        profileVC!.dismiss(animated: true, completion: {
                            if let top = appDel.topViewController {
                                top.displayMessage(message)
                            }
                        })
                    })
                }
            }
        }
        
        else if let storyId = self.storyId {
            
            let wait = appDel.topViewController?.addWaitSpinner()
            
            tVC.callReportStoryApi(storyId: storyId, reason: self.values[self.buttonIndex], message: textt ?? "", callback: {  (msg) in
                
                if let wait = wait {
                    appDel.topViewController?.removeWaitSpinner(waitView: wait)
                }
                
                if let message = msg{
                    
                    self.dismiss(animated: true, completion: {
                        
                        NotificationCenter.default.post(name: NSNotification.Name("ReloadStoryViewController"), object: nil)
                        
                        if let top = appDel.topViewController {
                            top.displayMessage(message)
                        }
                    })
                }
            })
        }
    }
}
